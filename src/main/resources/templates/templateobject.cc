#include <UniSetActivator.h>
#include <UHelpers.h>
#include "TemplateObject.h"
using namespace std;
using namespace uniset;

int main( int argc, const char** argv )
{
    try
    {
        auto conf = uniset_init(argc, argv);

        auto act = UniSetActivator::Instance();

	    auto templateobject = make_object<TemplateObject>("TemplateObject", "TemplateObject");
	    act->add(templateobject);
	
        SystemMessage sm(SystemMessage::StartUp);
        act->broadcast( sm.transport_msg() );
        
        std::cout << "[main]:" << "------------------ templateobject START ------------------" << std::endl;
        act->run(false);
        return 0;
    }
    catch(SystemError& err)
    {
        cerr << "[main templateobject]: " << err << endl;
    }
    catch(Exception& ex)
    {
        cerr << "[main templateobject]: " << ex << endl;
    }
    catch(...)
    {
        cerr << "[main templateobject]: catch(...)" << endl;
    }

    return 1;
}
