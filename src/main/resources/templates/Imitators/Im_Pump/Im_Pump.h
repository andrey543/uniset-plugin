#ifndef Im_Pump_H
#define Im_Pump_H
//-----------------------------------------------------------------------------
#include "Im_Pump_SK.h"
//-----------------------------------------------------------------------------
class Im_Pump:
	public Im_Pump_SK
{
public:
	enum Timers
	{
		tmStart,
		tmStop
	};
	Im_Pump(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");
	~Im_Pump() override;
protected:
	void sensorInfo( const uniset::SensorMessage* sm ) override;
	void timerInfo(const uniset::TimerMessage *tm) override;
	void setState( bool _state );

private:
	void on();
	void off();
};
//-----------------------------------------------------------------------------
#endif // Im_Pump_H
