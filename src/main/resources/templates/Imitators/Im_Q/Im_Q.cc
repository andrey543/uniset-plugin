#include "Im_Q.h"
//-----------------------------------------------------------------------------
using namespace std;
using namespace uniset;
//-----------------------------------------------------------------------------
Im_Q::Im_Q(uniset::ObjectId id, xmlNode* node, const std::string& argprefix):
	Im_Q_SK(id, node, argprefix)
{
	//рассчет тока, по которому рубится все. Рассчитывается здесь, пока я не рассчитываю ток в генераторе,
	//поэтому упрощенно и без cos(phi)
	thrCurrentProt = thrCurrentProt_proc * maxPower * 1000 / 100 / normalU;

	auto conf = uniset_conf();
	UniXML::iterator it(node);

	if( !it.getProp("defOn").empty() )
		def_state = true;

	if( !it.getProp("defQP").empty() )
		def_qp = true;
}

void Im_Q::timerInfo( const uniset::TimerMessage* tm )
{
}
void Im_Q::sensorInfo( const uniset::SensorMessage* sm )
{
	if ( def_qp )
		sensorInfo_qp(sm);
	else
		sensorInfo_q(sm);
}

void Im_Q::sensorInfo_qp( const uniset::SensorMessage* sm )
{
	//Пришел cr_ на изменение состояния
	if( sm->id == Q_switchOn_c )
	{
		myinfo << myname << ": Пришла cr_ на изменение состояния" << endl;

		switch (sm->value)
		{
			case QP_cmd::on:
			{
				setState(1, 0);
				break;
			}
			case QP_cmd::off:
			{
				setState(0, 1);
				break;
			}
			case QP_cmd::prot:
			{
				setState(0, 1, 1);
				break;
			}
			default:
				myinfo << myname << ": Неопределенное значение cr_" << endl;
				break;
		}
	}
}

void Im_Q::sensorInfo_q( const uniset::SensorMessage* sm )
{
	//Включение автомата
	if( sm->id == Q_switchOn_c && sm->value == true && out_Q_isOn_s == false)
	{
		//Для красивого вывода логов в QC
		if( GRU1_Q_switchOnSync_c == uniset::DefaultObjectId ) //этот лог выводится, если используется класс Q
		{
			myinfo << myname << ": Пришла команда на включение" << endl;
		}
		else //а этот лог выводится, если используется класс QC
		{
			myinfo << myname << ": Пришла команда от ГРУ1 на включение при одностороннем питании" << endl;
		}

		if(!in_cr_Q_noRemoteControl_fc)
		{
			setState(1, 0);
		}
		else
		{
			myinfo << myname << ": игнорирование команды на включение - отключено дистанционное управление" << endl;
		}
	}

	//Отключение автомата
	else if( sm->id == Q_switchOff_c && sm->value == true && out_Q_isOn_s == true )
	{
		//Для красивого вывода логов в QC
		if( GRU1_Q_switchOnSync_c == uniset::DefaultObjectId ) //этот лог выводится, если используется класс Q
		{
			myinfo << myname << ": Пришла команда на отключение" << endl;
		}
		else //а этот лог выводится, если используется класс QC
		{
			myinfo << myname << ": Пришла команда от ГРУ1 на отключение" << endl;
		}

		if(!in_cr_Q_noRemoteControl_fc)
		{
			setState(0, 1);
		}
		else
		{
			myinfo << myname << ": игнорирование команды на отключение - отключено дистанционное управление" << endl;
		}
	}

	//Внешнее отключение
	else if( ( sm->id == Q_ExternalOff_fc || sm->id == Q_ExternalOff_c ) && sm->value == true )
	{
		setState(0, 1);
		mywarn << myname << ": Внешнее отключение! Автомат разомкнут!" << endl;
	}

	//Аварийное отключение
	else if( ( sm->id == Q_EmergencyOff_fc || sm->id == Q_EmergencyOff_c ) && sm->value == true )
	{
		setState(0, 1, 1);
		mywarn << myname << ": Аварийное отключение! Автомат разомкнут!" << endl;
	}

	//Проверка обесточивания
	else if ( sm->id == SetU_fas )
	{
		if( sm->value == 0 && out_Q_isOn_s == true )
		{
			setState(0, 1);
			mywarn << myname << ": Сработал минимальный расцепитель!" << endl;
		}
	}

	//Проверка перегрузки по току
	else if ( sm->id == SetP_fas )
	{
		if( sm->value * 1000 / normalU > thrCurrentProt)
		{
			mywarn << myname << ": Сработала защита по току! Ток " << sm->value * 1000 / normalU <<
				   " A больше предельного " << thrCurrentProt << " A!" << endl;
			setState(0, 1, 1);
			mywarn << myname << ": Защитное отключение! Автомат разомкнут!" << endl;
			return;
		}

		//Проверка на ошибки
		if( sm->value != 0 && out_Q_isOn_s == false )
		{
			mycrit << myname << ": Ненулевая мощность " << in_SetP_fas << " при отключенном автомате!!!" << endl;
		}
	}

	//отключение дистанционного управления
	else if( sm->id == cr_Q_noRemoteControl_fc )
	{
		if( sm->value == true )
		{
			myinfo << myname << ": дистанционное управление отключено" << endl;
		}
		else
		{
			myinfo << myname << ": дистанционное управление включено" << endl;
		}
		out_Q_RemoteControl_s = !in_cr_Q_noRemoteControl_fc;
	}
	//Сброс защит
	else if( sm->id == cr_Q_ResetProtection_fc && sm->value == true )
	{
		out_Q_Protection_s = false;
		out_GRU2_Q_Protection_s = false;
		myinfo << myname << ": Сброс защит. Автомат работает в штатном режиме" << endl;
	}
}

//Выставление текущего состояния
void Im_Q::setState( bool on_, bool off_, bool prot_off_ )
{
	if( out_Q_Protection_s == false && out_GRU2_Q_Protection_s == false )
	{
		if( prot_off_ ) //выставление защит
		{
			out_Q_Protection_s = true;
			out_GRU2_Q_Protection_s = true;
		}

		if( on_ )
		{
			//проверка на не сброшенную команду отключения
			if( in_Q_ExternalOff_c || in_Q_ExternalOff_fc || in_Q_EmergencyOff_c || in_Q_EmergencyOff_fc )
			{
				mywarn << myname << ": включение невозможно при наличии сигнала внешнего отключения" << endl;
				return;
			}
			//проверка на обесточивание
			if( SetU_fas != uniset::DefaultObjectId  && in_SetU_fas == 0 && !def_qp)
			{
				mywarn << myname << ": включение с обесточенной шины невозможно" << endl;
				return;
			}
		}

		out_Q_isOn_s = on_;
		out_GRU2_Q_isOn_s = on_; //для QC
		out_Q_isOff_s = off_;
		out_GRU2_Q_isOff_s = off_; //для QC

		if( on_ && !off_ )
		{
			myinfo << myname << ": Автомат включен" << endl;
		}
		else if( !on_ && off_ && !prot_off_ )
		{
			myinfo << myname << ": Автомат отключен" << endl;
		}
	}
	else
	{
		mywarn << myname << ": Отработка команды невозможна - не сброшена защита!!!" << endl;
	}
}

long Im_Q::getP()
{
	return out_P_as;
}

long Im_Q::getU()
{
	return out_Q_isOn_s ? in_SetU_fas : 0;
}

long Im_Q::getf()
{
	return out_f_as;
}

void Im_Q::step()
{
	//прокидываем входную мощность на реальный датчик
	if(out_Q_isOn_s)
	{
		out_P_as = in_SetP_fas;
		out_f_as = in_Setf_fas;
		out_U_as = in_SetU_fas;
		if( out_U_as != 0 )
			out_I_as = floor( (double)out_P_as * 1000. / (double)out_U_as / pow(3, 0.5) + 0.5);
		else
			out_I_as = 0;

	}
	else
	{
		out_P_as = 0;
		out_f_as = 0;
		out_U_as = 0;
		out_I_as = 0;
	}
}

void Im_Q::sysCommand( const uniset::SystemMessage* sm )
{
	if( sm->command == SystemMessage::StartUp )
	{
		setState(def_state, !def_state);	//Инициализируемся в выключенном состоянии
		out_GRU2_Q_RemoteControl_s = !in_cr_Q_noRemoteControl_fc;
		out_Q_RemoteControl_s = !in_cr_Q_noRemoteControl_fc;
	}

	Im_Q_SK::sysCommand(sm);
}
