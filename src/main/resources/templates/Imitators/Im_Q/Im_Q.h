#ifndef Im_Q_H
#define Im_Q_H
//-----------------------------------------------------------------------------
//!todo Добавить чтобы в vmonit текстовое описание состояния (Alarm, Off, On etc)
/* В это классе представлена реализация как Q, там и QC, у которого в два раза больше входов и выходов.
 * Поэтому, если используете просто класс Q, то не обращайте внимания на кучу no_check_id датчиков -
 * они нужны только для совместимости с QC */
//-----------------------------------------------------------------------------
#include "Im_Q_SK.h"
#include "TEMPLATE_PROJECTConfiguration.h"
//-----------------------------------------------------------------------------
class Im_Q:
	public Im_Q_SK
{
	public:
		Im_Q(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");
		long getP();
		long getU();
		long getf();
	protected:
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( const uniset::TimerMessage* tm ) override;
		virtual void step() override;
		virtual void sysCommand( const uniset::SystemMessage* sm ) override;

		void setState( bool on_, bool off_, bool prot_off_ = false );
		//Разделил реализацию для обычных автоматов и по cr_
		void sensorInfo_q( const uniset::SensorMessage* sm );
		void sensorInfo_qp( const uniset::SensorMessage* sm );
		bool def_qp = {false};

		bool def_state = {false};

		enum QP_cmd
		{
			on = 0,
			off = 1,
			prot = 2
		};
};
//-----------------------------------------------------------------------------
#endif
