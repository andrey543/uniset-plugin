#ifndef Im_FC_H
#define Im_FC_H
//-----------------------------------------------------------------------------
#include "Im_FC_SK.h"
#include "TEMPLATE_PROJECTConfiguration.h"
#include "TEMPLATE_PROJECTEquipStates.h"
//-----------------------------------------------------------------------------
//! \todo Добавить реакцию ПЧ на внешнее отключение QM(по защите например)
class Im_FC:
	public Im_FC_SK
{
	public:
		Im_FC(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");
	protected:
		void sensorInfo( const uniset::SensorMessage* sm ) override;
		void timerInfo( const uniset::TimerMessage* tm ) override;
		void sysCommand( const uniset::SystemMessage* sm ) override;
		void setState(TEMPLATE_PROJECT::fcState state_ );
		void setPower();

		bool flagCMD = { false };	//флаг, что послали команду на QM
		long limitRPM = { nominalRPM };		//ограничение оборотов
		long faultLimit = { maxPower };	//ограничение мощности по неисправности
		float resultPower = { 0 };	//заданная мощность
		enum Timers
		{
			timerResetCMD,	//таймер сброса команд на QM
			timerCalcPower,	//таймер плавного наброса мощности
			timerChargeQP	//таймер переключения с QP на QM
		};

		//проверка нулевого задания
		inline bool setNoZero()
		{
			return (!in_FC_SetControlMode_ac && in_FC_SetRPM_ac ) or (in_FC_SetControlMode_ac && in_FC_SetPower_ac);
		}

		//перерасчет мощности в обороты
		inline float calcPowerToRPM ( float Power_ )
		{
			return 150 / 3.14 * pow((1000 * (double)Power_ / kVent), (double)1/(double)3);
		}
		//перерасчет оборотов в мощность
		inline float calcRPMToPower ( float RPM_ )
		{
			return kVent * pow(2 * 3.14 * RPM_ / (60 * 5), 3) / 1000;
		}
};
//-----------------------------------------------------------------------------
#endif // Im_FC_H
