#include "Im_FC.h"
//-----------------------------------------------------------------------------
using namespace std;
using namespace TEMPLATE_PROJECT;
using namespace uniset;
//-----------------------------------------------------------------------------
Im_FC::Im_FC(uniset::ObjectId id, xmlNode* node, const std::string& argprefix):
	Im_FC_SK(id, node, argprefix)
{
}

//Пока тут логики немного, оставляю так. Потом надо будет перелопатить в машину состояний
void Im_FC::sensorInfo( const uniset::SensorMessage* sm )
{
	//Обработка режима задания (обороты/мощность)
	if( sm->id == FC_SetControlMode_ac )
	{
		if( sm->value == fcControlMode::fcmUndef )
		{
			mywarn << myname << ": неопределённое состояние режима задания (управление отключено)" << endl;
		}
		else if( sm->value == fcControlMode::fcmSetRPM )
		{
			myinfo << myname << ": выбран режим управления по оборотам" << endl;
			setPower();
		}
		else if( sm->value == fcControlMode::fcmSetPower )
		{
			myinfo << myname << ": выбран режим управления по мощности" << endl;
			setPower();
		}
	}

	//Пришло задание по оборотам
	else if( sm->id == FC_SetRPM_ac )
	{
		myinfo << myname << ": пришло задание по оборотам " << sm->value << " об/мин" << endl;
		if( in_FC_SetControlMode_ac != fcControlMode::fcmSetRPM )
		{
			mywarn << myname << ": не выбран режим задания по оборотам! Задание по оборотам игнорируется! " << endl;
			return;
		}

		setPower();
	}

	//Пришло задание по мощности
	else if( sm->id == FC_SetPower_ac )
	{
		myinfo << myname << ": пришло задание по мощности " << sm->value << " кВт" << endl;
		if( in_FC_SetControlMode_ac != fcControlMode::fcmSetPower )
		{
			mywarn << myname << ": не выбран режим задания по мощности! Задание по мощности игнорируется! " << endl;
			return;
		}

		setPower();
	}

	//Пришло ограничение по мощности
	else if( sm->id == FC_limitPower_ac )
	{
		myinfo << myname << ": пришло ограничение по мощности " << sm->value << " кВт" << endl;
		setPower();
	}

	//Пришло ограничение по оборотам
	else if( sm->id == FC_70RPM_c )
	{
		if( sm->value == true )
		{
			myinfo << myname << ": пришло ограничение по оборотам: 70% от номинальных" << endl;
			limitRPM = floor((double)nominalRPM *0.7 + 0.5);
		}
		else
		{
			myinfo << myname << ": снялось ограничение по оборотам" << endl;
			limitRPM = nominalRPM;
		}
		setPower();
	}

	//Пришла команда на сбор схемы
	else if( sm->id == FC_On_fc && sm->value == true)
	{
		myinfo << myname << ": пришла команда на сбор схемы" << endl;

		if( out_FC_Alarm_s )
		{
			mywarn << myname << ": сбор схемы невозможен - ПЧ в Аварии!" << endl;
			return;
		}
		if( in_cr_noRemoteControl_fc )
		{
			mywarn << myname << ": сбор схемы невозможен - отключено дистанционное управление!" << endl;
			return;
		}

		out_QP_switchOn_fc = true;
		askTimer(timerResetCMD, tResetCMD);
		myinfo << myname << ": посылка команды на включение QP" << endl;
	}

	//Пришла команда на разбор схемы
	else if( sm->id == FC_Off_fc && sm->value == true)
	{
		myinfo << myname << ": пришла команда на расбор схемы" << endl;

		if( !in_QM_isOn_s )
		{
			mywarn << myname << ": расбор схемы невозможен - QM уже разомкнут" << endl;
			return;
		}

		if( in_cr_noRemoteControl_fc )
		{
			mywarn << myname << ": сбор схемы невозможен - отключено дистанционное управление!" << endl;
			return;
		}

		out_QM_switchOff_fc = true;
		askTimer(timerResetCMD, tResetCMD);
		flagCMD = true;
		myinfo << myname << ": посылка команды на отключение QM" << endl;
	}

	else if( sm->id == QP_isOn_s )
	{
		if( sm->value == true )
			askTimer(timerChargeQP, tChargeQP);
	}

	//Состояние автомата
	else if( sm->id == QM_isOn_s )
	{
		if( sm->value == true )
		{
			if( !flagCMD )
			{
				mywarn << myname << ": QM неожиданно включился" << endl;
			}
			askSensor(FC_SetPower_ac,UniversalIO::UIONotify);
			askSensor(FC_SetRPM_ac,UniversalIO::UIONotify);
			setState(fcState::fcReady2);
		}
		else if( sm->value == false )
		{
			if( !flagCMD )
			{
				mywarn << myname << ": QM неожиданно отключился" << endl;
			}
			//если не в аварии
			if( !out_FC_Alarm_s )
			{
				setState(fcState::fcReady1);
			}
			//также тут сбрасываем выходные мощность и обороты
			askTimer(timerCalcPower,0);
			out_FC_TargetRPM_as = 0;
			out_FC_RPM_as = 0;
			out_FC_P_as = 0;
		}

		flagCMD = false; //Сбрасываем
	}

	//Включение вентиляторов
	else if( sm->id == Fan_switchOn_c && sm->value == true && out_Fan_isOn_s == false )
	{
		out_Fan_isOn_s = true;
		myinfo << myname << ": пуск вентилятора охлаждения" << endl;
	}

	//Отключение вентиляторов
	else if( sm->id == Fan_switchOff_c && sm->value == true && out_Fan_isOn_s == true )
	{
		out_Fan_isOn_s = false;
		myinfo << myname << ": стоп вентилятора охлаждения" << endl;
	}

	//переход в аварию по cr_
	else if( sm->id == cr_setAlarm_fc )
	{
		myinfo << myname << ": пришел cr_ управления аварией" << endl;

		if( sm->value == true )
		{
			askTimer(timerCalcPower, 0);
			setState( fcState::fcAlarm );
			if( in_QM_isOn_s )
			{
				out_QM_ExternalOff_fc = true;
				askTimer(timerResetCMD, tResetCMD);
				flagCMD = true;
				myinfo << myname << ": аварийно отключаем QM" << endl;
			}
		}
		else
		{
			myinfo << myname << ": сброс причины аварии. Для работы нужно сбросить защиты" << endl;
		}
	}

	//снижение мощности по cr_
	else if( sm->id == cr_limitPower_FC )
	{
		myinfo << myname << ": пришел cr_ управления неисправностью" << endl;

		if( sm->value == true )
		{
			faultLimit = maxPower / 2;
			myinfo << myname << ": неисправность - мощность снижена на 50%" << endl;
		}
		else
		{
			faultLimit = maxPower;
			myinfo << myname << ": изчезло ограничение мощности, связанное с неисправностью" << endl;
		}
		setPower();
	}

	//сброс защит
	else if( sm->id == cr_ResetProtection_fc && sm->value == true )
	{
		myinfo << myname << ": пришел cr_ на сброс защит" << endl;
		if( out_FC_Alarm_s )
		{
			setState( fcState::fcReady1 );
		}
	}

	else if( sm->id == cr_noRemoteControl_fc )
	{
		myinfo << myname << ": пришел cr_ управления постом управления" << endl;

		if( sm->value == true )
		{
			out_FC_RemoteControl_s = false;
			myinfo << myname << ": включено местное управление" << endl;
		}
		else
		{
			out_FC_RemoteControl_s = true;
			myinfo << myname << ": включено дистанционное управление" << endl;
		}
	}
}

void Im_FC::timerInfo( const uniset::TimerMessage* tm )
{
	//таймер сброса команд на QM
	if( tm->id == timerResetCMD )
	{
		out_QM_switchOn_fc = false;
		out_QM_switchOff_fc = false;
		out_QM_ExternalOff_fc = false;
		out_QP_switchOn_fc = false;
		out_QP_switchOff_fc = false;
		askTimer(timerResetCMD, 0);
	}

	//таймер заряда через QP
	else if( tm->id == timerChargeQP )
	{
		out_QM_switchOn_fc = true;
		out_QP_switchOff_fc = true;
		askTimer(timerResetCMD, tResetCMD);
		flagCMD = true;
		myinfo << myname << ": посылка команды на включение QM" << endl;

		askTimer(timerChargeQP, 0);
	}

	//таймер плавного повышения мощности
	else if( tm->id == timerCalcPower )
	{
		if(!in_QM_isOn_s)
		{
			askTimer(timerCalcPower, 0);
			myinfo << myname << ": заход в таймер при отключенном автомате!" << endl;
			return;
		}
		long dP = maxPowerGDG*tCalcPower/tMaxLoadGDG; //изменение мощности за промежуток времени

		//повышаем мощность
		if( resultPower > out_FC_P_as )
		{
			if( out_FC_P_as + dP < resultPower )
			{
				out_FC_P_as += dP;
				out_FC_RPM_as = calcPowerToRPM(out_FC_P_as);
			}
			else
			{
				out_FC_P_as = lroundf(resultPower);
				out_FC_RPM_as = lroundf( calcPowerToRPM(resultPower) );
				askTimer(timerCalcPower, 0);

				if( in_FC_SetControlMode_ac == fcControlMode::fcmSetRPM )
				{
					myinfo << myname << ": текущая мощность при скорости " << out_FC_RPM_as << " об/мин равна " << out_FC_P_as << " кВт" << endl;
				}
				else
				{
					myinfo << myname << ": текущая скорость при мощности " << out_FC_P_as << " кВт равна " << out_FC_RPM_as << " об/мин" << endl;
				}
			}
		}

		//понижаем мощность
		else if( resultPower < out_FC_P_as )
		{
			if( out_FC_P_as - dP > resultPower )
			{
				out_FC_P_as -= dP;
				out_FC_RPM_as = calcPowerToRPM(out_FC_P_as);
			}
			else
			{
				out_FC_P_as = lroundf(resultPower);
				out_FC_RPM_as = lroundf( calcPowerToRPM(resultPower) );
				askTimer(timerCalcPower, 0);

				if( in_FC_SetControlMode_ac == fcControlMode::fcmSetRPM )
				{
					myinfo << myname << ": текущая мощность при скорости " << out_FC_RPM_as << " об/мин равна " << out_FC_P_as << " кВт" << endl;
				}
				else
				{
					myinfo << myname << ": текущая скорость при мощности " << out_FC_P_as << " кВт равна " << out_FC_RPM_as << " об/мин" << endl;
				}
			}
		}

		//переход в режим Готовность2 при нулевом задании и нулевой мощности
		if( out_FC_State_as == fcState::fcWorking && !setNoZero() && out_FC_P_as == 0)
		{
			setState(fcState::fcReady2);
		}
	}
}

void Im_FC::sysCommand( const SystemMessage* sm )
{
	//Инициализация в состоянии Готовность1
	if( sm->command == SystemMessage::StartUp)
	{
		setState( fcState::fcReady1 );
		out_FC_RemoteControl_s = true;
		out_Fan_isOn_s = false;
	}

	Im_FC_SK::sysCommand(sm);
}

void Im_FC::setState( TEMPLATE_PROJECT::fcState state_ )
{
	out_FC_State_as = state_;

	switch( state_ )
	{
		case fcState::fcUnknown :
		{
			myinfo << myname << ": переход в неизвестное состояние" << endl;
			out_FC_Ready1_s = false;
			out_FC_Ready2_s = false;
			out_FC_isWork_s = false;
			out_FC_Alarm_s = false;
			break;
		}
		case fcState::fcReady1 :
		{
			myinfo << myname << ": переход в состояние Готовность1" << endl;
			out_FC_Ready1_s = true;
			out_FC_Ready2_s = false;
			out_FC_isWork_s = false;
			out_FC_Alarm_s = false;
			break;
		}
		case fcState::fcReady2 :
		{
			myinfo << myname << ": переход в состояние Готовность2" << endl;
			out_FC_Ready1_s = false;
			out_FC_Ready2_s = true;
			out_FC_isWork_s = false;
			out_FC_Alarm_s = false;
			break;
		}
		case fcState::fcWorking :
		{
			myinfo << myname << ": переход в состояние Работа" << endl;
			out_FC_Ready1_s = false;
			out_FC_Ready2_s = false;
			out_FC_isWork_s = true;
			out_FC_Alarm_s = false;
			break;
		}
		case fcState::fcAlarm:
		{
			myinfo << myname << ": переход в состояние Авария" << endl;
			out_FC_Ready1_s = false;
			out_FC_Ready2_s = false;
			out_FC_isWork_s = false;
			out_FC_Alarm_s = true;
			break;
		}
		default:
		{
			mywarn << myname << ": неизвестная команда в setState ( " << state_ << ")" << endl;
			break;
		}
	}
}


//обработка задания по мощности или оборотам
void Im_FC::setPower()
{
	if( in_cr_noRemoteControl_fc )
	{
		mywarn << myname << ": отработка задания невозможна - отключено дистанционное управление!" << endl;
		return;
	}

	//были в состоянии Готовность2 и ненулевое задание
	if( out_FC_State_as == fcState::fcReady2 && setNoZero() )
	{
		setState(fcState::fcWorking);
	}
	else if( out_FC_State_as == fcState::fcAlarm )
	{
		mywarn << myname << ": ПЧ в состоянии Авария. Игнорирование задания" << endl;
		return;
	}
	else if( out_FC_State_as != fcState::fcWorking )
	{
		mywarn << myname << ": ПЧ не в состоянии Работа. Игнорирование задания" << endl;
		return;
	}

	//обрабатываем задание в зависимости от режима
	if( in_FC_SetControlMode_ac == fcControlMode::fcmSetRPM )
	{
		resultPower = TEMPLATE_PROJECT::multimin( calcRPMToPower(in_FC_SetRPM_ac), calcRPMToPower(limitRPM) , (float)in_FC_limitPower_ac, (float)faultLimit );

		out_FC_TargetRPM_as = in_FC_SetRPM_ac;
		askTimer(timerCalcPower, tCalcPower);
	}
	else if( in_FC_SetControlMode_ac == fcControlMode::fcmSetPower )
	{
		resultPower = TEMPLATE_PROJECT::multimin( (float)in_FC_SetPower_ac, (float)in_FC_limitPower_ac, calcRPMToPower(limitRPM), (float)faultLimit );
		out_FC_TargetRPM_as = lroundf( calcPowerToRPM(in_FC_SetPower_ac) );
		askTimer(timerCalcPower, tCalcPower);
	}
}
