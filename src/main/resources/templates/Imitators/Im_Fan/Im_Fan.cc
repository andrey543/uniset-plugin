#include "Im_Fan.h"
//-----------------------------------------------------------------------------
using namespace std;
using namespace uniset;
//-----------------------------------------------------------------------------
Im_Fan::Im_Fan(uniset::ObjectId id, xmlNode* node, const std::string& argprefix):
Im_Fan_SK(id, node, argprefix)
{
    auto conf = uniset::uniset_conf();

    if( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 )
        mylog->level( Debug::value(default_loglevel) );

     std::cout << "[" << getName() << "]: " << "created" << std::endl;
}

Im_Fan::~Im_Fan() = default;

void Im_Fan::sensorInfo( const uniset::SensorMessage* sm )
{
	//Команда на включение/отключение
	if( sm->id == On_c)
	{
		if( !in_RemoteControl_c )
		{
			myinfo << myname << ": " << logstr << " - игнорирование команды (МУ)" << endl;
			return;
		}

		if( sm->value )
		{
			myinfo << myname << ": " << logstr << " - команда на включение" << endl;
			on();
		}
	}

	if( sm->id == Off_c)
	{
		if( !in_RemoteControl_c )
		{
			myinfo << myname << ": " << logstr << " - игнорирование команды (МУ)" << endl;
			return;
		}

		if( sm->value )
		{
			myinfo << myname << ": " << logstr << " - команда на отключение" << endl;
			off();
		}
	}

	// Включение/отключение ДУ
	if( sm->id == RemoteControl_c)
	{
		if( sm->value == true )
		{
			myinfo << myname << ": " << logstr << " - дистанционное управление" << endl;
			setState(in_On_c);
		}
		else 
		{
			myinfo << myname << ": " << logstr << " - местное управление" << endl;
		}
	}
}

void Im_Fan::setState( bool _state )
{
	if( out_isOn_s == _state )
		return;

	if( _state )
	{
		myinfo << myname << ": " << logstr << " - включен" << endl;
	}
	else
	{
		myinfo << myname << ": " << logstr << " - отключен" << endl;
	}
	out_isOn_s = _state;
}

void Im_Fan::timerInfo(const uniset::TimerMessage *tm) {
	if(tm->id == Timers::tmStart)
	{
		setState(true);
		askTimer(Timers::tmStart, 0);
	}
	else if(tm->id == Timers::tmStop)
	{
		setState(false);
		askTimer(Timers::tmStop, 0);
	}

}

void Im_Fan::on() {
    if ( getTimeInterval(Timers::tmStart) > 0 )
    {
        myinfo << myname << ": " << logstr << " - Уже включается" << endl;
        return;
    }
    myinfo << myname << ": " << logstr << " - включение с задержкой(" << DELAY_START <<" мс)" << endl;
    askTimer(Timers::tmStart,DELAY_START);
}

void Im_Fan::off() {
    if ( getTimeInterval(Timers::tmStop) > 0 )
    {
        myinfo << myname << ": " << logstr << " - Уже отключается" << endl;
        return;
    }
    myinfo << myname << ": " << logstr << " - отключение с задержкой(" << DELAY_STOP <<" мс)" << endl;
    askTimer(Timers::tmStop,DELAY_STOP);
}




