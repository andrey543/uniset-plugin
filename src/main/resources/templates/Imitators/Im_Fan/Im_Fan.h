#ifndef Im_Fan_H
#define Im_Fan_H
//-----------------------------------------------------------------------------
#include "Im_Fan_SK.h"
//-----------------------------------------------------------------------------
class Im_Fan:
	public Im_Fan_SK
{
public:
	enum Timers
	{
		tmStart,
		tmStop
	};
	Im_Fan(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");
	~Im_Fan() override;
protected:
	void sensorInfo( const uniset::SensorMessage* sm ) override;
	void timerInfo(const uniset::TimerMessage *tm) override;
	void setState( bool _state );

private:
	void on();
	void off();
};
//-----------------------------------------------------------------------------
#endif // Im_Fan_H
