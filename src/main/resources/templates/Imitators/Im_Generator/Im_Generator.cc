#include "Im_Generator.h"
//-----------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
//-----------------------------------------------------------------------------
//!\todo Внести проверку включенной диф-защиты. без нее не запускаться

Im_Generator::Im_Generator(uniset::ObjectId id, xmlNode* node, const std::string& argprefix):
	Im_Generator_SK(id, node, argprefix)
{
	auto conf = uniset_conf();
    bool debug_logs = ( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 );

    if( debug_logs )
        mylog->level( Debug::value(default_loglevel) );
	out_kLoad = 0;
	vmonit(speedK);
}

void Im_Generator::sensorInfo( const uniset::SensorMessage* sm )
{
	//включение возбуждения
	if( sm->id == ExcitationSwitchOn_c && sm->value == true && out_ExcitationState_s == false)
	{
		myinfo << myname << ": Пришла команда на включение возбуждения" << endl;
		out_ExcitationState_s = true;
		myinfo << myname << ": Возбуждение включено" << endl;
	}

	//выключение возбуждения
	else if( sm->id == ExcitationSwitchOff_c && sm->value == true && out_ExcitationState_s == true)
	{
		myinfo << myname << ": Пришла команда на отключение возбуждения" << endl;
		out_ExcitationState_s = false;
		myinfo << myname << ": Возбуждение отключено" << endl;
	}

	//задание по мощности от PowerControl
	else if( sm->id == SetP_fas )
	{
		//myinfo << myname << ": Пришло задание по мощности " << sm->value << " кВт" << endl;

		if( sm->value < 0 )
		{
			mywarn << myname << ": Задание по мощности (" << sm->value << " кВт) отрицательное!!!" << endl;
			return;
		}

		if( in_QG_isOn_s == true ) //проверяем, что выведен на шины
		{
			setPower(sm->value);
		}

		else if( in_QG_isOn_s == false && sm->value != 0 )
		{
			mycrit << myname << ": Пришло задание по мощности на ГДГ, который не выведен на шины! Ошибка имитаторов!" << endl;
		}
	}

	//команда на разгрузку
	else if( sm->id == Unload_c )
	{
		//пришла команда на разгрузку
		if( sm->value == true )
		{
			myinfo << myname << ": Пришла команда на разгрузку" << endl;
			myinfo << myname << ": Начинается разгрузка с мощности " << out_P_as << " кВт" << endl;
			askTimer(timerLoad, tCalcK);
		}
		//снялась команда на разгрузку
		else
		{
			myinfo << myname << ": Снялась команда на разгрузку. Текущая мощность = " << out_P_as << " кВт" << endl;
			//при снятии команды на разгрузку, если kLoad = 0 и генератор выведен на шины, выставляем его в 1%, чтобы PowerControl дальше могло подхватить
			if( in_QG_isOn_s && out_kLoad == 0 )
			{
				out_kLoad = maxPower/100;
			}
		}
	}
	
	else if( sm->id == cr_noDiffProtection_FC )
	{
		if( sm->value == true )
		{
			myinfo << myname << ": Пришла команда на отключение дифф защиты" << endl;
			out_DiffProtection_isOn_s = false;
		}
		else
		{
			myinfo << myname << ": Пришла команда на включение дифф защиты" << endl;
			out_DiffProtection_isOn_s = true;
		}
	}
	
	else if( sm->id == cr_DiffProtection_FC )
	{
		if( sm->value == true && out_DiffProtection_isOn_s == true ) 
		{
			mywarn << myname << ": Сработала дифф. защита! Отключение генераторного автомата" << endl;
			externalOffQG();
		}
		else
		{
			myinfo << myname << ": Снят сигнал на срабатывание дифф. защиту" << endl;
		}
	}
	
	else if( sm->id == cr_ReversePower_FC )
	{
		if( sm->value == true ) 
		{
			mywarn << myname << ": Обратная мощность! Отключение генераторного автомата" << endl;
			externalOffQG();
		}
		else
		{
			myinfo << myname << ": Снят сигнал по обратной мощности" << endl;
		}
	}

	//После отключения QG сбрасываем внешнее отключение out_QG_ExternalOff_fc
	else if( sm->id == QG_isOn_s && sm->value == false && out_QG_ExternalOff_fc == true )
	{
		out_QG_ExternalOff_fc = false;
	}

	if( sm->id == QG_isOn_s )
	{
		//При включении выставляем loadK = 1 % от максимального коэффициента
		if( sm->value == true )
		{
			checkLoad();
			out_kLoad = maxPower/100;
		}
		//При отключении сбрасываем мощность и коэффициент нагрузочной способности
		else
		{
			setLoadLow5(false);
			setLoadLow20(false);
			out_P_as = 0;
			out_Overload_s = false;
			askTimer(timerLoad, 0);
			out_kLoad = 0;
			speedK = 0;
		}
	}
}

void Im_Generator::timerInfo( const uniset::TimerMessage* tm )
{
	//плавное снижение значения скорости наброса нагрузки
	if( tm->id == timerPower )
	{
		if( speedK > 0 )
		{
			if( speedK > 100*tCalcK/tMaxLoad )
			{
				speedK -= 100*tCalcK/tMaxLoad;
			}
			else
			{
				speedK = 0;
				askTimer(timerPower, 0);
			}
		}
		else
		{
			if( speedK < -100*tCalcK/tMaxLoad )
			{
				speedK += 100*tCalcK/tMaxLoad;
			}
			else
			{
				speedK = 0;
				askTimer(timerPower, 0);
			}
		}
	}

	//таймер рассчета коэффициента нагрузочной способности
	else if( tm->id == timerLoad)
	{
		calcKload();
	}

	//сработал таймер длительной перегрузки
	else if( tm->id == timerOverload )
	{
		if( !out_Overload120_s )
			return;

		externalOffQG();
		mywarn << myname << ": Сработала защита по перегрузке! Отключение генераторного автомата" << endl;
		myinfo << myname << ": Активная мощность " << out_P_as << " кВт" << endl;
	}
}

//функция выставления выходной мощности. все проверки тоже тут
void Im_Generator::setPower( const long& p )
{
	//заказываем таймер при выходе из нуля
	if( speedK == 0 )
		askTimer(timerPower, tCalcK);

	speedK += 100 * (p - out_P_as)/maxPower;

	//наброс
	if( speedK > 0 )
	{
		//проверка величины нагрузочной степени
		if( speedK <= thrStepLoad )
		{
			out_P_as = p;
			myinfo << myname << ": Наброс нагрузки на " << speedK << " % от номинальной" << endl;
			myinfo << myname << ": Текущая мощность " << out_P_as << " кВт" << endl;
		}
		else
		{
			mycrit << myname << ": Аварийное отключение! Превышение максимальной нагрузочной ступени (" << speedK
				   << " > " << thrStepLoad << " % от номинальной мощности)!" << endl;
			externalOffQG();
		}
	}

	//сброс
	else if( speedK < 0 )
	{
		//проверка величины разгрузочной степени
		if( abs(speedK) <= thrStepUnload )
		{
			out_P_as = p;
			myinfo << myname << ": Сброс нагрузки на " << speedK << " % от номинальной" << endl;
			myinfo << myname << ": Текущая мощность " << out_P_as << " кВт" << endl;
		}
		else
		{
			mycrit << myname << ": Аварийное отключение! Превышение максимальной разгрузочной ступени (" << abs(speedK) 
				   << " > " << thrStepUnload << " % от номинальной мощности)!" << endl;
			externalOffQG();
		}
	}

	//крайне маловероятно, но на всякий случай
	else
	{
		out_P_as = p;
	}

	//если таймер не заказан и ГДГ выведен на шины
	if( getTimeLeft(timerLoad) == 0 )
		askTimer(timerLoad, tCalcK);

	checkLoad();
}

//рассчет коэффициента нагрузочной способности
void Im_Generator::calcKload()
{
	if( !in_QG_isOn_s )
	{
		out_kLoad= 0;
		return;
	}

	if( in_SetP_fas == 0 && out_kLoad != 0 && !in_Unload_c)
	{
		//выставляем минимум
		out_kLoad = maxPower / 100;
		return;
	}

	long dP = maxPower*tCalcK/tMaxLoad; //изменение мощности за промежуток времени

	//проверка знака изменения коэффициента ( если разгрузка, то "минус" )
	if( in_Unload_c )
	{
		//чтобы не выходило за пороги
		if( out_kLoad - dP > 0 )
		{
			out_kLoad -= dP;
		}
		else
		{
			out_kLoad = 0;
			askTimer(timerLoad, 0);
		}
	}
	else
	{
		//чтобы не выходило за пороги
		if( out_kLoad + dP < maxPower )
		{
			out_kLoad += dP;
		}
		else
		{
			out_kLoad = maxPower;
			askTimer(timerLoad, 0);
		}
	}
}

//проверка порогов загрузки
void Im_Generator::checkLoad()
{
	thrOverload = thrOverload_proc * maxPower / 100;

	//проверка на нагрузку 5%
	if( out_P_as < 0.05 * maxPower )
	{
		setLoadLow5(true);
		if( trgLoadLow5.change(out_P_as < 0.05 * maxPower) )
		{
			myinfo << myname << ": Нагрузка ниже 5%" << endl;
		}
	}
	else
	{
		setLoadLow5(false);
		if( trgLoadLow5.change(out_P_as < 0.05 * maxPower) )
		{
			myinfo << myname << ": Нагрузка больше 5%" << endl;
		}
	}

	//проверка на нагрузку 20%
	if( out_P_as < 0.2 * maxPower )
	{
		setLoadLow20(true);
		if( trgLoadLow20.change(out_P_as < 0.2 * maxPower) )
		{
			myinfo << myname << ": Нагрузка ниже 20%" << endl;
		}
	}
	else
	{
		setLoadLow20(false);
		if( trgLoadLow20.change(out_P_as < 0.2 * maxPower) )
		{
			myinfo << myname << ": Нагрузка больше 20%" << endl;
		}
	}

	//проверка на нагрузку 95%
	if( out_P_as > thrLoad95_proc * maxPower )
	{
		out_Load95_s = true && in_QG_isOn_s;
		if( trgLoad95.change(out_P_as < thrLoad95_proc * maxPower) )
		{
			myinfo << myname << ": Нагрузка больше 95%" << endl;
		}
	}
	else
	{
		out_Load95_s = false;
		if( trgLoad95.change(out_P_as < thrLoad95_proc * maxPower) )
		{
			myinfo << myname << ": Нагрузка меньше 95%" << endl;
		}
	}

	//проверка на нагрузку 100%
	if( out_P_as > thrOverload100_proc * maxPower )
	{
		out_Overload_s = true && in_QG_isOn_s;
		if( trgOverload.change(out_P_as < thrOverload100_proc * maxPower) )
		{
			myinfo << myname << ": Нагрузка больше 100%" << endl;
		}
	}
	else
	{
		out_Overload_s = false;
		if( trgOverload.change(out_P_as < thrOverload100_proc * maxPower) )
		{
			myinfo << myname << ": Нагрузка меньше 100%" << endl;
		}
	}

	//проверка на перегрузку
	if ( trgOverload120.change(out_P_as > thrOverload) )
	{
		if( out_P_as > thrOverload )
		{
			out_Overload120_s = true;
			myinfo << myname << ": Перегрузка! Мощность генератора " << out_P_as <<
				   " кВт больше предела перегрузки " << thrOverload << " кВт!" << endl;
			myinfo << myname << ": Перегрузка! До автоматического размыкания генераторного автомата осталось " <<
				   tOverload / 1000 << " с!" << endl;
			askTimer(timerOverload, tOverload);
		}
		else
		{
			out_Overload120_s = false;
			myinfo << myname << ": Перегрузки нет - нагрузка снизилась до номинальных пределов" << endl;
			askTimer(timerOverload, 0);
		}
	}
}

void Im_Generator::step()
{
	//сбрасываем speedK, если автомат разомкнут
	if(!in_QG_isOn_s)
		speedK = 0;

	//если включено возбуждение - выставляем напряжения и токи
	if( out_ExcitationState_s )
	{
		out_U_as = in_SetU_fas;
		out_f_as = in_Setf_fas;
		//чтобы не было деления на 0
		if( out_U_as != 0)
			out_I_as = floor( (double)out_P_as * 1000. / (double)out_U_as / pow(3, 0.5) + 0.5);
		else if ( out_P_as == 0 )
			out_I_as = 0;
	}
	else
	{
		out_U_as = 0;
		out_f_as = 0;
		out_I_as = 0;
	}
}

//начальная инициализация
void Im_Generator::sysCommand( const uniset::SystemMessage* sm )
{
	if( sm->command == SystemMessage::StartUp )
	{
		out_DiffProtection_isOn_s = true;	//Дифзащита по умолчанию включена
		setLoadLow5(true);
		setLoadLow20(true);
	}

	Im_Generator_SK::sysCommand(sm);
}
