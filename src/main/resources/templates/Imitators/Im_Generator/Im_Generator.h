#ifndef Im_Generator_H
#define Im_Generator_H
//-----------------------------------------------------------------------------
#include "Im_Generator_SK.h"
#include "TEMPLATE_PROJECTConfiguration.h"
//-----------------------------------------------------------------------------
//!todo Добавить чтобы в vmonit текстовое описание состояния (Alarm, Off, On etc)

class Im_Generator:
	public Im_Generator_SK
{
	public:
		Im_Generator(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");
	protected:
		void sensorInfo( const uniset::SensorMessage* sm ) override;
		void timerInfo( const uniset::TimerMessage* tm ) override;
		void step() override;
		void sysCommand( const uniset::SystemMessage* sm );
		void checkLoad();
		void setPower( const long& p );
		void calcKload();

		uniset::Trigger trgLoadLow5; //триггер 5%
		uniset::Trigger trgLoadLow20; //триггер 20%
		uniset::Trigger trgLoad95; //триггер 95%
		uniset::Trigger trgOverload; //триггер перегрузки 105%
		uniset::Trigger trgOverload120; //триггер перегрузки 120%

		long speedK = { 0 }; //уровень скорости наброса нагрузки, % от номинальной мощности
		enum Timers
		{
			timerPower,
			timerOverload,
			timerLoad
		};

		//выставление сигналов "низкая нагрузка"
		inline void setLoadLow5( bool state_ )
		{
			out_LoadLow5_s = state_ && in_QG_isOn_s;
		}
		inline void setLoadLow20( bool state_ )
		{
			out_LoadLow20_s = state_ && in_QG_isOn_s;
		}

		//аварийное отключение генераторного автомата
		inline void externalOffQG()
		{
			askTimer(timerOverload, 0);
			askTimer(timerPower, 0);
			out_P_as = 0;
			speedK = 0;
			out_Overload_s = false;
			out_QG_ExternalOff_fc = true;
			askSensor(QG_isOn_s,UniversalIO::UIONotify); //если уже отключился
		}
};

//-----------------------------------------------------------------------------
#endif
