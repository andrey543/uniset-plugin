#ifndef Im_PowerControl_H
#define Im_PowerControl_H
//-----------------------------------------------------------------------------
#include <UniSetTypes.h>
#include <vector>
#include <iomanip>
#include "TEMPLATE_PROJECTConfiguration.h"
#include "Im_PowerControl_SK.h"
#include "Im_GRU.h"
#include "Im_GRSH220.h"
#include "DGInfo.h"
//-----------------------------------------------------------------------------
//! \todo Пока что сделаю, чтоб работал по датчикам, потом же буду из класса Im_SES передавать указатели на Q и все что необходимо
//! \todo Ну и неплохо было бы сделать автоматическую инициализацию структуры парсингом xml
//! \todo Еще сделать, что при пропаже потребителей(рубанули перемычку например), мощность скидывается в 0 сразу же
using namespace std;

class Im_PowerControl:
	public Im_PowerControl_SK
{
	public:
		Im_PowerControl(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");
		void setGRU( std::shared_ptr<Im_GRU> grsh1_, std::shared_ptr<Im_GRU> grsh2_);
		void setGRSH220( std::shared_ptr<Im_GRSH220> grsh220_);

	protected:
		void step() override;
		void set_group(short group1, short group2 = 0);
		void setVoltage(short group, bool reset = false);
		long getPower(short group);
		string getMonitInfo();

		std::shared_ptr<Im_GRU> grsh1; //указатели на ГРЩ. Прокидываются из Im_SES
		std::shared_ptr<Im_GRU> grsh2;
		std::shared_ptr<Im_GRSH220> grsh220;
		std::vector<shared_ptr<DGInfo>> dg_r_info; //вектор ДГ правого борта
		std::vector<shared_ptr<DGInfo>> dg_l_info; //вектор ДГ левого борта

		std::unordered_map<short,std::vector<shared_ptr<DGInfo>>> busmap; //map со всеми ДГ

		//уровень приоритета шин, нужен для их связи вместе
		enum Bus
		{
			bus_grsh1 = 1,
			bus_grsh2
		};

		void powerControl(); //функция распределения мощности

		//состояние перемычкек между секциями ГРЩ
		inline bool onBridgeGRSH1_GRSH2()
		{
			return in_QC1_isOn_s;
		}

	private:
		long sumPowerNeed = { 0 }; //необходимая мощность для шин
		long countKload = { 0 }; //весовой счетчик работающих ГДГ
};
//-----------------------------------------------------------------------------
#endif
