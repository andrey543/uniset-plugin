#include "Im_PowerControl.h"
//-----------------------------------------------------------------------------
using namespace uniset;
using namespace std;
//-----------------------------------------------------------------------------
//! \todo Перевести с vector на другой контейнер, который удобно и дешево будет склеивать на лету.
//! \todo Добавить учет скорости наброса СДГ при разгрузке ГДГ. Иначе СДГ вываливается п оперегрузке
//! \todo Убрать ненужные после рефакторинга датчики
//! Пока что, чтобы не тратить время, буду использовать vector и склеивать его в конструкторе (потом переписать на list и сравнить скорость выполнения)
//! Пока что делаю без учета ГРЩ! Только ГРУ и ЩПМ
Im_PowerControl::Im_PowerControl(uniset::ObjectId id, xmlNode* node, const std::string& argprefix):
	Im_PowerControl_SK(id, node, argprefix)
{
	//пока что придется делать это руками
	//инициализация
	auto gdg1 = make_shared<DGInfo>(&in_QG1_isOn_s, &out_GDG1_SetPower_fas, &in_GDG1_kLoad_fas, "ГДГ1");
	auto gdg2 = make_shared<DGInfo>(&in_QG2_isOn_s, &out_GDG2_SetPower_fas, &in_GDG2_kLoad_fas, "ГДГ2");
	auto gdg3 = make_shared<DGInfo>(&in_QG3_isOn_s, &out_GDG3_SetPower_fas, &in_GDG3_kLoad_fas, "ГДГ3");
	auto vdg1 = make_shared<DGInfo>(&in_QG4_isOn_s, &out_VDG1_SetPower_fas, &in_VDG1_kLoad_fas, "ВДГ1");
	auto vdg2 = make_shared<DGInfo>(&in_QG5_isOn_s, &out_VDG2_SetPower_fas, &in_VDG2_kLoad_fas, "ВДГ2");

	//ДГ правого борта
	dg_r_info.reserve(3);
	dg_r_info.insert(dg_r_info.end(), gdg1);
	dg_r_info.insert(dg_r_info.end(), gdg3);
	dg_r_info.insert(dg_r_info.end(), vdg1);

	//ДГ правого борта
	dg_l_info.reserve(2);
	dg_l_info.insert(dg_l_info.end(), gdg2);
	dg_l_info.insert(dg_l_info.end(), vdg2);

	//Все ГДГ
	busmap = {{bus_grsh1, dg_r_info},
			  {bus_grsh2, dg_l_info}};
}

void Im_PowerControl::step()
{
	//сбрасываем все привязки
	set_group(bus_grsh1);
	set_group(bus_grsh2);

	/*
	 * Основной метод группировки шин заключается в выставлении индексов групп соединненых между собой шин.
	 * Если смотреть по схеме, индексы шин увеличиваются по значению (с уменьшением приоритета).
	 * Если две шины соединены между собой, то им обоим выставляется одинаковый (наименьший из их двух индексов эих шин)
	 * номер группы. таким образом, шины объединяются для дальнейшего рассчета.
	 * Групп может быть от 1 (все шины соединены) до 7 (все работают сами по себе).
	 * Индексы:
	 * 1 - ГРЩ1
	 * 2 - ГРЩ2
	 * */

	if( onBridgeGRSH1_GRSH2() )
		set_group(bus_grsh1,bus_grsh2);

	powerControl();
}

void Im_PowerControl::powerControl()
{

	for( short group = 1; group <= busmap.size(); group++ )
	{
		//сбрасываем значения для каждой группы
		sumPowerNeed = 0;
		countKload = 0;
		bool bus_flag = false; //флаг присутствия шины в группе

        // Инициализируем требуемую мощность потребляемой мощностью ГРЩ220
        if(grsh220 != nullptr) {
            if( onBridgeGRSH1_GRSH2() )
                sumPowerNeed = grsh220->getP();
            else
                sumPowerNeed = grsh220->getP()/2;
        }

		//проход по всем ГРУ(ЩПМ) и поиск входящих в группу шин
		for( auto bus: busmap )
		{

			for( auto dg: bus.second )
			{
				if( dg->n_group == group) //если входит в рассчитываемую группу
				{
					//рассчет кол-ва ГДГ в работе
					countKload += dg->getKload();
					bus_flag = true;
				}
			}
			if( bus_flag )
			{
				sumPowerNeed += getPower(bus.first);
				bus_flag = false;
			}
		}

		//если нет ни одного выведенного дизеля в группе шин, то
		//выставляем на ГРУ(ЩПМ) этой группы нулевое напряжение и переходи к следующей группе
		if( countKload == 0 )
		{
			for( auto bus: busmap )
			{
				for ( auto dg: bus.second )
				{
					if( dg->n_group == group)
					{
						setVoltage(bus.first, true);
					}
				}
			}
			continue;
		}

		for( auto bus: busmap )
		{
			for ( auto dg: bus.second )
			{

				if( dg->n_group == group)
				{
					//Выставляем номинально напряжени
					setVoltage(bus.first);
	
					//рассчет задания на рабочие ГДГ с учетом веса
					double p = double(sumPowerNeed) * dg->getKload() / countKload;
					//При маленьком kLoad значение мощности может быть меньше 1,
					//поэтому принудительно вытавляю 1 в таком случае
					p = 0 < p && p < 1 ? 1 : p;
					dg->setOutPower( p );
				}
			}
		}
	}

}

//Выставление номера группы в векторе (сравниваем у кого группа меньше) 
void Im_PowerControl::set_group(short group1, short group2)
{
	auto g1 = busmap.find(group1); //ищем где поменять принадлежность

	if( group2 != 0 )
	{
		auto g2 = busmap.find(group2); //ищем где поменять принадлежность

		if( g1->second[0]->n_group > g2->second[0]->n_group )
		{
			for( auto dg: g1->second )
			{
				dg->n_group = g2->second[0]->n_group; //меняем на меньшее
			}
		}
		else
		{
			for( auto dg: g2->second )
			{
				dg->n_group = g1->second[0]->n_group; //меняем на меньшее
			}
		}
	}

	//если не задан второй параметр, то сбрасываем в значения по умолчанию
	else
	{
		for( auto dg: g1->second )
		{
			dg->n_group = group1; //меняем на значения по умолчанию
		}
	}
}


//определение необходимой мощности
void Im_PowerControl::setVoltage(short group, bool reset)
{
	long U_grsh = reset ? 0 : 400;

	switch (group)
	{
		case bus_grsh1:
		{
			grsh1->setU(U_grsh);
			break;
		}
		case bus_grsh2:
		{
			grsh2->setU(U_grsh);
			break;
		}
	}
}

//определение необходимой мощности
long Im_PowerControl::getPower(short group)
{
	switch (group)
	{
		case bus_grsh1:
		{
			return grsh1->getP();
		}
		case bus_grsh2:
		{
			return grsh2->getP();
		}

		default:
			return 0;
	}
}

//Присваиваем указатели на ГРУ
void Im_PowerControl::setGRU(std::shared_ptr<Im_GRU> grsh1_, std::shared_ptr<Im_GRU> grsh2_)
{
	if( grsh1 == nullptr || grsh1 == nullptr)
	{
		grsh1 = grsh1_;
		grsh2 = grsh2_;

		mylog8 << myname << ": grsh1, grsh2" << endl;
	}
	else
	{
		throw Exception( string(myname + ":setGRU(): grsh1 or grsh2 not nullptr!!!") );
	}
}
void Im_PowerControl::setGRSH220(std::shared_ptr<Im_GRSH220> grsh220_) {
	if( grsh220 == nullptr )
		grsh220 = grsh220_;
	else
		throw Exception( string(myname + ":setGRSH220(): grsh220 is not nullptr!!!") );
}

string Im_PowerControl::getMonitInfo()
{
	ostringstream s;
	for( short group = 1; group <= busmap.size(); group++ )
	{
		s << group << " GROUP:"<< endl;
		for( auto bus: busmap )
		{
			for( auto dg: bus.second )
			{
				if( dg->n_group == group)
				{
					s << setw(10);
					s << "bus: "	<< bus.first
					  << " name: "	<< setw(10) << dg->log_name
					  << " kLoad: "	<< setw(10) << dg->getKload()
					  << " stateQG: "	<< dg->getStateQG()
					  << endl;
				}
			}
		}
		s << "" << endl;
	}

	s << "DGInfo:" << endl;
	s << "------" << endl;
	for( auto bus: busmap )
	{
		for( auto dg: bus.second )
		{
			s << setw(10);
			s << "bus: "	<< bus.first
			  << " name: "	<< setw(10) << dg->log_name
			  << " kLoad: "	<< setw(10) << dg->getKload()
			  << " stateQG: "	<< dg->getStateQG()
			  << " n_group: "	<< dg->n_group
			  << endl;
		}
	}
	return s.str();
}
