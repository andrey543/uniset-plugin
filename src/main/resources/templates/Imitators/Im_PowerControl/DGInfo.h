#ifndef DGInfo_H
#define DGInfo_H
//-----------------------------------------------------------------------------
#include <UniSetTypes.h>
#include <string>
//-----------------------------------------------------------------------------
// Содержит информацию по состоянию ГДГ и Q (вынес отдельно, чтобы класс PowerControl не захламлять)
class DGInfo
{
	public:
		DGInfo( const long* inStateQG_, long* outSetPower_, const long* inKload_, const std::string& log_name_ )
		{
			inStateQG = inStateQG_;
			outSetPower = outSetPower_;
			inKload = inKload_;
			log_name = log_name_;
		}

		DGInfo( const std::string& log_name_ )
		{
			inStateQG = nullptr;
			outSetPower = nullptr;
			inKload = nullptr;
			log_name = log_name_;
		}
		

		~DGInfo() {}

		std::string log_name; //Имя для логов
		short n_group = { 0 }; //К чему подключен

		long getKload()
		{
			if( inKload != nullptr )
				return *inKload;
	
			return 0;
		}

		bool getStateQG()
		{
			if( inStateQG != nullptr )
				return *inStateQG;
	
			return false;
		}

		void setOutPower(long p)
		{
			if( outSetPower != nullptr )
			{
				*outSetPower = p;
			}
		}

	private:
		const long* inStateQG;	//Состояние ген автомата
		long* outSetPower; //Задание по мощности
		const long* inKload;	//Коэффициент нагрузочной способности
};

//-----------------------------------------------------------------------------
#endif
