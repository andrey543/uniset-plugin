#ifndef Im_DieselState_H
#define Im_DieselState_H
// ------------------------------------------------------------------------------------------------
#include "UniSetObject.h"
// ------------------------------------------------------------------------------------------------
#define d_info if( d->log()->debugging(Debug::INFO) ) d->log()->info() //кому надо, добавит еще
#define d_warn if( d->log()->debugging(Debug::WARN) ) d->log()->warn()
#define d_crit if( d->log()->debugging(Debug::CRIT) ) d->log()->crit()
// ------------------------------------------------------------------------------------------------
class Im_Diesel;
// ------------------------------------------------------------------------------------------------
//! \todo Сделать функцию init(), которую буду вызывать в Instanse(), чтобы сразу выставлять необходимые out_переменные соответственно с состоянием
//Класс, хранящий состояния

class Im_DieselState
{
	public:
		Im_DieselState();
		virtual ~Im_DieselState() {}
		virtual void sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm ) {}
		virtual void timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm ) {}
		virtual void step( Im_Diesel* d ) {}
		virtual void init( Im_Diesel* d ) {}
		virtual const std::string stateName()
		{
			return "Undefined stateName";
		}
	protected:
		void ChangeState(Im_Diesel* d, Im_DieselState* state);
};

//Выключенное состояние
class Im_DieselStateOff :
	public Im_DieselState
{
	public:
		static Im_DieselStateOff* Instance();
		void Destroy();
		virtual ~Im_DieselStateOff()
		{
			Destroy();
		}

		virtual void sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm ) override;
		virtual void init( Im_Diesel* d ) override;
		virtual const std::string stateName()
		{
			return "StateOff" ;
		}

	protected:
		Im_DieselStateOff() {}

	private:
		static Im_DieselStateOff* _inst;
};

//Включенное состояние
class Im_DieselStateWork :
	public Im_DieselState
{
	public:
		static Im_DieselStateWork* Instance();
		void Destroy();
		virtual ~Im_DieselStateWork()
		{
			Destroy();
		}

		virtual void sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm ) override;
		virtual void step( Im_Diesel* d );
		virtual void init( Im_Diesel* d );
		virtual const std::string stateName() override
		{
			return "StateWork" ;
		}

	protected:
		Im_DieselStateWork() {}

	private:
		static Im_DieselStateWork* _inst;
};

//Состояние разгона
class Im_DieselStateBoost :
	public Im_DieselState
{
	public:
		static Im_DieselStateBoost* Instance();
		void Destroy();
		virtual ~Im_DieselStateBoost()
		{
			Destroy();
		}

		virtual void sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm ) override;
		virtual void step( Im_Diesel* d );
		virtual const std::string stateName() override
		{
			return "StateBoost" ;
		}

		virtual void init(Im_Diesel* d ) override;
	protected:
		Im_DieselStateBoost() {}

	private:
		static Im_DieselStateBoost* _inst;
};

//Состояние замедления
class Im_DieselStateSlowdown :
	public Im_DieselState
{
	public:
		static Im_DieselStateSlowdown* Instance();
		void Destroy();
		virtual ~Im_DieselStateSlowdown()
		{
			Destroy();
		}

		virtual void sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm ) override;
		virtual void step( Im_Diesel* d );
		virtual const std::string stateName() override
		{
			return "StateSlowdown" ;
		}

		virtual void init(Im_Diesel* d ) override;
	protected:
		Im_DieselStateSlowdown() {}

	private:
		static Im_DieselStateSlowdown* _inst;
};

// ------------------------------------------------------------------------------------------
#endif
