#include "Im_DieselState.h"
using namespace std;
// ------------------------------------------------------------------------------------------
Im_DieselStateOn* Im_DieselStateOn::_inst = 0;
// ------------------------------------------------------------------------------------------

Im_DieselStateOn* Im_DieselStateOn::Instance()
{
	if (_inst == 0)
	{
		_inst = new Im_DieselStateOn;
	}

	return _inst;
}
void Im_DieselStateOn::Destroy()
{
	if (_inst)
	{
		delete _inst;
	}

	_inst = 0;
}

void Im_DieselStateOn::sensorInfo(Im_Diesel* q, const uniset::SensorMessage* sm )
{

}

void Im_DieselStateOn::timerInfo(Im_Diesel* q, const uniset::TimerMessage* tm)
{

}

