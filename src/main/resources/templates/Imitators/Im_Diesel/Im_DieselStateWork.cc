#include "Im_DieselState.h"
#include "Im_Diesel.h"
using namespace std;
// ------------------------------------------------------------------------------------------
Im_DieselStateWork* Im_DieselStateWork::_inst = 0;
// ------------------------------------------------------------------------------------------

Im_DieselStateWork* Im_DieselStateWork::Instance()
{
	if (_inst == 0)
	{
		_inst = new Im_DieselStateWork;
	}

	return _inst;
}
void Im_DieselStateWork::Destroy()
{
	if (_inst)
	{
		delete _inst;
	}

	_inst = 0;
}

void Im_DieselStateWork::init( Im_Diesel* d)
{
	d->out_RPM_as = d->normalRPM; //пока нет регулирования оборотов, просто приравниваю
	d_info << d << ": RPM = " << d->out_RPM_as << " об/мин " << endl;
}

void Im_DieselStateWork::sensorInfo(Im_Diesel* d, const uniset::SensorMessage* sm )
{
	if( sm->id == d->Stop_c && sm->value == true )
	{
		d_info << d << ": Пришла команда на останов" << endl;
		ChangeState(d, Im_DieselStateSlowdown::Instance());
	}
}

void Im_DieselStateWork::timerInfo(Im_Diesel* d, const uniset::TimerMessage* tm)
{
	int a;
}

void Im_DieselStateWork::step( Im_Diesel* d )
{

}
