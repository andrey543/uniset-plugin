#include "Im_DieselState.h"
#include "Im_Diesel.h"

using namespace std;
// ------------------------------------------------------------------------------------------
Im_DieselStateSlowdown* Im_DieselStateSlowdown::_inst = 0;
// ------------------------------------------------------------------------------------------

Im_DieselStateSlowdown* Im_DieselStateSlowdown::Instance()
{
	if (_inst == 0)
	{
		_inst = new Im_DieselStateSlowdown;
	}

	return _inst;
}
void Im_DieselStateSlowdown::Destroy()
{
	if (_inst)
	{
		delete _inst;
	}

	_inst = 0;
}

void Im_DieselStateSlowdown::init( Im_Diesel* d)
{
	d->askTimer( d->timerStoping, d->durationStart / d->durationStartFactor );
}

void Im_DieselStateSlowdown::sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm )
{
	int a;
}

void Im_DieselStateSlowdown::timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm )
{
	if( tm->id == d->timerStoping )
	{
		if( d->out_RPM_as > 0 )
		{
			d->askTimer(d->timerStoping, d->durationStart / d->durationStartFactor);
			//уменьшаем обороты, чтоб не было перерегулирования
			d->out_RPM_as = (d->out_RPM_as - d->normalRPM / d->durationStartFactor < 0 ? 0 : d->out_RPM_as - d->normalRPM / d->durationStartFactor);
			d_info << d << ": RPM = " << d->out_RPM_as << " об/мин " << endl;
		}
		else
		{
			d->out_ReadyToStart_s = true;
			d->askTimer(d->timerStoping, 0);
			ChangeState(d, Im_DieselStateOff::Instance());
		}
	}
}

void Im_DieselStateSlowdown::step( Im_Diesel* d )
{

	//проверка на 40% от оборотов
	if( d->trgWork.change( d->out_RPM_as >= 0.4 * d->normalRPM ) )
	{
		if( d->out_RPM_as <= 0.4 * d->normalRPM )
		{
			d_info << d << ": Двигатель работает на частоте вращения менее 40% от номинальной" << endl;
			d->out_IsWork_s = false;
		}
	}

	//проверка на готовность к нагрузке
	//todo вставить проверку на обобщенную аварию

	if( d->trgReadyToLoad.change( d->out_RPM_as <= 0.95 * d->normalRPM ) )
	{
		if( d->out_RPM_as <= 0.95 * d->normalRPM )
		{
			d_info << d << ": Двигатель не готов к нагрузке" << endl;
			d->out_ReadyToLoad_s = false;
			d->out_ExcitationSwitchOff_fc = true;
			d->out_ExcitationSwitchOn_fc = false;
		}
	}
}
