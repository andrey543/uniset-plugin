#include "Im_DieselState.h"
#include "Im_Diesel.h"

using namespace std;
// ------------------------------------------------------------------------------------------
Im_DieselStateBoost* Im_DieselStateBoost::_inst = 0;
// ------------------------------------------------------------------------------------------

Im_DieselStateBoost* Im_DieselStateBoost::Instance()
{
	if (_inst == 0)
	{
		_inst = new Im_DieselStateBoost;
	}

	return _inst;
}
void Im_DieselStateBoost::Destroy()
{
	if (_inst)
	{
		delete _inst;
	}

	_inst = 0;
}

void Im_DieselStateBoost::init( Im_Diesel* d)
{
	d->out_StopStatus_s = false;
	d->askTimer( d->timerStarting, d->durationStart / d->durationStartFactor );
}

void Im_DieselStateBoost::sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm )
{
	if( sm->id == d->Stop_c && sm->value == true )
	{
		d->askTimer(d->timerStarting, 0);
		d_info << d << ": Пришла команда на останов" << endl;
		ChangeState(d, Im_DieselStateSlowdown::Instance());
	}
}

void Im_DieselStateBoost::timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm )
{
	if( tm->id == d->timerStarting )
	{
		if( d->out_RPM_as < d->normalRPM )
		{
			d->askTimer(d->timerStarting, d->durationStart / d->durationStartFactor);
			//увеличиваем обороты, чтоб не было перерегулирования
			d->out_RPM_as = (d->out_RPM_as + d->normalRPM / d->durationStartFactor > d->normalRPM ? d->normalRPM : d->out_RPM_as + d->normalRPM / d->durationStartFactor);
			d_info << d << ": RPM = " << d->out_RPM_as << " об/мин " << endl;
		}
		else
		{
			d->askTimer(d->timerStarting, 0);
		}
	}
}

void Im_DieselStateBoost::step( Im_Diesel* d )
{
	//проверка на 40% от оборотов
	if( d->trgWork.change( d->out_RPM_as >= 0.4 * d->normalRPM ) )
	{
		if( d->out_RPM_as >= 0.4 * d->normalRPM )
		{
			d_info << d << ": Двигатель работает на частоте вращения более 40% от номинальной" << endl;
			d->out_IsWork_s = true;
		}
	}

	//проверка на готовность к нагрузке
	if( d->trgReadyToLoad.change( d->out_RPM_as >= 0.95 * d->normalRPM ) )
	{
		if( d->out_RPM_as >= 0.95 * d->normalRPM )
		{
			d_info << d << ": Двигатель готов к нагрузке" << endl;
			d->out_ReadyToLoad_s = true;
			d->out_ExcitationSwitchOff_fc = false;
			d->out_ExcitationSwitchOn_fc = true;
			ChangeState(d, Im_DieselStateWork::Instance());
		}
	}
}
