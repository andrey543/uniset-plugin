#include "Im_Diesel.h"
//-----------------------------------------------------------------------------
using namespace std;
using namespace uniset;
//-----------------------------------------------------------------------------
Im_Diesel::Im_Diesel(uniset::ObjectId id, xmlNode* node, const std::string& argprefix):
	Im_Diesel_SK(id, node, argprefix)
	, _state(0)
{
	//! \todo перенести первоначальную инициализацию в StartUp
	//! \todo в StartUp сделать подхват состояний при перезапуске процесса (скорее всего внести _FAS датчик состояний, который можно будет подхватывать)
	//инициализируемся в выключенном состоянии
	_state = Im_DieselStateOff::Instance();

	if( _state == 0 )
	{
		throw Exception( myname + "(init): FAILED! Failure to initialize in the Im_DieselStateOff state" );
	}
	_state->init(this);

	mylog8 << myname << ": Constructor end" << endl;
}

void Im_Diesel::sensorInfo(const uniset::SensorMessage* sm )
{
	_state -> sensorInfo(this, sm);
}

void Im_Diesel::timerInfo(const uniset::TimerMessage* tm)
{
	_state ->timerInfo(this, tm);
}

void Im_Diesel::step()
{
	_state->step(this);
}

void Im_Diesel::sysCommand( const uniset::SystemMessage* sm )
{
	if( sm->command == SystemMessage::StartUp )
	{
		out_RemoteControl_s = true;	//Пока что инициализируемся при включенном дистанционном управлении
		out_ReadyToStart_s = true;	// и в состоянии готовности
	}

	Im_Diesel_SK::sysCommand(sm);
}

void Im_Diesel::ChangeState(Im_DieselState* s)
{
	if( s == 0 )
	{
		throw Exception( myname + "(init): nullptr! FAILED! Failure change state" );
	}

	myinfo << this << ": Change state to " << s->stateName() << endl;
	_state = s;
	_state->init(this); //инициализация состояния
}

std::ostream& operator<<(std::ostream& os, Im_Diesel& d )
{
	if( d._state != 0 )
	{
		os << d.myname << "(" << d._state->stateName() << ")";
		return os;
	}

	os << d.myname << "(Undefined stateName)";
	return os;
}

std::ostream& operator<<(std::ostream& os, Im_Diesel* d )
{
	return os << (*d);
}
