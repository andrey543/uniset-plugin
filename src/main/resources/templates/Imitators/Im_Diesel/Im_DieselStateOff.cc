#include "Im_DieselState.h"
#include "Im_Diesel.h"

using namespace std;
// ------------------------------------------------------------------------------------------
Im_DieselStateOff* Im_DieselStateOff::_inst = 0;
// ------------------------------------------------------------------------------------------

Im_DieselStateOff* Im_DieselStateOff::Instance()
{
	if (_inst == 0)
	{
		_inst = new Im_DieselStateOff;
	}

	//init();
	return _inst;
}
void Im_DieselStateOff::Destroy()
{
	if (_inst)
	{
		delete _inst;
	}

	_inst = 0;
}

void Im_DieselStateOff::sensorInfo( Im_Diesel* d, const uniset::SensorMessage* sm )
{
	if( sm->id == d->Start_c && sm->value == true )
	{
		d->out_ReadyToStart_s = false;
		d_info << d << ": Пришла команда на запуск" << endl;
		ChangeState( d, Im_DieselStateBoost::Instance());
	}
}

void Im_DieselStateOff::timerInfo( Im_Diesel* d, const uniset::TimerMessage* tm )
{
	int b;
}

//инициализация
void Im_DieselStateOff::init(Im_Diesel* d)
{
	d->out_StopStatus_s = true;
	d->out_ReadyToStart_s = true;
}
