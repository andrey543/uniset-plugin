//
// Created by andrey on 11.03.19.
//

#ifndef Im_Gate_H
#define Im_Gate_H

#include "Im_Gate_SK.h"

class Im_Gate :
        public Im_Gate_SK{
public:
    Im_Gate(uniset::ObjectId id, xmlNode* node, const std::string& argprefix = "");

protected:
    void step() override;
    void timerInfo( const uniset::TimerMessage* tm ) override;
    void sensorInfo( const uniset::SensorMessage* sm ) override;
    void sysCommand( const uniset::SystemMessage* sm ) override;

private:
    bool isOpen;
    enum Timers
    {
        delayOpen,
        delayClose
    };
    const int DELAY;
private:
    void print(const std::string &message);
    int gen_delay()
    {
        srand(time(NULL));
        return MIN_DELAY + rand() % (MIN_DELAY * 2);
    }
};


#endif //Im_Gate_H
