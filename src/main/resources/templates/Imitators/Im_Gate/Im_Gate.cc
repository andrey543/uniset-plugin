//
// Created by andrey on 11.03.19.
//

#define CMD_ON 1
#define CMD_OFF 0

#include <TEMPLATE_PROJECTConfiguration.h>
#include "Im_Gate.h"


Im_Gate::Im_Gate(uniset::ObjectId id, xmlNode *node, const std::string &argprefix):
Im_Gate_SK(id,node,argprefix),
DELAY(gen_delay())
{
    auto conf = uniset::uniset_conf();

    if( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 )
        mylog->level( Debug::value(default_loglevel) );

    myinfo << PRELOG << "Created.." << std::endl;
}
void Im_Gate::sysCommand(const uniset::SystemMessage *sm) {

    // Uniset-инициализация(синхронизация внутреннего состояния с SharedMemory)
    if(sm->command == uniset::SystemMessage::StartUp)
    {
        isOpen = (bool) out_is_open_c;
    }
}
void Im_Gate::step() {
    out_is_open_c  = isOpen;
    out_is_close_c = !isOpen;
}
void Im_Gate::sensorInfo(const uniset::SensorMessage *sm) {
    if(sm->id == close_s && sm->value && isOpen)
    {
        if ( getTimeInterval(Timers::delayClose) > 0 )
        {
            myinfo << PRELOG << "Уже закрывается"  << std::endl;
            return;
        }

        myinfo << PRELOG << "Закрытие затвора с задержкой: " << std::to_string(DELAY) << " мс" << std::endl;
        askTimer(delayOpen,CMD_OFF);
        askTimer(delayClose, DELAY);
    }
    else if(sm->id == open_s && sm->value && !isOpen)
    {
        if ( getTimeInterval(Timers::delayOpen) > 0 )
        {
            myinfo << PRELOG << "Уже открывается"  << std::endl;
            return;
        }

        myinfo << PRELOG << "Открытие затвора с задержкой: " << std::to_string(DELAY) << " мс" << std::endl;
        askTimer(delayClose,CMD_OFF);
        askTimer(delayOpen, DELAY);
    }
}

void Im_Gate::timerInfo(const uniset::TimerMessage *tm) {
    if(tm->id == delayClose)
    {
        askTimer(delayClose,CMD_OFF);
        isOpen = CMD_OFF;
        myinfo << PRELOG << "Затвор закрыт" << std::endl;
    }
    else if(tm->id == delayOpen)
    {
        askTimer(delayOpen,CMD_OFF);
        isOpen = CMD_ON;
        myinfo << PRELOG << "Затвор открыт" << std::endl;
    }
}