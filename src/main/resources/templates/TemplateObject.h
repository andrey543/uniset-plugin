//
// Created from uniset-plugin by USER on DATE.
//

#ifndef PROJECT_TemplateObject_H
#define PROJECT_TemplateObject_H

#include "TemplateObject_SK.h"

class TemplateObject : public TemplateObject_SK {
public:
    TemplateObject(uniset::ObjectId id, xmlNode* cnode, const std::string& prefix = "");
    virtual ~TemplateObject();
protected:
    virtual void step() override;
    virtual void sensorInfo(const uniset::SensorMessage* sm) override;
    virtual void timerInfo( const uniset::TimerMessage* tm ) override;
};

#endif //PROJECT_TemplateObject_H
