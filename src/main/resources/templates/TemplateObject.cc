//
// Created from uniset-plugin by USER on DATE.
//

#include <TEMPLATE_PROJECTConfiguration.h>
#include "TemplateObject.h"

#define CMD_START 1
#define CMD_STOP 0

TemplateObject::TemplateObject(uniset::ObjectId id, xmlNode *cnode, const std::string &prefix):
TemplateObject_SK(id, cnode, prefix)
{
    auto conf = uniset::uniset_conf();

    bool debug_logs = ( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 );

    if( debug_logs )
        mylog->level( Debug::value(default_loglevel) );

    myinfo << PRELOG << "Created " << std::endl;
}

TemplateObject::~TemplateObject() {
    myinfo << PRELOG << "Destructed " << std::endl;
}

void TemplateObject::step() {

}

void TemplateObject::sensorInfo(const uniset::SensorMessage *sm) {
    if(sm->id == is_work_s)
    {
        if(sm->value) {
            myinfo << PRELOG << "Signal is work " << std::endl;
            out_open_c = CMD_START;
        }
    }
}

void TemplateObject::timerInfo(const uniset::TimerMessage *tm) {

}

