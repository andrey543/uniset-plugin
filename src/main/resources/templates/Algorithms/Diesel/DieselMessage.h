#ifndef DieselMessage_H_
#define DieselMessage_H_
// ------------------------------------------------------------------------------------------
#include "TEMPLATE_PROJECTMessages.h"
// -----------------------------------------------------------------------------
/*!	Сообщения процесса управления дизелем */
class DieselMessage:
	public TEMPLATE_PROJECT::TEMPLATE_PROJECTMessage
{
	public:

		static const int MessageTypeID = TEMPLATE_PROJECT::TEMPLATE_PROJECTMessage::DieselInfo;

		enum CtlMode
		{
			OnMode,
			OffMode
		};

		enum Commands
		{
			Start,	/*! запуск */
			Stop,	/*! остановка */
			Reset,	/*! сброс защиты */
			ResetCommand, /*! сброс команды (прекратить действие, если возможно) */
			OnControl, /*!< включить управление */
			OffControl /*!< отключить управление */
		};

		DieselMessage(uniset::ObjectId _id, Commands _cmd, Message::Priority p = Message::Medium):
			id(_id), cmd(_cmd)
		{
			type = DieselMessage::MessageTypeID;
			priority = p;
		}

		DieselMessage(const uniset::VoidMessage* msg)
		{
			memcpy(this, msg, sizeof(*this));
			assert(this->type == DieselMessage::MessageTypeID );
		}

		inline uniset::TransportMessage transport_msg() const
		{
			return transport(*this);
		}

		uniset::ObjectId id;
		Commands cmd;
};
// ------------------------------------------------------------------------------------------
#endif // Diesel_H_
// ------------------------------------------------------------------------------------------
