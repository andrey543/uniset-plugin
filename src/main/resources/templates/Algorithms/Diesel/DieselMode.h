// -----------------------------------------------------------------------------
#ifndef DieselMode_H_
#define DieselMode_H_
// ------------------------------------------------------------------------------------------
#include <string>
#include <sstream>
#include <UniSetObject.h>
#include <UniSetManager.h>
#include <UInterface.h>
#include <MessageType.h>
#include <ObjectMode.h>
#include "DieselMessage.h"
// ------------------------------------------------------------------------------------------
class Diesel;

/*!	Прототип реализации режимов работы. */
class DieselMode
{
	public:
		static DieselMode* inst();
		virtual ~DieselMode();

		virtual void timerInfo( Diesel* dis, const uniset::TimerMessage* tm ) {}
		virtual void sensorInfo( Diesel* dis, const uniset::SensorMessage* sm ) {}
		virtual void dieselCommand( Diesel* dis, const DieselMessage* m );

		virtual bool activate(Diesel* dis)
		{
			return true;
		}

		virtual bool disactivate(Diesel* dis)
		{
			return true;
		}

		virtual const std::string modeName()
		{
			return "???Mode";
		}

		virtual bool isTransientMode()
		{
			return false;
		}

	protected:
		DieselMode();

		void changeMode( Diesel* dis , DieselMode* m );
		void setProtection( Diesel* dis, bool state );

	private:
		static DieselMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page DieselPage
	\section secDiesel_OffMode Режим "Дизель выключен"
		Котроль параметров отсутствует.
*/

/*!
	Режим работы в выключенном состоянии
*/
class DieselOffMode:
	public DieselMode
{
	public:
		static DieselOffMode* inst();
		virtual ~DieselOffMode();

		// --------
		virtual void sensorInfo( Diesel* dis, const uniset::SensorMessage* sm );
		virtual void dieselCommand( Diesel* dis, const DieselMessage* m );

		virtual bool activate(Diesel* dis);
		virtual const std::string modeName()
		{
			return "DieselOffMode";
		}

	protected:
		DieselOffMode();

	private:
		static DieselOffMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page DieselPage
	\section secDiesel_OnMode Режим "Дизель включен"
*/

/*!
	Режим "Контроль параметров" (включённое состояние)
*/
class DieselOnMode:
	public DieselMode
{
	public:
		static DieselOnMode* inst();
		virtual ~DieselOnMode();

		// --------
		virtual void sensorInfo( Diesel* dis, const uniset::SensorMessage* sm );
		virtual void dieselCommand( Diesel* dis , const DieselMessage* m );

		virtual bool activate( Diesel* dis);
		virtual const std::string modeName()
		{
			return "DieselOnMode";
		}

	protected:
		DieselOnMode();

	private:
		static DieselOnMode* inst_;
};

// ---------------------------------------------------------------------------
/*!
	\page DieselPage
	\section secDiesel_OffControlMode Режим "Отключенное управление дизелем"
*/

/*!
	Режим работы, когда управление происходит с местного поста
*/
class DieselOffControlMode:
	public DieselMode
{
	public:
		static DieselOffControlMode* inst();
		virtual ~DieselOffControlMode();

		// --------
		virtual void sensorInfo( Diesel* dis, const uniset::SensorMessage* sm ) override;
		virtual void dieselCommand( Diesel* dis, const DieselMessage* m ) override;

		virtual bool activate(Diesel* dis);
		virtual const std::string modeName()
		{
			return "DieselOffControlMode";
		}

	protected:
		DieselOffControlMode();

	private:
		static DieselOffControlMode* inst_;
};

// ---------------------------------------------------------------------------
/*!
	\page DieselPage
	\section secDiesel_ProtectionMode Режим "срабатывание защиты дизеля"
	Это состояние является устойчивым. Переход в него возможен при срабатывании
	какой-либо защиты или неудачной попытке перехода из одного устойчивого
	состояния в другое. При активизации происходит сброс в ноль всех выходных сигналов.
	Попытка выхода из него происходит только при подаче команды "сброс защиты".
*/

/*!
	Режим "Сработала защита" или "неудачная попытка перехода в другой режим"
	\sa \ref DieselPage
*/
class DieselProtectionMode:
	public DieselMode
{
	public:
		static DieselProtectionMode* inst();
		virtual ~DieselProtectionMode();

		// --------
		virtual void sensorInfo( Diesel* dis, const uniset::SensorMessage* sm );
		virtual void dieselCommand( Diesel* dis, const DieselMessage* m );

		virtual bool activate(Diesel* dis);
		virtual bool disactivate(Diesel* dis);
		virtual const std::string modeName()
		{
			return "DieselProtectionMode";
		}

	protected:
		DieselProtectionMode();

		bool checkProtection(Diesel* dis);
	private:
		static DieselProtectionMode* inst_;
};

// ---------------------------------------------------------------------------

/*!
	\page DieselPage
	\section secDiesel_InitMode Режим инициализации состояния дизеля

	Определение фактического состояния происходит как представлено на рисунке.
	\image html diesel_init.png
*/

/*!
	\sa \ref DieselPage
*/
class DieselInitMode:
	public DieselMode
{
	public:
		static DieselInitMode* inst();
		virtual ~DieselInitMode();

		// --------
		virtual void dieselCommand( Diesel* dis, const DieselMessage* m );

		virtual bool activate(Diesel* dis);
		virtual const std::string modeName()
		{
			return "DieselInitMode";
		}

	protected:
		DieselInitMode();

	private:
		static DieselInitMode* inst_;
};

// ---------------------------------------------------------------------------
/*!
	\page DieselPage
	\section secDiesel_WaitMode Состояние "ожидание исполнения команды".

	Это состояние является переходным (не устойчивым) и объединяет в себе
	все возможные переходные состояния при переходе из одного устойчивого в другое.
	В основном здесь происходит подача команды и ожидание её выполнения по таймеру.
	В случае невыполнения команды за заданное время (и количество попыток) происходит переход
	в состояние указанное в качестве 'back', в случае успеха в состояние указанное как 'to'.
	(см. DieselTransientMode::inst ).
*/
/*!
	Режим работы, ожидание исполнения команды
	\sa \ref DieselPage
*/
class DieselTransientMode:
	public DieselMode
{
	private:
		/*! Функция инстанцирования для внутреннего использования (с сохранением back и atempt ).
			\param sc - указатель на управляемый объект
			\param to - целевое состояние
		*/
		static DieselTransientMode* inst( Diesel* dg, DieselMessage::CtlMode to);
	public:

		/*! Функция инстанцирования.
			\param sc - указатель на управляемый объект
			\param to - целевое состояние
			\param back - состояние в которое переходить в случае неудачной попытки
			\param atempt - количество попыток
		*/
		static DieselTransientMode* inst( Diesel* dis,
											 DieselMessage::CtlMode to,
											 DieselMode* back,// = DieselProtectionMode::inst(),
											 int atempt = 0 );

		virtual ~DieselTransientMode();

		// --------

		virtual void timerInfo( Diesel* dis, const uniset::TimerMessage* tm);
		virtual void sensorInfo( Diesel* dis, const uniset::SensorMessage* sm );

		virtual bool activate(Diesel* dis);
		virtual bool disactivate(Diesel* dis);
		virtual const std::string modeName()
		{
			return "DieselTransientMode";
		}
		virtual void dieselCommand( Diesel* dis, const DieselMessage* m );

		virtual bool isTransientMode()
		{
			return true;
		}

	protected:
		DieselTransientMode();
		void rollback( Diesel* dis );

	private:
		static DieselTransientMode* inst_;
};
// ---------------------------------------------------------------------------
#endif // DieselMode_H_
