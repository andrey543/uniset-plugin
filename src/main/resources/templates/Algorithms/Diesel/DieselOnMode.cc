#include "Diesel.h"
#include "DieselMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselOnMode* DieselOnMode::inst_ = 0;
// ---------------------------------------------------------------------------


DieselOnMode::~DieselOnMode()
{
}

DieselOnMode::DieselOnMode()
{
}

// ---------------------------------------------------------------------------

DieselOnMode* DieselOnMode::inst()
{
	if(inst_ == 0)
		inst_ =  new DieselOnMode();

	return inst_;
}

// ---------------------------------------------------------------------------
bool DieselOnMode::activate( Diesel* dis )
{
	if( dis->log()->is_info() )
		dis->log()->info() << dis << ": ...activate..." << endl;

	dis->askSensor(dis->Working_s, UniversalIO::UIONotify);

	if( dis->AlarmStop_s != DefaultObjectId )
		dis->askSensor(dis->AlarmStop_s, UniversalIO::UIONotify);

	return true;
}
// ------------------------------------------------------------------------------------------
void DieselOnMode::sensorInfo( Diesel* dis, const uniset::SensorMessage* sm )
{
	if( sm->id == dis->AlarmStop_s )
	{
		if( sm->value )
		{
			if( dis->log()->is_warn() )
				dis->log()->warn() << dis << "(sensorInfo): Аварийное отключение дизеля..." << endl;

			changeMode( dis, DieselProtectionMode::inst() );
		}
	}
	else if( sm->id == dis->Working_s ) // || sm->id == dis->ReadyForStart_s
	{
		if( !sm->value )
		{
			// даём паузу, чтобы датчики причин успели установится
			if( dis->AlarmPause )
			{
				msleep(dis->AlarmPause);
				dis->updateValues();
			}

			if( !dis->in_AlarmStop_s )
			{
				if( dis->log()->is_warn() )
					dis->log()->warn() << dis << "(sensorInfo): неожиданное отключение дизеля...\n";

				changeMode( dis, DieselInitMode::inst() );
			}
			else
			{
				if( dis->log()->is_warn() )
					dis->log()->warn() << dis << "(sensorInfo): Аварийное отключение дизеля..." << endl;

				changeMode( dis, DieselProtectionMode::inst() );
			}
		}
	}
}
// ------------------------------------------------------------------------------------------
void DieselOnMode::dieselCommand( Diesel* dis, const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::Start:
		{
			if( dis->log()->is_info() )
				dis->log()->info() << dis << ": START!" << endl;

			activate(dis);
		}
		break;

		case DieselMessage::Stop:
		{
			if( dis->log()->is_info() )
				dis->log()->info() << dis << ": STOP!" << endl;

			changeMode( dis, DieselTransientMode::inst(dis, DieselMessage::OffMode, this, dis->numberOfAttempts) );
		}
		break;

		default:
			DieselMode::dieselCommand(dis, m);
			break;
	}
}

// ------------------------------------------------------------------------------------------
