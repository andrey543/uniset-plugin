#include "DieselMode.h"
#include "Diesel.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselProtectionMode* DieselProtectionMode::inst_ = 0;
// ---------------------------------------------------------------------------

DieselProtectionMode::~DieselProtectionMode()
{
}

DieselProtectionMode::DieselProtectionMode()
{
}

// ---------------------------------------------------------------------------

DieselProtectionMode* DieselProtectionMode::inst()
{
	if(inst_ == 0)
		inst_ =  new DieselProtectionMode();

	return inst_;
}

// ---------------------------------------------------------------------------

void DieselProtectionMode::sensorInfo( Diesel* dis, const SensorMessage* sm )
{
	if( sm->id == dis->AlarmStop_s )
	{
		if( !sm->value )
			return;
	}
}

// ---------------------------------------------------------------------------

bool DieselProtectionMode::activate(Diesel* dis)
{
	if( dis->log()->is_info())
		dis->log()->info() << dis << ": ..............activate.............." << endl;

	// выставляем все выходы в ноль
	dis->out->reset();
	dis->setProtection(true);

	// даём паузу, чтобы датчики причин успели установится
	if( dis->AlarmPause )
	{
		msleep(dis->AlarmPause);
		dis->updateValues();
	}

	// если выставлен датчик 'аварийная остановка', то смотрим причину
	if( !dis->in_AlarmStop_s )
	{
		// значит перешли сюда без аварийной причины
		// уходим из этого режима
		changeMode( dis, DieselInitMode::inst() );
	}

	return true;
}

// ---------------------------------------------------------------------------

void DieselProtectionMode::dieselCommand( Diesel* dis, const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::Reset:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << "(dieselInfo): сброс защиты...\n";

			dis->setValue(dis->EmergencyStopReset_c, true);
			msleep(dis->CmdTime);
			dis->setValue(dis->EmergencyStopReset_c, false);
			dis->updateValues();

			if(!checkProtection(dis))
				changeMode( dis, DieselInitMode::inst() );
			else if( dis->log()->is_info())
				dis->log()->info() << dis  << "(dieselInfo): reset.. не сброшены признаки аварии..." << endl;

			//				activate(dis);
		}
		break;

		case DieselMessage::Start:
		case DieselMessage::Stop:
		{
			if( !checkProtection(dis) )
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis  << "(dieselInfo): FAILED! protecton not reset..." << endl;
			}
			else
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis  << "(dieselInfo): FAILED! protecton not reset..." << endl;
			}
		}
		break;

		default:
			DieselMode::dieselCommand(dis, m);
			break;
	}
}

// ------------------------------------------------------------------------------------------
bool DieselProtectionMode::checkProtection( Diesel* dis )
{
	if( dis->in_AlarmStop_s )
	{
		if( dis->log()->is_info())
			dis->log()->info() << dis << "(checkProtection): AlarmStop=1" << endl;

		dis->setMsg(dis->mid_NotResetProtection, true);
		return true;
	}

	dis->setMsg(dis->mid_NotResetProtection, false);
	dis->protect->commit(dis->AlarmStop_s, dis->in_AlarmStop_s);
	return false;
}
// ------------------------------------------------------------------------------------------
bool DieselProtectionMode::disactivate(Diesel* dis)
{
	dis->setProtection(false);
	dis->protect->commit(dis->AlarmStop_s, dis->in_AlarmStop_s);
	return true;
}
// ------------------------------------------------------------------------------------------
