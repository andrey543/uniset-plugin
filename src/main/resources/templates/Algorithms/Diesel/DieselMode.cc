// -----------------------------------------------------------------------------
#include "DieselMode.h"
#include "Diesel.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselMode* DieselMode::inst_ = 0;
// ---------------------------------------------------------------------------

DieselMode::~DieselMode()
{
}

DieselMode::DieselMode()
{
}

// ---------------------------------------------------------------------------

DieselMode* DieselMode::inst()
{
	if(inst_ == 0)
		inst_ =  new DieselMode();

	return inst_;
}

// ---------------------------------------------------------------------------
void DieselMode::changeMode( Diesel* ds, DieselMode* m )
{
	ds->changeMode(m);
}
// ---------------------------------------------------------------------------
void DieselMode::setProtection( Diesel* dis , bool state )
{
	dis->setProtection(state);
}
// ---------------------------------------------------------------------------
void DieselMode::dieselCommand( Diesel* ds, const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::OffControl:
		{
			if( ds->log()->is_info() )
				ds->log()->info() << ds << ": OFF CONTROL command.." << endl;

			changeMode( ds, DieselOffControlMode::inst() );
		}
		break;

		case DieselMessage::ResetCommand:
			break;

		default:
			break;
	}
}
// ---------------------------------------------------------------------------
