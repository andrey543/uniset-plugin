#include "Diesel.h"
#include "DieselMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselTransientMode* DieselTransientMode::inst_ = 0;
// ---------------------------------------------------------------------------

DieselTransientMode::DieselTransientMode()
{

}

DieselTransientMode::~DieselTransientMode()
{
}

// ---------------------------------------------------------------------------

DieselTransientMode* DieselTransientMode::inst( Diesel* dis, DieselMessage::CtlMode to)
{
	if(inst_ == 0)
	{
		ostringstream err;
		err << dis << "(inst): private instance should be called from already exited DieselTransientMode instance";
		if( dis->log()->is_crit() )
			dis->log()->crit() << dis << err.str()<< endl;
		throw SystemError(err.str());
	}

	dis->prevMode = dis->toMode;
	dis->toMode = to;
	return inst_;
}

// ---------------------------------------------------------------------------

DieselTransientMode* DieselTransientMode::inst( Diesel* dis,
		DieselMessage::CtlMode to,
		DieselMode* back, int attempt)
{
	if(inst_ == 0)
		inst_ =  new DieselTransientMode();

	dis->prevMode = dis->toMode;
	dis->toMode = to;
	dis->backMode = back;
	dis->attemptCounter = attempt;
	dis->attempt = 0;
	return inst_;
}

// ---------------------------------------------------------------------------
bool DieselTransientMode::activate( Diesel* dis )
{
	if( dis->log()->is_info())
		dis->log()->info() << dis << ": [ to ==> " << dis->toMode << " ]" << endl;

	switch( dis->toMode )
	{
		case DieselMessage::OffMode:
		{
			if( dis->in_AlarmStop_s )
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << "(activate): аварийно остановлен(не сброшена защита)" << endl;

				changeMode( dis, DieselProtectionMode::inst());
				break;
			}

			dis->out->set(dis->Stop_c, true);
			dis->tm->set(Diesel::OffTimer, dis->OffTime);
			dis->askSensor(dis->Working_s, UniversalIO::UIONotify);
		}
		break;

		case DieselMessage::OnMode:
		{
			if( dis->in_AlarmStop_s )
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << "(activate): аварийно остановлен(не сброшена защита)" << endl;

				changeMode( dis, DieselProtectionMode::inst() );
				break;
			}

			if( !dis->in_ReadyForStart_s )
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << "(activate): дизель не готов к работе" << endl;

				dis->setMsg(dis->mid_NotReadyForStart);
				changeMode( dis, dis->backMode );
				break;
			}

			dis->out->set(dis->Start_c, true);
			dis->tm->set(Diesel::OnTimer, dis->OnTime);
			dis->askSensor(dis->Working_s, UniversalIO::UIONotify);
		}
		break;

		default:
		{
			if( dis->log()->is_crit())
				dis->log()->crit() << dis << "(activate): Unkown command.. toMode=" << dis->toMode << endl;
		}
		break;
	}

	return true;
}
// ------------------------------------------------------------------------------------------

bool DieselTransientMode::disactivate( Diesel* dis )
{
	switch( dis->toMode )
	{
		case DieselMessage::OffMode:
			dis->tm->set(Diesel::OffTimer, 0);
			dis->out->set(dis->Stop_c, false);
			break;

		case DieselMessage::OnMode:
			dis->tm->set(Diesel::OnTimer, 0);
			dis->out->set(dis->Start_c, false);
			break;

		default:
			if( dis->log()->is_crit())
				dis->log()->crit() << dis << ": UNKNOWN MODE! toMode=" << dis->toMode << endl;

			return false;
	}

	return true;
}

// ------------------------------------------------------------------------------------------
void DieselTransientMode::timerInfo( Diesel* dis, const uniset::TimerMessage* tm )
{
	switch( tm->id )
	{
		case Diesel::OnTimer:
		{
			dis->tm->set(Diesel::OnTimer, 0);

			if( dis->toMode != DieselMessage::OnMode )
				return;

			if( dis->log()->is_crit())
				dis->log()->crit() << dis << ": START timeout.. " << dis->OnTime <<  " msec" << endl;

			dis->out->set(dis->Start_c, false);
		}
		break;

		case Diesel::OffTimer:
		{
			dis->tm->set(Diesel::OffTimer, 0);

			if( dis->toMode != DieselMessage::OffMode )
				return;

			if( dis->log()->is_crit())
				dis->log()->crit() << dis << ": STOP timeout " << dis->OffTime <<  " msec" << endl;

			dis->out->set(dis->Stop_c, false);
		}
		break;

		default:
			if( dis->log()->is_crit())
				dis->log()->crit() << dis << ":  Unknown timer tid=" << tm->id << endl;

			break;
	}

	if( dis->attempt >= dis->attemptCounter )
	{
		changeMode( dis, dis->backMode );
	}
	else
	{
		dis->attempt++;
		activate(dis);
	}
}
// ------------------------------------------------------------------------------------------

void DieselTransientMode::sensorInfo( Diesel* dis, const uniset::SensorMessage* sm )
{
	/*
		if( sm->id == dis->ReadyForLoading_s )
		{
			if( sm->value && dis->toMode == DieselMessage::OnMode )
			{
				dis->out->set(dis->Start_c, false);
				dis->tm->set(Diesel::OnTimer, 0);
				dis->info(dis->mid_endStarting);
				changeMode( DieselOnMode::inst(), dis);
			}
		}
		else if( sm->id == dis->Working_s )
	*/
	if( sm->id == dis->Working_s )
	{
		if( !sm->value && dis->toMode == DieselMessage::OffMode )
		{
			dis->out->set(dis->Stop_c, false);
			dis->tm->set(Diesel::OffTimer, 0);
			changeMode( dis, DieselOffMode::inst() );
		}
		else if( sm->value && dis->toMode == DieselMessage::OnMode )
		{
			dis->out->set(dis->Start_c, false);
			dis->tm->set(Diesel::OnTimer, 0);
			changeMode( dis, DieselOnMode::inst() );
		}
	}
	else if( sm->id == dis->AlarmStop_s )
	{
		if( sm->value )
		{
			dis->out->set(dis->Stop_c, false);
			dis->out->set(dis->Start_c, false);

			if( dis->log()->is_warn())
				dis->log()->warn() << dis << "(sensorInfo): Аварийное отключение дизеля..." << endl;

			changeMode( dis, DieselProtectionMode::inst());
		}
	}
}

// ------------------------------------------------------------------------------------------

void DieselTransientMode::dieselCommand( Diesel* dis, const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::Start:
		{
			if( dis->toMode == DieselMessage::OnMode )
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << ": START! already starting.." << endl;
			}
			else
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << ": START!(rollback)" << endl;

				rollback(dis);
				// при новой команде обновляем attemps
				changeMode( dis, DieselTransientMode::inst(dis, DieselMessage::OnMode, DieselInitMode::inst(), dis->numberOfAttempts) );
			}
		}
		break;

		case DieselMessage::Stop:
		{
			if( dis->toMode == DieselMessage::OffMode )
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << ": STOP! already stopping..." << endl;
			}
			else
			{
				if( dis->log()->is_info())
					dis->log()->info() << dis << ": STOP!(rollback)" << endl;

				rollback(dis);
				// при новой команде обновляем attemps
				changeMode( dis, DieselTransientMode::inst(dis, DieselMessage::OffMode, DieselInitMode::inst(), dis->numberOfAttempts) );
			}
		}
		break;

		case DieselMessage::OffControl:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": OFF CONTROL.." << endl;

			rollback(dis);
			changeMode( dis, DieselOffControlMode::inst() );
		}
		break;

		case DieselMessage::Reset:
			break;

		case DieselMessage::ResetCommand:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": RESET COMMAND.." << endl;

			dis->out->reset();
			changeMode( dis, DieselInitMode::inst() );
		}
		break;

		default:
			if( dis->log()->is_crit())
				dis->log()->crit() << dis << ": Unknown command (" << m->cmd << ")" << endl;

			break;
	}
}

// ------------------------------------------------------------------------------------------
void DieselTransientMode::rollback( Diesel* dis )
{
	if( dis->log()->is_warn())
		dis->log()->warn() << dis << "(rollback): отмена команды..." << endl;

	DieselMessage::CtlMode tmpMode(dis->toMode);
	dis->toMode = dis->prevMode;
	disactivate(dis);
	dis->toMode = tmpMode;
}
// ------------------------------------------------------------------------------------------
