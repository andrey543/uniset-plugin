/*!
 * \page DieselPage Интерфейс управления дизелем
 * \par Описание
	- \ref secDiesel_common
	- \ref secDiesel_OffMode
	- \ref secDiesel_OnMode
	- \ref secDiesel_WaitMode
	- \ref secDiesel_sleepMode
	- \ref secDiesel_ProtectionMode
	- \ref secDiesel_commands

	\section secDiesel_common Общее описание класса Diesel
		Алгоритм написан для управления дизелем фирмы Deutz.

	\section secDiesel_commands Переходные режимы в объекте дизель

	\note Времена для таймеров настраиваются в конфигурационном файле

	\par
		Переходы связанные с ожиданием реализованы в DieselTransientMode.

	\par Переход DieselOffMode -> DieselOnMode
		Переход осуществляется по приходу команды DieselMessage::OnCommand.
		При этом происходит подача выходного сигнала "запуск" и ожидание входного сигнала
		"о достижении ном. частоты вращения".
		Если за время Diesel::OnTime сигнал пришёл, процесс считается успешно завершенным,
		если нет осуществляется переход в \ref secDiesel_ProtectionMode.
	\note Подача команды осуществляется кратковременным сигналом, длительностью Diesel::CmdTime.



	\par Переход DieselOnMode -> DieselOffMode
		Переход осуществляется по приходу команды DieselMessage::OffCommand.
		При этом происходит подача выходного сигнала "останов" и ожидание
		снятия входного сигнала "дизель работает".
		Если за время Diesel::OffTime сигнал снят, процесс считается успешно завершенным,
		если нет осуществляется переход в \ref secDiesel_ProtectionMode.

	\note Подача команды осуществляется кратковременным сигналом, длительностью Diesel::CmdTime.
*/

// подразделы сформируются автоматически из описаний сделанных в соответствующих h-файлах

// ------------------------------------------------------------------------------------------
#ifndef Diesel_H_
#define Diesel_H_
// ------------------------------------------------------------------------------------------
#include <memory>
#include <DelayTimer.h>
#include <Trigger.h>
#include <TriggerOUT.h>
#include <TriggerOR.h>
#include "DieselMessage.h"
#include "Diesel_SK.h"
// ------------------------------------------------------------------------------------------
class DieselMode;
// ------------------------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& os, const DieselMessage::CtlMode& m);
// ------------------------------------------------------------------------------------------

/*!	Интерфейс управления дизелем */
class Diesel:
	public Diesel_SK
{
	public:
		Diesel( uniset::ObjectId id, xmlNode* cnode );
		virtual ~Diesel();

		inline bool isProtection() const
		{
			return protection;
		}

		inline bool isReadyForStart() const
		{
			return in_ReadyForStart_s;
		}

		inline bool isReadyForLoading() const
		{
			return in_ReadyForLoading_s;
		}

		inline bool isWorking() const
		{
			return in_Working_s;
		}

		inline bool isRemoteControl() const
		{
			return in_RemoteControl_s;
		}

		inline long getState() const
		{
			return out_state_as;
		}

		inline long getRPM() const
		{
			return in_RPM_as;
		}
		
		long getEstablishedRPM() const;
		bool isOn() const;
		bool isHot() const;

		void start();
		void stop();
		void reset();
		void setBlackoutStartMode( bool set );
		void offControl();
		void onControl();
		void resetCommand();
		void setRPM( const long& rpm, const uniset::timeout_t& msec = 0 );

		/*! время сколько двигатель был остановлен (время с момента последнего пуска)
		 * \warning Важно понимать, что мы заскаем время по сути с поледнего запуска нашей системы
		 * или даже нашего процесса управления, и сколько времени до этого "не работал" ГДГ мы не знаем..
		 */
		uniset::timeout_t getStationTime();

		// таймеры
		enum Timers
		{
			OnTimer,
			OffTimer
		};

		std::shared_ptr<uniset::TriggerOUT<Diesel, uniset::ObjectId, bool>> out;
		std::shared_ptr<uniset::TriggerOUT<Diesel, Diesel::Timers, int>> tm;
		std::shared_ptr<uniset::TriggerOR<Diesel, uniset::ObjectId>> protect; 		/*!< триггер 'состояния защиты' */

		friend std::ostream& operator<<(std::ostream& os, Diesel* d );
		friend std::ostream& operator<<(std::ostream& os, Diesel& d );

	protected:
		Diesel();

		virtual void step() override;
		virtual void processingMessage( const uniset::VoidMessage* msg ) override;
		virtual void sysCommand( const uniset::SystemMessage* sm ) override;
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( const uniset::TimerMessage* tm ) override;

		void dieselCommand( const DieselMessage* m );
		virtual std::string getTimerName( int id ) const override;
		virtual std::string getMonitInfo() const override;

		void changeMode( DieselMode* m );
		virtual bool deactivateObject() override;
		void setOut(uniset::ObjectId sid, bool state);
		void setTimer(Diesel::Timers tid, int val);
		void setProtection( bool st );

	private:

		friend class DieselMode;
		friend class DieselTransientMode;
		friend class DieselProtectionMode;

		DieselMode* mode;

		void init();
		std::atomic_bool protection;

		uniset::PassiveTimer ptStarted;
		uniset::PassiveTimer ptStopped;
		uniset::PassiveTimer ptRPMupdate;
		uniset::PassiveTimer ptRPMestablishDelay; // задержка на прием сигнала установившихся оборотов
		uniset::DelayTimer delayRPMset; // задержка на определение установившихся заданных оборотов
		uniset::DelayTimer delayRPMnom; // // задержка на определение установившихся номинальных оборотов
		uniset::Trigger trIsStarted; // триггер на завершенный пуск
		uniset::Trigger trIsStopping; // триггер на остановку
		
		bool readyToControlRPM;
		bool controlRPM;
		long targetRPM;
		long prevRPM;

		// Для переходных режимов
		DieselMessage::CtlMode toMode;	/*!< в какой режим переходим */
		DieselMessage::CtlMode prevMode;	/*!< предыдущий режим */
		DieselMode* backMode; 	/*!< из какого ушли */
		int attemptCounter;			/*!< счётчик неудачных попыток перехода */
		int attempt;				/*!< попытка */
};
// ------------------------------------------------------------------------------------------
namespace std
{
	template<>
	class hash<Diesel::Timers>
	{
		public:
			size_t operator()(const Diesel::Timers& s) const
			{
				return std::hash<int>()(s);
			}
	};

	std::string to_string( const Diesel::Timers& t );
}
// ------------------------------------------------------------------------------------------
#endif // Diesel_H_
