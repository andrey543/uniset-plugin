#include "DieselMode.h"
#include "Diesel.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselInitMode* DieselInitMode::inst_ = 0;
// ---------------------------------------------------------------------------

DieselInitMode::~DieselInitMode()
{
}

DieselInitMode::DieselInitMode()
{
}

// ---------------------------------------------------------------------------

DieselInitMode* DieselInitMode::inst()
{
	if(inst_ == 0)
		inst_ =  new DieselInitMode();

	return inst_;
}

// ---------------------------------------------------------------------------
bool DieselInitMode::activate( Diesel* dis )
{
	if( dis->log()->is_info())
		dis->log()->info() << dis << ": .activate." << endl;

	if( !dis->in_RemoteControl_s )
	{
		if( dis->log()->is_info())
			dis->log()->info() << dis << "(activate): (init): 'OFF CONTROL'" << endl;

		changeMode( dis, DieselOffControlMode::inst() );
		return true;
	}

	if( dis->in_AlarmStop_s )
	{
		if( dis->log()->is_info())
			dis->log()->info() << dis << "(activate): (init): 'PROTECTION'" << endl;

		changeMode( dis, DieselProtectionMode::inst());
		return true;
	}

	if( dis->in_Working_s )
	{
		if( dis->log()->is_info())
			dis->log()->info() << dis << "(activate): (init): 'ON'" << endl;

		changeMode( dis, DieselOnMode::inst() );
		return true;
	}

	if( dis->log()->is_info())
		dis->log()->info() << dis << "(activate): (init): 'OFF'" << endl;

	changeMode( dis, DieselOffMode::inst());
	return true;
}

// ---------------------------------------------------------------------------

void DieselInitMode::dieselCommand( Diesel* dis, const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::Start:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": START.." << endl;

			changeMode( dis, DieselTransientMode::inst(dis, DieselMessage::OnMode, this, dis->numberOfAttempts) );
		}
		break;

		case DieselMessage::Stop:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": STOP..." << endl;

			changeMode( dis, DieselTransientMode::inst(dis, DieselMessage::OffMode, this, dis->numberOfAttempts) );
		}
		break;

		default:
			break;
	}
}

// ------------------------------------------------------------------------------------------
