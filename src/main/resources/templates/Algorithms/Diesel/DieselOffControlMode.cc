#include "Diesel.h"
#include "DieselMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselOffControlMode* DieselOffControlMode::inst_ = 0;
// ---------------------------------------------------------------------------

DieselOffControlMode::~DieselOffControlMode()
{
}

DieselOffControlMode::DieselOffControlMode()
{
}

// ---------------------------------------------------------------------------

DieselOffControlMode* DieselOffControlMode::inst()
{
	if(inst_ == 0)
		inst_ =  new DieselOffControlMode();

	return inst_;
}

// ---------------------------------------------------------------------------
void DieselOffControlMode::sensorInfo( Diesel* dis, const SensorMessage* sm )
{
    /*
	if( sm->id == dis->AlarmStop_s )
	{
		if( sm->value )
		{
			dis->log()->warn() << dis << "(sensorInfo): Аварийное отключение дизеля..." << endl;
			changeMode( dis, DieselProtectionMode::inst() );
		}
	}
	*/
}
// ---------------------------------------------------------------------------
bool DieselOffControlMode::activate( Diesel* dis )
{
	if( dis->log()->is_info() )
		dis->log()->info() << dis << "(activate): OFF CONTROL.." << endl;

	dis->out->reset();
	dis->tm->reset();
	
	if( dis->AlarmStop_s != DefaultObjectId )
		dis->askSensor(dis->AlarmStop_s, UniversalIO::UIONotify);
	return true;
}
// ---------------------------------------------------------------------------
void DieselOffControlMode::dieselCommand( Diesel* dis , const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::OnControl:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": ON CONTROL!" << endl;

			changeMode( dis, DieselInitMode::inst() );
		}
		break;

		default:
			DieselMode::dieselCommand( dis, m );
			break;
	}
}
// ---------------------------------------------------------------------------
