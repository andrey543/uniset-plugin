#include "Diesel.h"
#include "DieselMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
DieselOffMode* DieselOffMode::inst_ = 0;
// ---------------------------------------------------------------------------


DieselOffMode::~DieselOffMode()
{
}

DieselOffMode::DieselOffMode()
{
}

// ---------------------------------------------------------------------------

DieselOffMode* DieselOffMode::inst()
{
	if(inst_ == 0)
		inst_ =  new DieselOffMode();

	return inst_;
}

// ---------------------------------------------------------------------------
bool DieselOffMode::activate( Diesel* dis )
{
	if( dis->log()->is_info())
		dis->log()->info() << dis << ": ..............activate.............." << endl;

	if( dis->AlarmStop_s != DefaultObjectId )
		dis->askSensor(dis->AlarmStop_s, UniversalIO::UIONotify);
	
	return true;
}
// ------------------------------------------------------------------------------------------
void DieselOffMode::sensorInfo( Diesel* dis, const uniset::SensorMessage* sm )
{
	if( sm->id == dis->AlarmStop_s )
	{
		if( sm->value )
		{
			dis->log()->warn() << dis << "(sensorInfo): Аварийное отключение дизеля..." << endl;
			changeMode( dis, DieselProtectionMode::inst() );
		}
	}
	/*	else if( sm->id == ds->ReadyForStart_s )
		{
			if( sm->state )
				ds->msg->set(ds->mid_stopped,true);
			else
				ds->msg->set(ds->mid_NotReadyForStart,true);
		}
	*/
	else if( sm->id == dis->Working_s )
	{
		if( sm->value )
		{
			if( dis->log()->is_warn())
				dis->log()->warn() << dis << "(sensorInfo): неожиданное ВКЛЮЧЕНИЕ ДИЗЕЛЯ..." << endl;

			changeMode( dis, DieselInitMode::inst());
		}
	}
}
// ------------------------------------------------------------------------------------------
void DieselOffMode::dieselCommand( Diesel* dis, const DieselMessage* m )
{
	switch(m->cmd)
	{
		case DieselMessage::Start:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": START!" << endl;

			changeMode( dis, DieselTransientMode::inst(dis, DieselMessage::OnMode, this, dis->numberOfAttempts) );
		}
		break;

		case DieselMessage::Stop:
		{
			if( dis->log()->is_info())
				dis->log()->info() << dis << ": STOP!" << endl;

			activate(dis);
		}
		break;

		default:
			DieselMode::dieselCommand(dis, m);
			break;
	}
}

// ------------------------------------------------------------------------------------------
