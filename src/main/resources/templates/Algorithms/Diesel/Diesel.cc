// -----------------------------------------------------------------------------
#include <sstream>
#include <iomanip>
#include <Exceptions.h>
#include <Debug.h>
#include <UniSetManager.h>
#include "Diesel.h"
#include "DieselMode.h"
#include <TEMPLATE_PROJECTConfiguration.h>
// ------------------------------------------------------------------------------------------
using namespace TEMPLATE_PROJECT;
using namespace uniset;
using namespace std;
using namespace UniSetAlgorithms;
// ------------------------------------------------------------------------------------------
std::string std::to_string( const Diesel::Timers& t )
{
	switch(t)
	{
		case Diesel::OnTimer:
			return "OnTimer";

		case Diesel::OffTimer:
			return "OffTimer";

		default:
			break;
	};

	return "Unknown";
}
// ------------------------------------------------------------------------------------------
#define check_time(t,test_exp) \
	{ \
		if( t test_exp ) \
		{ \
			myinfo << myname << ": init '" << __STRING(t) << "' time FAILED! Must be >0)"<< endl; \
			throw Exception( string(myname+": init '" +__STRING(t) + "' time FAILED! Must be >0") ); \
		} \
	}
// ------------------------------------------------------------------------------------------
Diesel::Diesel(ObjectId id, xmlNode* confnode ):
	Diesel_SK( id, confnode ),
	mode(0),
	protection(false),
	toMode(DieselMessage::OffMode),
	prevMode(DieselMessage::OffMode),
	attemptCounter(0),
	attempt(0),
	ptRPMupdate(0),
	targetRPM(RPMnom),
	readyToControlRPM(false),
	controlRPM(false)
{
	auto conf = uniset_conf();

	check_time(OnTime, <= 0);
	check_time(OffTime, <= 0);
	check_time(AlarmPause, < 0);
	check_time(CmdTime, < 0);
	
	// задержка на проверку оборотов 
	delayRPMset.set(RPMcheckDelay, 0);
	delayRPMnom.set(RPMcheckDelay, 0);
	ptRPMestablishDelay.setTiming(RPMestablishDelay);
	ptStarted.setTiming(WaitHotTime);

	if( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 )
		mylog->level( Debug::value(default_loglevel) );

	out = make_shared<uniset::TriggerOUT<Diesel, ObjectId, bool>>(this, &Diesel::setOut);
	out->add(Start_c, false);
	out->add(Stop_c, false);
	//	out->add(EmergencyStop_c, false);

	tm = make_shared<uniset::TriggerOUT<Diesel, Diesel::Timers, int>>(this, &Diesel::setTimer);
	tm->add(Diesel::OnTimer, 0);
	tm->add(Diesel::OffTimer, 0);

	protect = make_shared<uniset::TriggerOR<Diesel, ObjectId>>(this, &Diesel::setProtection);
	protect->add(AlarmStop_s, false);

	out_state_as = mUNKNOWN;
	mode = DieselOffControlMode::inst();
}

Diesel::~Diesel()
{
}

Diesel::Diesel()
{
	mycrit << myname << ": init failed!!!!!!!!!!!!!!!" << endl;
	throw Exception( string(myname + ": init failed!!!") );
}
// -----------------------------------------------------------------------------
void Diesel::step()
{
	int state = mOFF;

	if( in_ReadyForStart_s )
		state = mREADY;

	if( in_Working_s )
		state = mON;
    
    if( in_AlarmStop_s )
		state = mALARM;

	if( protection )
		state = mPROTECTION;

	if( mode && mode->isTransientMode() )
		state = mTRANSITIVE;

	out_state_as = state;
	
	if( out_state_as == mOFF ||  out_state_as == mREADY || out_state_as == mPROTECTION )
	{
		prevRPM = targetRPM = 0;
		out_SetRPM_as = 0;
		controlRPM = false;
		readyToControlRPM = false;
	}
	else
	{
		if( !ptRPMupdate.checkTime() )
		{
			double full = ptRPMupdate.getInterval();
			double current = ptRPMupdate.getCurrent();
			auto percentage = current / full;
			out_SetRPM_as = prevRPM + percentage * (targetRPM - prevRPM);
		}
		else
		{
			prevRPM = targetRPM;
			out_SetRPM_as = targetRPM;
		}
		
		// сбрасываем управление оборотами, когда реальные обороты достигли заданных
		if( controlRPM && delayRPMset.check( abs( getRPM() - targetRPM ) < dRPM ) && in_RPMestablished_s )
			controlRPM = false;
		// готовность к управлению оборотами формируется после пуска и выхода на номинальные обороты 
		if( trIsStarted.hi( in_Working_s && delayRPMnom.check( abs( getRPM() - RPMnom ) < dRPM ) ) )
			readyToControlRPM = true;
		// сбрасываем готовность к управлению оборотами если пошла команда на остановку
		if( trIsStopping.hi( out_Stop_c || out_EmergencyStop_c ) )
			readyToControlRPM = false;
	}

	out_ControlRPM_c = readyToControlRPM ? controlRPM : false;

	// Формируем свое состояние работает по оборотам
	out_Working_c = ( getRPM() > RPMmin );
}
// -----------------------------------------------------------------------------
void Diesel::processingMessage( const uniset::VoidMessage* msg )
{
	try
	{
		switch ( msg->type )
		{
			case DieselMessage::MessageTypeID:
			{
				dieselCommand(reinterpret_cast<const DieselMessage*>(msg));
				break;
			}

			default:
				Diesel_SK::processingMessage(msg);
				break;
		}
	}
	catch( std::exception& ex)
	{
		mycrit  << myname << "(processingMessage): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Diesel::sensorInfo( const SensorMessage* sm )
{
	if( sm->id == RemoteControl_s )
	{
		if( sm->value )
			onControl();
		else
			offControl();

		return;
	}
	else if( sm->id == Working_s )
	{
		if( sm->value )
			ptStarted.reset();
		else
			ptStopped.reset();
	}

	try
	{
		mode->sensorInfo(this, sm);
	}
	catch( std::exception& ex)
	{
		mycrit  << myname << "(sensorInfo): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Diesel::timerInfo( const TimerMessage* tm )
{
	try
	{
		mode->timerInfo(this, tm);
	}
	catch( std::exception& ex )
	{
		mycrit  << myname << "(timerInfo): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Diesel::sysCommand( const SystemMessage* sm )
{
	Diesel_SK::sysCommand(sm);

	if( sm->command == SystemMessage::StartUp )
		mode->activate(this);
}

// ------------------------------------------------------------------------------------------

void Diesel::changeMode( DieselMode* m )
{
	DieselMode* old = mode;
	mode = m;

	try
	{
		if( !mode->isTransientMode() )
			prevMode = toMode;

		old->disactivate(this);
		mode->activate(this);
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(changeMode): не удалось сменить режим!!! " << ex.what() << endl;
		mode = old;
		mode->activate(this);
	}
}
// ------------------------------------------------------------------------------------------
void Diesel::dieselCommand( const DieselMessage* m )
{
	try
	{
		if( m->cmd == DieselMessage::Reset )
		{
			protect->commit(AlarmStop_s, in_AlarmStop_s);
			protect->update();
		}

		mode->dieselCommand(this, m);
	}
	catch( std::exception& ex )
	{
		mycrit  << myname << "(dieselInfo): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
std::string Diesel::getTimerName(int id) const
{
	return to_string( (Timers)id );
}
// ------------------------------------------------------------------------------------------
string Diesel::getMonitInfo() const
{
	ostringstream s;

	s << "mode: " << ( mode ? mode->modeName() : " UNKNOWN" )
	  << " protection=" << protection
	  << endl;

	return std::move(s.str());
}
// ------------------------------------------------------------------------------------------
std::ostream& operator<<(std::ostream& os, Diesel& dis )
{
	if( dis.mode )
		return os << dis.myname << "(" << dis.mode->modeName() << ")";

	return os << dis.myname << "(UNKNOWN mode)";
}

std::ostream& operator<<(std::ostream& os, Diesel* dis )
{
	return os << (*dis);
}
// ------------------------------------------------------------------------------------------
bool Diesel::deactivateObject()
{
	try
	{
		out->reset();
	}
	catch( std::exception& ex )
	{
		mycrit  << myname << "(deactivateObject): " << ex.what() << endl;
	}

	return Diesel_SK::deactivateObject();
}
// ------------------------------------------------------------------------------------------
void Diesel::setOut(uniset::ObjectId sid, bool state)
{
	try
	{
		setValue(sid, (state ? 1 : 0));
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(setOut): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Diesel::setTimer(Diesel::Timers tid, int val)
{
	askTimer(tid, val, 1);
}
// ------------------------------------------------------------------------------------------
bool Diesel::isOn() const
{
	return in_Working_s;

	//	return ( out_state_as == mON );
#if 0

	// если переходный процесс
	// то, значит не включены
	if( mode->isTransientMode() )
		throw uniset::TimeOut( myname + "(isOnMode): неопределённое состояние дизеля");

	return in_Working_s;
#endif
}
// ------------------------------------------------------------------------------------------
bool Diesel::isHot() const
{
	// если прогрев не требуется
	if( WarmingUpRequired_s != uniset::DefaultObjectId && !in_WarmingUpRequired_s )
		return true;

	return in_Working_s && ptStarted.checkTime();
}
// ------------------------------------------------------------------------------------------
long Diesel::getEstablishedRPM() const
{
	if( !ptRPMestablishDelay.checkTime() )
		return -1;

	return in_RPMestablished_s ? out_SetRPM_as : -1 ;
}
// ------------------------------------------------------------------------------------------
void Diesel::setRPM( const long& rpm, const uniset::timeout_t& msec )
{
	targetRPM = rpm;
	ptRPMupdate.setTiming(msec);
	
	if( out_SetRPM_as != targetRPM )
	{
		controlRPM = true;
		ptRPMestablishDelay.reset();
	}
}
// ------------------------------------------------------------------------------------------
void Diesel::start()
{
	DieselMessage gm(getId(), DieselMessage::Start);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Diesel::stop()
{
	DieselMessage gm(getId(), DieselMessage::Stop);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Diesel::reset()
{
	DieselMessage gm(getId(), DieselMessage::Reset);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Diesel::setBlackoutStartMode( bool set )
{
	out_BlackoutStartMode_c = set;
}
// ------------------------------------------------------------------------------------------
void Diesel::offControl()
{
	DieselMessage gm(getId(), DieselMessage::OffControl);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Diesel::onControl()
{
	DieselMessage gm(getId(), DieselMessage::OnControl);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Diesel::resetCommand()
{
	DieselMessage gm(getId(), DieselMessage::ResetCommand);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
uniset::timeout_t Diesel::getStationTime()
{
	return ptStopped.getCurrent();
}
// ------------------------------------------------------------------------------------------
void Diesel::setProtection( bool st )
{
	if( protection != st )
	{
		myinfo << this << "(setProtection): protection=" << st << endl;
	}

	protection = st;
}
// ------------------------------------------------------------------------------------------
ostream& operator<<( ostream& os, const DieselMessage::CtlMode& m )
{
	if( m == DieselMessage::OnMode )
		return os << "OnMode";

	if( m == DieselMessage::OffMode )
		return os << "OffMode";

	return os;
}
// ------------------------------------------------------------------------------------------
