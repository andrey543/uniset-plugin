//
// Created by andrey on 21.01.19.
//

#include <TEMPLATE_PROJECTConfiguration.h>
#include "Pump.h"

#define CMD_START 1
#define CMD_STOP 0

Pump::Pump(uniset::ObjectId id, xmlNode *cnode, const std::string &prefix):
Pump_SK(id, cnode, prefix),
synchronized(false)
{
    auto conf = uniset::uniset_conf();

    bool debug_logs = ( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 );

    if( debug_logs )
        mylog->level( Debug::value(default_loglevel) );

    std::cout << "Created " << getName() << std::endl;
}
Pump::~Pump() {
    print("Destructed");
}
void Pump::step() {


}
void Pump::sensorInfo(const uniset::SensorMessage *sm) {

    // Изменение состояния
    if(sm->id == is_work_s)
    {
        // Стартовая синхронизация
        if(!synchronized)
        {
            is_on = (bool) in_is_work_s;
            synchronized = true;
            myinfo << PRELOG << "Стартовая СИНХРОНИЗАЦИЯ:[on=" << is_on <<"]"  << std::endl;

            return;
        }
        else
            checkFault();
    }
    // В режиме Дист.управления мы можем:
    if(in_remote_control_s) {

        // Сбрасывать неисправность
        if (sm->id == reset_alarm_fs && sm->value) {
            myinfo << PRELOG << "Сброс АВАРИЙ"  << std::endl;
            is_on = (bool) in_is_work_s;
            out_fault_fc = false;
            return;
        }
        // Запускать с GUI-панели
        if (sm->id == gui_start_s && sm->value) {
            myinfo << PRELOG << "Пришла команда с GUI: ПУСК насоса"  << std::endl;
            if (!out_starting_c) {
                transitionProcess(tmStart, out_start_c, out_stop_c, CMD_START);
                command(tmStart, tmStop, out_starting_c, out_stopping_c);
            }
        }
        // Останавливать с GUI-панели
        else if (sm->id == gui_stop_s && sm->value) {
            myinfo << PRELOG << "Пришла команда с GUI: СТОП насоса"  << std::endl;
            if (!out_stopping_c) {
                transitionProcess(tmStop, out_stop_c, out_start_c, CMD_STOP);
                command(tmStop, tmStart, out_stopping_c, out_starting_c);
            }
        }
    }

}
void Pump::command(const Timers on_command, const Timers off_command, long& on_process, long& off_process) {

    // Останавливаем предыдущий переходный процесс
    off_process = CMD_STOP;
    askTimer(off_command,CMD_STOP);

    // Запуск переходного процесса
    on_process = CMD_START;
    askTimer(on_command,REPEAT_TIME);
    askTimer(tmFault,FAULT_TIME);
}
void Pump::transitionProcess(Pump::Timers command, long &on, long &off, int done) {
    std::string command_name = "Неизвестная команда";
    if(&on == &out_start_c)
        command_name = "ПУСК";
    else if(&on == &out_stop_c)
        command_name = "СТОП";

    myinfo  << PRELOG << "Отработка команды: " << command_name << " насоса." << std::endl;

    off = CMD_STOP;
    if(in_is_work_s == done)
    {
        myinfo  << PRELOG << "Отработка команды: " << command_name << " насоса завершена УСПЕШНО!" << std::endl;
        is_on = (bool)done;
        out_stopping_c = CMD_STOP;
        out_starting_c = CMD_STOP;
        askTimer(command, CMD_STOP);
        askTimer(tmFault, CMD_STOP);
        on = CMD_STOP;
        return;
    }
    on = !on;
}

void Pump::timerInfo(const uniset::TimerMessage *tm) {

    if(tm->id == tmStart)
    {
        transitionProcess(tmStart, out_start_c, out_stop_c, CMD_START);
    }
    else if(tm->id == tmStop)
    {
        transitionProcess(tmStop, out_stop_c, out_start_c, CMD_STOP);
    }
    else if(tm->id == tmFault)
    {
        transitionFault();
    }
}

void Pump::transitionFault() {
    askTimer(tmStop,CMD_STOP);
    askTimer(tmStart,CMD_STOP);
    askTimer(tmFault, CMD_STOP);
    out_start_c = CMD_STOP;
    out_stop_c  = CMD_STOP;
    out_starting_c = CMD_STOP;
    out_stopping_c = CMD_STOP;

    out_fault_fc = true;
    mywarn  << PRELOG << "Неисправность насоса: Превышение времени ожидания команды(вкл/выкл)" << std::endl;
}
void Pump::print(const std::string &message) {
    std::cout << "[" << getName() << "]: " << message << std::endl;
}
void Pump::checkFault() {
    // Если мы не управляем(Мест. упр) то не можем формировать неисправность.
    if(in_remote_control_s)
    {
        // Если же управляем и внутр.состояние(is_on) стало отличаться от реального состояния(in_is_work),
        // и при этом не включены переходные процессы - это неисправность
        if(in_is_work_s != is_on && !out_starting_c && !out_stopping_c)
        {
            out_fault_fc = true;
            mywarn  << PRELOG << "Неисправность насоса: Неконтролируемое изменение состояния" << std::endl;
        }
    } else {
        // Только отслеживаем состояние
        is_on = (bool) in_is_work_s;
        return;
    }
}

