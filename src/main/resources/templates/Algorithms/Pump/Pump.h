//
// Created by andrey on 21.01.19.
//

#ifndef TEMPLATE_PROJECT_Pump_H
#define TEMPLATE_PROJECT_Pump_H

#include "Pump_SK.h"

class Pump : public Pump_SK {
public:
    Pump(uniset::ObjectId id, xmlNode* cnode, const std::string& prefix = "");
    virtual ~Pump();
protected:
    enum Timers
    {
        tmStart,
        tmStop,
        tmFault
    };
    virtual void step() override;
    virtual void sensorInfo(const uniset::SensorMessage* sm) override;
    virtual void timerInfo( const uniset::TimerMessage* tm ) override;

private:
    bool is_on;
    bool synchronized;
private:
    void command(const Timers on_command, const Timers off_command, long& on_process, long& off_process);
    void transitionProcess(Timers command, long &on, long &off, int done);
    void transitionFault();
    void checkFault();
    void print(const std::string& message);
};


#endif //PROJECT_Pump_H
