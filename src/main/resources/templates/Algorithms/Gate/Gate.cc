//
// Created by andrey on 29.01.19.
//

#include <TEMPLATE_PROJECTConfiguration.h>
#include "Gate.h"

#define CMD_START 1
#define CMD_STOP 0

Gate::Gate(uniset::ObjectId id, xmlNode *cnode, const std::string &prefix):
Gate_SK(id, cnode, prefix),
synchronized(false)
{
    auto conf = uniset::uniset_conf();

    bool debug_logs = ( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 );

    if( debug_logs )
        mylog->level( Debug::value(default_loglevel) );

    std::cout << "Created " << getName() << std::endl;
}

Gate::~Gate() {
    std::cout << "Destructed " << getName() << std::endl;
}
void Gate::step() {


}
void Gate::sensorInfo(const uniset::SensorMessage *sm) {
    if(sm->id == is_open_s || sm->id == is_close_s)
    {
        // Стартовая синхронизация
        if(!synchronized)
        {
            is_open = (bool) in_is_open_s;
            is_close = (bool) in_is_close_s;
            synchronized = true;

            myinfo << PRELOG << "Стартовая СИНХРОНИЗАЦИЯ:[on=" << is_open << ";off=" << is_close <<"]"  << std::endl;
        } else
            checkFault();
    }

    if(sm->id == gui_open_fs && sm->value)
    {
        myinfo  << PRELOG << "Пришла команда с GUI: Открыть затвор." << std::endl;

        if(out_fault_fc)
        {
            myinfo  << PRELOG << "Открытие НЕВОЗМОЖНО - не сброшена неисправность" << std::endl;
            setMsg(mid_Protection_is_On, true);
            return;
        }

        open();

    }
    else if(sm->id == gui_close_fs && sm->value)
    {
        myinfo << PRELOG << "Пришла команда с GUI: Закрыть затвор." << std::endl;

        if(out_fault_fc)
        {
            myinfo  << PRELOG << "Закрытие НЕВОЗМОЖНО - не сброшена неисправность" << std::endl;
            setMsg(mid_Protection_is_On, true);
            return;
        }

        close();
    }

    if(sm->id == reset_alarm_fs && sm->value)
    {
        myinfo << PRELOG << "Сброс АВАРИЙ"  << std::endl;
        is_open = (bool)in_is_open_s;
        is_close = (bool)in_is_close_s;
        out_fault_fc = false;
    }
}
void Gate::command(const Timers on_command, const Timers off_command, long& on_process, long& off_process) {
    // Останавливаем предыдущий переходный процесс(если он есть)
    off_process = CMD_STOP;
    askTimer(off_command,CMD_STOP);

    // Запуск переходного процесса
    on_process = CMD_START;
    askTimer(on_command,REPEAT_TIME);
    askTimer(tmFault,FAULT_TIME);
}
void Gate::transitionProcess(Gate::Timers command, long &on, long &off, const long& done) {

    std::string command_name = "Неизвестная команда";
    if(&on == &out_open_c)
        command_name = "ОТКРЫТЬ";
    else if(&on == &out_close_c)
        command_name = "ЗАКРЫТЬ";

    myinfo  << PRELOG << "Отработка команды: " << command_name << " затвор." << std::endl;

    if(IS_IMPULSE_COMMAND)
        impulseExec(command, command_name, on, off, done);
    else
        constExec(command, command_name, on, off, done);
}

void Gate::timerInfo(const uniset::TimerMessage *tm) {

    if(tm->id == tmStart)
    {
        transitionProcess(tmStart, out_open_c, out_close_c, in_is_open_s);
    }
    else if(tm->id == tmStop)
    {
        transitionProcess(tmStop, out_close_c, out_open_c, in_is_close_s);
    }
    else if(tm->id == tmFault)
    {
        transitionFault();
    }
}

void Gate::transitionFault() {
    askTimer(tmStop,CMD_STOP);
    askTimer(tmStart,CMD_STOP);
    askTimer(tmFault, CMD_STOP);

    if(IS_IMPULSE_COMMAND) {
        out_open_c = CMD_STOP;
        out_close_c = CMD_STOP;
    } else {
        out_open_c = in_is_open_s;
        out_close_c = in_is_close_s;
    }
    out_opening_fc = CMD_STOP;
    out_closing_fc = CMD_STOP;

    out_fault_fc = true;
    mywarn  << PRELOG << "Неисправность затвора: Превышение времени отработки команды(вкл/выкл)" << std::endl;
}
void Gate::checkFault() {

    if((in_is_close_s != is_close || in_is_open_s != is_open) && !out_opening_fc && !out_closing_fc)
    {
        out_fault_fc = true;
        mywarn  << PRELOG << "Неисправность затвора: Неконтролируемое изменение состояния" << std::endl;
    }
    if(in_is_open_s == in_is_close_s && !out_opening_fc && !out_closing_fc)
    {
        out_fault_fc = true;
        mywarn  << PRELOG << "Неисправность затвора: Неопределенное состояние затвора" << std::endl;
    }
}

void Gate::impulseExec(Gate::Timers command, std::string command_name, long &on, long &off, const long& done) {

    off = CMD_STOP;
    if(done)
    {
        myinfo  << PRELOG << "Импульсная отработка команды: " << command_name << " затвор завершена УСПЕШНО!" << std::endl;
        is_open = (bool)in_is_open_s;
        is_close = (bool)in_is_close_s;
        out_closing_fc = CMD_STOP;
        out_opening_fc = CMD_STOP;
        askTimer(command, CMD_STOP);
        askTimer(tmFault, CMD_STOP);
        on = CMD_STOP;
        return;
    }
    // Импульсное задание команды
    on = !on;

}

void Gate::constExec(Gate::Timers command, std::string command_name, long &on, long &off, const long &done) {

    off = CMD_STOP;
    if(done)
    {
        myinfo  << PRELOG << "Статическая отработка команды: " << command_name << " затвор завершена УСПЕШНО!" << std::endl;
        is_open = (bool)in_is_open_s;
        is_close = (bool)in_is_close_s;
        out_closing_fc = CMD_STOP;
        out_opening_fc = CMD_STOP;
        askTimer(command, CMD_STOP);
        askTimer(tmFault, CMD_STOP);
        return;
    }
    on = CMD_START;
}

void Gate::open() {
    if(!out_opening_fc){
        command(tmStart, tmStop, out_opening_fc, out_closing_fc);
        transitionProcess(tmStart, out_open_c, out_close_c, in_is_open_s);
    }

}

void Gate::close() {
    if(!out_closing_fc){
        command(tmStop, tmStart, out_closing_fc, out_opening_fc);
        transitionProcess(tmStop, out_close_c, out_open_c, in_is_close_s);
    }

}
