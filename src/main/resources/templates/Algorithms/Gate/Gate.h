//
// Created by andrey on 29.01.19.
//

#ifndef TEMPLATE_PROJECT_Gate_H
#define TEMPLATE_PROJECT_Gate_H

#include "Gate_SK.h"

class Gate : public Gate_SK {
public:
    Gate(uniset::ObjectId id, xmlNode* cnode, const std::string& prefix = "");
    virtual ~Gate();
    void open();
    void close();
protected:
    enum Timers
    {
        tmStart,
        tmStop,
        tmFault
    };
    virtual void step() override;
    virtual void sensorInfo(const uniset::SensorMessage* sm) override;
    virtual void timerInfo( const uniset::TimerMessage* tm ) override;

private:
    bool is_open;
    bool is_close;
    bool synchronized;
private:
    void command(const Timers on_command, const Timers off_command, long& on_process, long& off_process);
    void transitionProcess(Timers command, long &on, long &off, const long& done);
    void transitionFault();
    void checkFault();
    void impulseExec(Gate::Timers command, std::string command_name, long &on, long &off, const long& done);
    void constExec(Gate::Timers command, std::string command_name, long &on, long &off, const long& done);
};


#endif //PROJECT_Gate_H
