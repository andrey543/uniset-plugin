#include "GeneratorMode.h"
#include "Generator.h"
// ----------------------------------------------------------------------------
using namespace uniset;
using namespace std;
using namespace UniSetAlgorithms;
// -----------------------------------------------------------------------------
GeneratorOffControlMode* GeneratorOffControlMode::inst_ = 0;
// -----------------------------------------------------------------------------
GeneratorOffControlMode::~GeneratorOffControlMode()
{
}

GeneratorOffControlMode::GeneratorOffControlMode()
{
}

// -----------------------------------------------------------------------------

GeneratorOffControlMode* GeneratorOffControlMode::inst()
{
	if(inst_ == 0)
		inst_ =  new GeneratorOffControlMode();

	return inst_;
}

// -----------------------------------------------------------------------------

void GeneratorOffControlMode::sensorInfo( Generator* gen, const SensorMessage* sm )
{
}

// -----------------------------------------------------------------------------
void GeneratorOffControlMode::step( Generator* gen )
{
	if( gen->DiffProtectionIsOn_s != DefaultObjectId && !gen->in_DiffProtectionIsOn_s )
	{
		gen->out_state_as = mPROTECTION;
	}
	else if( gen->DiffProtection_s != DefaultObjectId && gen->in_DiffProtection_s )
	{
		gen->out_state_as = mPROTECTION;
	}
	else if( gen->in_Field_s  )
		gen->out_state_as = mON;
	else
		gen->out_state_as = mOFF;
}
// ---------------------------------------------------------------------------

bool GeneratorOffControlMode::activate(Generator* gen)
{
	if( gen->log()->is_info() )
		gen->log()->info() << gen << ": OFF CONTROL activate" << endl;

	gen->out->reset();
	gen->tm->reset();
	return true;
}

// -----------------------------------------------------------------------------
void GeneratorOffControlMode::genCommand( Generator* gen, const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::OnControl:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ": ON CONTROL" << endl;

			changeMode( gen, GeneratorInitMode::inst() );
		}
		break;

		default:
			GeneratorMode::genCommand( gen, m );
			break;
	}
}
// -----------------------------------------------------------------------------
