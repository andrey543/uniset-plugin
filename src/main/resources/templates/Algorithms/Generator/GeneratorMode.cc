// -----------------------------------------------------------------------------
#include "GeneratorMode.h"
#include "Generator.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
GeneratorMode* GeneratorMode::inst_ = 0;
// ---------------------------------------------------------------------------
GeneratorMode::~GeneratorMode()
{
}

GeneratorMode::GeneratorMode()
{
}

// ---------------------------------------------------------------------------

GeneratorMode* GeneratorMode::inst()
{
	if(inst_ == 0)
		inst_ =  new GeneratorMode();

	return inst_;
}

// ---------------------------------------------------------------------------
void GeneratorMode::sensorInfo(Generator* gen , const uniset::SensorMessage* sm)
{
	if( sm->id == gen->DiffProtection_s )
	{
		if( sm->value )
		{
			if( gen->log()->is_warn() )
				gen->log()->warn() << gen << ": DiffProtecrion!" << endl;

			changeMode( gen, GeneratorProtectionMode::inst() );
		}
	}
	else if( sm->id == gen->DiffProtectionIsOn_s )
	{
		if( !sm->value )
		{
			if( gen->log()->is_warn() )
				gen->log()->warn() << gen << ": DiffProtecrion is OFF!" << endl;

			changeMode( gen, GeneratorProtectionMode::inst() );
		}
	}
}
// ---------------------------------------------------------------------------
void GeneratorMode::changeMode( Generator* gen, GeneratorMode* m )
{
	gen->changeMode(m);
}
// ---------------------------------------------------------------------------

void GeneratorMode::genCommand(Generator* gen , const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::OffControl:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ": OFF CONTROL" << endl;

			changeMode( gen, GeneratorOffControlMode::inst() );
		}
		break;

		case GeneratorMessage::ResetCommand:
			break;

		default:
			break;
	}
}
// ------------------------------------------------------------------------------------------
