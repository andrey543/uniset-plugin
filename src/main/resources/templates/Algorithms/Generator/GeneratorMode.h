// -----------------------------------------------------------------------------
#ifndef GeneratorMode_H_
#define GeneratorMode_H_
// ------------------------------------------------------------------------------------------
#include <string>
#include <sstream>
#include <UniSetObject.h>
#include <UniSetManager.h>
#include <ObjectMode.h>
#include "GeneratorMessage.h"
// ------------------------------------------------------------------------------------------
class Generator;

/*!
	Прототип реализации режимов работы.
	\sa \ref GeneratorPage
*/
class GeneratorMode
{
	public:
		static GeneratorMode* inst();
		virtual ~GeneratorMode();

		virtual void timerInfo( Generator* gen, const uniset::TimerMessage* tm ) {}
		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );

		virtual bool activate(Generator* gen)
		{
			return true;
		}
		virtual bool disactivate(Generator* gen)
		{
			return true;
		}

		virtual const std::string modeName()
		{
			return "???Mode";
		};

		virtual bool isTransientMode()
		{
			return false;
		}

	protected:
		GeneratorMode();
		void changeMode( Generator* gen, GeneratorMode* m );

	private:
		static GeneratorMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page GeneratorPage
	\section secGenerator_OffMode Режим "Генератор выключен"
	Это состояние является устойчивым. Контроль параметров отсутствует.
*/

/*!
	Режим работы в выключенном состоянии
	\sa \ref GeneratorPage
*/
class GeneratorOffMode:
	public GeneratorMode
{
	public:
		static GeneratorOffMode* inst();
		virtual ~GeneratorOffMode();

		// --------
		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );
		virtual bool activate(Generator* gen);

		virtual const std::string modeName()
		{
			return "GeneratorOffMode";
		}

	protected:
		GeneratorOffMode();

	private:
		static GeneratorOffMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page GeneratorPage
	\section secGenerator_OnMode Режим "Генератор включен"
	В это устойчивое состояние переход осуществляется после успешного включения.
	В данном состоянии происходит постоянный контроль следующих параметров:
	- состояние б/к FUG
	- уровень напряжения ( выше/ниже нормы )

	В случае выхода параметров за пределы, формируется соответствующее сообщение.
*/

/*!
	Режим "Контроль параметров" (включённое состояние)
	\sa \ref GeneratorPage
*/
class GeneratorOnMode:
	public GeneratorMode
{
	public:
		static GeneratorOnMode* inst();
		virtual ~GeneratorOnMode();

		// --------

		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );

		virtual bool activate(Generator* gen);

		virtual const std::string modeName()
		{
			return "GeneratorOnMode";
		}

	protected:
		GeneratorOnMode();

	private:
		static GeneratorOnMode* inst_;
};

// ---------------------------------------------------------------------------
/*!
	\page GeneratorPage
	\section secGenerator_ProtectionMode Режим "срабатывание защиты генератора"
	Это состояние является устойчивым. Переход в него возможен при срабатывании
	какой-либо защиты или неудачной попытке перехода из одного устойчивого
	состояния в другое. При активизации происходит сброс в ноль всех выходных сигналов.
	Попытка выхода из него происходит только при подаче команды "сброс защиты".
*/

/*!
	Режим "Сработала защита" или "неудачная попытка перехода в другой режим"
	\sa \ref GeneratorPage
*/
class GeneratorProtectionMode:
	public GeneratorMode
{
	public:
		static GeneratorProtectionMode* inst();
		virtual ~GeneratorProtectionMode();

		// --------
		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );
		virtual bool activate(Generator* gen);
		virtual bool disactivate(Generator* gen);
		virtual const std::string modeName()
		{
			return "GeneratorProtectionMode";
		}

	protected:
		GeneratorProtectionMode();
		bool checkProtection(Generator* gen);

	private:
		static GeneratorProtectionMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page GeneratorPage
	\section secGenerator_InitMode Режим инициализации состояния генератора

	Это состояние является переходным (не устойчивым). В нем происходит идентификация текущего
	фактичкеского состояния органов управления и переход в соответсвующее им состояние.

	Инициализация фактического состояния происходит как представлено на рисунке.
	\image html generator_init.png
*/
/*!
	Режим "инициализации" или "повторной попытки"
	\sa \ref GeneratorPage
*/
class GeneratorInitMode:
	public GeneratorMode
{
	public:
		static GeneratorInitMode* inst();
		virtual ~GeneratorInitMode();

		// --------
		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );
		virtual bool activate(Generator* gen);
		virtual const std::string modeName()
		{
			return "GeneratorInitMode";
		}

	protected:
		GeneratorInitMode();

	private:
		static GeneratorInitMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page GeneratorPage
	\section secGenerator_WaitMode Состояние генератора "ожидание исполнения команды".

	Это состояние является переходным (не устойчивым) и объеденяет в себе
	все возможные переходные состояния при переходе из одного устойчивого в другое.
	В основном здесь происходит подача команды и ожидание её выполнения по таймеру.
	В случае невыполнения команды за заданное время (и количество попыток) происходит переход
	в состояние указанное в качестве 'back', в случае успеха в состояние указанное как 'to'.
	(см. GeneratorTransientMode ).
*/
/*!
	Режим работы, ожидание исполнения команды
	\sa \ref GeneratorPage
*/
class GeneratorTransientMode:
	public GeneratorMode
{
	private:
		/*! Функция инстанцирования для внутреннего использования (с сохранением back и atempt ).
			\param gen - указатель на управляемый объект
			\param to - целевое состояние
		*/
		static GeneratorTransientMode* inst( Generator* gen, GeneratorMessage::GeneratorMode to);
	public:
		/*! Функция инстанцирования.
			\param gen - указатель на управляемый объект
			\param to - целевое состояние
			\param back - состояние в которое переходить в случае неудачной попытки
			\param atempt - количество попыток
		*/
		static GeneratorTransientMode* inst( Generator* gen,
												GeneratorMessage::GeneratorMode to,
												GeneratorMode* back, int atempt = 0 );

		virtual ~GeneratorTransientMode();

		// --------
		virtual bool isTransientMode()
		{
			return true;
		}
		virtual void timerInfo( Generator* gen, const uniset::TimerMessage* tm );
		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );

		virtual bool activate(Generator* gen);
		virtual bool disactivate(Generator* gen);
		virtual const std::string modeName()
		{
			return "GeneratorTransientMode";
		}


	protected:
		GeneratorTransientMode();
		void rollback( Generator* gen );

	private:
		static GeneratorTransientMode* inst_;
};
// ---------------------------------------------------------------------------
/*!
	\page GeneratorPage
	\section secGenerator_sleepMode Режим работы "отключенное управление"
	В этом режиме полностью отключается все управление и контроль. Выход
	из него возможен только при переключении поста управления в состояние
	"Дистанционное".
*/

/*!
	Режим работы, когда управление происходит с местного поста
	\sa \ref GeneratorPage
*/
class GeneratorOffControlMode:
	public GeneratorMode
{
	public:
		static GeneratorOffControlMode* inst();
		virtual ~GeneratorOffControlMode();

		// --------

		virtual void sensorInfo( Generator* gen, const uniset::SensorMessage* sm );
		virtual void genCommand( Generator* gen, const GeneratorMessage* m );
		virtual void step( Generator* gen );
		virtual bool activate(Generator* gen);
		virtual const std::string modeName()
		{
			return "GeneratorOffControlMode";
		}

	protected:
		GeneratorOffControlMode();

	private:
		static GeneratorOffControlMode* inst_;
};
// ---------------------------------------------------------------------------

#endif // GeneratorMode_H_
