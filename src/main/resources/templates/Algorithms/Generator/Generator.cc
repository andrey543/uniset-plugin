// -----------------------------------------------------------------------------
#include <Exceptions.h>
#include <UniSetManager.h>
#include <TEMPLATE_PROJECTConfiguration.h>
#include "Generator.h"
#include "GeneratorMode.h"
// ------------------------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace UniSetAlgorithms;
// ------------------------------------------------------------------------------------------
std::string std::to_string( const Generator::Timers& t )
{
	switch(t)
	{
		case Generator::FieldOffTimer:
			return "FieldOffTimer";

		case Generator::FieldOnTimer:
			return "FieldOnTimer";

		case Generator::UincreaseTimer:
			return "UincreaseTimer";

		default:
			break;
	};

	return "Unknown";
}
// ------------------------------------------------------------------------------------------
Generator::Generator( uniset::ObjectId id, xmlNode* confnode ):
	Generator_SK(id, confnode),
	mode(0),
	attemptCounter(0),
	attempt(0),
	toMode(GeneratorMessage::OffMode),
	prevMode(GeneratorMessage::OffMode)
{
	auto conf = uniset_conf();

	if( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 )
		mylog->level( Debug::value(default_loglevel) );

	tm = make_shared<uniset::TriggerOUT<Generator, Generator::Timers, int>>(this, &Generator::setTimer);
	tm->add(UincreaseTimer, 0);
	tm->add(FieldOnTimer, 0);
	tm->add(FieldOffTimer, 0);

	out = make_shared<uniset::TriggerOUT<Generator, ObjectId, bool>>(this, &Generator::setOut);
	out->add(FieldOn_c, false);
	out->add(FieldOff_c, false);

	out_state_as = mUNKNOWN;
	mode = GeneratorOffControlMode::inst();
}
// ------------------------------------------------------------------------------------------
Generator::~Generator()
{
}
// ------------------------------------------------------------------------------------------
Generator::Generator():
	Generator_SK(0)
{
	mycrit << myname << ": init failed!!!!!!!!!!!!!!!" << endl;
	throw Exception( string(myname + ": init failed!!!") );
}
// ------------------------------------------------------------------------------------------
void Generator::step()
{
	int state = mOFF;

	if( in_Field_s )
		state = mON;

	if( protection )
		state = mPROTECTION;

	if( mode->isTransientMode() )
		state = mTRANSITIVE;

	out_state_as = state;
}
// ------------------------------------------------------------------------------------------
void Generator::processingMessage( const uniset::VoidMessage* msg )
{
	try
	{
		switch ( msg->type )
		{
			case GeneratorMessage::GeneratorInfo:
			{
				genCommand(reinterpret_cast<const GeneratorMessage*>(msg));
				break;
			}

			default:
				Generator_SK::processingMessage(msg);
				break;
		}
	}
	catch( std::exception& ex)
	{
		mycrit << myname << "(processingMessage): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Generator::sensorInfo( const SensorMessage* sm )
{
	if( sm->id == RemoteControl_s )
	{
		if( sm->value)
			onControl();
		else
			offControl();

		return;
	}

	try
	{
		mode->sensorInfo(this, sm);
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(sensorInfo): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Generator::timerInfo( const TimerMessage* tm )
{
	try
	{
		mode->timerInfo(this, tm);
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(timerInfo): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
void Generator::sysCommand( const SystemMessage* sm )
{
	Generator_SK::sysCommand(sm);

	switch(sm->command)
	{
		case SystemMessage::StartUp:
		{
			mode->activate(this);
			break;
		}

		default:
			break;
	}
}

// ------------------------------------------------------------------------------------------
void Generator::changeMode( GeneratorMode* m )
{
	GeneratorMode* old = mode;
	mode = m;

	try
	{
		if( !mode->isTransientMode() )
			prevMode = toMode;

		old->disactivate(this);
		mode->activate(this);
	}
	catch(Exception& ex)
	{
		mycrit << myname << "(changeMode): не удалось сменить режим!!! " << ex << endl;
		mode = old;
		mode->activate(this);
	}
}
// ------------------------------------------------------------------------------------------
string Generator::getTimerName(int id) const
{
	return to_string( (Timers)id );
}
// ------------------------------------------------------------------------------------------
string Generator::getMonitInfo() const
{
	ostringstream s;

	s << "mode: " << ( mode ? mode->modeName() : " UNKNOWN" )
	  << " protection=" << protection
	  << endl;

	return std::move(s.str());
}
// ------------------------------------------------------------------------------------------
void Generator::genCommand( const GeneratorMessage* m )
{
	try
	{
		mode->genCommand(this, m);
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(genInfo): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& os, Generator& g )
{
	if( g.mode )
		return os << g.myname << "(" << g.mode->modeName() << ")";

	return os << g.myname << "(UNKNOWN mode)";
}

std::ostream& operator<<(std::ostream& os, Generator* g)
{
	return os << (*g);
}

// ------------------------------------------------------------------------------------------
void Generator::setTimer(Generator::Timers tid, int val)
{
	askTimer(tid, val, 1);
}
// ------------------------------------------------------------------------------------------
bool Generator::isOn() const
{
	//return (out_state_as == mON);
	return in_Field_s;
}
// ------------------------------------------------------------------------------------------
void Generator::setProtection( bool st )
{
	protection = st;
}
// ------------------------------------------------------------------------------------------
void Generator::on()
{
	GeneratorMessage gm(GeneratorMessage::On);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Generator::off()
{
	GeneratorMessage gm(GeneratorMessage::Off);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Generator::reset()
{
	GeneratorMessage gm(GeneratorMessage::Reset);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Generator::offControl()
{
	GeneratorMessage gm(GeneratorMessage::OffControl);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Generator::onControl()
{
	GeneratorMessage gm(GeneratorMessage::OnControl);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Generator::resetCommand()
{
	GeneratorMessage gm(GeneratorMessage::ResetCommand);
	push(gm.transport_msg());
}
// ------------------------------------------------------------------------------------------
void Generator::setOut(uniset::ObjectId sid, bool state)
{
	try
	{
		if( sid != uniset::DefaultObjectId )
			setValue(sid, (state ? 1 : 0) );
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(setOut): " << ex.what() << endl;
	}
}
// ------------------------------------------------------------------------------------------
ostream& operator<<( ostream& os, const GeneratorMessage::GeneratorMode& m )
{
	if( m == GeneratorMessage::OnMode )
		return os << "OnMode";

	if( m == GeneratorMessage::OffMode )
		return os << "OffMode";

	if( m == GeneratorMessage::WaitUincreaseMode )
		return os << "WaitUincreaseMode";

	if( m == GeneratorMessage::FieldOffWaitMode )
		return os << "FieldOffWaitMode";

	if( m == GeneratorMessage::FieldOnWaitMode )
		return os << "FieldOnWaitMode";

	return os;
}
// ------------------------------------------------------------------------------------------

