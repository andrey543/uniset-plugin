// -----------------------------------------------------------------------------
#include "Generator.h"
#include "GeneratorMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
GeneratorOnMode* GeneratorOnMode::inst_ = 0;
// ---------------------------------------------------------------------------


GeneratorOnMode::~GeneratorOnMode()
{
}

GeneratorOnMode::GeneratorOnMode()
{
}

// ---------------------------------------------------------------------------

GeneratorOnMode* GeneratorOnMode::inst()
{
	if(inst_ == 0)
		inst_ =  new GeneratorOnMode();

	return inst_;
}

// ---------------------------------------------------------------------------

bool GeneratorOnMode::activate(Generator* gen)
{
	if( gen->log()->is_info() )
		gen->log()->info() << gen << ": ..............activate.............." << endl;

	gen->out->reset();
	return true;
}
// ------------------------------------------------------------------------------------------
void GeneratorOnMode::sensorInfo( Generator* gen, const uniset::SensorMessage* sm )
{
	if( sm->id == gen->U_Norm_s )
	{
		if( !sm->value )
		{
			if( gen->log()->is_warn() )
				gen->log()->warn()   << gen << ": U != normal.." << endl;

			// возможно это отключение по аварийному отключению дизеля
			// поэтому прежде чем перейти в OffMode делаем паузу (если задана)
			if( gen->offPause )
				msleep(gen->offPause);

			changeMode( gen, GeneratorOffMode::inst() );
		}
	}
	else if( sm->id == gen->Field_s )
	{
		if( !sm->value )
		{
			if( gen->log()->is_warn() )
				gen->log()->warn() << gen << ": Change Mode?!" << endl;

			// возможно это отключение по аварийному отключению дизеля
			// поэтому прежде чем перейти в OffMode делаем паузу (если задана)
			if( gen->offPause > 0 )
				msleep(gen->offPause);

			changeMode( gen, GeneratorInitMode::inst() );
		}
	}
	else
		GeneratorMode::sensorInfo(gen, sm);
}
// ------------------------------------------------------------------------------------------
void GeneratorOnMode::genCommand( Generator* gen, const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::On:
			activate(gen);
			break;

		case GeneratorMessage::Off:
			changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::OffMode, GeneratorInitMode::inst(), gen->numberOfAttempts));
			break;

		default:
			GeneratorMode::genCommand(gen, m);
			break;

	}
}
// ------------------------------------------------------------------------------------------
