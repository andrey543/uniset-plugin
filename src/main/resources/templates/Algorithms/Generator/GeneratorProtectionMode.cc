#include "Generator.h"
#include "GeneratorMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
GeneratorProtectionMode* GeneratorProtectionMode::inst_ = 0;
// ---------------------------------------------------------------------------
GeneratorProtectionMode::~GeneratorProtectionMode()
{
}

GeneratorProtectionMode::GeneratorProtectionMode()
{
}

// ---------------------------------------------------------------------------

GeneratorProtectionMode* GeneratorProtectionMode::inst()
{
	if(inst_ == 0)
		inst_ =  new GeneratorProtectionMode();

	return inst_;
}

// ---------------------------------------------------------------------------

void GeneratorProtectionMode::sensorInfo( Generator* gen, const SensorMessage* sm )
{
	//	if( sm->id == gen->Field_s )
	//		gen->agpInfo->commit(sm->id,sm->state);
}

// ---------------------------------------------------------------------------

bool GeneratorProtectionMode::activate(Generator* gen)
{
	if( gen->log()->is_info() )
		gen->log()->info() << gen << ": ..............activate.............." << endl;

	gen->setProtection(true);
	// отключаем все выходы
	gen->out->reset();
	return true;
}

// ---------------------------------------------------------------------------
bool GeneratorProtectionMode::disactivate(Generator* gen)
{
	gen->setProtection(false);
	return true;
}
// ------------------------------------------------------------------------------------------

void GeneratorProtectionMode::genCommand( Generator* gen, const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::Reset:
			if(!checkProtection(gen))
				changeMode( gen, GeneratorInitMode::inst());
			else
				activate(gen);

			break;

		case GeneratorMessage::On:
		case GeneratorMessage::Off:
			activate(gen);
			break;

		default:
			GeneratorMode::genCommand(gen, m);
			break;
	}
}
// ---------------------------------------------------------------------------
bool GeneratorProtectionMode::checkProtection(Generator* gen)
{
	if( gen->DiffProtectionIsOn_s != DefaultObjectId && !gen->in_DiffProtectionIsOn_s )
	{
		if( gen->log()->is_info() )
			gen->log()->info() << gen << ": DiffProtection IS OFF.." << endl;

		return true;
	}

	if( gen->DiffProtection_s != DefaultObjectId && gen->in_DiffProtection_s )
	{
		if( gen->log()->is_info() )
			gen->log()->info() << gen << ": DiffProtection.." << endl;

		return true;
	}

	return false;
}
// ---------------------------------------------------------------------------
