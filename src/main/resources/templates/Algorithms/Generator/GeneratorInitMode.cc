#include "GeneratorMode.h"
#include "Generator.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
GeneratorInitMode* GeneratorInitMode::inst_ = 0;
// ---------------------------------------------------------------------------
GeneratorInitMode::~GeneratorInitMode()
{
}

GeneratorInitMode::GeneratorInitMode()
{
}

// ---------------------------------------------------------------------------

GeneratorInitMode* GeneratorInitMode::inst()
{
	if(inst_ == 0)
		inst_ =  new GeneratorInitMode();

	return inst_;
}

// ---------------------------------------------------------------------------

void GeneratorInitMode::sensorInfo( Generator* gen, const SensorMessage* sm )
{
	if( sm->id == gen->U_Norm_s )
	{
		if( sm->value )
			changeMode( gen, GeneratorOnMode::inst() );
		else
			changeMode( gen, GeneratorOffMode::inst() );
	}
}

// ---------------------------------------------------------------------------

bool GeneratorInitMode::activate(Generator* gen)
{
	if( gen->log()->is_info() )
		gen->log()->info() << gen << ": ...activate..." << endl;

	if( gen->DiffProtectionIsOn_s != DefaultObjectId && !gen->in_DiffProtectionIsOn_s )
	{
		if( gen->log()->is_info() )
			gen->log()->info() << gen << ": DiffProtection IS OFF.." << endl;

		changeMode( gen, GeneratorProtectionMode::inst() );
		return true;
	}

	if( gen->DiffProtection_s != DefaultObjectId && gen->in_DiffProtection_s )
	{
		if( gen->log()->is_info() )
			gen->log()->info() << gen << ": DiffProtection.." << endl;

		changeMode( gen, GeneratorProtectionMode::inst() );
		return true;
	}

	if( gen->in_Field_s && gen->in_U_Norm_s )
	{
		if( gen->log()->is_info() )
			gen->log()->info() << gen << "(activate): init: OnMode.." << endl;

		changeMode( gen, GeneratorOnMode::inst() );
		return true;
	}

	if( gen->log()->is_info() )
		gen->log()->info() << gen << "(activate): init: OffMode.." << endl;

	changeMode( gen, GeneratorOffMode::inst() );
	return true;
}

// ---------------------------------------------------------------------------
void GeneratorInitMode::genCommand( Generator* gen, const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::On:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ":  ON..." << endl;

			changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::OnMode, this, gen->numberOfAttempts) );
		}
		break;

		case GeneratorMessage::Off:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ":  OFF..." << endl;

			changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::OffMode, this, gen->numberOfAttempts) );
		}
		break;

		default:
			break;
	}
}
// ---------------------------------------------------------------------------
