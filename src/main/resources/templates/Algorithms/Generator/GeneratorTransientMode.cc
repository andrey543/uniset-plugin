// -----------------------------------------------------------------------------
#include "Generator.h"
#include "GeneratorMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
GeneratorTransientMode* GeneratorTransientMode::inst_ = 0;
// ---------------------------------------------------------------------------

GeneratorTransientMode::~GeneratorTransientMode()
{
}

GeneratorTransientMode::GeneratorTransientMode()
{
}

// ---------------------------------------------------------------------------

GeneratorTransientMode* GeneratorTransientMode::inst( Generator* gen, GeneratorMessage::GeneratorMode to)
{
	if(inst_ == 0)
	{
		ostringstream err;
		err << gen << "(inst): private instance should be called from already exited GeneratorTransientMode instance";
		if( gen->log()->is_crit() )
			gen->log()->crit() << gen << err.str()<< endl;
		throw SystemError(err.str());
	}

	gen->prevMode = gen->toMode;
	gen->toMode = to;
	return inst_;
}

// ---------------------------------------------------------------------------

GeneratorTransientMode* GeneratorTransientMode::inst( Generator* gen,
		GeneratorMessage::GeneratorMode to,
		GeneratorMode* back, int attempt)
{
	if(!back)
		back = GeneratorProtectionMode::inst();

	if(inst_ == 0)
		inst_ =  new GeneratorTransientMode();

	gen->prevMode = gen->toMode;
	gen->toMode = to;
	gen->backMode = back;
	gen->attemptCounter = attempt;
	gen->attempt = 0;
	return inst_;
}

// ---------------------------------------------------------------------------
bool GeneratorTransientMode::activate(Generator* gen)
{
	if( gen->log()->is_info() )
		gen->log()->info() << gen << ": [ to ==> " << gen->toMode << " ]" << endl;

	switch( gen->toMode )
	{
		case GeneratorMessage::OffMode:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ": generator off..." << endl;

			changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::FieldOffWaitMode));
		}
		break;

		case GeneratorMessage::OnMode:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ": generator on.." << endl;

			// просто ждём роста напряжения
			changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::FieldOnWaitMode) );
		}
		break;

		case GeneratorMessage::WaitUincreaseMode:
		{
			gen->tm->set(Generator::UincreaseTimer, gen->UincreaseTime);
			gen->askSensor( gen->U_Norm_s, UniversalIO::UIONotify );

			if( gen->log()->is_info() )
				gen->log()->info() << gen << ":  waiting U normal " << gen->UincreaseTime << " msec." << endl;
		}
		break;

		case GeneratorMessage::FieldOffWaitMode:
		{
			gen->tm->set(Generator::FieldOffTimer, 0);
			gen->out->set(gen->FieldOff_c, true);

			if( gen->log()->is_info() )
				gen->log()->info() << gen << ":  waiting field off " << gen->FieldOffTime << " msec." << endl;

			gen->tm->set(Generator::FieldOffTimer, gen->FieldOffTime);
			gen->askSensor(gen->Field_s, UniversalIO::UIONotify);
		}
		break;

		case GeneratorMessage::FieldOnWaitMode:
		{
			gen->tm->set(Generator::FieldOnTimer, 0);
			gen->out->set(gen->FieldOn_c, true);

			if( gen->log()->is_info() )
				gen->log()->info() << gen << ":  waiting field on " << gen->FieldOnTime << " msec." << endl;

			gen->tm->set(Generator::FieldOnTimer, gen->FieldOnTime);
			gen->askSensor(gen->Field_s, UniversalIO::UIONotify);
		}
		break;

		default:
			break;
	}

	return true;
}
// ---------------------------------------------------------------------------
bool GeneratorTransientMode::disactivate(Generator* gen)
{

	switch( gen->toMode )
	{
		case GeneratorMessage::FieldOffWaitMode:
			gen->out->set(gen->FieldOff_c, false);
			gen->tm->set(Generator::FieldOffTimer, 0);
			break;

		case GeneratorMessage::FieldOnWaitMode:
			gen->out->set(gen->FieldOn_c, false);
			gen->tm->set(Generator::FieldOnTimer, 0);
			break;

		case GeneratorMessage::WaitUincreaseMode:
			gen->tm->set(Generator::UincreaseTimer, 0);
			break;

		default:
			break;
	}

	return true;
}

// ------------------------------------------------------------------------------------------
void GeneratorTransientMode::timerInfo( Generator* gen, const uniset::TimerMessage* tm )
{
	switch( tm->id )
	{
		case Generator::FieldOnTimer:
		{
			gen->tm->set(Generator::FieldOnTimer, 0);

			if( gen->toMode != GeneratorMessage::FieldOnWaitMode )
				return;

			if( gen->log()->is_crit() )
				gen->log()->crit() << gen << "(timerInfo): Field ON timeout.. "
								   << gen->FieldOnTime << " msec" << endl;
		}
		break;

		case Generator::FieldOffTimer:
		{
			gen->tm->set(Generator::FieldOffTimer, 0);

			if( gen->toMode != GeneratorMessage::FieldOffWaitMode )
				return;

			if( gen->log()->is_crit() )
				gen->log()->crit() << gen << "(timerInfo): AGP OFF timeout.. "
								   << gen->FieldOffTime << " msec" << endl;
		}
		break;

		case Generator::UincreaseTimer:
		{
			gen->tm->set(Generator::UincreaseTimer, 0);

			if( gen->toMode != GeneratorMessage::WaitUincreaseMode )
				return;

			if( gen->log()->is_crit() )
				gen->log()->crit() << gen << "(timerInfo): wait Unormal timeout.. "
								   << gen->UincreaseTime << " msec" << endl;
		}
		break;

		default:
			if( gen->log()->is_crit() )
				gen->log()->crit() << gen << "(timerInfo): Unknonwn timer! tid=" << tm->id << endl;

			break;
	}

	if( gen->attempt >= gen->attemptCounter )
	{
		changeMode( gen, gen->backMode );		// Возвращаемся в предыдущий режим
	}
	else
	{
		gen->attempt++;
		activate(gen);
	}
}
// ------------------------------------------------------------------------------------------
void GeneratorTransientMode::sensorInfo( Generator* gen, const uniset::SensorMessage* sm )
{
	if( sm->id == gen->U_Norm_s )
	{
		if( gen->toMode == GeneratorMessage::WaitUincreaseMode && sm->value )
		{
			gen->tm->set(Generator::UincreaseTimer, 0);

			if( gen->log()->is_info() )
				gen->log()->info() << gen << "(sensorInfo): Unormal.. OK" << endl;

			changeMode( gen, GeneratorOnMode::inst());
		}
	}
	else if( sm->id == gen->Field_s )
	{
		if( sm->value)
		{
			if( gen->toMode == GeneratorMessage::FieldOnWaitMode )
			{
				if( gen->log()->is_info() )
					gen->log()->info() << gen << "(sensorInfo): AGP on.. OK" << endl;

				changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::WaitUincreaseMode) );
			}
		}
		else
		{
			if( gen->toMode == GeneratorMessage::FieldOffWaitMode )
			{
				if( gen->log()->is_info() )
					gen->log()->info() << gen << "(sensorInfo): Field off.. OK" << endl;

				changeMode( gen, GeneratorOffMode::inst());
			}
			else if( gen->toMode == GeneratorMessage::WaitUincreaseMode )
			{
				if( gen->log()->is_warn() )
					gen->log()->warn() << gen << "(sensorInfo): Field OFF?!" << endl;

				changeMode( gen, GeneratorOffMode::inst() );
			}
		}
	}
}
// ------------------------------------------------------------------------------------------
void GeneratorTransientMode::genCommand( Generator* gen, const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::On:
		{
			if( gen->toMode == GeneratorMessage::GeneratorMessage::WaitUincreaseMode )
			{
				if( gen->log()->is_info() )
					gen->log()->info() << gen << "(genInfo): ON... already." << endl;
			}
			else
			{
				if( gen->log()->is_info() )
					gen->log()->info() << gen << "(genInfo): ON.. rollback" << endl;

				rollback(gen);
				// при новой команде обновляем attemps
				changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::OnMode, GeneratorInitMode::inst(), gen->numberOfAttempts));
			}
		}
		break;

		case GeneratorMessage::Off:
		{
			if( gen->toMode == GeneratorMessage::OffMode )
			{
				if( gen->log()->is_info() )
					gen->log()->info() << gen << "(genInfo): OFF... already" << endl;
			}
			else
			{
				if( gen->log()->is_info() )
					gen->log()->info() << gen << "(genInfo): OFF...rollback" << endl;

				rollback(gen);
				// при новой команде обновляем attemps
				changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::OffMode, GeneratorInitMode::inst(), gen->numberOfAttempts) );
			}
		}
		break;

		case GeneratorMessage::OffControl:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << "(genInfo): OFF CONTROL" << endl;

			rollback(gen);
			changeMode( gen, GeneratorOffControlMode::inst() );
		}
		break;

		case GeneratorMessage::ResetCommand:
		{
			if( gen->log()->is_info())
				gen->log()->info() << gen << ": RESET COMMAND.." << endl;

			gen->out->reset();
			changeMode( gen, GeneratorInitMode::inst() );
		}
		break;

		case GeneratorMessage::Reset:
			break;

		default:
			if( gen->log()->is_crit() )
				gen->log()->crit() << gen << "(genInfo): Unknown command! cmd=" << m->cmd << endl;

			break;
	}
}
// ------------------------------------------------------------------------------------------
void GeneratorTransientMode::rollback( Generator* gen )
{
	if( gen->log()->is_warn() )
		gen->log()->warn() << gen << "(rollback): rollback command..." << endl;

	GeneratorMessage::GeneratorMode tmpMode(gen->toMode);
	gen->toMode = gen->prevMode;
	disactivate(gen);
	gen->toMode = tmpMode;
}
// ------------------------------------------------------------------------------------------
