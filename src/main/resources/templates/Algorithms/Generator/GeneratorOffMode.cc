// -----------------------------------------------------------------------------
#include "Generator.h"
#include "GeneratorMode.h"
// -------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// ---------------------------------------------------------------------------
GeneratorOffMode* GeneratorOffMode::inst_ = 0;
// ---------------------------------------------------------------------------


GeneratorOffMode::~GeneratorOffMode()
{
}

GeneratorOffMode::GeneratorOffMode()
{
}

// ---------------------------------------------------------------------------

GeneratorOffMode* GeneratorOffMode::inst()
{
	if(inst_ == 0)
		inst_ =  new GeneratorOffMode();

	return inst_;
}

// ---------------------------------------------------------------------------

bool GeneratorOffMode::activate( Generator* gen )
{
	if( gen->log()->is_info() )
		gen->log()->info() << gen << ": ..............activate.............." << endl;

	gen->out->reset();
	return true;
}
// ------------------------------------------------------------------------------------------
void GeneratorOffMode::genCommand( Generator* gen, const GeneratorMessage* m )
{
	switch(m->cmd)
	{
		case GeneratorMessage::On:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ": ON.." << endl;

			changeMode( gen, GeneratorTransientMode::inst(gen, GeneratorMessage::OnMode, GeneratorInitMode::inst(), gen->numberOfAttempts) );
		}
		break;

		case GeneratorMessage::Off:
		{
			if( gen->log()->is_info() )
				gen->log()->info() << gen << ": OFF.." << endl;

			activate(gen);
		}
		break;

		default:
			GeneratorMode::genCommand(gen, m);
			break;
	}
}
// ------------------------------------------------------------------------------------------
void GeneratorOffMode::sensorInfo( Generator* gen, const uniset::SensorMessage* sm )
{
	if( sm->id == gen->Field_s )
	{
		if( sm->value )
		{
			if( gen->log()->is_warn() )
				gen->log()->warn() << gen << ": ChangeMode?!" << endl;

			changeMode( gen, GeneratorInitMode::inst() );
		}
	}
	else if( sm->id == gen->U_Norm_s  )
	{
		if( sm->value )
		{
			if( gen->log()->is_warn() )
				gen->log()->warn()  << gen << ": ChangeMode?!" << endl;

			changeMode( gen, GeneratorInitMode::inst() );
		}
	}
	else
		GeneratorMode::sensorInfo(gen, sm);
}
// ------------------------------------------------------------------------------------------
