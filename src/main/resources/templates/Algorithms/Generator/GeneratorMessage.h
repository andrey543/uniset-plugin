#ifndef GeneratorMessage_H_
#define GeneratorMessage_H_
// ------------------------------------------------------------------------------------------
#include "TEMPLATE_PROJECTMessages.h"
// -----------------------------------------------------------------------------
/*!	Сообщения от объекта ПЧ (и команды ему) */
class GeneratorMessage:
	public TEMPLATE_PROJECT::TEMPLATE_PROJECTMessage
{
	public:

		static const int MessageTypeID = TEMPLATE_PROJECT::TEMPLATE_PROJECTMessage::GeneratorInfo;

		enum GeneratorMode
		{
			OnMode,
			OffMode,
			WaitUincreaseMode,
			FieldOffWaitMode,
			FieldOnWaitMode
		};

		enum Commands
		{
			On,		/*!< включение */
			Off,	/*!< отключение */
			Reset,	/*!< сброс защиты */
			ResetCommand, /*! сброс команды (прекратить действие, если возможно) */
			OnControl, /*!< включить управление */
			OffControl /*!< отключить управление */
		};

		GeneratorMessage(Commands _cmd, Message::Priority p = Message::Medium):
			cmd(_cmd)
		{
			type = GeneratorMessage::MessageTypeID;
			priority = p;
		}

		GeneratorMessage(const uniset::VoidMessage* msg)
		{
			memcpy(this, msg, sizeof(*this));
			assert(this->type == GeneratorMessage::MessageTypeID );
		}

		inline uniset::TransportMessage transport_msg() const
		{
			return transport(*this);
		}

		Commands cmd;
};
// ------------------------------------------------------------------------------------------
#endif // Generator_H_
// ------------------------------------------------------------------------------------------
