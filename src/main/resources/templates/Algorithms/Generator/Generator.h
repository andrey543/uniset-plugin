/*!
 * \page GeneratorPage Интерфейс управления генератором
 * \par Описание
	- \ref secGenerator_common
	- \ref secGenerator_InitMode
	- \ref secGenerator_OffMode
	- \ref secGenerator_OnMode
	- \ref secGenerator_ProtectionMode
	- \ref secGenerator_sleepMode
	- \ref secGenerator_commands

	\section secGenerator_common Общее описание
	Управление генератором.	Реализовано на основе принципов "конечного автомата".
	Работа алгоритма координируется объектом реализующим алгоритм управления СЭС
	(см. \ref SEESPage)

	Реализация: \ref Generator
	\note Времена для таймеров настраиваются в конфигурационном файле

	\section secGenerator_commands Переходные режимы в объекте генератор

	\par
		Переходы связанные с ожиданием реализованы в GeneratorTransientMode.

	\par Включение (переход GeneratorOffMode -> GeneratorOnMode).
		Переход осуществляется по приходу команды Gidrograph::GeneratorMessage::OnCommand.
		В данном проекте нет управления автоматом гашения поля, поэтому просто идёт
		ожидание выхода напряжения в норму Generator::Unormal.
		При достижении напряжением нормального уровня процесс
		пуска генератора считается успешно завершённым
		и осуществляется переход в \ref secGenerator_OnMode.

	\par \b Отключение (переход GeneratorOnMode -> GeneratorOffMode).
		Переход осуществляется по приходу команды Gidrograph::GeneratorMessage::OffCommand.
		Т.к. в данном проекте нет управления автоматом гашения поля, проиходит проверка
		снижения напряжения и осуществляется переход в \ref secGenerator_OffMode.
*/

// подразделы сформируются автоматически из описаний сделанных в соответствующих h-файлах

// ------------------------------------------------------------------------------------------
#ifndef Generator_H_
#define Generator_H_
// ------------------------------------------------------------------------------------------
#include <TriggerOUT.h>
#include "GeneratorMessage.h"
#include "Generator_SK.h"
// ------------------------------------------------------------------------------------------
class GeneratorMode;
// ------------------------------------------------------------------------------------------
std::ostream& operator<< (std::ostream& os, const GeneratorMessage::GeneratorMode& m);
// ------------------------------------------------------------------------------------------

/*!	Интерфейс управления генератором */
class Generator:
	public Generator_SK
{
	public:
		Generator(uniset::ObjectId id, xmlNode* confnode );
		virtual ~Generator();

		virtual bool isOn() const;
		virtual void on();
		virtual void off();
		virtual void reset();
		virtual void offControl();
		virtual void onControl();
		virtual void resetCommand();

		inline bool isProtection() const
		{
			return protection;
		}

		inline long getState() const
		{
			return out_state_as;
		}

		// таймеры
		enum Timers
		{
			FieldOffTimer,
			FieldOnTimer,
			UincreaseTimer
		};

		std::shared_ptr<uniset::TriggerOUT<Generator, Generator::Timers, int>> tm;
		std::shared_ptr<uniset::TriggerOUT<Generator, uniset::ObjectId, bool>> out;

	protected:
		Generator();

		virtual void step() override;
		virtual void processingMessage( const uniset::VoidMessage* msg ) override;
		void sysCommand( const uniset::SystemMessage* sm ) override;
		void sensorInfo( const uniset::SensorMessage* sm ) override;
		void timerInfo( const uniset::TimerMessage* tm ) override;
		void genCommand( const GeneratorMessage* m );
		void changeMode( GeneratorMode* m );
		virtual std::string getTimerName( int id ) const override;
		virtual std::string getMonitInfo() const override;

		void setTimer(Generator::Timers tid, int val);
		void setVoltageWarn(bool state);
		void setFreqWarn(bool state);
		void setProtection(bool st);
		void setOut(uniset::ObjectId sid, bool state);

		std::atomic_bool protection;
		size_t num;

	private:
		friend class GeneratorMode;
		friend class GeneratorTransientMode;
		friend class GeneratorProtectionMode;
		friend std::ostream& operator<<(std::ostream& os, Generator* g);
		friend std::ostream& operator<<(std::ostream& os, Generator& g);

		GeneratorMode* mode;

		void init();
		int attemptCounter; /*!< счётчик неудачных попыток перехода */
		int attempt;   /*!< попытка */
		GeneratorMessage::GeneratorMode toMode; /*!< в какой режим перейти */
		GeneratorMessage::GeneratorMode prevMode; /*!< предыдущий режим */
		GeneratorMode* backMode; /*!< из какого ушли */
};
// ------------------------------------------------------------------------------------------
namespace std
{
	template<>
	class hash<Generator::Timers>
	{
		public:
			size_t operator()(const Generator::Timers& s) const
			{
				return std::hash<int>()(s);
			}
	};

	std::string to_string( const Generator::Timers& t );
}
// ------------------------------------------------------------------------------------------
#endif // Generator_H_
// ------------------------------------------------------------------------------------------
