#ifndef FanControl_H_
#define FanControl_H_
// -----------------------------------------------------------------------------
#include <string>
#include <TriggerOUT.h>
#include "FanControl_SK.h"
// -----------------------------------------------------------------------------
/*!
	\page pageFanControl Управление вентиляторами трансформатора

    - \ref sec_tv_Common

	\section sec_tv_Common Описание алгоритма управления вентиляторами трансформатора (FanControl)
	Класс реализует управление вентиляторами охлаждения:
	- теплообменников
	- корпусными (тангенциальными) (BKTT)
	При этом возможны три режима управления:
	- от БКТТ (данный процесс ничего не делает)
	- автоматическое управление (включение вентиляторов по замыканию автомата QTV, отключение после размыкания с расхолаживанием)
	- ручное (управление с экранов GUI1,2)
*/
class FanControl:
	public FanControl_SK
{
	public:
		FanControl( uniset::ObjectId id, xmlNode* cnode, const std::string& prefix = "" );
		virtual ~FanControl();

		inline bool isCooling() const noexcept
		{
			return iscooling;
		}

		bool isCoolingSysFault() const noexcept; /*!< неисправность системы охлаждения */

		enum idControlMode
		{
			cmAutoControl,	/*!< управление от СУ (от нас) */
			cmHandControl,	/*!< дистанционное управление с GUI1,2 */
			cmLocalControl	/*!< управление с местного поста */
		};

		friend std::ostream& operator<<(std::ostream& os, const idControlMode& m );

		enum Timers
		{
			tmCoolingTimer
		};

		inline bool isOn() const noexcept
		{
			return fan1.isOn();
		}

		inline bool isOff() const noexcept
		{
			return !fan1.isOn();
		}

		inline idControlMode getControlMode() const noexcept
		{
			return cmMode;
		}

	protected:
		virtual void step() override;
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( const uniset::TimerMessage* tm ) override;
		virtual std::string getMonitInfo() const override;
		virtual std::string getTimerName( int id ) const override;

		void setOut( uniset::ObjectId sid, bool state );

		struct Fan;

		void fan_on( Fan* f ); /*!< запустить вентилятор */
		void fan_off( Fan* f ); /*!< остановить вентилятор */

		void resetAutoControl();

		//		std::shared_ptr< uniset::TriggerOUT<FanControl, uniset::ObjectId, bool> > fan1; /*!< управление вентиляторами N1 (БКТТ1) */
		//		std::shared_ptr< uniset::TriggerOUT<FanControl, uniset::ObjectId, bool> > fan2; /*!< управление вентиляторами N2 (БКТТ2) */

		idControlMode cmMode = { cmAutoControl }; // по умолчанию режим управления авто!
		bool iscooling = { false };

		// Структура для облегчения работы с одинаковыми Fan1 и Fan2
		struct Fan
		{
			FanControl* fc = { nullptr };
			std::string fname;
			std::string myname;

			const long* alarm_s = { nullptr };
			const long* remote_s = { nullptr };
			const long* isOn_s = { nullptr };

			uniset::ObjectId on_c = { uniset::DefaultObjectId };
			uniset::ObjectId off_c = { uniset::DefaultObjectId };
			uniset::ObjectId remote_id = { uniset::DefaultObjectId };
			uniset::ObjectId alarm_id = { uniset::DefaultObjectId };

			// сообщения
			uniset::ObjectId mid_NotOn = { uniset::DefaultObjectId };
			uniset::ObjectId mid_NotOff = { uniset::DefaultObjectId };
			uniset::ObjectId mid_NotOn_Alarm = { uniset::DefaultObjectId };
			uniset::ObjectId mid_OffControl = { uniset::DefaultObjectId };

			std::shared_ptr< uniset::TriggerOUT<FanControl, uniset::ObjectId, bool> > cmd;

			// управление вентиляторами отключено (или не инициализировано)
			bool isOffControl() noexcept;
			long getState() const noexcept;

			// функции для удобства
			void on();
			void off();
			void reset(); // сброс команд
			bool checkReady() noexcept;
			bool checkOn() noexcept;
			bool checkOff() noexcept;

			inline bool isAlarm() const noexcept
			{
				return (bool)(*alarm_s);
			}
			inline bool isOn() const noexcept
			{
				return (bool)(*isOn_s);
			}
			inline bool isRemoteControl() const noexcept
			{
				return (bool)(*remote_s);
			}

		};

		Fan fan1; // вентилятор теплообменников N1

	private:

};
// -----------------------------------------------------------------------------
#endif // FanControl_H_
