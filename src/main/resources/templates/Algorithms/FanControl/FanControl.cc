#include <sstream>
#include <UniXML.h>
#include <Exceptions.h>
#include <ObjectMode.h>
#include "TEMPLATE_PROJECTConfiguration.h"
#include "FanControl.h"
// -----------------------------------------------------------------------------
using namespace uniset;
using namespace std;
// -----------------------------------------------------------------------------
FanControl::FanControl( uniset::ObjectId id, xmlNode* cnode, const string& prefix ):
	FanControl_SK(id, cnode, prefix)
{
	if( cnode == NULL )
		throw Exception( myname + "(FanControl): FAILED! not found confnode in confile!" );

	auto conf = uniset_conf();

	if( uniset::findArgParam("--debug-logs", conf->getArgc(), conf->getArgv()) != -1 )
		mylog->level( Debug::value(default_loglevel) );

	UniXML::iterator it(cnode);

	// Такая вот "некрасивая" инициализация..

	fan1.fc = this;
	fan1.fname = "fan1";
	fan1.myname = myname;
	fan1.alarm_s = valptr(Fan_Alarm_s);
	fan1.remote_s = valptr(Fans_RemoteContol_s);
	fan1.isOn_s = valptr(Fan_IsOn_s);
	fan1.on_c = Fan_On_c;
	fan1.off_c = Fan_Off_c;
	fan1.remote_id = Fans_RemoteContol_s;
	fan1.alarm_id = Fan_Alarm_s;
	fan1.mid_NotOn = mid_Fan_NotOn;
	fan1.mid_NotOff = mid_Fan_NotOff;
	fan1.mid_NotOn_Alarm = mid_Fan_NotOn_Alarm;
	fan1.mid_OffControl = mid_Fan_OffControl;
	fan1.cmd = make_shared< uniset::TriggerOUT<FanControl, ObjectId, bool> >(this, &FanControl::setOut);
	fan1.cmd->add(Fan_On_c, false);
	fan1.cmd->add(Fan_Off_c, false);


	// если есть переключатель на местном посту, то при запуске считаем что управление на местном
	if( Fans_RemoteContol_s != DefaultObjectId )
		cmMode = cmLocalControl;
}
// -----------------------------------------------------------------------------
FanControl::~FanControl()
{
}
// -----------------------------------------------------------------------------
bool FanControl::isCoolingSysFault() const noexcept
{
	// пока проверяем так.. (чтоб вентиляторы должны быть запущены, но не запущены)
	return ( in_qtv_on_s && !fan1.isOn() );
}
// -----------------------------------------------------------------------------
ostream& operator<<( ostream& os, const FanControl::idControlMode& m )
{
	switch(m)
	{
		case FanControl::cmAutoControl:
			return os << "AutoControl";
			break;

		case FanControl::cmHandControl:
			return os << "HandControl";
			break;

		case FanControl::cmLocalControl:
			return os << "LocalControl";
			break;
	}

	return os;
}
// -----------------------------------------------------------------------------
void FanControl::step()
{
	// формируем обратную связь по режиму
	out_Fan_AutoControl_s = ( cmMode == cmAutoControl );
	out_Fan_HandControl_s = ( cmMode == cmHandControl );

	out_fan_state_fs = fan1.getState();
	
	// независимо от алгоритма, управление управления может быть как на МПУ, так и на ЦПУ или ПУС (за это отвечатет UProxyPanel::MultiChannel), поэтому просто транслируем разрешение
	out_Fans_RemoteContol_c = ( cmMode != cmLocalControl ) && in_Fans_RemoteContol_On_s;
}
// -----------------------------------------------------------------------------
void FanControl::sensorInfo( const uniset::SensorMessage* sm )
{
	if(sm->id == Fans_RemoteContol_On_s)
	{
		if( cmMode == cmLocalControl )
		{
			myinfo << PRELOG << "Включено местное уравление! Игнорируем команду переключения управления" << endl;
			setMsg(mid_Control_Off, true);
			return;
		}
	}

	if( sm->id == Fan_On1_s ||  sm->id == Fan_On2_s )
	{
		if( !sm->value )
			return;

		if( cmMode != cmHandControl )
		{
			myinfo << PRELOG << "Игнорируем команду на запуск вентиляторов. Т.к. НЕ включён режим 'HandControl'" << endl;
			return;
		}

		myinfo << PRELOG << "Запуск вентиляторов с GUI" << endl;
		fan_on(&fan1);

	}
	else if( sm->id == Fan_Off1_s ||  sm->id == Fan_Off2_s )
	{
		if( !sm->value )
			return;

		if( cmMode != cmHandControl )
		{
			myinfo << PRELOG << "Игнорируем команду на отключение вентиляторов. Т.к. НЕ включён режим 'HandControl'" << endl;
			return;
		}

		myinfo << PRELOG << "Отключение вентиляторов с GUI" << endl;
		fan_off(&fan1);
	}
	else if( sm->id == qtv_on_s )
	{
		// игнорируем состояние QTV
		// если мы не в режиме автоматического управления
		if( cmMode != cmAutoControl )
			return;

		if( sm->value )
		{
			myinfo << PRELOG << "запуск вентиляторов (замкнулся автомат QTV)" << endl;
			iscooling = false;
			askTimer(tmCoolingTimer, 0);

			// если вентиляторы включены.. то и делать ничего не надо..(?)
			if( fan1.isOn() )
				return;

			setMsg(mid_Fan_Auto_On);
			msleep(500); // делаем паузу чтобы сообщение заведомо пришло раньше следующих..
			fan_on(&fan1);
		}
		else
		{
			// если вентиляторы не включены.. то и расхолаживания не нужно..
			if( !fan1.isOn() )
			{
				myinfo << PRELOG << "игнорируем отключение QTV. Вентиляторы не запущены" << endl;
				return;
			}

			if( !iscooling && CoolingTime > 0 )
			{
				myinfo << PRELOG << "Запускаем расхолаживание " << CoolingTime << " msec" << endl;
				iscooling = true;
				askTimer(tmCoolingTimer, CoolingTime);
				setMsg(mid_Fan_Auto_StartCooldown);
			}
			else
			{
				myinfo << PRELOG << "отключение вентиляторов БЕЗ расхолаживания.." << endl;
				setMsg(mid_Fan_Auto_Off);
				msleep(500); // делаем паузу чтобы сообщение заведомо пришло раньше следующих..
				fan_off(&fan1);
			}
		}
	}
	else if( sm->id == Fans_RemoteContol_s )
	{
		if( !sm->value )
		{
			myinfo << PRELOG << "НА МЕСТНОМ ПОСТУ ОТКЛЮЧЕНО ДИСТАНЦИОННОЕ УПРАВЛЕНИЕ" << endl;
			cmMode = cmLocalControl;
			step();
			resetAutoControl();
		}
		else
		{
			// по умолчанию... включаем 'АВТО'
			myinfo << PRELOG << "Отдано управление с местного поста. Включаем режим 'АВТО'" << endl;
			cmMode = cmAutoControl;
			step();
			askSensor(qtv_on_s, UniversalIO::UIONotify);
		}
	}
	else if( sm->id == Fan_AutoControl_On_s )
	{
		if( !sm->value )
			return;

		if( cmMode == cmLocalControl )
		{
			myinfo << PRELOG << "Включено местное уравление! Игнорируем команду перехода в режим 'АВТО'" << endl;
			setMsg(mid_Control_Off, true);
			return;
		}

		myinfo << PRELOG << "Команда на отключение управления от БКТТ (включение режима АВТОМАТ)" << endl;
		cmMode = cmAutoControl;
		step();
		askSensor(qtv_on_s, UniversalIO::UIONotify);
	}
	else if( sm->id == Fan_HandControl_On_s )
	{
		if( !sm->value )
			return;

		if( cmMode == cmLocalControl )
		{
			myinfo << PRELOG << "Включено местное уравление! Игнорируем команду перехода в режим 'ДИСТ'" << endl;
			setMsg(mid_Control_Off, true);
			return;
		}

		myinfo << PRELOG << "Команда на включение РУЧНОГО управления вентиляторами ('ДИСТ')" << endl;
		cmMode = cmHandControl;
		step();
		resetAutoControl();
	}
}
// -----------------------------------------------------------------------------
void FanControl::timerInfo(const TimerMessage* tm)
{
	if( tm->id == tmCoolingTimer )
	{
		askTimer(tmCoolingTimer, 0);

		if( cmMode != cmAutoControl )
			return;

		if( !iscooling )
		{
			mywarn << PRELOG << "Сработал timer расхолаживания, но флаг iscooling уже сброшен!" << endl;
			return;
		}

		setMsg(mid_Fan_Auto_ColdownCompleted);
		iscooling = false;
		myinfo << PRELOG << "отключение вентиляторов после расхолаживания.." << endl;
		setMsg(mid_Fan_Auto_Off);
		msleep(500); // делаем паузу чтобы сообщение заведомо пришло раньше следующих..
		fan_off(&fan1);
	}
}
// -----------------------------------------------------------------------------
string FanControl::getMonitInfo() const
{
	ostringstream s;

	s << " " << strval(Fan_IsOn_s, false)
	  << " " << strval(Fan_Alarm_s, false)
	  << " " << strval(Fan_IsOn_s, false)
	  << " " << strval(Fan_Alarm_s, false)
	  << " Current fans control mode: " << cmMode
	  << endl;

	return std::move(s.str());
}
// -----------------------------------------------------------------------------
string FanControl::getTimerName(int id) const
{
	if( id == tmCoolingTimer )
		return "CoolingTimer";

	return "";
}
// -----------------------------------------------------------------------------
void FanControl::resetAutoControl()
{
	fan1.reset();
	askTimer(tmCoolingTimer, 0);
	iscooling = false;
}
// -----------------------------------------------------------------------------
void FanControl::setOut( uniset::ObjectId sid, bool st )
{
	if( sid == DefaultObjectId )
		return;

	try
	{
		mylog8 << myname << "(setOut): sid=" << sid << " state=" << st << endl;
		setValue(sid, st);
	}
	catch( const Exception& ex )
	{
		mycrit << myname << "(setOut): " << ex << endl;
	}
}
// -----------------------------------------------------------------------------
bool FanControl::Fan::isOffControl() noexcept
{
	if( on_c == DefaultObjectId && off_c == DefaultObjectId )
		return true;

	return false;
}
// -----------------------------------------------------------------------------
long FanControl::Fan::getState() const noexcept
{
	if( isAlarm() )
		return UniSetAlgorithms::mALARM;

	if( cmd->getState(on_c) || cmd->getState(off_c) )
		return UniSetAlgorithms::mTRANSITIVE;

	if( isOn() )
		return UniSetAlgorithms::mON;

	return UniSetAlgorithms::mOFF;
}
// -----------------------------------------------------------------------------
void FanControl::Fan::on()
{
	if( isOffControl() )
		return;

	if( checkReady() )
		cmd->set(on_c, true);
}
// -----------------------------------------------------------------------------
void FanControl::Fan::off()
{
	if( isOffControl() )
		return;

	cmd->set(off_c, true);
}
// -----------------------------------------------------------------------------
void FanControl::Fan::reset()
{
	cmd->reset();
}
// -----------------------------------------------------------------------------
bool FanControl::Fan::checkReady() noexcept
{
	if( isOffControl() )
		return false;

	if( remote_id != DefaultObjectId && !isRemoteControl() )
	{
		if( fc->log()->is_info() )
			fc->log()->info() << PRELOG << "Не разрешено дистанционное управление вентиляторами " << fname << " (с местного поста).." << endl;

		fc->setMsg(mid_OffControl, true);
		return false;
	}

	if( alarm_id != DefaultObjectId && isAlarm() )
	{
		if( fc->log()->is_info() )
			fc->log()->info() << PRELOG << "Отказ включения " << fname << ". Не сброшена авария вентиляторов." << endl;

		fc->setMsg(mid_NotOn_Alarm, true);
		return false;
	}

	return true;
}
// -----------------------------------------------------------------------------
bool FanControl::Fan::checkOn() noexcept
{
	if( isOffControl() )
		return false;

	if( !isOn() )
	{
		if( fc->log()->is_info() )
			fc->log()->info() << PRELOG << "Отказ включения " << fname << endl;

		fc->setMsg(mid_NotOn, true);
		return false;
	}

	return true;
}
// -----------------------------------------------------------------------------
bool FanControl::Fan::checkOff() noexcept
{
	if( isOffControl() )
		return false;

	if( isOn() )
	{
		if( fc->log()->is_info() )
			fc->log()->info() << PRELOG << "Отказ отключения " << fname << endl;

		fc->setMsg(mid_NotOff, true);
		return false;
	}

	return true;
}
// -----------------------------------------------------------------------------
void FanControl::fan_on( Fan* f )
{
	if( cmMode == cmLocalControl  )
	{
		myinfo << PRELOG << "Включено местное управление..." << endl;
		setMsg(mid_Control_Off, true);
		return;
	}

	f->on();

	updateOutputs(true);
	msleep(CheckStateTime);
	updateValues();

	f->checkOn();
	f->reset();
	updateOutputs(true);
}
// -----------------------------------------------------------------------------
void FanControl::fan_off( Fan* f )
{
	if( cmMode == cmLocalControl )
	{
		myinfo << PRELOG << "Включено местное управление..." << endl;
		setMsg(mid_Control_Off, true);
		return;
	}

	f->off();
	updateOutputs(true);
	msleep(CheckStateTime);
	updateValues();
	f->checkOff();
	f->reset();
	updateOutputs(true);
}
// -----------------------------------------------------------------------------
