#include "FC.h"
#include "FCMode.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCTransientMode* FCTransientMode::_inst = 0;
// ---------------------------------------------------------------------------


FCTransientMode::~FCTransientMode()
{
}

FCTransientMode::FCTransientMode()
{
}

// ---------------------------------------------------------------------------

FCTransientMode* FCTransientMode::inst( FC* fqc,
										FCMessage::FCMode to,
										FCMode* back, int attempt)
{
	if(_inst == 0)
		_inst =  new FCTransientMode();

	fqc->prevMode = fqc->toMode;
	fqc->toMode = to;
	fqc->backMode = back;
	fqc->attemptCounter = attempt;
	fqc->attempt = 0;
	return _inst;
}

// ---------------------------------------------------------------------------

bool FCTransientMode::activate(FC* fc)
{
	switch( fc->toMode )
	{
		case FCMessage::OnMode:
		{
			if( fc->in_Protection_s || fc->in_Mode_as == TEMPLATE_PROJECT::fcAlarm )
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << ": присутствует сигнал 'Защита ПЧ'" << endl;

				changeMode( fc, FCProtectionMode::inst());
				break;
			}

			fc->out->set(fc->On_c, true);
			fc->tm->set(FC::OnTimer, fc->OnTime);

			if( fc->log()->is_info() )
				fc->log()->info() << fc << ": ждем ВКЛЮЧЕНИЯ ПЧ" << endl;

			fc->setMsg(fc->mid_wait_on, true);
			fc->askSensor(fc->Mode_as, UniversalIO::UIONotify);
		}
		break;

		case FCMessage::OffMode:
		{
			fc->out->set(fc->Off_c, true);
			fc->tm->set(FC::OffTimer, fc->OffTime);
			fc->askSensor(fc->Mode_as, UniversalIO::UIONotify);

			if( fc->log()->is_info() )
				fc->log()->info() << fc << ": ждем ОТКЛЮЧЕНИЯ ПЧ" << endl;
		}
		break;

		default:
			if( fc->log()->is_crit() )
				fc->log()->crit() << fc << ": НЕИЗВЕСТНАЯ КОМАНДА..." << endl;

			break;
	}

	return true;
}
// ---------------------------------------------------------------------------
bool FCTransientMode::disactivate(FC* fqc)
{
	switch( fqc->prevMode )
	{
		case FCMessage::OffMode:
			fqc->tm->set(FC::OffTimer, 0);
			fqc->out->set(fqc->Off_c, false);
			break;

		case FCMessage::OnMode:
			fqc->tm->set(FC::OnTimer, 0);
			fqc->out->set(fqc->On_c, false);
			break;
	}

	return true;
}

// ------------------------------------------------------------------------------------------
void FCTransientMode::timerInfo( FC* fc, const uniset::TimerMessage* tm )
{
	switch( tm->id )
	{
		case FC::OnTimer:
		{
			if( fc->toMode != FCMessage::OnMode )
				return;

			fc->tm->set(FC::OnTimer, 0);

			if( fc->log()->is_warn() )
				fc->log()->warn() << fc << ":  ПЧ НЕ ВКЛЮЧИЛСЯ за время " << fc->OnTime << endl << flush;
		}
		break;

		case FC::OffTimer:
		{
			if( fc->toMode != FCMessage::OffMode )
				return;

			fc->tm->set(FC::OffTimer, 0);

			if( fc->log()->is_warn() )
				fc->log()->warn() << fc << ":  ПЧ НЕ ОТКЛЮЧИЛСЯ за время " << fc->OffTime << endl << flush;
		}
		break;

	}

	if( fc->attempt >= fc->attemptCounter )
	{
		changeMode( fc, fc->backMode );	// Возвращаемся в предыдущий режим
	}
	else
	{
		fc->attempt++;
		activate(fc);
	}
}
// ------------------------------------------------------------------------------------------
void FCTransientMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->Mode_as )
	{
		switch( fc->in_Mode_as )
		{
			case TEMPLATE_PROJECT::fcAlarm:
			{
				fc->out->set(fc->Off_c, false);
				fc->out->set(fc->On_c, false);
				changeMode( fc, FCProtectionMode::inst() );
			}
			break;

			case TEMPLATE_PROJECT::fcReady1:
			{
				if( fc->toMode == FCMessage::OffMode )
					changeMode( fc, FCOffMode::inst() );
			}
			break;

			case TEMPLATE_PROJECT::fcReady2:
			case TEMPLATE_PROJECT::fcWorking:
			case TEMPLATE_PROJECT::fcStopped:
			case TEMPLATE_PROJECT::fcRotation:
			{
				if( fc->toMode == FCMessage::OnMode )
					changeMode( fc, FCOnMode::inst() );
			}
			break;

			default:
				break;
		}
	}
	else if( sm->id == fc->Protection_s )
	{
		if( sm->value )
		{
			if( fc->log()->is_info() )
				fc->log()->info() << fc  << ": сигнал 'Защита ПЧ'(защита)\n" << flush;

			changeMode( fc, FCProtectionMode::inst() );
		}
	}
}
// ------------------------------------------------------------------------------------------
void FCTransientMode::rollback( FC* fc )
{
	if( fc->log()->is_warn() )
		fc->log()->warn() << fc << "(rollback): откат команды..." << endl;

	FCMessage::FCMode tmpCmd(fc->toMode);
	fc->toMode = fc->prevMode;
	disactivate(fc);
	fc->toMode = tmpCmd;
}
// ------------------------------------------------------------------------------------------
void FCTransientMode::fcCommand( FC* fc, const FCMessage* m )
{
	switch( m->inf )
	{
		case FCMessage::Start:
		{
			if( fc->toMode == FCMessage::OnMode )
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << ": Start! (но мы и так запускаемся)..." << endl;
			}
			else
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << ": Start!(rollback)" << endl;

				rollback(fc);
				changeMode( fc, FCTransientMode::inst(fc, FCMessage::OnMode) );
			}
		}
		break;

		case FCMessage::Stop:
		{
			if( fc->toMode == FCMessage::OffMode )
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << ": Stop! (но мы и так отключаемся)..." << endl;
			}
			else
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << ": Stop!(rollback)" << endl;

				rollback(fc);
				changeMode( fc, FCTransientMode::inst(fc, FCMessage::OffMode) );
			}
		}
		break;

		case FCMessage::Reset:
			break;

		default:
			if( fc->log()->is_crit() )
				fc->log()->crit() << fc << "(fqcCommand): неизвестная команда..." << endl;

			break;
	}
}
// ------------------------------------------------------------------------------------------
