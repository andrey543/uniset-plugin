#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCOnMode* FCOnMode::_inst = 0;
// ---------------------------------------------------------------------------
FCOnMode::~FCOnMode()
{
}

FCOnMode::FCOnMode()
{
}

// ---------------------------------------------------------------------------

FCOnMode* FCOnMode::inst()
{
	if(_inst == 0)
		_inst =  new FCOnMode();

	return _inst;
}

// ---------------------------------------------------------------------------

void FCOnMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->Mode_as )
	{
		switch( fc->in_Mode_as )
		{
			case TEMPLATE_PROJECT::fcReady2:
			case TEMPLATE_PROJECT::fcWorking:
			case TEMPLATE_PROJECT::fcStopped:
			case TEMPLATE_PROJECT::fcRotation:
				break;

			default:
				changeMode( fc, FCInitMode::inst() );
				break;
		}
	}
	else
		FCMode::sensorInfo(fc, sm);
}
// ---------------------------------------------------------------------------
bool FCOnMode::activate( FC* fc )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": ..............activate.............." << endl;

	fc->askSensor(fc->Protection_s, UniversalIO::UIONotify);
	fc->askSensor(fc->Mode_as, UniversalIO::UIONotify);
	return true;
}
// ---------------------------------------------------------------------------

void FCOnMode::fcCommand( FC* fc, const FCMessage* m )
{
	switch( m->inf )
	{
		case FCMessage::Start:
			activate(fc);
			break;

		case FCMessage::Stop:
		{
			if( fc->log()->is_info() )
				fc->log()->info() << fc << "(fqcCommand): отключение ПЧ" << endl;

			changeMode( fc, FCTransientMode::inst(fc, FCMessage::OffMode, this));
		}
		break;

		case FCMessage::Reset:
			break;

		default:
			if( fc->log()->is_crit() )
				fc->log()->crit() << fc << "(fqcCommand): НЕИЗВЕСТНАЯ КОМАНДА!" << endl;

			break;
	}
}
// ------------------------------------------------------------------------------------------
