#ifndef FCMessage_H_
#define FCMessage_H_
// ------------------------------------------------------------------------------------------
#include "TEMPLATE_PROJECTMessages.h"
// -----------------------------------------------------------------------------
/*!	Сообщения от объекта ПЧ (и команды ему) */
class FCMessage:
	public TEMPLATE_PROJECT::TEMPLATE_PROJECTMessage
{
	public:

		static const int MessageTypeID = TEMPLATE_PROJECT::TEMPLATE_PROJECTMessage::FCInfo;

		enum Events
		{
			Protection,		/*!< сработала защита	*/
			ChangeMode,		/*!< автомат сменил состояние */
			Command,
			RepeatCommand,	/*!< повторить команду */
			CommandNotComplete /*!< выполнение команды завершилось неудачей */
		};

		enum FCMode
		{
			OnMode,
			OffMode
		};

		enum Commands
		{
			Start,				/*! команда на подготовку ко включению */
			Stop,				/*! команда на завершение отключения (снятие сигналов) */
			Reset,				/*! сброс защиты */
			OnControl,
			OffControl
		};

		FCMessage(Events ev, int inf, Message::Priority p = Message::Medium):
			evnt(ev), inf(inf), errcode(0)
		{
			type = FCMessage::MessageTypeID;
			priority = p;
		}

		FCMessage(const uniset::VoidMessage* msg)
		{
			memcpy(this, msg, sizeof(*this));
			assert(this->type == FCMessage::MessageTypeID );
		}

		inline uniset::TransportMessage transport_msg() const
		{
			return transport(*this);
		}

		Events evnt;
		int inf;
		int errcode;
};
// ------------------------------------------------------------------------------------------
#endif // FC_H_
// ------------------------------------------------------------------------------------------
