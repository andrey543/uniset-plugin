#ifndef FCMode_H_
#define FCMode_H_
// -----------------------------------------------------------------------------
#include <string>
#include <sstream>
#include <UniSetObject.h>
#include <MessageType.h>
#include "FCMessage.h"
// ------------------------------------------------------------------------------
class FC;

/*!
	Прототип реализации режимов работы.
	\sa \ref FCPage
*/
class FCMode
{
	public:
		static FCMode* inst();
		virtual ~FCMode();

		virtual void timerInfo( FC* fc, const uniset::TimerMessage* tm ) {}
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm );
		virtual void fcCommand( FC* fc, const FCMessage* m ) {}
		virtual bool activate( FC* fc )
		{
			return true;
		}
		virtual bool disactivate( FC* fc )
		{
			return true;
		}

		virtual const std::string modeName()
		{
			return "???Mode";
		}

		virtual bool isTransientMode()
		{
			return false;
		}

	protected:
		FCMode();

		void changeMode( FC* fc, FCMode* m );
		void setProtection( FC* fc, bool p );
		long getRealValue( FC* fc, uniset::ObjectId id );

	private:
		static FCMode* _inst;
};
// ---------------------------------------------------------------------------
/*!
	\page FCPage
	\section secFC_OffMode Состояние "ПЧ выключен".
		В этом контролируется датчик 'ёкости заряжены'. Если он срабатывает,
	то считаем, что запуск произведён в ручную и переходим \ref secFC_OffMode
*/

/*!
	Режим работы в выключенном состоянии.
	\sa \ref FCPage
*/
class FCOffMode:
	public FCMode
{
	public:
		static FCOffMode* inst();
		virtual ~FCOffMode();

		// --------
		virtual void timerInfo( FC* fc, const uniset::TimerMessage* tm ) override;
		virtual void fcCommand( FC* fc, const FCMessage* m ) override;
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm ) override;
		virtual bool activate( FC* fc ) override;
		virtual bool disactivate( FC* fc ) override;
		virtual const std::string modeName() override
		{
			return "FCOffMode";
		}

	protected:
		FCOffMode();

	private:
		static FCOffMode* _inst;
};
// ---------------------------------------------------------------------------
/*!
	\page FCPage
	\section secFC_OnMode Состояние "ПЧ включён".
	В это устойчивое состояние переход осуществляется после успешного включения.
	В данном состоянии происходит постоянный контроль следующих параметров:
	- питание цепей управления
	- срабатывание защитного отключения ГА

	В случае срабатывания любой из защит осуществляется переход в \ref secFC_ProtectionMode.
	Если приходит команда от GEU о FCMessage::StopEnd, это означает, что отключение(QM) было произведено
	в ручную, в этом случае сразу переходим в \ref secFC_OffMode.

*/

/*!
	Состояние - "Включен".
	\sa \ref FCPage
*/
class FCOnMode:
	public FCMode
{
	public:
		static FCOnMode* inst();
		virtual ~FCOnMode();

		// --------
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm ) override;
		virtual void fcCommand( FC* fc, const FCMessage* m ) override;

		virtual bool activate( FC* fc ) override;
		virtual const std::string modeName() override
		{
			return "FCOnMode";
		}

	protected:
		FCOnMode();
		FCOnMode( FC* fc );

	private:
		static FCOnMode* _inst;
};

// ---------------------------------------------------------------------------
/*!
	\page FCPage
	\section secFC_ProtectionMode Состояние "сработала защита ПЧ".
	Это состояние является устойчивым. Переход в него возможен при срабатывании
	какой-либо защиты или неудочной попытке перехода из одного устойчивого
	состояния в другое.
	Попытка выхода из него происходит только при подаче команды "сброс защиты",
	при этом осуществляется проверка состояния защит и переход в
	\ref secFC_InitMode.
*/
/*!
	Состояние "Сработала защита"
	\sa \ref FCPage
*/
class FCProtectionMode:
	public FCMode
{
	public:
		static FCProtectionMode* inst();
		virtual ~FCProtectionMode();

		// --------
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm) override;
		virtual bool activate( FC* fc ) override;
		virtual bool disactivate( FC* fc ) override;
		virtual void fcCommand( FC* fc, const FCMessage* m) override;
		virtual const std::string modeName() override
		{
			return "FCProtectionMode";
		}

		bool checkProtection( FC* fc );

	protected:
		FCProtectionMode();

	private:
		static FCProtectionMode* _inst;
};

// ---------------------------------------------------------------------------

/*!
	\page FCPage
	\section secFC_WaitMode Управление ПЧ
	Это состояние является переходным (не устойчивым) и объединяет в себе
	все возможные переходные состояния при переходе из одного устойчивого в другое.
	В основном здесь происходит подача команды и ожидание её выполнения по таймеру.
	В случае невыполнения команды за заданное время (и количество попыток) происходит переход
	в состояние указанное в качестве 'back', в случае успеха в состояние указанное как 'to'.
	(см. FCWaitingMode::inst).

	\par Включение ПЧ
	Включение происходит по команде от GEU. По приходу команды в течение FC::OnTime ожидается сигнал
	"К включению готов"(ёмкости заряжены) датчик FC::Ready_S и осуществляется переход в \ref secFC_OnMode.

		\image html fc_on.png

	\par Отключение ПЧ
		Отключение осуществляется по приходу команды от GEU.
	При этом объект ожидает разряда конденсаторов по датчику FC::Ready_S и дождавшись переходит
	в \ref secFC_OffMode.
		\image html fc_off.png
*/

/*!
	Состояние - "ожидание исполнения команды ПЧ"
	\sa \ref FCPage
*/
class FCTransientMode:
	public FCMode
{
	public:

		/*! Функция инстанцирования.
			\param fc - указатель на управляемый объект
			\param to - целевое состояние
			\param back - состояние в которое переходить в случае неудачной попытки
			\param atempt - количество попыток
		*/
		static FCTransientMode* inst( FC* fc,
									  FCMessage::FCMode to,
									  FCMode* back = FCProtectionMode::inst(),
									  int atempt = 0 );
		virtual ~FCTransientMode();

		// --------

		virtual void timerInfo( FC* fc, const uniset::TimerMessage* tm ) override;
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm ) override;
		virtual void fcCommand( FC* fc, const FCMessage* m ) override;
		virtual bool isTransientMode()
		{
			return true;
		}

		virtual bool activate( FC* fc ) override;
		virtual bool disactivate( FC* fc ) override;
		virtual const std::string modeName() override
		{
			return "FCWaitingMode";
		}

	protected:
		FCTransientMode();
		void rollback( FC* am );

	private:
		static FCTransientMode* _inst;
};
// ---------------------------------------------------------------------------
/*!
	\page FCPage
	\section secFC_InitMode Инициализация состояния ПЧ.

	Это состояние является переходным (не устойчивым). В нем происходит идентификация текущего
	фактичкеского состояния автомата и переход в соответсвующее ему програмное состояние.
	Возможны переходы в
	- \ref secFC_OnMode
	- \ref secFC_OffMode
	- \ref secFC_ProtectionMode
*/
/*!
	Режим инициализации
	\sa \ref FCPage
*/
class FCInitMode:
	public FCMode
{
	public:
		static FCInitMode* inst();
		virtual ~FCInitMode();

		// --------
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm) override;
		virtual void fcCommand( FC* fc, const FCMessage* m ) override;
		virtual bool activate( FC* fc ) override;
		virtual const std::string modeName() override
		{
			return "FCInitMode";
		}

	protected:
		FCInitMode();

	private:
		static FCInitMode* _inst;
};
// ---------------------------------------------------------------------------
class FCUnknownMode:
	public FCMode
{
	public:
		static FCUnknownMode* inst();
		virtual ~FCUnknownMode();

		// --------
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm) override;
		virtual void fcCommand( FC* fc, const FCMessage* m ) override;
		virtual bool activate( FC* fc ) override;
		virtual const std::string modeName() override
		{
			return "FCUnknownMode";
		}

	protected:
		FCUnknownMode();

	private:
		static FCUnknownMode* _inst;
};
// ---------------------------------------------------------------------------
class FCOffControlMode:
	public FCMode
{
	public:
		static FCOffControlMode* inst();
		virtual ~FCOffControlMode();

		// --------
		virtual void sensorInfo( FC* fc, const uniset::SensorMessage* sm) override;
		virtual void fcCommand( FC* fc, const FCMessage* m ) override;
		virtual bool activate( FC* fc ) override;
		virtual const std::string modeName() override
		{
			return "FCOffControlMode";
		}

	protected:
		FCOffControlMode();

	private:
		static FCOffControlMode* _inst;
};
// ---------------------------------------------------------------------------
#endif // FCMode_H_
// ---------------------------------------------------------------------------
