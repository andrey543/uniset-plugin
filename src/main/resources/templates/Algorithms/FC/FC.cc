#include <sstream>
#include <iomanip>
#include <Exceptions.h>
#include <Debug.h>
#include <UHelpers.h>
#include <UniSetActivator.h>
#include "FC.h"
#include "TEMPLATE_PROJECTConfiguration.h"
#include "FCMode.h"
// -----------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// -----------------------------------------------------------------------------
std::string std::to_string( const FC::Timers& t ) noexcept
{
	switch(t)
	{
		case FC::OnTimer:
			return "OnTimer";

		case FC::OffTimer:
			return "OffTimer";

		case FC::CheckTimer:
			return "CheckTimer";

		default:
			break;
	};

	return "Unknown";
}
// -----------------------------------------------------------------------------
FC::FC(ObjectId id, xmlNode* confnode ):
	FC_SK(id, confnode),
	mode(0),
	protection(false),
	protectionOFF(false)
{

	UniXML_iterator it(confnode);
	RPMnom = it.getPIntProp("RPMnom", 1500);

	int treset = it.getPIntProp("ResetTime", 2000);
	ptReset.setTiming(treset);
	
	auto act = UniSetActivator::Instance();
	xmlNode* tnode = uniset_conf()->findNode(confnode->children, "Transfer", "");
	if( tnode != nullptr )
	{
		UniXML_iterator it(tnode);
		transfer = uniset::make_object<Transfer>(it.getProp("name"), "Transfer");
		act->add(transfer);
	}

	// формируем набор взаимоисключающих таймеров
	tm = make_shared<uniset::TriggerOUT<FC, FC::Timers, int>>(this, &FC::setTimer);
	tm->add(OnTimer, 0);
	tm->add(OffTimer, 0);

	out = make_shared<uniset::TriggerOUT<FC, ObjectId, bool>>(this, &FC::setOut);
	out->add(On_c, false);
	out->add(Off_c, false);

	vmonit(out_targetRPM_f);
	vmonit(protectionOFF);
	vmonit(in_Reset_s);
	vmonit(RPMnom);

	mode = FCInitMode::inst();
}
// -----------------------------------------------------------------------------
FC::~FC()
{
}
// -----------------------------------------------------------------------------
FC::FC()
{
	myinfo << myname << ": init failed!!!!!!!!!!!!!!!" << endl;
	throw Exception();
}
// -----------------------------------------------------------------------------
void FC::processingMessage( const uniset::VoidMessage* msg )
{
	try
	{
		switch( msg->type )
		{
			case FCMessage::MessageTypeID:
			{
				fqcCommand(reinterpret_cast<const FCMessage*>(msg));
				break;
			}

			default:
				FC_SK::processingMessage(msg);
				break;
		}
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(processingMessage): " << ex.what() << endl;
	}
}
// -----------------------------------------------------------------------------
void FC::sensorInfo( const SensorMessage* sm )
{
	try
	{
		mode->sensorInfo(this, sm);
	}
	catch( std::exception& ex )
	{
		mycrit << myname << "(sensorInfo): " << ex.what() << endl;
	}
}
// -----------------------------------------------------------------------------
void FC::timerInfo( const TimerMessage* tm )
{
	try
	{
		mode->timerInfo(this, tm);
	}
	catch( std::exception& ex )
	{
		mycrit << PRELOG << ex.what() << endl;
	}

}
// -----------------------------------------------------------------------------
void FC::sysCommand( const SystemMessage* sm )
{
	FC_SK::sysCommand(sm);

	if( sm->command == SystemMessage::StartUp )
		mode->activate(this);
}
// -----------------------------------------------------------------------------
void FC::changeMode( FCMode* m )
{
	FCMode* old = mode;
	mode = m;

	try
	{
		if( mode->modeName() != "FCWaitingMode" )
			prevMode = toMode;

		old->disactivate(this);
		mode->activate(this);
	}
	catch(Exception& ex)
	{
		mycrit << myname << "(changeMode): не удалось сменить режим!!! " << ex << endl;
		mode = old;
		mode->activate(this);
	}
}
// -----------------------------------------------------------------------------
void FC::on()
{
	push( FCMessage(FCMessage::Command, FCMessage::Start).transport_msg() );
}
// -----------------------------------------------------------------------------
void FC::off()
{
	push( FCMessage(FCMessage::Command, FCMessage::Stop).transport_msg() );
}
// -----------------------------------------------------------------------------
void FC::reset()
{
	push( FCMessage(FCMessage::Command, FCMessage::Reset).transport_msg() );
}
// -----------------------------------------------------------------------------
void FC::fqcCommand( const FCMessage* m )
{
	if( m->evnt == FCMessage::Command )
	{
		if( m->inf == FCMessage::Reset )
		{
			in_Reset_s = true;
			ptReset.reset();
		}

		mode->fcCommand(this, m);
	}
	else
		mycrit << myname << "(fqcCommand): пришло сообщение не содержащее команду!!!" << endl;
}
// -----------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& os, FC& f )
{
	if( f.mode )
		return os << f.myname << "(" << f.mode->modeName() << ")";

	return os << f.myname << "(???Mode)";
}

std::ostream& operator<<(std::ostream& os, FC* f )
{
	if(f->mode)
		return os << f->myname << "(" << f->mode->modeName() << ")";

	return os << f->myname << "(???Mode)";
}

// -----------------------------------------------------------------------------
void FC::setTimer(FC::Timers tid, int val)
{
	askTimer(tid, val, 1);
}
// -----------------------------------------------------------------------------
bool FC::isOnMode() const noexcept
{
	return ( in_Mode_as == TEMPLATE_PROJECT::fcWorking ||
			 in_Mode_as == TEMPLATE_PROJECT::fcStopped ||
			 in_Mode_as == TEMPLATE_PROJECT::fcReady2 ||
			 in_Mode_as == TEMPLATE_PROJECT::fcRotation );
}
// -----------------------------------------------------------------------------
bool FC::isReadyToStart() const noexcept
{
	return ( in_Mode_as == TEMPLATE_PROJECT::fcReady1 );
}
// -----------------------------------------------------------------------------
void FC::step()
{
	//	эти датчики выставляются через функции setXXX (см. FC.h и GEU.сс)
	//	out_targetRPM_c = out_targetRPM_f;
	//	out_PowerLimit_c = !in_PowerLimit_as ? 0 : in_PowerLimit_as;

	if( in_Reset_s && ptReset.checkTime() )
		in_Reset_s = false;

	out_Reset_c = in_Reset_s;

	out_isOn_s = isOnMode();
	out_LocalControl_s = !in_RemoteControl_s;

	out_RPM_Limit70_c  = in_RPM_Limit70_s;

	out_Alarm_fs = ( in_Mode_as == TEMPLATE_PROJECT::fcAlarm );

	out_protectionOFF_c = isProtectionOFF();
}
// -----------------------------------------------------------------------------
string FC::getTimerName(int id) const
{
	return to_string( (Timers)id );
}
// -----------------------------------------------------------------------------
string FC::getMonitInfo() const
{
	ostringstream s;

	s << "mode: " << ( mode ? mode->modeName() : " UNKNOWN" )
	  << " protection=" << protection
	  << endl;

	return std::move(s.str());
}
// -----------------------------------------------------------------------------
void FC::setOut( uniset::ObjectId sid, bool st )
{
	try
	{
		myinfo << myname << "(setOut): sid=" << sid << " state=" << st << endl;
		setValue(sid, (st ? 1 : 0));
	}
	catch(Exception& ex)
	{
		mycrit << myname << "(setOut): " << ex << endl;
	}
}
// ------------------------------------------------------------------------------------------
bool FC::isProtectionOFF() const noexcept
{
	return protectionOFF;
}
// ------------------------------------------------------------------------------------------
bool FC::isPowerLimit() const noexcept
{
	return in_isPowerLimit_s;
}
// ------------------------------------------------------------------------------------------
bool FC::isRemoteControl() const noexcept
{
	return in_RemoteControl_s;
}
// ------------------------------------------------------------------------------------------
bool FC::isProtection() const noexcept
{
	return protection;
}
// ------------------------------------------------------------------------------------------
bool FC::isFault() const noexcept
{
	return (in_numFaults_s > 0);
}
// ------------------------------------------------------------------------------------------
void FC::setRPM(long val) noexcept
{
	out_targetRPM_c = val;
}
// ------------------------------------------------------------------------------------------
void FC::setPWR(long val) noexcept
{
	out_targetPower_c = val;
}
// ------------------------------------------------------------------------------------------
void FC::setControlMode( TEMPLATE_PROJECT::fcControlMode m )
{
	out_controlMode_c = (long)m;
}
// ------------------------------------------------------------------------------------------
void FC::setPlim(long val) noexcept
{
	out_PowerLimit_c = val;
}
// ------------------------------------------------------------------------------------------
void FC::setProtectionOFF( bool st ) noexcept
{
	protectionOFF = st;
}
// ------------------------------------------------------------------------------------------
long FC::percent2RPM( long p )
{
	return lroundf( ( (float)p * (float)RPMnom ) / 100. );
}
// ------------------------------------------------------------------------------------------
long FC::getP() const noexcept
{
	return in_P_as;
}
// ------------------------------------------------------------------------------------------
long FC::getRPM() const noexcept
{
	return in_RPM_as;
}
// ------------------------------------------------------------------------------------------
long FC::getI() const noexcept
{
	return in_I_as;
}
// ------------------------------------------------------------------------------------------
long FC::getPmax() const noexcept
{
	return Pnom;
}
// ------------------------------------------------------------------------------------------
