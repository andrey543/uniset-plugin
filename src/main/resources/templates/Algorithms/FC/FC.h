#ifndef FC_H_
#define FC_H_
// ------------------------------------------------------------------------------------------
#include <cmath>
#include <memory>
#include <TriggerOUT.h>
#include <TriggerOR.h>
#include <Debug.h>
#include <Transfer.h>
#include "FCMessage.h"
#include "TEMPLATE_PROJECTEquipStates.h"
#include "FC_SK.h"
// -----------------------------------------------------------------------------
class FCMode;
class FCTransientMode;
// -----------------------------------------------------------------------------
/*!.
	\page FCPage Управление ПЧ

	\section fqcpage_secCommand Общее описание
		Данный процесс предназначен для взаимодействия с конкретным ПЧ,
	реализации управления (пуск, остановка, режим защиты), задания оборотов и т.п.
*/
// ----------------------------------------------------------------------------
/*! Интерфейс для работы с ПЧ */
class FC:
	public FC_SK
{
	public:
		FC( uniset::ObjectId id, xmlNode* node );
		virtual ~FC();

		long getPmax() const noexcept;
		long getP() const noexcept; 	/*!< текущая мощность */
		long getRPM() const noexcept; 	/*!< текущие обороты */
		long getI() const noexcept; 	/*!< ток */

		void on();	/*!< включение (замыкание автомата) */
		void off();	/*!< отключние (разбор схемы) */
		void reset();	/*!< сброс защиты */

		bool isOnMode() const noexcept;
		bool isReadyToStart() const noexcept;
		bool isProtectionOFF() const noexcept;
		bool isPowerLimit() const noexcept;
		bool isRemoteControl() const noexcept;

		bool isProtection() const noexcept;
		bool isFault() const noexcept; /*!< неиcправность */

		/*! установить задание по оборотам */
		void setRPM( long val ) noexcept;

		/*! установить задание по мощности */
		void setPWR( long val ) noexcept;

		/*! установить режим управления */
		void setControlMode( TEMPLATE_PROJECT::fcControlMode m );

		/*! установить текущее огрничение по мощности */
		void setPlim( long val ) noexcept;

		/*! включение режима "Отключение защит" */
		void setProtectionOFF( bool st ) noexcept;

		inline long percent2RPM( long p );

		// таймеры
		enum Timers
		{
			OnTimer,
			OffTimer,
			CheckTimer
		};

		std::shared_ptr<uniset::TriggerOUT<FC, FC::Timers, int>> tm;

		/*! для посылки сообщений */
		std::shared_ptr<uniset::TriggerOUT<FC, uniset::ObjectId, bool>> out;

		friend std::ostream& operator<<(std::ostream& os, FC& am );
		friend std::ostream& operator<<(std::ostream& os, FC* am );

	protected:
		FC();

		virtual void processingMessage( const uniset::VoidMessage* msg ) override;
		virtual void sysCommand( const uniset::SystemMessage* sm ) override;
		virtual void sensorInfo( const uniset::SensorMessage* sm ) override;
		virtual void timerInfo( const uniset::TimerMessage* tm ) override;
		virtual void fqcCommand( const FCMessage* m );
		virtual void step() override;
		virtual std::string getTimerName( int id ) const override;
		virtual std::string getMonitInfo() const override;

		void changeMode( FCMode* m );
		void setOut(uniset::ObjectId sid, bool state);
		void setTimer(FC::Timers tid, int val);

		inline void setProtection( bool st )
		{
			protection = st;
		};

	private:

		FCMode* mode;
		std::atomic_bool protection = { false };
		long out_targetRPM_f = { 0 };

		long RPMnom; /*!< номинальные заданные обороты (для перевода из % к заданным) */

		bool protectionOFF = { false }; /*!< признак режима снятия защиты */

		bool in_Reset_s; /*!< команда сброс защиты */
		uniset::PassiveTimer ptReset; /*!< таймер для реализации длительности команды Reset */

		std::shared_ptr<Transfer> transfer;
		
		friend class FCMode;
		friend class FCTransientMode;

		// Для переходных режимов
		int attemptCounter;			/*!< счётчик неудачных попыток перехода */
		int attempt;				/*!< попытка */
		FCMessage::FCMode toMode;	/*!< в какой режим перейти */
		FCMessage::FCMode prevMode;	/*!< предидущий режим */
		FCMode* backMode;
};
// ------------------------------------------------------------------------------------------
namespace std
{
	template<>
	class hash<FC::Timers>
	{
		public:
			size_t operator()(const FC::Timers& s) const noexcept
			{
				return std::hash<int>()(s);
			}
	};

	std::string to_string( const FC::Timers& t ) noexcept;
}
// ------------------------------------------------------------------------------------------
#endif // FC_H_
// ------------------------------------------------------------------------------------------
