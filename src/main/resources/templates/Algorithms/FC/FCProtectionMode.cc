#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCProtectionMode* FCProtectionMode::_inst = 0;
// ---------------------------------------------------------------------------
FCProtectionMode::~FCProtectionMode()
{
}

FCProtectionMode::FCProtectionMode()
{
}

// ---------------------------------------------------------------------------

FCProtectionMode* FCProtectionMode::inst()
{
	if(_inst == 0)
		_inst =  new FCProtectionMode();

	return _inst;
}

// ---------------------------------------------------------------------------

void FCProtectionMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
}
// ---------------------------------------------------------------------------

bool FCProtectionMode::activate( FC* fc )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": ..............activate.............." << endl;

	setProtection(fc, true);
	return true;
}
// ---------------------------------------------------------------------------

bool FCProtectionMode::checkProtection( FC* fc )
{
	if( fc->in_Protection_s || fc->in_Mode_as == TEMPLATE_PROJECT::fcAlarm )
	{
		fc->setMsg(fc->mid_NotResetProtection, true);
		return true;
	}

	return false;
}

// ---------------------------------------------------------------------------
void FCProtectionMode::fcCommand( FC* fc, const FCMessage* m )
{
	switch( m->inf )
	{
		case FCMessage::Reset:
		{
			if( !checkProtection(fc) )
				changeMode( fc, FCInitMode::inst());
			else if( fc->log()->is_info() )
				fc->log()->info() << fc << "(automatCommand): reset.. не сброшены признаки защиты...\n";
		}
		break;

		case FCMessage::Start:
		case FCMessage::Stop:
		{
			if( !checkProtection(fc) )
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << "(automatCommand): command.. не восстановлена защита\n";
			}
			else
			{
				if( fc->log()->is_info() )
					fc->log()->info() << fc << "(automatCommand): command.. не сброшены признаки защиты...\n";
			}
		}
		break;
	}

}
// ---------------------------------------------------------------------------
bool FCProtectionMode::disactivate( FC* fc )
{
	setProtection(fc, false);
	return true;
}
// ---------------------------------------------------------------------------
