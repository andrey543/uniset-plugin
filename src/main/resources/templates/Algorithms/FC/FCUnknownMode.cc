#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCUnknownMode* FCUnknownMode::_inst = 0;
// ---------------------------------------------------------------------------
FCUnknownMode::~FCUnknownMode()
{
}

FCUnknownMode::FCUnknownMode()
{
}
// ---------------------------------------------------------------------------
FCUnknownMode* FCUnknownMode::inst()
{
	if(_inst == 0)
		_inst =  new FCUnknownMode();

	return _inst;
}
// ---------------------------------------------------------------------------

void FCUnknownMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->Protection_s )
	{
		if( sm->value )
			changeMode(fc, FCProtectionMode::inst());
	}
	else if( sm->id == fc->Mode_as )
		changeMode( fc, FCInitMode::inst() );
}

// ---------------------------------------------------------------------------
bool FCUnknownMode::activate( FC* fc )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": activate...(fc_mode=" << fc->in_Mode_as << ")" << endl;

	return true;
}
// ---------------------------------------------------------------------------
void FCUnknownMode::fcCommand( FC* fc, const FCMessage* m )
{
	switch( m->inf )
	{
		case FCMessage::Start:
		case FCMessage::Stop:
			break;

		case FCMessage::Reset:
			changeMode( fc, FCInitMode::inst() );
			break;

		default:
			break;
	}
}
// ---------------------------------------------------------------------------
