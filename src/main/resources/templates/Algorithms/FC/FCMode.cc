#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCMode* FCMode::_inst = 0;
// ---------------------------------------------------------------------------

FCMode::~FCMode()
{
}

FCMode::FCMode()
{
}

// ---------------------------------------------------------------------------

FCMode* FCMode::inst()
{
	if(_inst == 0)
		_inst =  new FCMode();

	return _inst;
}

// ---------------------------------------------------------------------------
void FCMode::changeMode( FC* fc, FCMode* m )
{
	fc->changeMode(m);
}
// ---------------------------------------------------------------------------
void FCMode::setProtection(FC* fc, bool p )
{
	fc->setProtection(p);
}
// ---------------------------------------------------------------------------
long FCMode::getRealValue(FC* fc, uniset::ObjectId id )
{
	return fc->ui->getValue(id);
}
// ---------------------------------------------------------------------------
void FCMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->RemoteControl_s )
	{
		if( !sm->value )
		{
			if( fc->log()->is_info() )
				fc->log()->info() << fc << ": RemoteControl OFF" << endl;

			changeMode( fc, FCOffControlMode::inst() );
		}
	}
	else if( sm->id == fc->Protection_s )
	{
		if( sm->value )
		{
			if( fc->log()->is_info() )
				fc->log()->info() << fc << ": Сигнал 'Неисправность ПЧ'(защита)" << endl;

			changeMode( fc, FCProtectionMode::inst() );
		}
	}
}

// ---------------------------------------------------------------------------
