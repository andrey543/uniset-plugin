#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCOffMode* FCOffMode::_inst = 0;
// ---------------------------------------------------------------------------

FCOffMode::~FCOffMode()
{
}

FCOffMode::FCOffMode()
{
}

// ---------------------------------------------------------------------------

FCOffMode* FCOffMode::inst()
{
	if(_inst == 0)
		_inst =  new FCOffMode();

	return _inst;
}

// ---------------------------------------------------------------------------

bool FCOffMode::activate( FC* fc )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": ..............activate.............." << endl;

	fc->askSensor(fc->Protection_s, UniversalIO::UIONotify);
	fc->askSensor(fc->Mode_as, UniversalIO::UIONotify);

	if( fc->CheckTime > 0 )
		fc->askTimer(FC::CheckTimer, fc->CheckTime);

	return true;
}
// ---------------------------------------------------------------------------
bool FCOffMode::disactivate( FC* fc )
{
	fc->askTimer(FC::CheckTimer, 0);
	return true;
}

// ---------------------------------------------------------------------------
void FCOffMode::fcCommand( FC* fc, const FCMessage* m )
{
	switch( m->inf )
	{
		case FCMessage::Start:
		{
			if( fc->log()->is_info() )
				fc->log()->info() << fc << "(fqcCommand): пуск ПЧ" << endl;

			changeMode( fc, FCTransientMode::inst(fc, FCMessage::OnMode, this));
		}
		break;

		case FCMessage::Stop:
			activate(fc);
			break;
	}
}

// ------------------------------------------------------------------------------------------
void FCOffMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->Mode_as )
	{
		if( sm->value != TEMPLATE_PROJECT::fcReady1 )
			changeMode( fc, FCInitMode::inst() );
	}
	else
		FCMode::sensorInfo(fc, sm);
}
// ------------------------------------------------------------------------------------------
void FCOffMode::timerInfo( FC* fc, const uniset::TimerMessage* tm )
{
	if( tm->id == FC::CheckTimer )
	{
		try
		{
			if( fc->log()->is_level3() )
				fc->log()->level3() << fc << "(imerInfo): ask mode_as..current=" << fc->in_Mode_as << endl;

			long now = getRealValue( fc, fc->Mode_as );

			if( fc->in_Mode_as != now || now != TEMPLATE_PROJECT::fcReady1 )
			{
				fc->askSensor(fc->Mode_as, UniversalIO::UIONotify);
				//changeMode( fc, FCInitMode::inst() );
			}
		}
		catch( std::exception& ex )
		{
			if( fc->log()->is_warn() )
				fc->log()->warn() << fc << "(timerInfo): " << ex.what() << endl;
		}
	}
}
// ------------------------------------------------------------------------------------------
