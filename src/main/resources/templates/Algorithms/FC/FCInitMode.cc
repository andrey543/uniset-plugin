#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCInitMode* FCInitMode::_inst = 0;
// ---------------------------------------------------------------------------
FCInitMode::~FCInitMode()
{
}

FCInitMode::FCInitMode()
{
}

// ---------------------------------------------------------------------------

FCInitMode* FCInitMode::inst()
{
	if(_inst == 0)
		_inst =  new FCInitMode();

	return _inst;
}

// ---------------------------------------------------------------------------

void FCInitMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->Protection_s )
	{
		if( sm->value )
			changeMode(fc, FCProtectionMode::inst());
	}
}

// ---------------------------------------------------------------------------

bool FCInitMode::activate( FC* fc )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": activate...(init state)" << endl;

	if( !fc->in_RemoteControl_s )
	{
		changeMode( fc, FCOffControlMode::inst() );
		return true;
	}

	if( fc->in_Protection_s )
	{
		changeMode( fc, FCProtectionMode::inst());
	}
	else
	{
		switch( fc->in_Mode_as )
		{
			case TEMPLATE_PROJECT::fcAlarm:
				changeMode( fc, FCProtectionMode::inst() );
				break;

			case TEMPLATE_PROJECT::fcReady1:
				changeMode( fc, FCOffMode::inst() );
				break;

			case TEMPLATE_PROJECT::fcReady2:
			case TEMPLATE_PROJECT::fcWorking:
			case TEMPLATE_PROJECT::fcStopped:
			case TEMPLATE_PROJECT::fcRotation:
				changeMode( fc, FCOnMode::inst() );
				break;

			default:
				changeMode( fc, FCUnknownMode::inst() );
				break;
		}
	}

	return true;
}
// ---------------------------------------------------------------------------
void FCInitMode::fcCommand( FC* fc, const FCMessage* m )
{
	switch( m->inf )
	{
		case FCMessage::Start:
			changeMode( fc, FCTransientMode::inst(fc, FCMessage::OnMode) );
			break;

		case FCMessage::Stop:
			changeMode( fc, FCTransientMode::inst(fc, FCMessage::OffMode) );
			break;
	}
}
// ---------------------------------------------------------------------------
