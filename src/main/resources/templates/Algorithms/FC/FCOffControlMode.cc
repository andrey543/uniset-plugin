#include "FCMode.h"
#include "FC.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// ---------------------------------------------------------------------------
FCOffControlMode* FCOffControlMode::_inst = 0;
// ---------------------------------------------------------------------------
FCOffControlMode::~FCOffControlMode()
{
}

FCOffControlMode::FCOffControlMode()
{
}

// ---------------------------------------------------------------------------

FCOffControlMode* FCOffControlMode::inst()
{
	if(_inst == 0)
		_inst =  new FCOffControlMode();

	return _inst;
}

// ---------------------------------------------------------------------------

void FCOffControlMode::sensorInfo( FC* fc, const uniset::SensorMessage* sm )
{
	if( sm->id == fc->RemoteControl_s )
	{
		if( sm->value )
			changeMode( fc, FCInitMode::inst() );
	}
}

// ---------------------------------------------------------------------------
bool FCOffControlMode::activate( FC* fc )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": activate...(init state)" << endl;

	fc->out->set(fc->Off_c, false);
	fc->out->set(fc->On_c, false);
	return true;
}
// ---------------------------------------------------------------------------
void FCOffControlMode::fcCommand( FC* fc, const FCMessage* m )
{
	if( fc->log()->is_info() )
		fc->log()->info() << fc  << ": ignore command...(RemoteControl=0)" << endl;
}
// ---------------------------------------------------------------------------
