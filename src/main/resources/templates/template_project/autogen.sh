#! /bin/sh

GUI_CFLAGS= 
GUI_LIBS=
PROJECT_PATH=/home/$USER/packages
GUI_PATH=$PROJECT_PATH/uniwidgets
INCGUI_PATH=$GUI_PATH
SRCGUI_PATH=$GUI_PATH/lib
GUI_PATH2=$PROJECT_PATH/setwidgets
INCGUI_PATH2=$GUI_PATH2
SRCGUI_PATH2=$GUI_PATH2/lib

ExportGUI()
{
	GUI_CFLAGS="-I$INCGUI_PATH -I$INCGUI_PATH/include -I$INCGUI_PATH/uniwidgets -I$INCGUI_PATH2 -I$INCGUI_PATH2/include -I$INCGUI_PATH2/uniwidgets"
	GUI_LIBS="-L$SRCGUI_PATH/.libs -lUniWidgets2 -L$SRCGUI_PATH2/.libs -lSetWidgets2"
}

# if local arg, build with libraries from git tree
if [ "$1" = "local" ] ; then
	shift
	ExportGUI
fi

echo $GUI_CFLAGS
echo $GUI_LIBS
#export CFLAGS='-pipe -O2 -march=i586 -mtune=i686'
export GUI_CFLAGS="$GUI_CFLAGS"
export GUI_LIBS="$GUI_LIBS"

# We run just autoreconf, updates all needed
autoreconf -fiv

./configure --enable-maintainer-mode --prefix=/usr $*
exit 0
