<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="text" indent="yes" encoding="utf-8"/>

<xsl:template match="//sensors">
"","","Файл сгенерирован на основе <xsl:value-of select="$FILENAME"/>","Время: <xsl:value-of select="date:date()"/>"
"АПС","ID","Название","Текстовое название"
<xsl:for-each select="//sensors/item">
<xsl:sort select="@aps" order="ascending" data-type="text"/>
<xsl:if test="normalize-space(@aps)!=''">
"<xsl:value-of select="@aps"/>","<xsl:value-of select="@id"/>","<xsl:value-of select="@name"/>","<xsl:value-of select="@textname"/>"<xsl:text>
</xsl:text>
</xsl:if>
</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
