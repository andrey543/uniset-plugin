<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="text" indent="yes" encoding="utf-8"/>

<xsl:template match="//sensors">

<xsl:for-each select="//sensors/item">
<xsl:sort select="@tcp_mbreg" order="ascending" data-type="number"/>

<xsl:if test="normalize-space(@CAN2sender) = $SYS">
<xsl:value-of select="@id"/>
<xsl:text>          </xsl:text><xsl:value-of select="@tcp_mbaddr"/>
<xsl:text>          </xsl:text><xsl:value-of select="@tcp_mbreg"/>
<xsl:text>          </xsl:text><xsl:value-of select="@name"/><xsl:text>
</xsl:text>
</xsl:if>

</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
