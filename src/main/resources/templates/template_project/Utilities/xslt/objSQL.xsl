<?xml version='1.0' encoding="utf-8" ?>
<xsl:stylesheet  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
	             xmlns:date="http://exslt.org/dates-and-times">

<xsl:output method="text" indent="yes" encoding="utf-8"/>

<xsl:template match="/">

<xsl:for-each select="//nodes/item">
<xsl:call-template name="out"/>
</xsl:for-each>

<xsl:for-each select="//sensors/item">
<xsl:call-template name="out"/>
</xsl:for-each>

<xsl:for-each select="//controllers/item">
<xsl:call-template name="out"/>
</xsl:for-each>

<xsl:for-each select="//services/item">
<xsl:call-template name="out"/>
</xsl:for-each>

<xsl:for-each select="//objects/item">
<xsl:call-template name="out"/>
</xsl:for-each>

</xsl:template>

<xsl:template name="out">INSERT IGNORE INTO ObjectsMap(name,rep_name,id,msg,act) VALUES("<xsl:value-of select="@textname"/>","<xsl:value-of select="@name"/>","<xsl:value-of select="number(@id)"/>","<xsl:call-template name="msg"/>","<xsl:call-template name="act"/>");<xsl:text>
</xsl:text>
</xsl:template>


<xsl:template name="msg">
<xsl:if test="normalize-space(@mtype)!=''"><xsl:value-of select="1"/></xsl:if>
<xsl:if test="count(./MessagesList/*)>0"><xsl:value-of select="1"/></xsl:if>
</xsl:template> 


<xsl:template name="act">
<xsl:if test="normalize-space(@action)!=''"><xsl:value-of select="1"/></xsl:if>
<xsl:if test="normalize-space(@action)=''"><xsl:value-of select="0"/></xsl:if>
</xsl:template> 

</xsl:stylesheet>
