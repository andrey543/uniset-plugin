#!/bin/sh

ARGS=$*
COMMON_DIR="./src/"

[[ $ARGS == *"-c"* || $ARGS == *"--create"* ]] && $COMMON_DIR/Services/Administrator/create

RUN="
GUI/start_fg.sh \
Imitators/Im_DRK/start_fg_drk1.sh \
Algorithms/ED/start_fg_ed1.sh \
Algorithms/GEU/start_fg_geu1.sh \
"

konsole --new-tab --workdir "${COMMON_DIR}GUI" -e "${COMMON_DIR}SharedMemory/start_fg.sh"&
sleep 5

for R in $RUN
do
    D=${R%/*}
    konsole --new-tab --workdir "${COMMON_DIR}${D}" -e "${COMMON_DIR}${R}"&
    sleep 0.3
done

konsole --new-tab --workdir "./src/Services/Administrator"
konsole --new-tab --workdir "./src/Services/TSViewer"
