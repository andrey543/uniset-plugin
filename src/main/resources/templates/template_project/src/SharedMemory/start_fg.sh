#!/bin/sh

ulimit -Sc 100000000
START=uniset2-start.sh

${START} -f uniset2-smemory --smemory-id SharedMemory1 \
--heartbeat-node gru1 --e-filter evnt_gru1 \
--dlog-add-levels info,warn,crit \
--pulsar-id pulsar \
--pulsar-msec 3000 \
$*
#--ffunideb-add-levels warn,crit,info,system,level1,level2,level3 \
