#!/bin/sh

ulimit -Sc 10000000
START=uniset2-start.sh

PERSIST_CON="1"

EXTPARAM=" \
     $*"

${START} -f ./smemory2-TEMPLATE_PROJECT $EXTPARAM --dlog-add-levels info,warn,crit --smemory-id SharedMemory1 \
 --SharedMemory1-log-add-levels any  --pulsar-id pulsar --pulsar-msec 3000 \
 $*
#--e-filter evnt_ses1
