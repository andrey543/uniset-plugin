#include "Smemory.h"

using namespace std;
using namespace uniset;
using namespace uniset::extensions;
using namespace TEMPLATE_PROJECT;

Smemory::Smemory(const std::shared_ptr<uniset::Configuration> conf,
        const std::shared_ptr<uniset::UniSetActivator> act,
        const std::shared_ptr<uniset::SharedMemory> shm):
        conf(conf),
        act(act),
        shm(shm)
{

}

Smemory::~Smemory() {

}

void Smemory::init_mbm(int argc, const char** argv, bool modbus_check_off) {
    // ------------- MBTCPMaster --------------
    std::vector<std::string> mbm_vector = get_modbus_names(argc, argv, "--add-mbm[^m].*");
    if(!mbm_vector.empty()) {
        double progress = 0;
        for (auto &&mbm_str: mbm_vector) {



            cout << flush << "\r";
            std::string title = "[MBMM LOADING " + std::to_string((int)(round((++progress / mbm_vector.size()) * 100))) + "%] ";

            if (!modbus_check_off) {

                const std::string arg_mbs = "--mbm" + mbm_str;
                const std::string name_value = uniset::getArgParam(arg_mbs + "-name", argc, argv, "");

                title = "[CHECKING]" + title;

                if (!is_correct_modbus(argc, argv, conf, mbm_str, name_value, "--mbm")) {
                    cout << "(smemory2): "<< title;
                    dcrit << "(smemory2): " << name_value << " will not created..." << std::endl;
                    continue;
                }
            }

            auto mbm = MBTCPMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbm" + mbm_str);
            if (!mbm) {
                cout << "Ошибка при создании MBM_" << mbm_str << endl;
            }
            else
                act->add(mbm);

            cout << "(smemory2): "<< title;
        }
        cout << endl;
    }

}

void Smemory::init_mmbm(int argc, const char** argv, bool modbus_check_off) {
    // ------------- MBTCPMultiMasters --------------
    std::vector<std::string> mbmm_vector = get_modbus_names(argc, argv, "--add-mbmm.*");
    if(!mbmm_vector.empty()) {
        double progress = 0;
        for (auto &&mbmm_str: mbmm_vector) {

            cout << flush << "\r";
            std::string title = "[MBM LOADING " + std::to_string((int)(round((++progress / mbmm_vector.size()) * 100))) + "%] ";

            if (!modbus_check_off) {

                const std::string arg_mbs = "--mbmm" + mbmm_str;
                const std::string name_value = uniset::getArgParam(arg_mbs + "-name", argc, argv, "");

                title = "[CHECKING]" + title;

                if (!is_correct_modbus(argc, argv, conf, mbmm_str, name_value, "--mbmm")) {
                    cout << "(smemory2): "<< title;
                    dcrit << "(smemory2): " << name_value << " will not created..." << std::endl;
                    continue;
                }
            }
            auto mbmm = MBTCPMultiMaster::init_mbmaster(argc, argv, shm->getId(), shm, "mbmm" + mbmm_str);
            if (!mbmm) {
                cout << "Ошибка при создании MBMM_" << mbmm_str << endl;
            } else
                act->add(mbmm);

            cout << "(smemory2): "<< title;
        }
        cout << endl;
    }
}

void Smemory::init_mbs(int argc, const char** argv, bool modbus_check_off) {
    // ------------- MBSlave --------------
    const std::vector<std::string> mbs_vector = get_modbus_names(argc, argv, "--add-mbs.*");
    if(!mbs_vector.empty()) {

        double progress = 0;
        for (auto &&mbs_str: mbs_vector) {

            cout << flush << "\r";
            std::string title = "[MBS LOADING " + std::to_string((int)(round((++progress / mbs_vector.size()) * 100))) + "%] ";

            if (!modbus_check_off) {
                const std::string arg_mbs = "--mbs" + mbs_str;
                const std::string name_value = uniset::getArgParam(arg_mbs + "-name", argc, argv, "");

                title = "[CHECKING]" + title;

                if (!is_correct_modbus(argc, argv, conf, mbs_str, name_value, "--mbs")) {
                    cout << "(smemory2): "<< title;
                    dcrit << "(smemory2): " << name_value << " will not created..." << std::endl;
                    continue;
                }
            }

            auto mbs = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs" + mbs_str);
            if (!mbs) {
                dcrit << "Ошибка при создании MBSlave " << mbs_str << endl;
            } else
                act->add(mbs);

            cout << "(smemory2): "<< title;
        }
        cout << endl;
    }

}

bool Smemory::match(const std::string &str, const std::string &regex)
{
    const std::regex re(regex);
    std::smatch match;

    return std::regex_search(str, match, re);
}

std::vector<std::string> Smemory::get_arguments_by_substring(int argc, const char** argv, const std::string &regex) {
    std::vector<std::string> vector;
    while (--argc >= 0) {
        const std::string &next_str = argv[argc];
        if (match(next_str, regex))
            vector.push_back(next_str);
    }

    return vector;
}

std::vector<std::string> Smemory::get_modbus_names(int argc, const char** argv, const std::string &regex)
{
    std::vector<std::string> names;
    for (auto && mbmm_str: get_arguments_by_substring(argc, argv, regex)) {
        mbmm_str = uniset::replace_all(mbmm_str, "--add-mbmm", "");
        mbmm_str = uniset::replace_all(mbmm_str, "--add-mbm", "");
        mbmm_str = uniset::replace_all(mbmm_str, "--add-mbs", "");
        names.push_back(mbmm_str);
    }

    return names;
}

vector<pair<const std::string, vector<pair<const std::string, const std::string>>>> Smemory::find_duplicates(
        vector<UniXMLPropList> sensors,
const std::string &prefix,
const std::string &name,
const vector<std::string> &additional_props,
const vector<pair<const string, const string>> &props){
    vector<pair<const std::string, vector<pair<const std::string, const std::string>>>> duplicates;
    for(int i = 0; i < sensors.size(); i++)
    {
        std::string _name;
        vector<pair<const string, const string>> _props;
        for(auto j = 0; j < sensors[i].size(); j++)
        {
            if(_name.empty() && sensors[i][j].first == "name") {
                if(sensors[i][j].second != name)
                    _name = sensors[i][j].second;
            }
            else if(match(sensors[i][j].first, prefix + ".*"))
                _props.push_back(sensors[i][j]);
            else {
                for(const std::string &a: additional_props)
                    if(match(sensors[i][j].first, a + ".*"))
                        _props.push_back(sensors[i][j]);
            }
        }

        if(_name.empty())
            continue;

        bool isFieldEquals = true;
        for(auto it = props.begin(); it != props.end(); it++)
        {
            bool isFound = false;
            for(auto _it = _props.begin(); _it != _props.end(); _it++)
            {
                if(it->first == _it->first)
                    isFound = true;
            }

            if(!isFound) {
                isFieldEquals = false;
                break;
            }
        }

        if(!isFieldEquals)
            continue;

        if(props.size() == _props.size())
        {
            bool isEquals = true;
            for(auto it = props.begin(); it != props.end(); it++)
            {
                for(auto _it = _props.begin(); _it != _props.end(); _it++)
                {
                    if(it->first == _it->first)
                    {
                        if(it->second != _it->second)
                            isEquals = false;
                        break;
                    }
                }
                if(!isEquals)
                    break;
            }

            if(isEquals){
                pair<std::string, vector<pair<const std::string, const std::string>>> p;
                p.first = _name;
                for(int i = 0; i < props.size(); i++)
                {
                    pair<std::string, std::string> pp;
                    pp.first  = props[i].first;
                    pp.second = props[i].second;

                    p.second.emplace_back(pp);
                }

                duplicates.emplace_back(p);
            }

        }
    }

    return duplicates;
}

vector<UniXMLPropList> Smemory::find_list(
        const std::string &name,
        const vector<pair<const std::string, vector<pair<const std::string, const std::string>>>> &duplicates,
        vector<UniXMLPropList> sensors){
    vector<UniXMLPropList> list;
    for(int i = 0; i < sensors.size(); i++)
    {
        bool isNeedToAdd = true;
        for(int j = 0; j < sensors[i].size(); j++)
        {
            if(sensors[i][j].first == "name" && sensors[i][j].second == name)
            {
                isNeedToAdd = false;
                break;
            }
        }
        if(!isNeedToAdd)
            continue;

        std::string prop_name;
        for(int j = 0; j < sensors[i].size(); j++)
        {
            if(sensors[i][j].first == "name") {
                prop_name = sensors[i][j].second;
                break;
            }
        }
        if(prop_name.empty())
            continue;

        for(auto dup_prop = duplicates.begin(); dup_prop != duplicates.end(); dup_prop++ )
        {
            if(prop_name == (*dup_prop).first) {
                isNeedToAdd = false;
                break;
            }
        }

        if(isNeedToAdd)
            list.push_back(sensors[i]);
    }

    return list;
}

bool Smemory::is_correct_props(const std::string &mbs_name, vector<UniXMLPropList> sensors, const std::string &prefix)
{

    vector<std::string> additional_props;
    additional_props.emplace_back("ioinvert");
    vector<pair<const std::string, vector<pair<const std::string, const std::string>>>> duplicates;
    for(int i = 0; i < sensors.size(); i++)
    {
        std::string name;
        vector<pair<const string, const string>> props;
        for(int j = 0; j < sensors[i].size(); j++)
        {
            if(name.empty() && sensors[i][j].first == "name")
                name = sensors[i][j].second;
            else if(match(sensors[i][j].first, prefix + ".*"))
                props.push_back(sensors[i][j]);
            else {
                for(const std::string &a: additional_props)
                    if(match(sensors[i][j].first, a + ".*"))
                        props.push_back(sensors[i][j]);
            }
        }
        vector<UniXMLPropList> list = find_list(name, duplicates, sensors);
        vector<pair<const std::string, vector<pair<const std::string, const std::string>>>> dups = find_duplicates(list, prefix, name, additional_props, props);
        if(!dups.empty()) {
            pair<std::string, vector<pair<const std::string, const std::string>>> p(name, props);
            duplicates.emplace_back(p);
            for (const auto &dup : dups) {
                duplicates.push_back(dup);
            }
        }
    }

    if(!duplicates.empty())
    {
        dcrit << "(smemory2): " << mbs_name << " has sensors with same props:" << std::endl;
        for(int i = 0; i < duplicates.size(); i++)
        {
            std::string p;
            vector<pair<const std::string, const std::string>> props = duplicates[i].second;
            for(int j = 0; j < props.size(); j++)
            {
                p += props[j].first + "=\"" + props[j].second + "\" ";
            }

            dcrit << "(smemory2): " << mbs_name <<
                  ": Sensor [name=" << duplicates[i].first << "]: " + p << std::endl;

        }

        return false;
    }

    return true;
}

bool Smemory::has_modbus(const std::shared_ptr<Configuration> &conf, const std::string &name)
{

    bool isFoundInSettings = false;
    UniXML::iterator it_settings(conf->getNode("settings"));
    if(!it_settings.goChildren()) {
        std::string err_msg = "(smemory2): Configure section the settings is empty or corrupted";
        dcrit << err_msg << std::endl;
        throw uniset::SystemError(err_msg);
    }
    else {
        for(; it_settings.getCurrent(); it_settings++)
        {
            const std::string tag_name   = it_settings.getName();

            if(tag_name == name) {
                const std::string name_value = it_settings.getProp("name");
                if(name_value == name) {
                    isFoundInSettings = true;
                    break;
                }
            }
        }
    }

    if(!isFoundInSettings)
        return false;

    bool isFoundInObjects = false;
    UniXML::iterator it_objects(conf->getNode("objects"));
    if(!it_objects.goChildren()) {
        std::string err_msg = "(smemory2): Configure section the settings is empty or corrupted";
        dcrit << err_msg << std::endl;
        throw uniset::SystemError(err_msg);
    }
    else {
        for(; it_objects.getCurrent(); it_objects++)
        {
            if(it_objects.getProp("name") == name) {
                isFoundInObjects = true;
                break;
            }
        }
    }

    return isFoundInObjects;
}

bool Smemory::is_correct_modbus(int argc, const char **argv, const std::shared_ptr<Configuration> &conf,
                                const std::string &name, const std::string &name_value, const std::string &mbprefix)
{
    // Проверка аргумента --mb-name
    if(name_value.empty()) {
        dcrit << "(smemory2): Modbus " << name << " has not specified name arg..." << std::endl;
        return false;
    }

    // Проверяем наличие у данного Modbus в configure.xml
    if(!has_modbus(conf, name_value)){
        dcrit << "(smemory2): " << name_value << " is not found in configure.xml..." << std::endl;
        return false;
    }

    // Проверка аргумента --mb-filter-field
    const std::string arg_modbus = mbprefix + name;
    const std::string filter_field = uniset::getArgParam(arg_modbus + "-filter-field", argc, argv, "");
    if(filter_field.empty()) {
        dcrit << "(smemory2): " << name_value << " has not specified filter-field arg..." << std::endl;
        return false;
    }

    // Проверка аргумента --mb-filter-value
    std::string filter_value = uniset::getArgParam(arg_modbus + "-filter-value", argc, argv, "");
    if(filter_value.empty()) {
        dcrit << "(smemory2): " << name_value << " has not specified filter-value arg..." << std::endl;
        return false;
    }

    std::string prefix = uniset::getArgParam(arg_modbus + "-set-prop-prefix", argc, argv, "");
    if(prefix.empty()) prefix = filter_field + "_";


    // Отбираем у данного Modbus датчики на основе filter-field и filter-value
    vector<UniXMLPropList> sensors;
    UniXML::iterator it(conf->getNode("sensors"));
    if(!it.goChildren()) {
        std::string err_msg = "(smemory2): Configure section the sensors is empty or corrupted";
        dcrit << err_msg << std::endl;
        throw uniset::SystemError(err_msg);
    }
    else {
        for(; it.getCurrent(); it++)
        {
            const std::string &next = it.getProp(filter_field);
            if(!next.empty() && next == filter_value) {
                sensors.push_back(it.getPropList());
            }
        }
    }

    // Если у данного Modbus нету датчиков то его создание некорректно
    if(sensors.empty()){
        dcrit << "(smemory2): " << name_value << " has an empty iomap..." << std::endl;
        return false;
    }

    // Проверяем коррекность свойств данного Modbus
    return is_correct_props(name_value, sensors, prefix);

}
