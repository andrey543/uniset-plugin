#ifndef TEMPLATE_PROJECT_SMEMORY_H
#define TEMPLATE_PROJECT_SMEMORY_H


#include <string>
#include <vector>
#include <MBSlave.h>
#include <MBTCPMaster.h>
#include <MBTCPMultiMaster.h>
#include <UniSetActivator.h>
#include <extensions/Extensions.h>
#include <TEMPLATE_PROJECTConfiguration.h>

class Smemory {
public:
    Smemory(const std::shared_ptr<uniset::Configuration> conf, const std::shared_ptr<uniset::UniSetActivator> act, const std::shared_ptr<uniset::SharedMemory> shm);
    virtual ~Smemory();
    void init_mbm(int argc, const char** argv, bool modbus_check_off = false);
    void init_mmbm(int argc, const char** argv, bool modbus_check_off = false);
    void init_mbs(int argc, const char** argv, bool modbus_check_off = false);

private:
    bool match(const std::string &str, const std::string &regex);
    std::vector<std::string> get_arguments_by_substring(int argc, const char** argv, const std::string &regex);
    std::vector<std::string> get_modbus_names(int argc, const char** argv, const std::string &regex);
    std::vector<std::pair<const std::string, std::vector<std::pair<const std::string, const std::string>>>> find_duplicates(
            std::vector<uniset::UniXMLPropList> sensors,
            const std::string &prefix,
            const std::string &name,
            const std::vector<std::string> &additional_props,
            const std::vector<std::pair<const std::string, const std::string>> &props);
    std::vector<uniset::UniXMLPropList> find_list(
            const std::string &name,
            const std::vector<std::pair<const std::string, std::vector<std::pair<const std::string, const std::string>>>> &duplicates,
            std::vector<uniset::UniXMLPropList> sensors);
    bool is_correct_props(const std::string &mbs_name, std::vector<uniset::UniXMLPropList> sensors, const std::string &prefix);
    bool has_modbus(const std::shared_ptr<uniset::Configuration> &conf, const std::string &name);
    bool is_correct_modbus(int argc, const char **argv, const std::shared_ptr<uniset::Configuration> &conf,
                               const std::string &name, const std::string &name_value, const std::string &mbprefix);

private:
    const std::shared_ptr<uniset::Configuration> conf;
    const std::shared_ptr<uniset::UniSetActivator> act;
    const std::shared_ptr<uniset::SharedMemory> shm;

};


#endif //TEMPLATE_PROJECT_SMEMORY_H
