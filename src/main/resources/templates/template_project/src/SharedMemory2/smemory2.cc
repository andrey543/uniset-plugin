// --------------------------------------------------------------------------
#include <string>
#include <sys/wait.h>
#include <Debug.h>
#include <UniSetActivator.h>
#include <ThreadCreator.h>
#include <Extensions.h>
#include <MBSlave.h>
#include <MBTCPMaster.h>
#include <MBTCPMultiMaster.h>
#include <SharedMemory.h>
#include <UNetExchange.h>
#include <extensions/logicproc/PassiveLProcessor.h>
#include <TEMPLATE_PROJECTConfiguration.h>
#include "Smemory.h"
// --------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace uniset::extensions;
using namespace TEMPLATE_PROJECT;
// --------------------------------------------------------------------------
static void help_print( int argc, const char* argv[] );

// --------------------------------------------------------------------------
int main( int argc, const char** argv )
{
    bool help             = findArgParam("--help", argc, argv) != -1 ||
                            findArgParam("--h", argc, argv) != -1;
    if( help )
    {
        help_print( argc, argv);
        return 0;
    }

    bool add_unet         = findArgParam("--add-unet", argc, argv) != -1;
    bool modbus_check_off = findArgParam("--modbus-check-off",argc,argv) != -1;

	try
	{
		auto conf = uniset_init(argc, argv);

		auto act = UniSetActivator::Instance();
		// ------------ SharedMemory ----------------
		auto shm = SharedMemory::init_smemory(argc, argv);

		if( !shm )
			return 1;

		act->add(shm);

		Smemory smemory(conf, act, shm);

		// ------------- UNetExchange 1 --------------
		if( add_unet )
		{
			auto unet = UNetExchange::init_unetexchange(argc, argv, shm->getId(), shm, "unet");

			if( !unet )
				return 1;

			act->add(unet);
		}
		// ------------- Modbus exchange --------------
        smemory.init_mmbm(argc, argv);
        smemory.init_mbm(argc, argv);
        smemory.init_mbs(argc, argv, modbus_check_off);

		// -------------------------------------------

		SystemMessage sm(SystemMessage::StartUp);
		act->broadcast( sm.transport_msg() );

		act->run(false);

		while( waitpid(-1, 0, 0) > 0 );

		return 0;
	}
	catch(Exception& ex)
	{
		dcrit << "(smemory2): " << ex << endl;
	}
	catch( CORBA::SystemException& ex )
	{
		dcrit << "(smemory2): " << ex.NP_minorString() << endl;
	}
	catch(...)
	{
		dcrit << "(smemory2): catch(...)" << endl;
	}

	while( waitpid(-1, 0, 0) > 0 );

	return 1;
}

// --------------------------------------------------------------------------
void help_print( int argc, const char* argv[])
{
    cout << "\n   ###### smemory options ###### \n" << endl;

    cout << "--add-mbsXXX       - Start ModbusSlave  (--mbsXXX)" << endl;
    cout << "--add-mbmXXX       - Start MBTCPMaster  (--mbmXXX)" << endl;
    cout << "--add-mbmmXXX      - Start MBTCPMaster2 (--mbmmXXX)" << endl;
    cout << "--add-unet         - Start UNetNetwork  (--unet-xxx)" << endl;
    cout << "--modbus-check-off - Turn off validation of modbus" << endl;

    cout << "\n   ###### SM options ###### \n" << endl;
    SharedMemory::help_print(argc, argv);
}
