#include <gtkmm.h>
#include <gladeui/glade.h>

#include <UDefaultFunctions.h>
#include <plugins.h>

extern "C" void TEMPLATE_PROJECT_init()
{
	Gtk::Main::init_gtkmm_internals();
}
//----------------------------------------------------------------------------------
