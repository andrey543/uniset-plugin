#include "TEMPLATE_PROJECTConfiguration.h"
#include "extensions/SharedMemory.h"
#include <MainWindow.h>
#include "UniSetActivator.h"
#include <UniSetTypes.h>
#include <extensions/Extensions.h>
#include <dynamic_libs.h>
#include "Main.h"

using namespace std;
using namespace TEMPLATE_PROJECT;
using namespace uniset;
using namespace uniset::extensions;

int main (int argc, const char** argv)
{
	uniset_init(argc, (const char**)(argv) );
	auto conf = uniset::uniset_conf();

	Main Kit(argc, (char**&)argv);

	try
	{
		string logfilename = conf->getArgParam("--logfile", "gui.log");
		string logname( conf->getLogDir() + logfilename );
		conf->initLogStream(ulog(), "dlog");
		uniset::extensions::dlog()->logFile( logname );
		ulog()->logFile( logname );
		string theme = conf->getArgParam( "--gtkthemedir", "/usr/share/TEMPLATE_PROJECT/theme/");

		if ( !theme.empty() )
		{
			gtk_rc_parse( (theme + "gtkrc").c_str() );
		}

		ObjectId ID(DefaultObjectId);
		string name = conf->getArgParam("--name", "GUI");
		ID = conf->getObjectID(name);

		if( ID == uniset::DefaultObjectId )
		{
			cerr << "(main): идентификатор '" << name
				 << "' не найден в конф. файле!"
				 << " в секции " << conf->getObjectsSection() << endl;
			return 0;
		}

		string gladedir = conf->getArgParam("--gladedir", "/usr/share/TEMPLATE_PROJECT/glade/" );
		string guifile  = conf->getArgParam("--guifile", "gui.glade" );
		string svgdir   = conf->getArgParam("--svgdir", "/usr/share/TEMPLATE_PROJECT/svg/" );
		Glib::ustring screen = conf->getArgParam( "--screen", "" );
		auto act = UniSetActivator::Instance();

		MainWindow* mw = MainWindow::Instance(gladedir, guifile, svgdir, screen, act);

		Kit.run(*mw->w);

		return 0;
	}
	catch( uniset::SystemError& ex )
	{
		ucrit << "(GUI::main): " << ex << endl;
	}
	catch( uniset::Exception& ex )
	{
		ucrit << "(GUI::main): " << ex << endl;
	}
	catch(CORBA::NO_IMPLEMENT)
	{
		ucrit << "(GUI:main): CORBA::NO_IMPLEMENT..." << endl;
	}
	catch(CORBA::OBJECT_NOT_EXIST)
	{
		ucrit << "(GUI:main): CORBA::OBJECT_NOT_EXIST..." << endl;
	}
	catch(CORBA::COMM_FAILURE& ex)
	{
		ucrit << "(GUI:main): CORBA::COMM_FAILURE..." << endl;
	}
	catch(CORBA::SystemException& ex)
	{
		ucrit << "(GUI:main): (CORBA::SystemException): "
							   << ex.NP_minorString() << endl;
	}
	catch ( omniORB::fatalException& fe )
	{
		ucrit << "(GUI::main): поймали omniORB::fatalException:" << endl;
		ucrit << "  file: " << fe.file() << endl;
		ucrit << "  line: " << fe.line() << endl;
		ucrit << "  mesg: " << fe.errmsg() << endl;
	}
	catch( Glib::Error& ex )
	{
		ucrit << "(GUI::main): " << ex.what() << endl;
	}
	catch( std::exception& ex )
	{
		ucrit << "(GUI::main): " << ex.what() << endl;
	}
	catch(...)
	{
		ucrit << "(GUI::main): catch ..." << endl;
	}

	return 1;

}
