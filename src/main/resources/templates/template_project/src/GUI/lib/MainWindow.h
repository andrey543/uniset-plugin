#ifndef _MainWindow_H_
#define _MainWindow_H_
// ------------------------------------------------------------------------------
#include <gtkmm.h>
#include "UniSetTypes.h"
#include "UniSetActivator.h"
//#include "GladeTemplate.h"
#include "Clock.h"
#include "Menu.h"
#include "AsyncScript.h"
#include "ConfigWindowLogic.h"
#include "ControlWindowLogic.h"
#include "AutomatWindowLogic.h"
#include "SelectWindowLogic.h"
#include "SpinWindowLogic.h"
#include "RangeWindowLogic.h"
#include "ScaleWindowLogic.h"
// ------------------------------------------------------------------------------
namespace TEMPLATE_PROJECT
{
	class MainWindow;
	extern MainWindow* gwindow;

	class MainWindow
	{
		public:
			static MainWindow* Instance(std::string gladedir, std::string guifile, std::string svgdir, Glib::ustring screen = "", uniset::UniSetActivatorPtr act = nullptr);
			inline ConnectorRef get_connector(){return connector_;};
			Gtk::Window* w;
			void activate();
			Glib::RefPtr<Gnome::Glade::Xml> get_gxml();
			static void print_help(const std::string& file);
			
		protected:
			MainWindow(std::string gladedir, std::string guifile, std::string svgdir, Glib::ustring screen = "", uniset::UniSetActivatorPtr act = nullptr);
			MainWindow();
			~MainWindow();
			virtual void on_show();

		private:
			friend class ConfigWindowLogic;
			Glib::RefPtr<Gnome::Glade::Xml> gxml;
			int get_page(const std::string& file);
			void on_xml_created(Glib::RefPtr<Gnome::Glade::Xml> gxml, const std::string& file);
			void draw_splash(const double& perc);
			std::string getSaveDir();
			/* Установить время*/
			static void set_time(std::string time);
			/* Сделать скриншот */
			static void screenshot();

			double progress;
			Gtk::Window splash;
			Gtk::Alignment alignment;
			Gtk::ProgressBar bar;
			ConnectorRef connector_;
			std::shared_ptr<Clock> clock;
			std::shared_ptr<Menu> menu;
			AsyncScript clockscript;
			std::string savedir;
			struct Logics
			{
				ConfigWindowLogic config;
				ControlWindowLogic control;
				AutomatWindowLogic automat;
				SelectWindowLogic select;
				SpinWindowLogic spin;
				RangeWindowLogic range;
				ScaleWindowLogic scale;
			} logics;
	};
}
// ------------------------------------------------------------------------------
#endif // _MainWindow_H_
// ------------------------------------------------------------------------------
