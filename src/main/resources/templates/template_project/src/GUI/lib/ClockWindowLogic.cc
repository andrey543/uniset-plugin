#include "ClockWindowLogic.h"
#include <glade/glade.h>
#include "Clock.h"
#include <iomanip>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
void ClockWindowLogic::on_show(UWindow* window)
{
	SpinWindowLogic::on_show(window);
	std::shared_ptr<Clock> clock = Clock::Instance();
	if( clock )
	{
		add_spin(window, "hours", clock->get_tm()->tm_hour, 0, 23);
		add_spin(window, "minutes", clock->get_tm()->tm_min, 0, 59);
		add_spin(window, "seconds", clock->get_tm()->tm_sec, 0, 59);
		add_spin(window, "year", clock->get_tm()->tm_year + 1900, 2015, 3015);
		add_spin(window, "month", clock->get_tm()->tm_mon + 1, 1, 12);
		add_spin(window, "day", clock->get_tm()->tm_mday, 1, 31);
		update_widget(window, "hours");
		update_widget(window, "minutes");
		update_widget(window, "seconds");
		update_widget(window, "year");
		update_widget(window, "month");
		update_widget(window, "day");
	}
	signal_show_.emit();
}
// -------------------------------------------------------------------------
void ClockWindowLogic::on_action(UWindow* window, std::string name, long value)
{
	if( name == OK )
	{
		if( value == 0 )
		{
			stringstream out;
			out.width(2);
			out.fill('0');
			out << window->variables["command"] << " ";
			out << setfill('0') << setw(2) << spins[window]["hours"].value << " ";
			out << setfill('0') << setw(2) << spins[window]["minutes"].value  << " ";
			out << setfill('0') << setw(2) << spins[window]["seconds"].value  << " ";
			out << setfill('0') << setw(2) << spins[window]["day"].value  << " ";
			out << setfill('0') << setw(2) << spins[window]["month"].value << " ";
			out << setfill('0') << setw(2) << spins[window]["year"].value;
			signal_set_time_.emit( out.str() );
		}
	}
	else
		SpinWindowLogic::on_action(window, name, value);
}