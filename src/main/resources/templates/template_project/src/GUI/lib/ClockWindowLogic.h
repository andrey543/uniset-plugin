#ifndef _ClockWindowLogic_H
#define _ClockWindowLogic_H

#include <SpinWindowLogic.h>

namespace TEMPLATE_PROJECT
{
	class ClockWindowLogic:
		public SpinWindowLogic
	{
		public:
			ClockWindowLogic(){};
			virtual ~ClockWindowLogic(){};
			inline sigc::signal<void, std::string>& signal_set_time(){ return signal_set_time_; };
			inline sigc::signal<void>& signal_show(){ return signal_show_; };
		
		protected:
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void on_action( SetWidgets::UWindow* window, std::string name, long value ) override;
			
		private:
			sigc::signal<void, std::string> signal_set_time_;
			sigc::signal<void> signal_show_;
	};
}

#endif /* _ClockWindowLogic_H */
