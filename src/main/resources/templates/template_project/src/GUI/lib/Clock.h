#ifndef _CLOCK_H
#define _CLOCK_H

#include <gtkmm.h>
#include <libglademm.h>
#include <sys/time.h>
#include <sigc++/sigc++.h>
#include <usvgwidgets/USVGText.h>
#include <Configuration.h>

namespace TEMPLATE_PROJECT
{
	/*! Класс управляет настройкой времени */
	class Clock
	{
		public:
			static std::shared_ptr<Clock> clock;
			static std::shared_ptr<Clock> Instance(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml = Glib::RefPtr<Gnome::Glade::Xml>());
			~Clock();
			inline struct tm* get_tm(){ return tm; };
		protected:
			Clock(){};
			Clock(USVGText* w1, USVGText* w2);
		private:
			USVGText* widget1, *widget2;
			gint timer_id;
			std::string fmt1, fmt2, fmt3;
			struct tm* tm;
			sigc::connection connPoll;
			int tz_corr;

			void gen_str();
			bool timer_callback();
	};
	
	
}

#endif /* _CLOCK_H */
