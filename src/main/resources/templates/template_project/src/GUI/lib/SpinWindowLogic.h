#ifndef _SpinWindowLogic_H
#define _SpinWindowLogic_H

#include "SetWindowLogic.h"
#include <usvgwidgets/USVGText.h>
#include <uwidgets/UValueIndicator.h>
#define OK "ok"

namespace TEMPLATE_PROJECT
{
	class SpinWindowLogic:
		public SetWindowLogic
	{
		public:
			SpinWindowLogic();
			virtual ~SpinWindowLogic(){};
			
			struct Spin
			{
				Spin():
					value(0),
					min(0x80000000),
					max(0x7FFFFFFF),
					chars(2),
					precision(0),
					digits(0)
				{}
				long value;
				long min;
				long max;
				int chars;
				int precision;
				int digits;
			};
			
		protected:
			typedef std::unordered_map<std::string, Spin> Spins;
			typedef std::unordered_map< SetWidgets::UWindow*, Spins > WindowSpins;
			WindowSpins spins;
			virtual void on_action( SetWidgets::UWindow* window, std::string name, long value ) override;
			virtual void update_widget(SetWidgets::UWindow* window, const std::string& name) override;
			virtual void update_sensor(SetWidgets::UWindow* window, SetWidgets::UWindow::Sensors::iterator& it) override;
			
			bool add_sensor_spin(SetWidgets::UWindow* window, const std::string& name);
			void add_spin(SetWidgets::UWindow* window, const std::string& name, long value, long min = 0x80000000, long max = 0x7FFFFFFF, int chars = 2, int precision = 0, int digits = -1);
			void update_spin(SetWidgets::UWindow* window, const std::string& name);
			void set_min(SetWidgets::UWindow* window, const std::string& name, long value);
			void set_max(SetWidgets::UWindow* window, const std::string& name, long value);
			void set_chars(SetWidgets::UWindow* window, const std::string& name, int value);
			void set_precision(SetWidgets::UWindow* window, const std::string& name, int value);
			void set_digits(SetWidgets::UWindow* window, const std::string& name, int value);
	};
}

static TEMPLATE_PROJECT::SpinWindowLogic::Spin widget_get_state(UValueIndicator* widget);
static void widget_set_state(USVGText* widget, const TEMPLATE_PROJECT::SpinWindowLogic::Spin& state);

#endif /* _SpinWindowLogic_H */
