#ifndef _AllConfirm_H
#define _AllConfirm_H

#include <PassiveTimer.h>
#include <Connector.h>
#include <UniSetTypes.h>
#include <MessageType.h>

namespace TEMPLATE_PROJECT
{
	class AllConfirm
	{
		public:
			static void connect(ConnectorRef connector, uniset::ObjectId confirm_id);
			inline static void set_time(uniset::timeout_t t){ confirm_time = t; };
			inline static void set_timeout(uniset::timeout_t t){ confirm_timeout = t; };
			static bool emit_confirm();
		protected:
			AllConfirm() {};
			~AllConfirm(){};
			static void check(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value);
		private:
			static uniset::PassiveTimer confirm_pt;
			static uniset::timeout_t confirm_time;
			static uniset::timeout_t confirm_timeout;
			static sigc::connection confirm_connection;
			static ConnectorRef connector;

	};
}

#endif /* _AllConfirm_H */
