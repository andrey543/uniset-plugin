#include "AutomatWindowLogic.h"
#include <glade/glade.h>
#include "TEMPLATE_PROJECTEquipStates.h"
#include <uwidgets/UValueIndicator.h>
#include <uwidgets/UIndicatorContainer.h>
#include <usvgwidgets/USVGText.h>
#include <uniwidgets/UniText.h>
#include <uniwidgets/UWindowReference.h>
#include <usvgwidgets/USVGButton.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
AutomatWindowLogic::AutomatWindowLogic()
{
	follow_property("current-value");
}
// -------------------------------------------------------------------------
void AutomatWindowLogic::on_show( UWindow* window )
{
	//USVGTEXT_SET_VAR(window, text1);
	//USVGTEXT_SET_VAR(window, text2);
	// для автоматов TVxx
	if( dynamic_cast<UWindowReference*>(window->caller) )
	{
		USVGButton* ump2onbtn1 = dynamic_cast<USVGButton*>(window->widgets["ump2onbtn1"]);
		USVGButton* ump2onbtn2 = dynamic_cast<USVGButton*>(window->widgets["ump2onbtn2"]);
		if( ump2onbtn1 && ump2onbtn2 )
		{
			ump2onbtn1->set_property_text(window->variables["ump2_button_label"]);
			ump2onbtn2->set_property_text(window->variables["ump2_button_label"]);
		}
	}
}
#define BSTATE object->get_property_page()
#define QSTATE qobject->get_property_page()
#define RSTATE robject->get_property_page()
#define ctlREMOTE 1
// -------------------------------------------------------------------------
void AutomatWindowLogic::on_property_changed( UWindow* window )
{
	UIndicatorContainer* qobject = dynamic_cast<UIndicatorContainer*>(window->parents["qobject"]);
	UIndicatorContainer* robject = dynamic_cast<UIndicatorContainer*>(window->parents["remoteobject"]);
	if( qobject )
	{
		int btnpage = abNONE;
		// для автоматов TVxx проверяем что онкно вызвано с экрана УМП
		if( dynamic_cast<UWindowReference*>(window->caller) )
		{
			btnpage = abNONE2;
			UniText* ump2ready = dynamic_cast<UniText*>(window->parents["check_ump2_ready"]);
			if( ump2ready )
			{
				if(robject && RSTATE != ctlREMOTE)
					btnpage = abNONE2;
				else if(QSTATE == qOff && ump2ready->get_current_value() == 1)
					btnpage = abAVR;
			}
		}
		else
		{
			if(robject && RSTATE != ctlREMOTE)
			{
				btnpage = abNONE;
			}
			else if(QSTATE == qOff)
			{
				btnpage = abON;
				// для автоматов ТСН проверяем включен ли "Режим аварийного хода"
				SensorProp* avrsens = window->sensors["check_avr_sensor"];
				if( avrsens && avrsens->get_state() )
					btnpage = abAVR;
				// для автоматов ПН и ТНА проверяем готовность ПН или ТНА
				SensorProp* blocksens = window->sensors["check_block_sensor"];
				if( blocksens && blocksens->get_state() )
					btnpage = abNONE;
			}
			else if(QSTATE == qOn)
			{
				btnpage = abOFF;
				// для автоматов ПН и ТНА проверяем включен ли ПН или ТНА
				SensorProp* onsens = window->sensors["check_on_sensor"];
				if( onsens && onsens->get_state() )
					btnpage = abAVR;
			}
		}
		UINDICATORCONTAINER_SET_PAGE(window, btncontainer, btnpage);
	}
	USVGTEXT_UPDATE_BY_UVALUE(window, value1);
	USVGTEXT_UPDATE_BY_UVALUE(window, value2);
	USVGTEXT_UPDATE_BY_UVALUE(window, value3);
	USVGTEXT_UPDATE_BY_UVALUE(window, value4);
}