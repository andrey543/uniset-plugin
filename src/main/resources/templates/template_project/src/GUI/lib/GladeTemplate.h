#ifndef _GladeTemplate_H_
#define _GladeTemplate_H_
// ------------------------------------------------------------------------------
#include <glade/glade.h>
#include <libgladexml.h>
#include "TEMPLATE_PROJECTConfiguration.h"
#include <uniwidgets/UReplace.h>
// ------------------------------------------------------------------------------
/*!

*/
namespace TEMPLATE_PROJECT
{

	class GladeTemplate:
		public Gnome::Glade::XmlBuffer<SetWidgets::UReplace, uniset::UniXML>
	{
		public:
			GladeTemplate(){};
			GladeTemplate(const std::string& filename, int pagenum, bool force = false);
			GladeTemplate(const std::string& filename);
			virtual ~GladeTemplate(){};
	};

}
// ------------------------------------------------------------------------------------------
#endif // _GladeTemplate_H_
// ------------------------------------------------------------------------------------------
