#include "ScaleWindowLogic.h"
#include <glade/glade.h>
#include <usvgwidgets/USVGBar2.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
void ScaleWindowLogic::on_action(UWindow* window, std::string name, long value)
{
	UWindow::Sensors::iterator sensIt = window->sensors.find(name);
	if( sensIt != window->sensors.end() )
	{
		Gtk::Scale* scale = dynamic_cast<Gtk::Scale*>(window->widgets[sensIt->first]);
		if( scale )
		{
			//ulog6 << "ScaleWindowLogic::set_sensor(): name=" << name << " id=" << sensIt->second->get_sens_name() << " value=" << scale->get_value() << std::endl;
			SetWindowLogic::on_action(window, name, lroundf(scale->get_value() * 10) * 100);
			return;
		}
	}
	SetWindowLogic::on_action(window, name, value);
}
// -------------------------------------------------------------------------
void ScaleWindowLogic::update_widget(UWindow* window, const std::string& name)
{
	//ulog6 << "ScaleWindowLogic::update_widget() name=" << name << endl;
	USVGBar2* obj = dynamic_cast<USVGBar2*>(window->parents[name+"object"]);
	Gtk::Scale* scale = dynamic_cast<Gtk::Scale*>(window->widgets[name]);
	if(obj && scale)
		scale->set_value((double)(obj->property_value().get_value()) / 1000);
}
// -------------------------------------------------------------------------
void ScaleWindowLogic::update_sensor(UWindow* window, UWindow::Sensors::iterator& it)
{
	if( it != window->sensors.end() )
	{
		USVGBar2* obj = dynamic_cast<USVGBar2*>(window->parents[it->first+"object"]);
		if(obj)
		{
			float value = obj->property_value().get_value();
			if ( value != it->second->get_value() )
				it->second->save_value(value);
		}
	}
}