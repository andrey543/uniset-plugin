#include "ConfigWindowLogic.h"
#include "MainWindow.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
ConfigWindowLogic::ConfigWindowLogic() :
	mywindow(nullptr)
	,touch_blocked(false)
{
	clock.signal_show().connect(sigc::mem_fun(*this, &ConfigWindowLogic::hide));
	script.signal_show().connect(sigc::mem_fun(*this, &ConfigWindowLogic::hide));
	screenshot.signal_show().connect(sigc::mem_fun(*this, &ConfigWindowLogic::hide));
	screenshot.signal_show().connect(&MainWindow::screenshot);
	blockscript.signal_show().connect(sigc::mem_fun(*this, &ConfigWindowLogic::hide));
	blockscript.signal_success().connect(sigc::mem_fun(*this, &ConfigWindowLogic::on_block_touch_switched));
}
// -------------------------------------------------------------------------
void ConfigWindowLogic::on_show(UWindow* window)
{
	mywindow = window;
	if( !mywindow )
		return;
	UIndicatorContainer* conn = dynamic_cast<UIndicatorContainer*>(window->widgets["blockcontainer"]);
	if( conn )
		conn->set_page(touch_blocked);
}
// -------------------------------------------------------------------------
void ConfigWindowLogic::on_block_touch_switched()
{
	touch_blocked = !touch_blocked;
	cout << " on_block_touch_switched() " << touch_blocked;
}
// -------------------------------------------------------------------------
void ConfigWindowLogic::hide()
{
	if(mywindow)
		mywindow->hide();
}