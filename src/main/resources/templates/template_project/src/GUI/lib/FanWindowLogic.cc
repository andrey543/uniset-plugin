#include "FanWindowLogic.h"
#include <glade/glade.h>
#include "TEMPLATE_PROJECTEquipStates.h"
#include <uwidgets/UIndicatorContainer.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace UniSetTypes;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
void FanWindowLogic::on_show(UWindow* window)
{
	ControlWindowLogic::on_show(window);
}
// -------------------------------------------------------------------------
void FanWindowLogic::on_property_changed(UWindow* window)
{
	if( check_control(window) )
	{
		UINDICATORCONTAINER_TRANSLATE(window, btncontainer1, object1);
		UINDICATORCONTAINER_TRANSLATE(window, btncontainer2, object3);
		UINDICATORCONTAINER_TRANSLATE(window, btncontainer3, object5);
		UINDICATORCONTAINER_TRANSLATE(window, btncontainer4, object7);
	}
	else
	{
		UINDICATORCONTAINER_SET_PAGE(window, btncontainer1, 2);
		UINDICATORCONTAINER_SET_PAGE(window, btncontainer2, 2);
		UINDICATORCONTAINER_SET_PAGE(window, btncontainer3, 2);
		UINDICATORCONTAINER_SET_PAGE(window, btncontainer4, 2);
	}
}