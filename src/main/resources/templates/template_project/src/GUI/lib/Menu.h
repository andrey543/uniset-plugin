#ifndef _Menu_H
#define _Menu_H

#include <gtkmm.h>
#include <libglademm.h>
#include <usvgwidgets/UniSVGButton.h>
#include <uwidgets/ULockNotebook.h>

namespace TEMPLATE_PROJECT
{
	class Menu
	{
		public:
			static std::shared_ptr<Menu> menu;
			static std::shared_ptr<Menu> Instance(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml = Glib::RefPtr<Gnome::Glade::Xml>());
                        void connect_switch_lock(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml);
			~Menu();
			
		protected:
			Menu(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml);
			Menu() {};
			void connect_button_click(Gtk::Widget& w);
			void connect_switch_page(Gtk::Widget& w);
			void on_switch_page(Gtk::Notebook* book);
			void on_main_switch_page();
			bool on_mouse_button_release_event(GdkEvent* event);
			bool on_back_clicked();
			bool on_tab_clicked(UniSVGButton* button);
			/* Вкл./Откл. блокировку автоматического переключения вкладок */
			static void switch_lock(GtkWidget* w);
			static void enable_lock(Gtk::Widget& w, bool lock);
			
		private:
			UniSVGButton* backbtn;
			ULockNotebook* nbook;
			std::stack<int> backpages;

	};
}

#endif /* _Menu_H */
