#include "uniwidgets/UniText.h"
#include "SelectWindowLogic.h"
#include <glade/glade.h>
#include "TEMPLATE_PROJECTEquipStates.h"
#include <uwidgets/UIndicatorContainer.h>
#include <UniSetTypes.h>
#include <Configuration.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
SelectWindowLogic::SelectWindowLogic()
{
	signal_connect("toggled", G_CALLBACK(SelectWindowLogic::on_toggled));
}
// -------------------------------------------------------------------------
void SelectWindowLogic::on_property_changed(UWindow* window)
{
	GTKCHECKBUTTON_RESET(window, check1);
	GTKCHECKBUTTON_RESET(window, check2);
	GTKCHECKBUTTON_RESET(window, check3);
	GTKCHECKBUTTON_RESET(window, check4);
	UIndicatorContainer* object1 = dynamic_cast<UIndicatorContainer*>(window->parents["object1"]);
	UIndicatorContainer* object2 = dynamic_cast<UIndicatorContainer*>(window->parents["object2"]);
	UIndicatorContainer* object3 = dynamic_cast<UIndicatorContainer*>(window->parents["object3"]);
	UIndicatorContainer* object4 = dynamic_cast<UIndicatorContainer*>(window->parents["object4"]);
	if( object1 && object1->get_property_page() )
		GTKCHECKBUTTON_SET(window, check1)
	else if( object2 && object2->get_property_page() )
		GTKCHECKBUTTON_SET(window, check2)
	else if( object3 && object3->get_property_page() )
		GTKCHECKBUTTON_SET(window, check3)
	else if( object4 && object4->get_property_page() )
		GTKCHECKBUTTON_SET(window, check4)
}
// -------------------------------------------------------------------------
void SelectWindowLogic::on_action(UWindow* window, std::string name, long value)
{
	bool bool1, bool2, bool3, bool4;
	GTKCHECKBUTTON_GET(window, check1, bool1);
	GTKCHECKBUTTON_GET(window, check2, bool2);
	GTKCHECKBUTTON_GET(window, check3, bool3);
	GTKCHECKBUTTON_GET(window, check4, bool4);
	if( name == OK )
	{
		GTKCHECKBUTTON_SET_AI_SENSOR(window, bool1, sensor, value1);
		GTKCHECKBUTTON_SET_AI_SENSOR(window, bool2, sensor, value2);
		GTKCHECKBUTTON_SET_AI_SENSOR(window, bool3, sensor, value3);
		GTKCHECKBUTTON_SET_AI_SENSOR(window, bool4, sensor, value4);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor1, bool1);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor2, bool2);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor3, bool3);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor4, bool4);
	}
	else if ( name == RESET )
	{
		//GTKCHECKBUTTON_SET_AI_SENSOR(window, true, sensor, 0);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor1, false);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor2, false);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor3, false);
		GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor4, false);
	}
	else
		UWindowCommonLogic::on_action(window, name, value);
}
// -------------------------------------------------------------------------
bool SelectWindowLogic::on_toggled(GtkWidget* w)
{
	string name = gtk_widget_get_name(w);
	UWindow* window = dynamic_cast<UWindow*>(UWindow::get_window_label(w));
	if( window )
	{
		ucrit << "SelectWindowLogic::toggled(): name=" << name << endl;
		toggle(window, dynamic_cast<Gtk::CheckButton*>(Glib::wrap(w)));
	}
	return true; // прерываем обработку сигнала так как в toggle() мы сами проверяем и выставляем чек-боксы которые надо
}
// -------------------------------------------------------------------------
void SelectWindowLogic::toggle(UWindow* window, Gtk::CheckButton* check)
{
	if( check )
	{
		GTKCHECKBUTTON_TOGGLE(window, check1, check);
		GTKCHECKBUTTON_TOGGLE(window, check2, check);
		GTKCHECKBUTTON_TOGGLE(window, check3, check);
		GTKCHECKBUTTON_TOGGLE(window, check4, check);
	}
}