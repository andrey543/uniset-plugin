#ifndef _Main_H
#define _Main_H

#include <gtkmm.h>
#include <unordered_map>
#include <iostream>
#include <SmartSignal.h>

namespace TEMPLATE_PROJECT
{
	class Main:
		public Gtk::Main
	{
		public:
			Main (int& argc, char**& argv, bool set_locale=true);
			virtual ~Main();
			
			static void send_signal1( std::string prog = "TEMPLATE_PROJECT-gui" );
			static void send_signal2( std::string prog = "TEMPLATE_PROJECT-gui" );
			void run(Gtk::Window& window);
			std::string get_info();
			
		protected:
			static pid_t pid;
			static pid_t get_pid( std::string prog );
			static void sig_hdl(int sig);
			static void child_setup(){};
			
		private:
			struct GlibEventInfo
			{
				GlibEventInfo():
				name("")
				,eval_time(0)
				,count(0)
				{
				}
				GlibEventInfo(int n):
				name("")
				,eval_time(0)
				,count(0)
				{
					std::ostringstream os;
					os << n;
					name = os.str();
					signal.set_name(name);
					conn = signal.connect(&Gtk::Main::iteration);
				}
				GlibEventInfo(std::string n):
				name(n)
				,eval_time(0)
				,count(0)
				,signal(n)
				{
					conn = signal.connect(&Gtk::Main::iteration);
				}
				~GlibEventInfo()
				{
					conn.disconnect();
				}
				std::string name;
				unsigned long eval_time;
				unsigned long count;
				SmartSignal<bool, bool> signal;
				sigc::connection conn;
			};
			SmartSignal<void, bool, GlibEventInfo*> all_signal;
			sigc::connection all_conn;
			static std::unordered_map< int, GlibEventInfo* > iters;
			static GPollFunc originalPollFunc;
			static GPollFunc pollFunc;
			static gint poll(GPollFD *ufds, guint nfsd, gint timeout_);
			static void iteration(bool block, GlibEventInfo* info);
	};
}

#endif /* _Main_H */
