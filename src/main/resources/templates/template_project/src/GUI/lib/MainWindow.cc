#include <sys/time.h>
#include "MainWindow.h"
#include <uwidgets/UProxyWidget.h>
#include <usvgwidgets/UniSVGButton.h>
#include <uniwidgets/UniDBInterface.h>
#include "AllConfirm.h"
#include "GladeTemplate.h"
#include "Display.h"

#define TRY() \
try \
{
#define CATCH() ; \
} \
catch( const std::exception& ex ) \
{ \
	ucrit << "MainWindow::MainWindow(): some object was not created successfull" << endl; \
	ucrit << ex.what() << endl; \
}\
catch( Gnome::Glade::XmlError& ex ) \
{ \
	ucrit << ex.what() << endl; \
}
// ------------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace SetWidgets;
using namespace TEMPLATE_PROJECT;

MainWindow* TEMPLATE_PROJECT::gwindow = nullptr;
// ------------------------------------------------------------------------------
MainWindow::MainWindow (std::string gladedir, std::string guifile, std::string svgdir, Glib::ustring screen, UniSetActivatorPtr act) :
        progress(0),
        alignment(0.5, 0.5, 0, 0)
{
	splash.modify_bg(Gtk::STATE_NORMAL, Gdk::Color("#000000"));
	alignment.add(bar);
	splash.add(alignment);
        splash.set_default_size(1024, 768);
	//splash.maximize();
	splash.show_all();
	
	draw_splash(0);
		
	savedir   = uniset_conf()->getArgParam("--savedir", "/srv/storage" );

	Gnome::Glade::XmlSignal::connect_xml_created( sigc::mem_fun(*this, &MainWindow::on_xml_created) );
	gxml = Gnome::Glade::XmlReplace::create<UReplace>(gladedir + guifile, get_page(gladedir + guifile) );
	TRY() logics.config.assign_to("config_window");	CATCH()
	TRY() logics.config.clock.assign_to("clock_window");		CATCH()
	TRY() logics.config.clock.signal_set_time().connect(&MainWindow::set_time);	CATCH()
	TRY() logics.config.script.assign_to("script_window");		CATCH()
	TRY() logics.config.blockscript.assign_to("blockscript_window");CATCH()
	TRY() logics.config.screenshot.assign_to("screenshot_window");	CATCH()
	TRY() logics.control.assign_to("control_window");		CATCH()
	TRY() logics.automat.assign_to("automat_window");		CATCH()
	TRY() logics.select.assign_to("select_window");	CATCH()
	TRY() logics.spin.assign_to("spin_window");	CATCH()
	TRY() logics.range.assign_to("range_window");	CATCH()
	TRY() logics.scale.assign_to("scale_window");	CATCH()
	/*Получить connector для опроса датчиков*/
	UProxyWidget* pw;
	gxml->get_widget("uproxywidget1", pw);
	assert( pw != 0 );
	connector_ = pw->get_connector();
	AllConfirm::connect( connector_, uni_atoi(pw->get_property_confirm_sensor()) );
	AllConfirm::set_time(uniset_conf()->getArgPInt("--confirm-all-time", 0));
	AllConfirm::set_timeout(uniset_conf()->getArgPInt("--confirm-all-timeout", 50));
	
	gxml->get_widget("window1", w);
	assert( w != 0 );
	w->signal_show().connect(sigc::mem_fun(*this, &MainWindow::on_show) );

	// Часы
	clock = Clock::Instance(gxml);
	// Меню (обработка кнопки назад и вложенных notebook)
        menu = Menu::Instance(gxml);
	
	draw_splash(1);

	SetWidgets::UniDBInterface* db;
	gxml->get_widget("unidbinterface1", db);
	
	//while( AllConfirm::emit_confirm(0) );
	//if( !pw->activated() )
	//	pw->activate();
}

MainWindow::~MainWindow()
{
	//delete w;
}
MainWindow* MainWindow::Instance(std::string gladedir, std::string guifile, std::string svgdir, Glib::ustring screen, UniSetActivatorPtr act)
{
	if(!gwindow)
	{
		gwindow = new MainWindow(gladedir, guifile, svgdir, screen, act);
	}

	return gwindow;
}

void MainWindow::draw_splash(const double& perc)
{
	bar.set_fraction(perc);
	while(Gtk::Main::events_pending())
	{
		Gtk::Main::iteration();
	}
}

void MainWindow::on_xml_created(Glib::RefPtr<Gnome::Glade::Xml> gxml, const std::string& file )
{
        //cout << "MainWindow::on_xml_created() file=" << file << endl;

        if(file.find("config") != string::npos)
            menu->connect_switch_lock(gxml);

        progress += 0.025;
	draw_splash(progress);
}

void MainWindow::on_show()
{
	cout << "MainWindow::on_show()" << endl;
	// Gdk::Window и Gdk::Display доступны только после on_realized, поэтому Display::Instance инциализируем с Gdk::Window здесь
	Display::Instance(w->get_window());
	splash.hide();
}

void MainWindow::set_time(std::string cmd)
{
	assert( gwindow != 0 );
	cout << cmd << endl;
	gwindow->clockscript.run(cmd, 5);
}

void MainWindow::screenshot()
{
	assert( gwindow != 0 );
	using Glib::ustring;

	Glib::RefPtr<Gdk::Screen> scr = gwindow->w->get_screen();
	Glib::RefPtr<Gdk::Window> win = gwindow->w->get_window();
	int wd, h;
	win->get_size(wd, h);

	/* If the dialog window was shown we first should close it then redraw the main window and be sure
	   that the process is completed to avoid artifacts (aka black rectangle at the place of the hidden window */
	{
		win->invalidate_rect(Gdk::Rectangle(0, 0, wd, h), true);
		win->process_updates(true);
	}
	

//	Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create((Glib::RefPtr<Gdk::Drawable>)wnd, 0, 0, wd, h);

	Glib::RefPtr<Gdk::Image> img = win->get_image(0,0,wd,h);
	Glib::RefPtr<Gdk::Colormap> cmap = scr->get_system_colormap();
	Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create(img,cmap,0,0,0,0,img->get_width(),img->get_height());
	
	gwindow->logics.config.screenshot.show();

	struct tm t;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, &t);

	ustring dir("");
	try
	{
	 	dir = gwindow->getSaveDir();
	}
	catch( SystemError& ex )
	{
		cerr << "(MainWindow:make_screenshot): can`t create SaveDir: " << ex << endl;
		gwindow->logics.config.screenshot.finish(ScriptWindowLogic::fsMESSAGE, "Ошибка сохранения снимка экрана:\nНевозможно создать каталог");
		return;
	}

	std::ostringstream s;
	s << dir << "/snap" << uniset::dateToString(time(0),".") << "_" << uniset::timeToString(time(0),".") << ".png";

	try {
		pixbuf->save(s.str(), "png");
		cout<<"Save snapshot... to "<<s.str()<<endl;
		gwindow->logics.config.screenshot.finish(ScriptWindowLogic::fsMESSAGE, "Снимок экрана успешно сохранён:\n" + s.str() );
	}
	catch (const Glib::FileError& err) {
		gwindow->logics.config.screenshot.finish(ScriptWindowLogic::fsMESSAGE, "Ошибка сохранения снимка экрана:\n" + err.what());
	}
	catch (const Gdk::PixbufError& err) {
		gwindow->logics.config.screenshot.finish(ScriptWindowLogic::fsMESSAGE, "Ошибка сохранения снимка экрана:\n" + err.what());
	}
}

std::string MainWindow::getSaveDir()
{
	if( savedir.empty() )
		savedir = "/tmp";
	
	std::string todaydir = savedir;
	todaydir += "/" + uniset::dateToString(time(0),".");
	const char *sd = todaydir.c_str();
	if(access(sd, F_OK) != 0) {/* Direcrory not exists, we need to create it */
		if(mkdir(sd, 0777) != 0) {
			ucrit << "(getSaveDir): Can't make dir... '" << todaydir << endl;
		}
	}
	
	struct stat status;
	stat(sd, &status);
		if(!status.st_mode & S_IFDIR) {
		ucrit << "(getSaveDir): '" << todaydir << "' not a dir..." << endl;
	}
	
	if((!status.st_mode & S_IRUSR) || (!status.st_mode & S_IWUSR) || (!status.st_mode & S_IXUSR)) {
		ucrit << "(getSaveDir): Need more rights for dir '" << todaydir << endl;
	}

	return todaydir;
}

int MainWindow::get_page(const std::string& file)
{
	std::string pagestr = uniset_conf()->getArgParam("--screen", "");
	if( pagestr.empty() )
		return 0;
	UReplace::Replaces replaces;
	GladeTemplate buffer(file);
	UReplace::get_replaces(replaces, buffer);
	for( auto&& pageit:replaces )
	{
		std::string file = pageit.second["uglade1"]["property"]["file"];
		file.erase(file.find("glade/"),6);
		file.erase(file.find(".glade"),6);
		if( file == pagestr )
			return pageit.first;
	}
	return 0;
}

void MainWindow::print_help(const std::string& file)
{
	cout << "--add-sm             - Add SharedMemory" << endl;
	cout << "--screen name        - Load only exact page. List of available pages: ";// << endl;
//	cout << "                       List of available pages:" << endl;
	UReplace::Replaces unordered_replaces;
	GladeTemplate buffer(file);
	UReplace::get_replaces(unordered_replaces, buffer);
	std::map<UReplace::ReplaceKey, UReplace::ReplaceWidgets> replaces;
	for( auto&& pageit:unordered_replaces )
		replaces[pageit.first] = pageit.second;
	int counter = -1;
	std::string prev = "";
	for( auto&& pageit:replaces )
	{
		std::string file = pageit.second["uglade1"]["property"]["file"];
		file.erase(file.find("glade/"),6);
		file.erase(file.find(".glade"),6);
		std::string name(file);
		std::string::size_type pos = name.find_first_of("123456789");
		if(pos != std::string::npos)
			name.erase(pos, name.size()-pos);
		//cout << "                           - " << file << endl;
		if( prev.empty() )
			cout << endl << "                           " << file;
		else if( name != prev )
			cout << "," << endl << "                           " << file;
		else
			cout << ", " << file;
		prev = name;
	}
	cout << endl;
}

void MainWindow::activate()
{
        UProxyWidget* pw = nullptr;
        gxml->get_widget("uproxywidget1", pw);

        if (pw == nullptr)
        {
                throw Exception("Не найден uproxywidget");
        }

        pw->set_property_module("uniset2");
}

Glib::RefPtr<Gnome::Glade::Xml> MainWindow::get_gxml()
{
        return gxml;
}
