#include "TimeDiagram.h"
#include "MainWindow.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
// -------------------------------------------------------------------------
TimeDiagram::TimeDiagram(UDiagram* diagram) :
	rotateindicators(nullptr),
	indicators(nullptr),
	buttons(nullptr),
	locked_page(-1),
	amount(0)
{
	if(!diagram)
	{
		ucrit << "TimeDiagram(): Null widget pointer!" << endl;
		throw;
	}

	diagram->foreach(sigc::mem_fun(this, &TimeDiagram::parse_child));
	/*gladexml->get_widget("indicatorcontainer1", indicators);
	gladexml->get_widget("switchcontainer1", buttons);
	glade_xml_signal_connect(gladexml->gobj(), "switch_indicators_to", G_CALLBACK(TimeDiagram::switch_indicators));

	if(!indicators)
	{
		ucrit << "TimeDiagram(): No indicator container!" << endl;
		//throw;
	}
	else
	{
		rotateindicators = dynamic_cast<UniAnimateContainer*>(indicators);

		if(!rotateindicators)
		{
			uwarn << "TimeDiagram(): Not animate indicator container!" << endl;
			//throw;
		}
	}

	if(!buttons)
	{
		ucrit << "TimeDiagram(): No indicator container!" << endl;
		//throw;
	}*/
}
// -------------------------------------------------------------------------
TimeDiagram::~TimeDiagram()
{
}
// -------------------------------------------------------------------------
void TimeDiagram::parse_child(Gtk::Widget& w)
{
	UniOscilChannel* channel = dynamic_cast<UniOscilChannel*>(&w);
	if(channel)
		channels[amount++] = channel;
}
// -------------------------------------------------------------------------
void TimeDiagram::switch_indicators(GtkWidget* w)
{
	if(gwindow && gwindow->slock)
	{
		gwindow->slock->switch_page(G_OBJECT(w));
	}
}
// -------------------------------------------------------------------------
void TimeDiagram::switch_page(GObject* obj)
{
	gint p;
	g_object_get(obj, "sensor_value", &p, NULL);

	//ucrit << "TimeDiagram::switch_page(): page=" << p << endl;
	if(locked_page == p)
	{
		p = 3;
	}

	if(rotateindicators)
	{
		rotateindicators->stop_rotate();

		if( p >= rotateindicators->get_children ().size() )
		{
			//rotateindicators->set_page(0);
			rotateindicators->start_rotate();
		}
		else
		{
			rotateindicators->set_page(p);
		}
	}
	else if(indicators)
	{
		indicators->set_page(p);
	}

	if(buttons)
	{
		buttons->set_page(p);
		buttons->on_page_changed();
	}

	locked_page = p;
}