#include "AllConfirm.h"
#include <glibmm/main.h>
#include <ConfirmSignal.h>
#include <CheckedSignal.h>
#include <sys/time.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
// -------------------------------------------------------------------------
uniset::PassiveTimer AllConfirm::confirm_pt;
sigc::connection AllConfirm::confirm_connection;
uniset::timeout_t AllConfirm::confirm_time = 0;
uniset::timeout_t AllConfirm::confirm_timeout = 50;
ConnectorRef AllConfirm::connector;
// -------------------------------------------------------------------------
void AllConfirm::connect(ConnectorRef conref, uniset::ObjectId confirm_id)
{
	if(!conref)
		return;
	connector = conref;
	connector->signals().connect_value_changed(&AllConfirm::check, confirm_id, DefaultObjectId);
	set_time(PassiveTimer::WaitUpTime);
}
// -------------------------------------------------------------------------
void AllConfirm::check(UniWidgetsTypes::ObjectId id, UniWidgetsTypes::ObjectId node, long value)
{
	if ( value )
		confirm_pt.setTiming( ( confirm_time > 0) ? confirm_time : PassiveTimer::WaitUpTime );
	else if ( !value && confirm_pt.checkTime() && confirm_timeout > 0 )
	{
		//ucrit << "AllConfirm::check(): all confirm!!!" << endl;
		confirm_pt.stop();
		confirm_connection.disconnect();
		confirm_connection = Glib::signal_timeout().
			connect(&AllConfirm::emit_confirm, confirm_timeout);
	}
}
// -------------------------------------------------------------------------
bool AllConfirm::emit_confirm()
{
	struct timeval tm;
	tm.tv_sec = 0;
	tm.tv_usec = 0;
	gettimeofday(&tm, nullptr);
	time_t time_sec = tm.tv_sec;
	return ( connector->signal_checked().emit() ? ( connector->signal_confirm().emit(time_sec) || true ) : connector->signal_confirm().emit(time_sec) );
}
