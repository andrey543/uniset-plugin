#ifndef _RangeWindowLogic_H
#define _RangeWindowLogic_H

#include "SpinWindowLogic.h"

#define UNIOSCIL_SET_XRANGE(window, varname, range) \
UniOscillograph* varname = dynamic_cast<UniOscillograph*>(window->parents[#varname]); \
if(varname) \
	varname->set_prop_XRange(range);

namespace TEMPLATE_PROJECT
{
	class RangeWindowLogic:
		public SpinWindowLogic
	{
		public:
			RangeWindowLogic(){};
			virtual ~RangeWindowLogic(){};
		
		protected:
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void on_property_changed( SetWidgets::UWindow* window ) override;
			virtual void on_action( SetWidgets::UWindow* window, std::string name, long value ) override;
	};
}

#endif /* _RangeWindowLogic_H */
