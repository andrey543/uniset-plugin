#include <string.h>
#include <time.h>
#include <sys/time.h>
#include "Clock.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace TEMPLATE_PROJECT;
// -------------------------------------------------------------------------
std::shared_ptr<Clock> Clock::clock = std::shared_ptr<Clock>();
// -------------------------------------------------------------------------
std::shared_ptr<Clock> Clock::Instance(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml)
{
	if( clock )
		return clock;
	if( !gladexml )
	{
		ucrit << "Clock(): Wrong glade!" << endl;
		return clock;
	}
	USVGText* widget1, *widget2;
	gladexml->get_widget("time", widget1);
	gladexml->get_widget("date", widget2);
	if( widget1 && widget2 )
		clock = shared_ptr<Clock>( new Clock(widget1, widget2) );
	return clock;
}
// -------------------------------------------------------------------------
Clock::Clock(USVGText* w1, USVGText* w2) :
	widget1(w1),
	widget2(w2),
	fmt1("%H:%M:%S"),
        fmt2("%a %d.%m.%Y")
{
        setlocale (LC_ALL,"");
	tm = new struct tm;

	gen_str();
	connPoll = Glib::signal_timeout().connect(sigc::mem_fun(*this, &Clock::timer_callback), 1000);
}
// -------------------------------------------------------------------------
Clock::~Clock()
{
	connPoll.disconnect();
	delete tm;
}
// -------------------------------------------------------------------------
void Clock::gen_str()
{
	gchar timestr[64];
	struct timeval tv;

	gettimeofday(&tv, NULL);
	localtime_r(&tv.tv_sec, tm);
	strftime(timestr, 64, fmt1.c_str(), tm);
	widget1->setText(Glib::locale_to_utf8(timestr));
	strftime(timestr, 64, fmt2.c_str(), tm);
	widget2->setText(Glib::locale_to_utf8(timestr));
}
// -------------------------------------------------------------------------
bool Clock::timer_callback()
{
	GDK_THREADS_ENTER ();
	gen_str();
	GDK_THREADS_LEAVE ();
	return true;
}
