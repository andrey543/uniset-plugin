#ifndef _FanWindowLogic_H
#define _FanWindowLogic_H

#include "ControlWindowLogic.h"
	
namespace TEMPLATE_PROJECT
{
	class FanWindowLogic:
		public ControlWindowLogic
	{
		public:
			FanWindowLogic(){};
			virtual ~FanWindowLogic(){};
			
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void on_property_changed( SetWidgets::UWindow* window ) override;
	};
}

#endif /* _FanWindowLogic_H */
