#include "ControlWindowLogic.h"
#include <glade/glade.h>
#include <uwidgets/UIndicatorContainer.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
void ControlWindowLogic::on_show(UWindow* window)
{
}
// -------------------------------------------------------------------------
void ControlWindowLogic::on_property_changed(UWindow* window)
{
	if( check_control(window) )
		UINDICATORCONTAINER_TRANSLATE(window, btncontainer, object1)
	else
		UINDICATORCONTAINER_SET_PAGE(window, btncontainer, 2)
}
// -------------------------------------------------------------------------
bool ControlWindowLogic::check_control(UWindow* window)
{
	UIndicatorContainer* control = dynamic_cast<UIndicatorContainer*>(window->parents["control"]);
	if( !control || control && control->get_property_page() >= 1 )
		return true;
	return false;
}