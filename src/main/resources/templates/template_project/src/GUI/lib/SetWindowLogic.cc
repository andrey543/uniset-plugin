#include "SetWindowLogic.h"
#include <glade/glade.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
void SetWindowLogic::on_show(UWindow* window)
{
	UWindowCommonLogic::on_show(window);
	
	for( UWindow::Sensors::iterator sensIt = window->sensors.begin(); sensIt != window->sensors.end(); ++sensIt )
		update_widget(window, sensIt->first);
}
// -------------------------------------------------------------------------
void SetWindowLogic::on_property_changed(UWindow* window)
{
	UWindowCommonLogic::on_property_changed(window);
	
	for( UWindow::Sensors::iterator sensIt = window->sensors.begin(); sensIt != window->sensors.end(); ++sensIt )
		update_sensor(window, sensIt);
}
// -------------------------------------------------------------------------
void SetWindowLogic::on_action(UWindow* window, std::string name, long value)
{
	UWindowCommonLogic::on_action(window, name, value);
}