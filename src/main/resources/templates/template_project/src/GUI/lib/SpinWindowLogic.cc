#include "SpinWindowLogic.h"
#include <glade/glade.h>
#include <UniSetTypes.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
SpinWindowLogic::SpinWindowLogic()
{
	follow_property("value");
	register_translate(&translate<USVGText,UValueIndicator>);
}
// -------------------------------------------------------------------------
void SpinWindowLogic::on_action( UWindow* window, std::string name, long dvalue )
{
	if( name == OK )
	{
		auto spinIt = spins[window].begin();
		for( ; spinIt != spins[window].end() ; ++spinIt )
			SetWindowLogic::on_action(window, spinIt->first, spinIt->second.value);
	}
	else
	{
		auto spinIt = spins[window].find(name);
		if( spinIt == spins[window].end() )
		{
			SetWindowLogic::on_action(window, name, dvalue);
			return;
		}
		auto& spin = spinIt->second;
		spin.value += dvalue;
		int k = pow(10, spin.precision);
		if(spin.value < spin.min*k)
			spin.value = spin.min*k;
		if(spin.value > spin.max*k)
			spin.value = spin.max*k;
		update_spin(window, name);
	}
}
// -------------------------------------------------------------------------
void SpinWindowLogic::update_sensor(UWindow* window, UWindow::Sensors::iterator& it)
{
	if( it == window->sensors.end() )
		return;
	auto spinIt = spins[window].find(it->first);
	if( spinIt != spins[window].end() )
	{
		UValueIndicator* obj = dynamic_cast<UValueIndicator*>(window->parents[it->first+"_object"]);
		if(obj)
			it->second->save_value(obj->get_property_value_());
	}
}
// -------------------------------------------------------------------------
void SpinWindowLogic::update_widget(UWindow* window, const std::string& name)
{
	USVGText* w = dynamic_cast<USVGText*>(window->widgets[name]);
	if( !w )
		return;
	auto spinIt = spins[window].find(name);
	if( spinIt == spins[window].end() && !add_sensor_spin(window, name) )
		return;
	UValueIndicator* obj = dynamic_cast<UValueIndicator*>(window->parents[name+"_object"]);
	if(obj)
		spins[window][name].value = obj->get_property_value_();
	update_spin(window, name);
}
// -------------------------------------------------------------------------
void SpinWindowLogic::update_spin(UWindow* window, const std::string& name)
{
	USVGText* w = dynamic_cast<USVGText*>(window->widgets[name]);
	if( !w )
		return;
	auto& spin = spins[window][name];
	int k = pow(10, spin.precision);
	ostringstream os;
	os.width(spin.chars);
	os.fill('0');
	os.precision(spin.digits);
	os.setf(ios_base::fixed, ios_base::floatfield);
	os << (double)spin.value / k;
	w->setText(os.str());
}
// -------------------------------------------------------------------------
bool SpinWindowLogic::add_sensor_spin(UWindow* window, const std::string& name)
{
	auto sensIt = window->sensors.find(name);
	if(sensIt == window->sensors.end())
		return false;
	add_spin(window, name, sensIt->second->get_value() );
	auto minIt = window->variables.find(name + "_min");
	auto maxIt = window->variables.find(name + "_max");
	auto chIt = window->variables.find(name + "_chars");
	auto precIt = window->variables.find(name + "_prec");
	auto dIt = window->variables.find(name + "_digits");
	if( minIt != window->variables.end() )
		set_min(window, name, uni_atoi(minIt->second));
	if( maxIt != window->variables.end() )
		set_max(window, name, uni_atoi(maxIt->second));
	if( chIt != window->variables.end() )
		set_chars(window, name, uni_atoi(chIt->second));
	if( precIt != window->variables.end() )
	{
		set_precision(window, name, uni_atoi(precIt->second));
		set_digits(window, name, uni_atoi(precIt->second));
	}
	if( dIt != window->variables.end() )
		set_digits(window, name, uni_atoi(dIt->second));
	return true;
}
// -------------------------------------------------------------------------
void SpinWindowLogic::add_spin(UWindow* window, const std::string& name, long value, long min, long max, int chars, int precision, int digits)
{
	auto& spin = spins[window][name];
	spin.min = min;
	spin.max = max;
	spin.value = value;
	spin.chars = chars;
	spin.precision = precision;
	if(digits != -1)
		spin.digits = digits;
	else
		spin.digits = precision;
}
// -------------------------------------------------------------------------
void SpinWindowLogic::set_min(UWindow* window, const std::string& name, long value)
{
	spins[window][name].min = value;
}
// -------------------------------------------------------------------------
void SpinWindowLogic::set_max(UWindow* window, const std::string& name, long value)
{
	spins[window][name].max = value;
}
// -------------------------------------------------------------------------
void SpinWindowLogic::set_chars(UWindow* window, const std::string& name, int value)
{
	spins[window][name].chars = value;
}
// -------------------------------------------------------------------------
void SpinWindowLogic::set_precision(UWindow* window, const std::string& name, int value)
{
	spins[window][name].precision = value;
}
// -------------------------------------------------------------------------
void SpinWindowLogic::set_digits(UWindow* window, const std::string& name, int value)
{
	spins[window][name].digits = value;
}
// -------------------------------------------------------------------------
SpinWindowLogic::Spin widget_get_state(UValueIndicator* widget)
{
	SpinWindowLogic::Spin spin;
	spin.value = widget->get_property_value_();
	spin.precision = widget->get_property_precision_();
	spin.digits = widget->get_property_digits_();
	spin.chars = widget->get_property_fill_digits_();
	return spin;
}
// -------------------------------------------------------------------------
void widget_set_state(USVGText* widget, const SpinWindowLogic::Spin& spin)
{
	int k = pow(10, spin.precision);
	ostringstream os;
	os.width(spin.chars);
	os.fill('0');
	os.precision(spin.digits);
	os.setf(ios_base::fixed, ios_base::floatfield);
	os << (double)spin.value / k;
	widget->setText(os.str());
}