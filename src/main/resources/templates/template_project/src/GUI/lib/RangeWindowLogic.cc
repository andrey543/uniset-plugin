#include "RangeWindowLogic.h"
#include <glade/glade.h>
#include <uwidgets/UniOscillograph.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
void RangeWindowLogic::on_show(UWindow* window)
{
	int tempsec = 0;
	UniOscillograph* oscil = dynamic_cast<UniOscillograph*>(window->parents["oscil1"]);
	if(oscil)
	{
		tempsec = oscil->get_prop_XRange();
		long min = tempsec / 60;
		long sec = tempsec % 60;
		add_spin(window, "minutes", min, 0);
		add_spin(window, "seconds", sec, 0, 59);
		update_widget(window, "minutes");
		update_widget(window, "seconds");
	}
}
// -------------------------------------------------------------------------
void RangeWindowLogic::on_property_changed(UWindow* window)
{
	SpinWindowLogic::on_property_changed(window);
}
// -------------------------------------------------------------------------
void RangeWindowLogic::on_action( UWindow* window, std::string name, long value )
{
	if( name == OK )
	{
		long resultsec = spins[window]["minutes"].value * 60 + spins[window]["seconds"].value;
		UNIOSCIL_SET_XRANGE(window, oscil1, resultsec);
		UNIOSCIL_SET_XRANGE(window, oscil2, resultsec);
		UNIOSCIL_SET_XRANGE(window, oscil3, resultsec);
		UNIOSCIL_SET_XRANGE(window, oscil4, resultsec);
		UNIOSCIL_SET_XRANGE(window, oscil5, resultsec);
	}
	else
		SpinWindowLogic::on_action(window, name, value);
}