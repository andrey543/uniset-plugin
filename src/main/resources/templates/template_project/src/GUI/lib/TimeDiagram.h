#ifndef _TimeDiagram_H
#define _TimeDiagram_H

#include <uwidgets/UIndicatorContainer.h>
#include "UniAnimateContainer.h"
#include <libglademm.h>
#include <uwidgets/UDiagram.h>

namespace TEMPLATE_PROJECT
{
	class TimeDiagram
	{
		public:
			TimeDiagram(UDiagram* diagram);
			~TimeDiagram();
			static void switch_indicators(GtkWidget* w);
			void switch_page(GObject* p);
		protected:
			TimeDiagram() {};
			void parse_child(Gtk::Widget& w);
		private:
			UniAnimateContainer* rotateindicators;
			UIndicatorContainer* indicators;
			UIndicatorContainer* buttons;
			int locked_page;
			
			typedef std::map<int,UniOscilChannel*> ChannelsMap;
			ChannelsMap channels;
			int amount;

	};
}

#endif /* _TimeDiagram_H */
