#include "Display.h"
#include "TEMPLATE_PROJECTConfiguration.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace TEMPLATE_PROJECT;
using namespace uniset;
// -------------------------------------------------------------------------
std::shared_ptr<Display> Display::display_ptr = std::shared_ptr<Display>();
// -------------------------------------------------------------------------
std::shared_ptr<Display> Display::Instance(const Glib::RefPtr< Gdk::Window >& win)
{
	if( !display_ptr )
		display_ptr = shared_ptr<Display>( new Display() );
	if( win )
		display_ptr->window = win;
	if( display_ptr->window )
		display_ptr->display = display_ptr->window->get_display();
	return display_ptr;
}
// -------------------------------------------------------------------------
Display::Display() :
	window()
	,display()
{
}
// -------------------------------------------------------------------------
Display::~Display()
{
}
// -------------------------------------------------------------------------
void Display::iteration()
{
	if( !display )
		throw Exception("Display(): Gdk::display is not initialized.");
	if( event_types.empty() )
		return;
	std::list<GdkEvent*> stack;
	GdkEvent* ev = NULL;
	while( ev = display->get_event() )
	{
		auto stop_emit = false;
		for( auto type : event_types )
		{
			if( type == ev->type )
			{
				stop_emit = signal_event_.emit(ev);
				break;
			}
		}
		if( !stop_emit )
			stack.push_back(ev);
		else
			gdk_event_free(ev);
	}
	for( auto&& s : stack )
	{
		display->put_event(s);
		// почему то не нашел аналога free в gtkmm
		gdk_event_free(s);
	}
}
// -------------------------------------------------------------------------
void Display::connect_signal_event( sigc::slot<bool, GdkEvent*>& func, GdkEventType event_type )
{
	signal_event_.connect(func);
	for( auto&& type : event_types )
		if( type == event_type )
			return;
	event_types.push_back(event_type);
}
// -------------------------------------------------------------------------
