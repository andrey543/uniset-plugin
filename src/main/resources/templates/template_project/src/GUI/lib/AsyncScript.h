// --------------------------------------------------------------------------
#ifndef _AsyncScript_H_
#define _AsyncScript_H_
// --------------------------------------------------------------------------
#include <string>
#include <glibmm.h>
#include <sigc++/sigc++.h>
// --------------------------------------------------------------------------
/*! Обёртка для асинхронного запуска запуска скриптов и отслеживания состояния. */
class AsyncScript
{
	public:

		AsyncScript( const std::string& script, const std::string& def_params="" );
		AsyncScript();
		~AsyncScript();
		
		bool is_running();

		void set_workdir( const std::string& d ){ workdir = d; }
		inline std::string get_workdir(){ return workdir; }

		/*! 
			\param tout_sec - таймаут на попытки запуска или работу скрипта 
			\param force - запускать даже если уже запущен
			\param argv - аргументы
		*/
		bool run( int tout_sec, bool force=false );
		
		bool run( const std::string& cmd, int tout_sec, bool force=false );

		void stop();

		enum FinishStatus
		{
			fsNONE,
			fsOK,
			fsFAIL,
			fsLastStatus
		};

		typedef sigc::signal<void,FinishStatus> ScriptFinish_Signal;

		inline ScriptFinish_Signal signal_script_finish(){ return signal_sfinish; }

		inline int get_last_status(){ return last_status; }

	protected:

		bool on_timeout();
		void watch_handler( Glib::Pid pid, int child_status );
		void child_setup(){}

		std::string workdir;
		std::string script;
		std::string def_params;
		int child_pid;
		sigc::connection connWatch;

		sigc::connection connTimeout;

		ScriptFinish_Signal signal_sfinish;

		int last_status;

	private:
};

// --------------------------------------------------------------------------
#endif // _AsyncScript_H_
// --------------------------------------------------------------------------

