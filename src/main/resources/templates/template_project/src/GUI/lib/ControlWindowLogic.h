#ifndef _ControlWindowLogic_H
#define _ControlWindowLogic_H

#include <uniwidgets/UWindowLogic.h>
	
namespace TEMPLATE_PROJECT
{
	class ControlWindowLogic:
		public SetWidgets::UWindowLogic
	{
		public:
			ControlWindowLogic(){};
			virtual ~ControlWindowLogic(){};
		
		protected:	
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void on_property_changed( SetWidgets::UWindow* window ) override;
			bool check_control( SetWidgets::UWindow* window );
	};
}

#endif /* _ControlWindowLogic_H */
