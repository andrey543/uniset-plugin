#include <sys/time.h>
#include "Main.h"
#include "TEMPLATE_PROJECTConfiguration.h"
#include <iostream>
#include <UVoid.h>
#include "Display.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace TEMPLATE_PROJECT;
// -------------------------------------------------------------------------
pid_t Main::pid = 0;
std::unordered_map< int, Main::GlibEventInfo* > Main::iters;
GPollFunc Main::originalPollFunc = nullptr;
GPollFunc Main::pollFunc = nullptr;
// -------------------------------------------------------------------------
Main::Main(int& argc, char**& argv, bool set_locale):
	Gtk::Main(argc, argv, set_locale)
	,all_signal("ALL")
{
	if(pid != 0)
		throw uniset::Exception("Main(): process already runnig ");
	
	pid = getpid();
	
	struct sigaction act;
	memset(&act, 0, sizeof(act));
	act.sa_handler = Main::sig_hdl;
	
	sigset_t   set; 
	sigemptyset(&set);                                                             
	sigaddset(&set, SIGUSR1); 
	sigaddset(&set, SIGUSR2);
	act.sa_mask = set;
	
	sigaction(SIGUSR1, &act, 0);
	sigaction(SIGUSR2, &act, 0);
	
	if (!g_thread_supported())
	{
		g_thread_init(NULL);
	}
	
	originalPollFunc = Glib::MainContext::get_default()->get_poll_func();
	pollFunc = poll;
	iters[Glib::PRIORITY_HIGH] = new GlibEventInfo("HIGH");
	iters[Glib::PRIORITY_DEFAULT] = new GlibEventInfo("DEFAULT");
	iters[Glib::PRIORITY_HIGH_IDLE] = new GlibEventInfo("HIGH_IDLE");
	iters[Glib::PRIORITY_HIGH_IDLE + 10] = new GlibEventInfo("RESIZE");
	iters[Glib::PRIORITY_HIGH_IDLE + 20] = new GlibEventInfo("REDRAW");
	iters[Glib::PRIORITY_DEFAULT_IDLE] = new GlibEventInfo("IDLE");
	iters[Glib::PRIORITY_LOW] = new GlibEventInfo("LOW");
	all_conn = all_signal.connect(&Main::iteration);
}
// -------------------------------------------------------------------------
Main::~Main()
{
	for( auto& it: iters )
		delete it.second;
	all_conn.disconnect();
}
// -------------------------------------------------------------------------
gint Main::poll(GPollFD *ufds, guint nfsd, gint timeout_)
{
	gint ret = (*originalPollFunc)(ufds, nfsd, timeout_);
	//if(ret > 0)
	//	cout << ret << " ";
	return ret;
}
// -------------------------------------------------------------------------
std::string Main::get_info()
{
	std::ostringstream inf;
	inf << all_signal.get_info();
	for( auto& it: iters )
		inf << it.second->signal.get_short_info();
	return inf.str();
}
// -------------------------------------------------------------------------
ConnectorRef get_connector(Gtk::Widget& w)
{
	if(UVoid* uv = dynamic_cast<UVoid*>(&w))
	{
		ConnectorRef conref = uv->get_connector();
		if(conref)
			return conref;
	}
	Gtk::Container* con = dynamic_cast<Gtk::Container*>(&w);
	if(con)
	{
		auto childs = con->get_children ( );
		for( auto child : childs )
		{
			ConnectorRef conref = get_connector(*child);
			if(conref)
				return conref;
		}
	}
	return ConnectorRef();
}
// -------------------------------------------------------------------------
void Main::run(Gtk::Window& window)
{
	ConnectorRef conref = get_connector(window);
	if( conref )
		conref->signals().connect_get_info_slot(sigc::mem_fun(*this, &Main::get_info), "TEMPLATE_PROJECT", "Main::events");
	window.show();
	/*ucrit << "PRIORITY_HIGH		= " << Glib::PRIORITY_HIGH << endl;
	ucrit << "PRIORITY_DEFAULT	= " << Glib::PRIORITY_DEFAULT << endl;
	ucrit << "PRIORITY_HIGH_IDLE	= " << Glib::PRIORITY_HIGH_IDLE << endl;
	ucrit << "PRIORITY_DEFAULT_IDLE = " << Glib::PRIORITY_DEFAULT_IDLE << endl;
	ucrit << "PRIORITY_LOW		= " << Glib::PRIORITY_LOW << endl;*/
	auto mc = Glib::MainContext::get_default();
	std::shared_ptr<Display> display = Display::Instance();
	while(true)
	{
		while(Gtk::Main::events_pending())
		{
			std::vector< Glib::PollFD >  fds;
			int timeout = 1000;
			int prior = Glib::PRIORITY_HIGH;
			mc->prepare(prior);
			
			if( iters.find(prior) == iters.end() )
				iters[prior] = new GlibEventInfo(prior);
			auto& event = iters[prior];
			
			struct timeval tm1;
			struct timeval tm2;
			{
				struct timezone tz;
				gettimeofday(&tm1, &tz);
			}
			
			display->iteration();
			all_signal.emit(true, event);
			
			{
				struct timezone tz;
				gettimeofday(&tm2, &tz);
			}
			
			unsigned long eval_time = (tm2.tv_sec - tm1.tv_sec) * 1000000 + tm2.tv_usec - tm1.tv_usec;
			event->eval_time += eval_time;
			event->count += 1;
		}
		msleep(10);
	}
	Gtk::Main::quit();
}
// -------------------------------------------------------------------------
void Main::iteration(bool block, GlibEventInfo* info)
{
	info->signal.emit(block);
}
// -------------------------------------------------------------------------
void Main::sig_hdl(int sig)
{
	//ucrit << sig << endl;
	if(sig == SIGUSR1)
	{
		//ucrit << "paused" << endl;
		sigset_t   set;
		sigemptyset(&set);
		sigaddset(&set, SIGUSR1);
		
		ucrit << "events():" << endl;
		for( auto& it: iters )
		{
			ucrit << it.second->name << "	= " << it.second->eval_time  << " usec (" << it.second->count << " events)" << endl;
			it.second->eval_time = 0;
			it.second->count = 0;
		}
		//int sig;
		//sigwait(&set, &sig);
		//ucrit << "continue" << endl;
	}
	else if(sig == SIGUSR2)
	{
		//ucrit << "paused" << endl;
		sigset_t   set;
		sigemptyset(&set);
		sigaddset(&set, SIGUSR2);
		//sigwait(&set, &sig);
		//ucrit << "continue" << endl;
		if(Glib::MainContext::get_default()->get_poll_func() == originalPollFunc)
		{
			ucrit << "set my poll func" << endl;
			Glib::MainContext::get_default()->set_poll_func(pollFunc);
		}
		else
		{
			ucrit << "set original poll func" << endl;
			Glib::MainContext::get_default()->set_poll_func(originalPollFunc);
		}
		/*int sig;
		sigwait(&set, &sig);
		ucrit << "continue" << endl;*/
	}
}
// -------------------------------------------------------------------------
pid_t Main::get_pid( string prog )
{
	if( pid != 0 )
		return pid;
	std::list<std::string> lenvp;
	std::list<std::string> largv; largv = Glib::shell_parse_argv("/bin/pidof " + prog);
	int status;
	string out;
	string error;
	
	try
	{
		//cerr << "(check_pid): RUN " << script << endl;
		Glib::SpawnFlags fl;
		Glib::spawn_sync("", largv, lenvp, fl, &Main::child_setup, &out, &error, &status);
		if( status != 0)
		{
			cerr << "(check_pid): RUN FAILED: status=" << status << " error=" << error << endl;
			return 0;
		}
		return uniset::uni_atoi(out);
	}
	catch( Glib::Exception& ex )
	{
		cerr << "(check_pid): RUN FAILED: status=" << status << " err: " <<  ex.what() << endl;
		return 0;
	}
}
// -------------------------------------------------------------------------
void Main::send_signal1( string prog )
{
	pid_t pid = get_pid(prog);
	if( pid != 0 )
		kill(pid, SIGUSR1);
}
// -------------------------------------------------------------------------
void Main::send_signal2( string prog )
{
	pid_t pid = get_pid(prog);
	if( pid != 0 )
		kill(pid, SIGUSR2);
}