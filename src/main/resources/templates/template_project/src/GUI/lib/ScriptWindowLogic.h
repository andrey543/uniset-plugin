#ifndef _ScriptWindowLogic_H
#define _ScriptWindowLogic_H

#include <uniwidgets/UWindowLogic.h>
#include "AsyncScript.h"
	
namespace TEMPLATE_PROJECT
{
	class ScriptWindowLogic:
		public SetWidgets::UWindowLogic
	{
		public:
			ScriptWindowLogic();
			virtual ~ScriptWindowLogic(){};
			inline sigc::signal<void>& signal_show(){ return signal_show_; };
			inline sigc::signal<void>& signal_success(){ return signal_success_; };
			inline sigc::signal<void>& signal_failure(){ return signal_failure_; };
			
			virtual void on_show( SetWidgets::UWindow* window ) override;
			
			enum FinishStatus
			{
				fsMESSAGE = AsyncScript::fsLastStatus+1,
				fsNOERROR,
				fsERROR
			};
			void finish(int status, SetWidgets::UWindow* window, std::string msg = "");

		protected:
			void ask_password(SetWidgets::UWindow* window);
			void reset_attempts();
			AsyncScript as;
		
		private:
			std::map< SetWidgets::UWindow*, sigc::connection > connections;
			sigc::signal<void> signal_show_;
			sigc::signal<void> signal_success_;
			sigc::signal<void> signal_failure_;
			Gtk::Entry entry4keyboard;
			sigc::connection entryconn;
			sigc::connection keyboardconn;
			int attemps;
	};
	
	class Screenshot:
		public ScriptWindowLogic
	{
		public:
			Screenshot():mywindow(nullptr),busy(false){};
			virtual ~Screenshot(){};
			
			virtual void on_show( SetWidgets::UWindow* window );
			virtual void show( );
			void finish(int status, std::string msg);
			
		private:
			SetWidgets::UWindow* mywindow;
			bool busy;
	};
	
}

#endif /* _ScriptWindowLogic_H */
