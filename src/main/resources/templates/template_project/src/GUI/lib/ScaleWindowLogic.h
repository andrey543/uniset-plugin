#ifndef _ScaleWindowLogic_H
#define _ScaleWindowLogic_H

#include "SetWindowLogic.h"

namespace TEMPLATE_PROJECT
{
	class ScaleWindowLogic:
		public SetWindowLogic
	{
		public:
			ScaleWindowLogic(){};
			virtual ~ScaleWindowLogic(){};
			
		protected:
			virtual void on_action( SetWidgets::UWindow* window, std::string name, long value ) override;
			virtual void update_widget(SetWidgets::UWindow* window, const std::string& name) override;
			virtual void update_sensor(SetWidgets::UWindow* window, SetWidgets::UWindow::Sensors::iterator& it) override;
	};
}

#endif /* _ScaleWindowLogic_H */
