#ifndef _Display_H
#define _Display_H
#include <memory>
#include <sigc++/sigc++.h>
#include <gdkmm/display.h>

namespace TEMPLATE_PROJECT
{
	class Display
	{
		public:
			static std::shared_ptr<Display> Instance( const Glib::RefPtr< Gdk::Window >& win = Glib::RefPtr< Gdk::Window >() );
			~Display();
			void iteration();
			void connect_signal_event( sigc::slot<bool, GdkEvent*>& func, GdkEventType event_type );
			inline sigc::signal<bool, GdkEvent* >& signal_event(){ return signal_event_; };
			
		protected:
			Display();
			
		private:
			Glib::RefPtr<Gdk::Window> window;
			Glib::RefPtr<Gdk::Display> display;
			static std::shared_ptr<Display> display_ptr;
			sigc::signal<bool, GdkEvent*> signal_event_;
			std::list<GdkEventType> event_types;

	};
}

#endif /* _Display_H */
