#include "ScriptWindowLogic.h"
#include <usvgwidgets/USVGText.h>
#include <UniSetTypes.h>
#include <Configuration.h>
#include <uwidgets/UDKeyboard.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
using namespace SetWidgets;
// -------------------------------------------------------------------------
ScriptWindowLogic::ScriptWindowLogic() :
	attemps(0)
{
	entry4keyboard.set_visibility(false);
}
// -------------------------------------------------------------------------
void ScriptWindowLogic::ask_password(UWindow* window)
{
	// после ввода пароля автоматически вызывается show();
	entryconn = entry4keyboard.signal_changed().connect(sigc::bind(sigc::mem_fun(*window, &UWindow::show), nullptr));
	UDKeyboard* keyboard = dynamic_cast<UDKeyboard*>(window->parents["keyboard"]);
	if( keyboard )
	{
		keyboardconn.disconnect();
		keyboardconn = keyboard->get_window()->signal_hide().connect(sigc::mem_fun(*this, &ScriptWindowLogic::reset_attempts));
		keyboard->start_edit(&entry4keyboard, nullptr);
	}
}
// -------------------------------------------------------------------------
void ScriptWindowLogic::reset_attempts()
{
	entry4keyboard.set_text("");
	attemps = 0;
}
// -------------------------------------------------------------------------
void ScriptWindowLogic::on_show(UWindow* window)
{
	entryconn.disconnect();
	if( !window->variables["password"].empty() && entry4keyboard.get_text() != window->variables["password"] )
	{
		if( attemps < 1 )
		{
			window->hide();
			signal_show_.emit();
			ask_password(window);
			attemps++;
		}
		else
		{
			reset_attempts();
			finish(fsMESSAGE, window, "Неверный пароль");
		}
		return;
	}
	reset_attempts();
	
	if( !window->variables["hide"].empty() )
		window->hide();
	
	window->widgets["ok"]->hide();
	USVGTEXT_SET_VAR(window, message);
	if( !window->variables["command"].empty() )
	{
		int timeout = 5;
		if( !window->variables["timeout"].empty() )
			timeout = uniset::uni_atoi(window->variables["timeout"]);
		connections[window].disconnect();
		connections[window] = as.signal_script_finish().connect(sigc::bind(sigc::mem_fun(this, &ScriptWindowLogic::finish), window, "" ));
		as.run(window->variables["command"], timeout);
		signal_show_.emit();
		return;
	}
	finish(fsMESSAGE, window, "No script to execute");
}
// -----------------------------------------------------------------------------
void ScriptWindowLogic::finish(int status, UWindow* window, std::string msg)
{
	connections[window].disconnect();
	uinfo << "ScriptWindowLogic::finish() window=" << window << " status=" << status << endl;
	switch(status)
	{
		case fsMESSAGE:
			{
				ostringstream os;
				os << msg ;
				USVGTEXT_SET_TEXT(window, message, os.str());
				signal_failure_.emit();
			}
			break;
		case fsERROR:
			{
				ostringstream os;
				if( !window->variables["failure_message"].empty() )
					os << window->variables["failure_message"] << " ";
				os << msg ;
				USVGTEXT_SET_TEXT(window, message, os.str());
				signal_failure_.emit();
			}
			break;
		case AsyncScript::fsFAIL:
			{
				ostringstream os;
				if( as.get_last_status() == 0 )
					os << window->variables["failure_message"] << "\n(Timeout)";
				else
					os << window->variables["failure_message"] << "\n(Error: " << as.get_last_status() << ")";
				USVGTEXT_SET_TEXT(window, message, os.str());
				signal_failure_.emit();
			}
			break;
		case fsNOERROR:
		case AsyncScript::fsOK:
			if( !window->variables["ok_message"].empty() )
			{
				ostringstream os;
				os << window->variables["ok_message"];
				USVGTEXT_SET_TEXT(window, message, os.str());
			}
			signal_success_.emit();
			if( !window->variables["hide_on_success"].empty() )
			{
				window->hide();
				return;
			}
			if( !window->variables["block_on_success"].empty() )
				return;
			break;
		case AsyncScript::fsNONE:
		default:
			USVGTEXT_SET_TEXT(window, message, "Negative error messages are still not processed");
			break;
	}
	window->widgets["ok"]->show();
}
// -------------------------------------------------------------------------
void Screenshot::on_show(UWindow* window)
{
	if(busy)
		return;
	mywindow = window;
	mywindow->hide();
	busy = true;
	ScriptWindowLogic::on_show(mywindow);
	//signal_show_.emit();
}
// -------------------------------------------------------------------------
void Screenshot::show()
{
	if(mywindow)
		mywindow->show();
}
// -----------------------------------------------------------------------------
void Screenshot::finish(int status, std::string msg)
{
	if(mywindow)
	{
		ScriptWindowLogic::finish(status, mywindow, msg);
		as.stop();
		busy = false;
	}
}