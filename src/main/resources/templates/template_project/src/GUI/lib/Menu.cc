#include "Menu.h"
#include <uniwidgets/UWindow.h>
#include <Configuration.h>
#include "Display.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
using namespace TEMPLATE_PROJECT;
// -------------------------------------------------------------------------
std::shared_ptr<Menu> Menu::menu = std::shared_ptr<Menu>();
// -------------------------------------------------------------------------
Menu::Menu(const Glib::RefPtr<Gnome::Glade::Xml>& gxml) :
	backbtn(nullptr),
	nbook(nullptr)
{
	if( !gxml )
	{
		ucrit << "Menu(): Wrong glade!" << endl;
		throw;
	}
	Gtk::Container* con = nullptr; 
	gxml->get_widget("tabbox", con);
	gxml->get_widget("back", backbtn);
	gxml->get_widget("ulocknotebook", nbook);
        if( !con || !nbook ) //if( !con || !backbtn || !nbook )
	{
		ucrit << "Menu(): No 'back' or 'tabbox' or 'ulocknotebook' in glade!" << endl;
		throw;
	}
        //backbtn->signal_button_release_event().connect(sigc::hide(sigc::mem_fun(this, &Menu::on_back_clicked)));
	sigc::slot<bool, GdkEvent*> button_slot = sigc::mem_fun( this, &Menu::on_mouse_button_release_event );
	Display::Instance()->connect_signal_event(button_slot, GDK_BUTTON_RELEASE);
	//nbook->signal_switch_page().connect(sigc::hide(sigc::hide(sigc::mem_fun(this, &Menu::on_main_switch_page))));
	nbook->foreach(sigc::mem_fun(this, &Menu::connect_switch_page));
        con->foreach(sigc::mem_fun(this, &Menu::connect_button_click));
        on_main_switch_page();
	nbook->enable_lock(true);
}
// -------------------------------------------------------------------------
Menu::~Menu()
{
}
// -------------------------------------------------------------------------
std::shared_ptr<Menu> Menu::Instance(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml)
{
	if( !menu )
		menu = std::shared_ptr<Menu>(new Menu(gladexml));
	return menu;
}
void Menu::connect_switch_lock(const Glib::RefPtr<Gnome::Glade::Xml>& gladexml)
{
    SetWidgets::UWindow::signal_connect(gladexml, "switch_lock", G_CALLBACK(Menu::switch_lock));
}
// -------------------------------------------------------------------------
void Menu::switch_lock(GtkWidget* w)
{
	if( !menu )
		return;
	Gtk::CheckButton* check = dynamic_cast<Gtk::CheckButton*>(Glib::wrap(w));
	if( check )
	{
                bool lock = check->get_active();
		enable_lock(*menu->nbook, lock);
		menu->nbook->foreach(sigc::bind(&Menu::enable_lock, lock));
	}
}
// -------------------------------------------------------------------------
void Menu::enable_lock(Gtk::Widget& w, bool lock)
{
	ULockNotebook* lockbook = dynamic_cast<ULockNotebook*>( &w );
	if(lockbook)
		lockbook->enable_lock(lock);
}
// -------------------------------------------------------------------------
void Menu::connect_button_click(Gtk::Widget& w)
{
	UniSVGButton* btn = dynamic_cast<UniSVGButton*>( &w );
        if( btn ) //if( btn && btn != backbtn )
		btn->signal_button_release_event().connect(sigc::hide(sigc::bind(sigc::mem_fun(this, &Menu::on_tab_clicked),btn)));
}
// -------------------------------------------------------------------------
void Menu::connect_switch_page(Gtk::Widget& w)
{
	Gtk::Notebook* book = dynamic_cast<Gtk::Notebook*>( &w );
	if(book)
		book->signal_switch_page().connect(sigc::hide(sigc::hide(sigc::bind(sigc::mem_fun(this, &Menu::on_switch_page),book))));
}
// -------------------------------------------------------------------------
void Menu::on_switch_page(Gtk::Notebook* book)
{
	if( book )
	{
		if( backpages.empty() || backpages.top() != book->get_current_page() )
			backpages.push( book->get_current_page() );
	}
        //backbtn->hide();
	if( backpages.size() > 1 )
                //backbtn->show();
	
	cout << "Menu::on_switch_page() ";
	std::stack<int> tmppages = backpages;
	while( !tmppages.empty() )
	{
		cout << tmppages.top() << " ";
		tmppages.pop();
	}
	cout << endl;
}
// -------------------------------------------------------------------------
void Menu::on_main_switch_page()
{
	while( !backpages.empty() )
		backpages.pop();
	//Gtk::Notebook* chnbook = dynamic_cast<Gtk::Notebook*>( Glib::wrap( w ) );
	backpages.push( 0 );
	Gtk::Notebook* chnbook = dynamic_cast<Gtk::Notebook*>( nbook->get_nth_page(nbook->get_current_page()) );
	if( chnbook )
		chnbook->set_current_page(0);
        //backbtn->hide();
	if( backpages.size() > 1 )
                //backbtn->show();
	
	cout << "Menu::on_main_switch_page() ";
	std::stack<int> tmppages = backpages;
	while( !tmppages.empty() )
	{
		cout << tmppages.top() << " ";
		tmppages.pop();
	}
	cout << endl;
}
// -------------------------------------------------------------------------
bool Menu::on_mouse_button_release_event(GdkEvent* ev)
{
	GdkEventButton* event = (GdkEventButton*)ev;
	if( event->button == 3 )
	{
		menu->on_back_clicked();
		return true;
        }
	return false;
}
// -------------------------------------------------------------------------
bool Menu::on_back_clicked()
{
	cout << "Menu::on_back_clicked() ";
	Gtk::Notebook* chnbook = dynamic_cast<Gtk::Notebook*>( nbook->get_nth_page(nbook->get_current_page()) );
	if( chnbook && backpages.size() > 1 )
	{
		backpages.pop();
		cout << backpages.top() << " ";
		chnbook->set_current_page( backpages.top() );
	}
	cout << endl;
        //backbtn->hide();
	if( backpages.size() > 1 )
                //backbtn->show();
	return false;
}
// -------------------------------------------------------------------------
bool Menu::on_tab_clicked(UniSVGButton* button)
{
	if( !button )
		return false;
	if( nbook->get_current_page() == button->property_sensor_value() )
		on_main_switch_page();
	return false;
}
