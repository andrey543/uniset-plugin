#ifndef _ConfigWindowLogic_H
#define _ConfigWindowLogic_H

#include <uniwidgets/UWindowLogic.h>
#include "ClockWindowLogic.h"
#include "ScriptWindowLogic.h"

namespace TEMPLATE_PROJECT
{
	class ConfigWindowLogic:
		public SetWidgets::UWindowLogic
	{
		public:
			ConfigWindowLogic();
			virtual ~ConfigWindowLogic(){};
			
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void hide();
			
			ClockWindowLogic clock;
			ScriptWindowLogic script;
			ScriptWindowLogic blockscript;
			Screenshot screenshot;
			
		private:
			SetWidgets::UWindow* mywindow;
			bool touch_blocked;
			void on_block_touch_switched();
	};
}

#endif /* _ConfigWindowLogic_H */
