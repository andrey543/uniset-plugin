#ifndef _SelectWindowLogic_H
#define _SelectWindowLogic_H

#include <uniwidgets/UWindowLogic.h>
#define OK "ok"
#define RESET "reset"

#define GTKCHECKBUTTON_RESET(window, varname1) \
{ \
	Gtk::CheckButton* varname1 = dynamic_cast<Gtk::CheckButton*>(window->widgets[#varname1]); \
	if( varname1 ) \
		varname1->set_active(false); \
}

#define GTKCHECKBUTTON_SET(window, varname1) \
{ \
	Gtk::CheckButton* varname1 = dynamic_cast<Gtk::CheckButton*>(window->widgets[#varname1]); \
	if( varname1 ) \
		varname1->set_active(true); \
}

#define GTKCHECKBUTTON_GET(window, varname1, varname2) \
{ \
	varname2 = false; \
	Gtk::CheckButton* varname1 = dynamic_cast<Gtk::CheckButton*>(window->widgets[#varname1]); \
	if( varname1 ) \
		varname2 = varname1->get_active(); \
}

#define GTKCHECKBUTTON_TOGGLE(window, varname, check) \
{ \
	Gtk::CheckButton* varname = dynamic_cast<Gtk::CheckButton*>(window->widgets[#varname]); \
	if( varname ) \
	{ \
		if(check == varname ) \
			varname->set_active(true); \
		else \
			varname->set_active(false); \
	} \
}

#define GTKCHECKBUTTON_SET_DI_SENSOR(window, sensor, value) \
{ \
	UWindowCommonLogic::on_action(window, #sensor, value); \
}

#define GTKCHECKBUTTON_SET_AI_SENSOR(window, check, sensor, value) \
if(check) \
{ \
	UWindowCommonLogic::on_action(window, #sensor, uni_atoi(window->variables[#value])); \
}

namespace TEMPLATE_PROJECT
{
	class SelectWindowLogic:
		public SetWidgets::UWindowCommonLogic
	{
		public:
			SelectWindowLogic();
			virtual ~SelectWindowLogic(){};
			
			virtual void on_property_changed(SetWidgets::UWindow* window) override;
			virtual void on_action(SetWidgets::UWindow* window, std::string name, long value) override;
			static bool on_toggled(GtkWidget* w);
			static void toggle(SetWidgets::UWindow* window, Gtk::CheckButton* check);
	};
}

#endif /* _SelectWindowLogic_H */
