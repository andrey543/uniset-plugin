#ifndef _AutomatWindowLogic_H
#define _AutomatWindowLogic_H

#include <uniwidgets/UWindowLogic.h>

#define USVGTEXT_UPDATE_BY_UVALUE(window, varname) \
{ \
	UValueIndicator* varname = dynamic_cast<UValueIndicator*>(window->parents[#varname]); \
	if( varname ) \
	{ \
		ostringstream os; \
		os << varname->get_property_value_(); \
		USVGTEXT_SET_TEXT(window, varname, os.str()); \
	} \
} \

namespace TEMPLATE_PROJECT
{
	class AutomatWindowLogic:
		public SetWidgets::UWindowLogic
	{
		public:
			AutomatWindowLogic();
			virtual ~AutomatWindowLogic(){};
			enum ActiveButton
			{
				abON,
				abOFF,
				abNONE,
				abAVR,
				abNONE2
			};
		
		protected:
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void on_property_changed( SetWidgets::UWindow* window ) override;
	};
}

#endif /* _AutomatWindowLogic_H */
