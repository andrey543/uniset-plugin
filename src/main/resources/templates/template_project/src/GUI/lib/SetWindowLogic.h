#ifndef _SetWindowLogic_H
#define _SetWindowLogic_H

#include <uniwidgets/UWindowLogic.h>

namespace TEMPLATE_PROJECT
{
	class SetWindowLogic:
		public SetWidgets::UWindowCommonLogic
	{
		public:
			SetWindowLogic(){};
			virtual ~SetWindowLogic(){};
			
		protected:
			virtual void on_show( SetWidgets::UWindow* window ) override;
			virtual void on_property_changed( SetWidgets::UWindow* window ) override;
			virtual void on_action( SetWidgets::UWindow* window, std::string name, long value ) override;
			virtual void update_widget(SetWidgets::UWindow* window, const std::string& name) = 0;
			virtual void update_sensor(SetWidgets::UWindow* window, SetWidgets::UWindow::Sensors::iterator& it) = 0;
	};
}

#endif /* _SetWindowLogic_H */
