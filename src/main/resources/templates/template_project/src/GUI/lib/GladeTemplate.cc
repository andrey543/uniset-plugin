#include "GladeTemplate.h"
using namespace std;
using namespace TEMPLATE_PROJECT;
using namespace uniset;
using namespace SetWidgets;
// ------------------------------------------------------------------------------------------
GladeTemplate::GladeTemplate(const std::string& filename):
	Gnome::Glade::XmlBuffer<UReplace, UniXML>()
{
	try
	{
		UniXML::open(filename);
	}
	catch(uniset::NameNotFound ex)
	{
		throw Gnome::Glade::XmlError(ex.what());
	}
}
// ------------------------------------------------------------------------------------------
GladeTemplate::GladeTemplate(const std::string& filename, int page, bool force):
	Gnome::Glade::XmlBuffer<UReplace, UniXML>()
{
	if( !force && page > 0 )
	{
		string fname = filename;
		ostringstream os;
		os << page << ".glade";
		fname.replace( fname.rfind(".glade"), 6, os.str() );
		try
		{
			UniXML::open(fname);
			cout << "GladeTemplate::open(): " << fname << endl;
		}
		catch( const NameNotFound& ex )
		{
			UniXML::open(filename);
		}
	}
	else
	{
		key = page;
		UniXML::open(filename);
		parse();
	}
	::xmlDocDumpMemory(doc.get(), &buffer, &size);
}