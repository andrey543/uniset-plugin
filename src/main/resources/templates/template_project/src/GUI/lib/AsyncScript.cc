// --------------------------------------------------------------------------
#include <sstream>
#include <iomanip>
#include <iostream>
#include "AsyncScript.h"
// --------------------------------------------------------------------------
using namespace std;
// -------------------------------------------------------------------------
AsyncScript::AsyncScript(  const std::string& script, const std::string& def_params ):
	workdir("."),
	script(script),
	def_params(def_params),
	child_pid(0),
	last_status(0)
{
}
// --------------------------------------------------------------------------
AsyncScript::AsyncScript():
	workdir("."),
	script(""),
	def_params(""),
	child_pid(0),
	last_status(0)
{

}
// --------------------------------------------------------------------------
AsyncScript::~AsyncScript()
{

}
// --------------------------------------------------------------------------
bool AsyncScript::is_running()
{
	return (child_pid != 0);
}
// --------------------------------------------------------------------------
void AsyncScript::stop()
{	
	Glib::spawn_close_pid(child_pid);
	child_pid = 0;
	connTimeout.disconnect();
	connWatch.disconnect();
}
// --------------------------------------------------------------------------
bool AsyncScript::run( const std::string& cmd, int tout_sec, bool force )
{
	script = cmd;
	return run(tout_sec,force);
}
// --------------------------------------------------------------------------
bool AsyncScript::run( int tout_sec, bool force )
{
	if( script.empty() )
	{
		cerr << "(AsyncScript): script not defined! " << endl; 
		signal_sfinish.emit(fsFAIL);
		return false;
	}

	if( !force && child_pid > 0 )
		return true;

	if( force && child_pid > 0 )
		Glib::spawn_close_pid(child_pid);

	std::list<std::string> lenvp;
	std::list<std::string> largv; largv = Glib::shell_parse_argv(script);

	if( !def_params.empty() )
		largv.push_back( def_params );

	if( tout_sec < 0 )
		tout_sec = 0;

//	cout << "(cycleSaveTime): running " << cmd.str() << endl;
	try
	{
		cerr << "(AsyncScript): RUN " << script << endl;
		//Glib::SpawnFlags fl;
		Glib::spawn_async(workdir,largv,lenvp,Glib::SPAWN_DO_NOT_REAP_CHILD,
				sigc::mem_fun(*this, &AsyncScript::child_setup),&child_pid);

		if( child_pid )
		{ 
			connWatch.disconnect();
			connTimeout.disconnect();

			if( tout_sec > 0 )
			{
				connTimeout = Glib::signal_timeout().connect(sigc::mem_fun(*this, &AsyncScript::on_timeout),tout_sec*1000);
			}

			connWatch = Glib::signal_child_watch().connect(sigc::mem_fun(*this, &AsyncScript::watch_handler), child_pid );
		}
		else
			child_pid = 0;
	}
	catch( Glib::Exception& ex )
	{
		cerr << "(AsyncScript): RUN FAILED: pid=" << child_pid << " err: " <<  ex.what() << endl;
		Glib::spawn_close_pid(child_pid);
		child_pid = 0;
	}

	if( child_pid == 0 )
	{
		connWatch.disconnect();
		connTimeout.disconnect();
		signal_sfinish.emit(fsFAIL);
		return false;
	}

	return true;
}
// --------------------------------------------------------------------------
void AsyncScript::watch_handler( Glib::Pid pid, int child_status )
{
	cerr << "(AsyncScript): CHILD EXITED!!! cmd='" << script << "' status=" << child_status << endl;
	Glib::spawn_close_pid(child_pid);
	child_pid = 0;
	last_status = child_status;
	connTimeout.disconnect();

  	int ret = WEXITSTATUS(child_status);
	if( ret==1 || ret==127 )
		signal_sfinish.emit(fsFAIL);
	else
		signal_sfinish.emit(fsOK);
}
// --------------------------------------------------------------------------
bool AsyncScript::on_timeout()
{
	if( child_pid )
	{
		cerr << "(AsyncScript): CHILD RUN TIMEOUT!!! cmd='" << script << "'" << endl;
		Glib::spawn_close_pid(child_pid);
		child_pid = 0;
		connWatch.disconnect();
	}	

	signal_sfinish.emit(fsFAIL);
	return false;
}
// --------------------------------------------------------------------------
