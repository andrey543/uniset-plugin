#include "GladeTemplate.h"

using namespace std;
using namespace TEMPLATE_PROJECT;
using namespace uniset;

int main (int argc, const char** argv)
{
    try
    {
	if(argc < 3)
		return 1;
        string gladefile = argv[1];
        for (int i=2; i<argc; i++)
        {
            string numgladefile_str = argv[i];
            int numgladefile = atoi(argv[i]);

            GladeTemplate tmp_glade( gladefile + ".glade", numgladefile, true );
            tmp_glade.save( gladefile + numgladefile_str + ".glade" );
            cout << "Создан файл " << gladefile << numgladefile_str << ".glade" <<endl;
        }
        
        return 0;
    }
    catch(Exception& ex)
    {
        ucrit << "(gen-glade): " << ex << endl;
    }
    catch(...)
    {
        ucrit << "(gen-glade): catch(...)" << endl;
    }

    return 1;

}
