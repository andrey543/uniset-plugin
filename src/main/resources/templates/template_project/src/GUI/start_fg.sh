#!/bin/sh
#mv ~/.gtkrc-2.0 ~/.gtkrc-2.0.tmp
#cp -f .gtkrc-2.0 ~/


PROJECT_PATH=`dirs -l`
PROJECT_PATH=${PROJECT_PATH}/../..
WIDGETS_PATH=$PROJECT_PATH/src/GUI/widgets
LIBSWIDGETS_PATH=$WIDGETS_PATH/.libs
LIBSSWIDGETS_PATH=${PROJECT_PATH}/../setwidgets/plugins/libglade/.libs
LIBSUWIDGETS_PATH=${PROJECT_PATH}/../uniwidgets/plugins/libglade/.libs

LIBGLADE_MODULE_PATH="$LIBSWIDGETS_PATH"
test "`arch`" = "x86_64" && UNIWIDGETS_MODULE_PATH="/usr/lib64"
case $1 in
	-l|--local|local)
		shift
		LIBGLADE_MODULE_PATH="$LIBGLADE_MODULE_PATH:$LIBSSWIDGETS_PATH:$LIBSUWIDGETS_PATH"
		UNIWIDGETS_MODULE_PATH=${PROJECT_PATH}/../uniwidgets/plugins/uniset2/.libs
		;;
esac
export LIBGLADE_MODULE_PATH
export UNIWIDGETS_MODULE_PATH

START=uniset2-start.sh
#strace
extparam="--locale-dir ../../po \
			--svg-dir ./svg/ \
			--cbid 51  \
			--guifile gui.glade \
			--pass-check-off \
			--gladedir ./glade/ \
			--gtkthemedir ./theme/ \
			--smemory-id SharedMemory1
			--sm-no-history 1 --sm-no-dump 1 \
			--activate-timeout 20000 \
			--pulsar-id GUI_GEU1_Pulsar_S \
			--pulsar-msec 3000 \
			--confirm-all-time 2000 \
			--confirm-all-timeout 50 \
			--dlog-add-levels warn,crit,any \
			--unideb-add-levels info,warn,crit,any \
			--add-sm \
			--mbmgate-log-add-levels any \
			--signals-debug 1 \
			$*"

MAIN_GLADE=./glade/gui.glade

if cat ${MAIN_GLADE} | grep -q '<requires lib="setwidgets2"/>';
then
    echo 'Already exist <requires lib="setwidgets2"/>';
else
    subst 's|<!-- interface-requires gtk+ 2.6 -->|<!-- interface-requires gtk+ 2.6 -->\n  <requires lib=\"uniwidgets\"/><requires lib=\"setwidgets2\"/>|' ${MAIN_GLADE}
    echo "Add  <requires lib="setwidgets2"/>"
fi


#subst "s|<!-- interface-requires ��� 2.6 -->||" ${MAIN_GLADE}

#jmake && 
GTK2_RC_FILES= LC_NUMERIC=C LANG=ru_RU.UTF-8 ${START} -f ./TEMPLATE_PROJECT-gui $extparam
# GTK2_RC_FILES=../theme/gtkrc
#|| GTK2_RC_FILES= LC_NUMERIC=C ${START} -f ./TEMPLATE_PROJECT-gui-geu $extparam
			#&>libslog
#rm -f  ~/.gtkrc-2.0
#mv ~/.gtkrc-2.0.tmp ~/.gtkrc-2.0


