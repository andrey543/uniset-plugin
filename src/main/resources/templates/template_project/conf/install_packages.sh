#!/bin/sh

RETVAL=0
PROG="${0##*/}"
ACTION="update"

. ctl-conf.sh

check_key

set_list "$*"

init_pssh_vars

exec pssh -o $LOGDIR -e $LOGDIR -t $PSSH_TIMEOUT -p $PSSH_MAX_THREAD -l $SSHUSER -h $hlist /usr/bin/ctl-$ACTION.sh
