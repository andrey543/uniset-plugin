#!/bin/sh
# Вспомогательный скрипт для подготовки и сборки rpm-пакета с системой

ETERBUILDVERSION=163
. /usr/share/eterbuild/eterbuild
load_mod spec

REL=setri
MAILDOMAIN=server

unset CCACHE_PATH
export GCC_USE_CCACHE=1
export CCACHE_DIR=$HOME/.ccache
export CC=gcc CXX=g++

# TEMPLATE_PROJECT pocket path
TOPDIR=/var/ftp/Ourside/

PKGNAME=TEMPLATE_PROJECT
SPECNAME=TEMPLATE_PROJECT.spec

PROJECT=$1
test -n "$PROJECT" || PROJECT=$PKGNAME

if [ -d "$TOPDIR" ] ; then
	GEN="genbasedir --create --progress --topdir=$TOPDIR i586 $PROJECT"
else
	# For NoteBook build
	TOPDIR=/var/ftp/pub/Ourside/
	GEN=/var/ftp/pub/Ourside/i586/genb.sh
fi
FTPDIR=$TOPDIR/i586/RPMS.$PROJECT
BACKUPDIR=$FTPDIR/backup
COMMIT="$(git rev-parse --verify HEAD)"

VSTAND_OPT="--enable=vstand"

fatal()
{
	echo "Error: $@"
	exit 1
}

function send_notify()
{
	export EMAIL="$USER@$MAILDOMAIN"
	CURDATE=`date`
	MAILTO="devel@$MAILDOMAIN"
# FIXME: проверка отправки
mutt $MAILTO -s "[$PROJECT] New build: $BUILDNAME" <<EOF
Готова новая сборка: $BUILDNAME
-- 
your $0
$CURDATE
EOF
echo "inform mail sent to $MAILTO"
}

function cp2ftp()
{
	RPMBINDIR=$RPMDIR/RPMS
	test -d $RPMBINDIR/i586 && RPMBINDIR=$RPMBINDIR/i586
	mkdir -p $BACKUPDIR
	mv -f $FTPDIR/*$PKGNAME* $BACKUPDIR/
	mv -f $RPMBINDIR/*$PKGNAME* $FTPDIR/
	chmod 'a+rw' $FTPDIR/*$PKGNAME*
	$GEN
}


# ------------------------------------------------------------------------
if [ -n "$BUILD_VSTAND" ]; then 
    add_changelog -e "- (build for vstand): commit $COMMIT" $SPECNAME
    rpmbb $VSTAND_OPT $SPECNAME || fatal "Can't build"
else
    buildhost=`hostname -s`
    add_changelog -e "- (${buildhost}): build on commit $COMMIT" $SPECNAME
    rpmbb $SPECNAME || fatal "Can't build"
fi

cp2ftp

echo "remove $HOME/RPM/BUILD/${PKGNAME}*"
rm -rf $HOME/RPM/BUILD/${PKGNAME}*

echo "remove old backup files from $BACKUPDIR"
find $BACKUPDIR/ -type f -mtime +15 -delete

#rpmbs $SPECNAME
#send_notify

# Увеличиваем релиз и запоминаем спек после успешной сборки
# inc_release $SPECNAME
