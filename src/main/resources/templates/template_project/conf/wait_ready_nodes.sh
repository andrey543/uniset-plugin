#!/bin/sh

PROG="${0##*/}"
[ -z "$2" ] && wait_limit=120 || wait_limit=$1

. ctl-conf.sh
names_hosts=
all_ok=0
tab=
one_dump=
inf=
NODES_R1="r1_vo11 r1_vo12 r1_vo21 r1_vo22 r1_ged1_io r1_ged1_prot r1_ged2_io r1_ged2_prot r1_ged3_io r1_ged3_prot r1_geu1 r1_geu2 r1_geu3 r1_gru1 r1_gru2 r1_gui1 r1_gui2 \
r1_geu1cpu r1_geu1hr r1_geu1hr_r r1_geu1hr_l r1_geu1kp r1_geu2cpu r1_geu2hr r1_geu2hr_r r1_geu2hr_l r1_geu2kp r1_geu3cpu r1_geu3hr r1_geu3hr_r r1_geu3hr_l r1_geu3kp \
r1_pultsed"
NODES_R2="r2_vo11 r2_vo12 r2_vo21 r2_vo22 r2_ged1_io r2_ged1_prot r2_ged2_io r2_ged2_prot r2_ged3_io r2_ged3_prot r2_geu1 r2_geu2 r2_geu3 r2_gru1 r2_gru2 r2_gui1 r2_gui2 \
r2_geu1cpu r2_geu1hr r2_geu1hr_r r2_geu1hr_l r2_geu1kp r2_geu2cpu r2_geu2hr r2_geu2hr_r r2_geu2hr_l r2_geu2kp r2_geu3cpu r2_geu3hr r2_geu3hr_r r2_geu3hr_l r2_geu3kp \
r2_pultsed"

[ "$1" = "-r1" ] && NODES="$NODES_R1"
[ "$1" = "-r2" ] && NODES="$NODES_R2"
[ "$1" = "-a" ] && NODES="$NODES_R1 $NODES_R2"
#временное решение
[ "$1" = "-d" ] && one_dump=1
[ "$2" = "-d" ] && one_dump=1

#временное решение
[ "$1" = "-i" ] && inf=1
[ "$2" = "-i" ] && inf=1

echo "Check availability nodes"
echo "----------------"
while :;do
	names_hosts=$(./ping_host.sh "$NODES" | grep "FAIL" )
	all_ok=1
	for X in $NODES; do
		echo "$X" | grep -q "imitator" && tab="" || tab="	";
		ip=`resolveip -s $X`
		if echo "$names_hosts" | grep -q "'$X'" ;
					then echo "$X:$tab		$ip		FAIL	(timeout: $wait_limit s)" && all_ok=0
					else echo "$X:$tab		$ip		OK"
		fi
	done
	[ -n "$one_dump" ] && exit $((!all_ok))
	echo "----------------"
	[ -n "$inf" ] && sleep 5 && continue;
	[ "$all_ok" = 1 ] && echo "ALL NODES ALREADY" && exit 0;
	[ "$wait_limit" -le 0 ] && echo "" && echo "Warning: Вышло время ожидания. Выполение остановлено" && exit 1;
	wait_limit=$((wait_limit - 5))
	sleep 5
done
