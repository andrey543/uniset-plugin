#!/bin/bash

PROG="${0##*/}"
SRC_FILE=$1
REPL_FILE=$2

BEGIN_TAG=$3
END_TAG=$4

OLD_IFS=$IFS

print_help()
{
	  [ "$1" = 0 ] || exec >&2
	  cat <<EOF
Usage:	$PROG SRC_FILE REPL_FILE BEGIN_TAG END_TAG

	SRC_FILE		- файл, в который вставляем измененные датчики (configure.xml)
	REPL_FILE		- файл со списком датчиков после выполнения скрипта *_make.sh
	BEGIN_TAG		- начальный тег, с которого начинается область замены в SRC_FILE
	END_TAG		- конец области замены в SRC_FILE
EOF
	[ -n "$1" ] && exit "$1" || exit
}

[ -z "$1" ] && print_help 1

number_line_begin=`cat ${SRC_FILE} | grep -n "<!--\s*$BEGIN_TAG\s*-->"`
number_line_end=`cat ${SRC_FILE} | grep -n "<!--\s*$END_TAG\s*-->"`
number_line_begin=${number_line_begin%%:*}
number_line_end=${number_line_end%%:*}
number_line_all=`cat ${SRC_FILE} | wc -l`

IFS="";
cat ${SRC_FILE} | grep -m $number_line_begin ""
cat ${REPL_FILE}
cat ${SRC_FILE} | grep --after-context=$((number_line_all - number_line_end)) "<!--\s*$END_TAG\s*-->"
IFS=$OLD_IFS
