#!/bin/bash

. make_common.sh

[ -z "$correctlist" ] && correctlist=common_rules

input=$1
#Номер, с которого начнется отсчет id
number=$2
[ -z "$number" ] && number=54000
inc_number=100

shift_number=$number

PROG="${0##*/}"

print_help()
{
		[ "$1" = 0 ] || exec >&2
		cat <<EOF
		Usage: $PROG name_file [BEGIN_ID]
		name_file - input file(*.csv with separator ";")
		BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1

#Генерируем правила замен и common_rules
rules=$(get_autozamena_sed_rules "$correctlist")

for NUM in `seq 2`; do
	echo "			<!-- Датчики сообщений для ДРК${NUM} (сгенерировано из ${input}) $USER: `date -u +"%d.%m.%Y"` -->"
	for OBJ in "Pump1#Насос1" "Pump2#Насос2" "Pump3#Насос3" "Pump4#Насос4" "Heater#Обогрев" \
	"Fan1#Вентилятор 1" "Fan2#Вентилятор 2" "Feeder1#Фидер питания 1" "Feeder2#Фидер питания 2"; do

		NAME=${OBJ%#*}
		DESC=${OBJ#*#}
		echo "			<!-- Датчики сообщений для ${DESC} -->"
	
		while read line; do
		    if [[ $line == *"[OBJ]"* ]]
		    then
			echo $line | sed "s|^id;|			<item id=\"$number|g;s|\[1\]|${NUM}|g;s|\[OBJ\]|${NAME}|g;s|\[DESC\]|${DESC}|g;${rules}"
			number=$((number+1))
		    fi
		done < $input
	
		let number=$((shift_number+inc_number))
		shift_number=$number
	done
	
	# Генерируем датчики-сообщения без [OBJ]
	echo "			<!-- Прочие датчики сообщений для ДРК${NUM} -->"
	while read line; do
	    if [[ $line != *"[OBJ]"* ]]
	    then
		echo $line | sed "s|^id;|			<item id=\"$number|g;s|\[1\]|${NUM}|g;${rules}"
		number=$((number+1))
	    fi
	done < $input
	
	let number=$((shift_number+inc_number))
	shift_number=$number
done