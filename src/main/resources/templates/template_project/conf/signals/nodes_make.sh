#!/bin/bash

. make_common.sh

[ -z "$correctlist" ] && correctlist=common_rules

input=$1
#Номер, с которого начнется отсчет id
number=$2
[ -z "$number" ] && number=100
#Сдвиговый параметр, на сколько будет отступ между датчиками (например, между TV!1 и TV2)
inc_number=10

shift_number=$number

PROG="${0##*/}"

print_help()
{
		[ "$1" = 0 ] || exec >&2
		cat <<EOF
		Usage: $PROG name_file [BEGIN_ID]
		name_file - input file(*.csv with separator ";")
		BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1

#Генерируем правила замен и common_rules
rules=$(get_autozamena_sed_rules "$correctlist")

# Генерируются датчики для управления с каждого поста GUI1,GUI2,MAIN
# управление КСУ вносится в отдельном csv..

echo "			<!-- Датчики для узлов (сгенерировано из csv ${1}) $USER: `date -u +"%d.%m.%Y"` -->"
for N in gui1 vrk geu geu_aps gui2; do

	NB=$(echo $N | tr '[:lower:]' '[:upper:]')

	echo "			<!-- Узел: $N -->"

	while read line; do

		echo $line | sed "s|id;|			<item id=\"$number|g;s|NODE|${NB}|g;${rules}"
		number=$((number+1))

	done< $input

	let number=$((shift_number+inc_number))
	shift_number=$number
done
