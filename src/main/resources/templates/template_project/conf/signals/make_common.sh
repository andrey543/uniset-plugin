#!/bin/sh
#Скрипт с общими функциями

DEL_ARM_RULES=";s|;arm[^;]*;[^;]*||g;"
DEL_MCSES_RULES=";s|;mcses[^;]*;[^;]*||g;"
#убираем mtype у датчиков S1 и S2
DEL_MTYPE_RULES=";s|;mtype;[0-9]||g;"
#убираем message_l у датчиков S1 и S2
DEL_MSG_RULES=";s|;message_l;[^;]*|#message_l|g;s|#message_l;[^;]*:[^;]*|#message_l|g;s|#message_l||g;"

#Раскомментировать строчку при сборке для судна!
#DEL_TCP_RULES=";s|;tcp[^;]*;[^;]*||g;"

get_autozamena_sed_rules()
{
	OLD_IFS=$IFS
	[[ -z "$1" ]] && echo "(get_autozamena_sed_rules): ERROR: Unknown file rules..." && exit 1
	list=$1
	sed_rules=
	while read line; do
	IFS="		"
	set -- $line
	sed_rules="${sed_rules};s:$1:$2:g"
	done < $list
	IFS=$OLD_IFS
	echo $sed_rules
}

generate_sensor_channels()
{
	file=$1
	mc_filter=$2

	MC1_RULES="s|;name;\([^;]*\);\(.*\);${mc_filter}_iotype;\(..\)|;name;\11;\2;unet;ses1;${mc_filter};1;${mc_filter}_iotype;\3;${mc_filter}_sensor;\1|g;s|;tcp;\([^;]*\)|;tcp;\1_ch1|g;s|;textname;\([^;]*\);|;textname;\1 (канал N1);|g;s|@\([^;]*\)@|@\11@|g"
	MC2_RULES="s|;name;\([^;]*\);\(.*\);${mc_filter}_iotype;\(..\)|;name;\12;\2;unet;ses2;${mc_filter};2;${mc_filter}_iotype;\3;${mc_filter}_sensor;\1|g;s|;tcp;\([^;]*\)|;tcp;\1_ch2|g;s|;textname;\([^;]*\);|;textname;\1 (канал N2);|g;s|@\([^;]*\)@|@\12@|g"

	while read line; do
		if `echo $line | grep ${mc_filter}_iotype | grep -qG ";name;[^;]*S;"`
		then
			#Обобщенный датчик
			echo $line | sed "${DEL_TCP_RULES}"
			#Датчик по первому каналу
			echo $line | sed "${DEL_ARM_RULES}${DEL_MCSES_RULES}${DEL_MTYPE_RULES}${DEL_MSG_RULES}${MC1_RULES}"
			#Датчик по второму каналу
			echo $line | sed "${DEL_ARM_RULES}${DEL_MCSES_RULES}${DEL_MTYPE_RULES}${DEL_MSG_RULES}${MC2_RULES}"
		else
			echo $line
		fi
	done < $file
}
