#!/bin/bash

#Скрипт, предназначенный для пост-обработки списков датчиков
PROG="${0##*/}"

IN_FILE=$1
TMP_FILE="$1"_tmp

#Переменные для генерирования MessageList'а
MLIST_FILE=
MLIST_TEXT=
MLIST_TEXTNAME=
MSG_REPLACE=

#Переменные для генерирования тэгов
TAGDIR="tag.d"
TAG_REPLACE=

print_help()
{
	[ "$1" = 0 ] || exec >&2
	cat <<EOF
	Usage: $PROG name_file.xml
EOF
	[ -n "$1" ] && exit
}
#поиск replace'ов
find_message_replace()
{
	#обнуляем правила
	MSG_REPLACE=
	#проверяем, есть ли правила замен
	echo $MLIST_FILE | grep -q ";" && MSG_REPLACE=`echo ${MLIST_FILE#*;} | sed -e "s|^|;s\:|g;s|,$||g;s|,|:g;s:|g;s|$|:g;|g"`

}

#Поиск и обработка MessageList'а
message_replace()
{

	OLD_IFS=$IFS
	IFS=""

	while read line; do
		if echo "$line" | grep -q "Message_LIST";
		then
			MLIST_TEXT=
			MLIST_TEXTNAME=`echo "$line" | sed -e "s|.* textname=\"\([^\"]*\)\".*|\1|g"`

			while echo $line | grep -q "Message_LIST" ;
			do
				#ищем в настройках message-файл
				MLIST_FILE=`echo $line | sed -e "s|.*Message_LIST=\"\([^\"]*\)\".*|\1|g"`

				find_message_replace

				MLIST_TEXT="${MLIST_TEXT}`eval cat ${MLIST_FILE%;*} | tr '>\n' '#%' | sed -e "s|#%|>newLine|g;${MSG_REPLACE}"`"
				#убираем из configure найденный message-файл
				line=`echo "$line" | sed -e "s| Message_LIST=\"${MLIST_FILE%;*};[^\"]*\"||g;s| Message_LIST=\"${MLIST_FILE}||g"`
			done

			echo "$line" | sed -e "s|/>$|>\n${MLIST_TEXT}newLine			</item>|g;s|newLinenewLine|newLine|g;s|MLIST_TEXTNAME|$MLIST_TEXTNAME|g;s|newLine|\n|g" >>$TMP_FILE
		else
			echo "$line" >>$TMP_FILE
		fi
	done < $IN_FILE

	#Переписываем из временного в исходный файл
	cat $TMP_FILE > $IN_FILE

	IFS=$OLD_IFS
	rm -f $TMP_FILE
}

#Поиск и раскрытие тэгов
tag_replace()
{
	local count_loop=1

	#Расставляем @SENSORNAME@ в файле
	subst "s| name=\"\(\w*\)\"\(.*\)/>$| name=\"\1\"\2 @\1@/>|" $IN_FILE

	#крутим цикл, пока не раскроем все вложенные тэги
	#выходим из цикла если уровень вложенности больше 5
	while [[ "${count_loop}" -le "5" ]];
	do
		unset TAGLIST TAG_REPLACE REPLACE_LIST

		#Получаем список тэгов в файле
		TAGLIST=`cat $IN_FILE | grep -oG "@[A-Za-z0-9_]*@"`

		#выходим из цикла если больше замен нет
		[[ -z "${TAGLIST}" ]] && break

		for TAG in $TAGLIST; do
			#Ищем файлы с заменами и правила замен в tag.d и генерируем правило sed'а (замена на пустоту, если не нашли)
			flagtag=
			ls ${TAGDIR}/ | grep -qG "\.conf$" && cat ${TAGDIR}/*.conf | grep -qG "^${TAG}" && flagtag=1
			if [[ -n ${flagtag} ]];
			then
				#Список всех объявленных замен для этого тэга
				REPLACE_LIST=`eval cat ${TAGDIR}/*.conf | grep -G "^${TAG}"`
				TAG_REPLACE="${TAG_REPLACE}`echo ${REPLACE_LIST} | sed "s|\([^^]\)${TAG}\s*|\1|g;s|^|;s:|;s|${TAG}\s*|${TAG}:|;s|$|:g;|;"`"
			else
				TAG_REPLACE="${TAG_REPLACE};s:${TAG}::g;"
			fi

		done

		#Применяем все правила разом
		sed "$TAG_REPLACE" $IN_FILE > $TMP_FILE
		#Переписываем из временного в исходный файл
		cat $TMP_FILE > $IN_FILE

		#Для поиска зацикливаний
		#echo ${count_loop}
		#echo ${TAG_REPLACE}

		#Увеличиваем счетчик вложенности
		count_loop=$((count_loop + 1))
	done

	rm -f $TMP_FILE
}

[ -z "$1" ] && print_help 1

#Генерация замен по тэгам
tag_replace

#Если в файле есть MessageList, то тогда генерируем
cat $IN_FILE | grep -q "Message_LIST" && message_replace

exit 0
