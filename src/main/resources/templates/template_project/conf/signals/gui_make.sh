#!/bin/bash

. make_common.sh

[ -z "$correctlist" ] && correctlist=common_rules

input=$1
#Номер, с которого начнется отсчет id
number=$2
[ -z "$number" ] && number=55000

PROG="${0##*/}"

print_help()
{
		[ "$1" = 0 ] || exec >&2
		cat <<EOF
		Usage: $PROG name_file [BEGIN_ID]
		name_file - input file(*.csv with separator ";")
		BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1

#Генерируем правила замен и common_rules
rules=$(get_autozamena_sed_rules "$correctlist")

while read line; do
	echo $line | sed "s|id;|			<item id=\"$number|g;s|\[1\]|${N}|g;${rules}"
	number=$((number+1))
done < $input
