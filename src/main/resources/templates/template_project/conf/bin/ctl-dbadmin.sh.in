#!/bin/sh

# Source function library.
. ctl-functions-@PACKAGE@.sh

PROGNAME=ctl-dbadmin.sh

DBNAME=@PACKAGE@
DBARCHIVENAME=${DBNAME}_archive
DBTMPNAME=${DBNAME}_tmp
DBUSER=dbadmin
DBPASS=dbadmin
objxsl=$DATADIR/objSQL.xsl

# ВРЕМЯ УСТАРЕВАНИЯ В ДНЯХ
# (т.е. все сообщения старее DAY_AGO дней
# будут перенесены в архив при rotate )
DAYS_AGO=10

# OLD_DAYS - величина, показывающая, за сколько дней будут удалены старые логи (начиная с самого древнего)
OLD_DAYS=3


RETVAL=1

usage()
{
    echo "${0##*/} {create|repair|restore|rotate|objmap_create|dump_export <path_to_dump_dir> [since_date]|dump_import <path_to_dump_file> [db_name]|clean}"
}

sudocmd="sudo"
[ $UID = 0 ] && sudocmd=

function create()
{
    echo "Creating databases"
    $sudocmd /usr/bin/mysqladmin create ${DBNAME} 
    cat $CONFDIR/db_create.sql | sed -e "s/DBNAME/${DBNAME}/g" | mysql -u root ${DBNAME}
    $sudocmd /usr/bin/mysqladmin create ${DBARCHIVENAME}
    cat $CONFDIR/db_create.sql | sed -e "s/DBNAME/${DBARCHIVENAME}/g" | mysql -u root ${DBARCHIVENAME}
    $sudocmd /usr/bin/mysqladmin create ${DBTMPNAME}
    cat $CONFDIR/db_create.sql | sed -e "s/DBNAME/${DBTMPNAME}/g" | mysql -u root ${DBTMPNAME}
    return $?
}

function restore()
{
    $sudocmd /usr/bin/mysqladmin drop ${DBNAME} \
    && $sudocmd /usr/bin/mysqladmin drop ${DBARCHIVENAME} \
    && $sudocmd /usr/bin/mysqladmin drop ${DBTMPNAME} \
    && create
    return $?
}

function repair()
{
    # чиним БД
    msg "(repair_db): check & repair DB...( mysql --user=$DBUSER $DBNAME  )"
    nice -n 20 /usr/bin/mysqlcheck --user=$DBUSER --password=$DBPASS --repair --auto-repair $DBNAME >> $MYSYSLOG
}

function dump_export()
{
    if [ -n "$1" ] ; then
	DUMPPATH=$1
    else
	echo "Path not found"
	exit
    fi

    echo "Exporting..."

	cat $CONFDIR/db_cleantmp.sql | sed "s/DBNAME/$DBTMPNAME/" | nice -n 20 /usr/bin/mysql --user=$DBUSER --password=$DBPASS

    if [ -n "$2" ] ; then
	STARTDATE=$2
	FINISHDATE=`date +%F`
	if [ -n "$3" ] ; then
	    FINISHDATE=$3
	fi
	echo "since $STARTDATE until $FINISHDATE"
	cat $CONFDIR/db_dump.sql | sed -e "s/STARTDATE/${STARTDATE}/g" | sed -e "s/FINISHDATE/${FINISHDATE}/g" | nice -n 20 /usr/bin/mysql --user=$DBUSER --password=$DBPASS
	$sudocmd /usr/bin/mysqldump --opt -t $DBTMPNAME > $DUMPPATH/${DBNAME}_dump.sql

	cat $CONFDIR/db_cleantmp.sql | sed "s/DBNAME/$DBTMPNAME/" | nice -n 20 /usr/bin/mysql --user=$DBUSER --password=$DBPASS

    else
	echo "all data"
	$sudocmd /usr/bin/mysqldump --opt -t $DBARCHIVENAME > $DUMPPATH/${DBNAME}_dump.sql
	$sudocmd /usr/bin/mysqldump --opt -t $DBNAME >> $DUMPPATH/${DBNAME}_dump.sql
    fi

}

function dump_import()
{
    if [ -n "$1" ] ; then
	DUMPFILE="$1"
    else
	echo "Path to dump is not exist"
	exit
    fi

    [ -n "$2" ] && DB="$2" || DB=$DBNAME

    echo "Importing into ${DBNAME}..."

    cat ${DUMPFILE} | sed -e 's/INSERT/INSERT IGNORE/g' | $BINDIR/mysql $DB
}

function store_init()
{
    [ -n "$1" ] && STORENAME="$1" || STORENAME=$DBNAME
    echo "Creating store..."
    $sudocmd /usr/bin/mysqladmin create ${STORENAME} \
                    && cat $CONFDIR/db_create.sql | sed -e "s/DBNAME/${STORENAME}/g" | mysql -u root ${STORENAME}
    return $?
}

function objmap_init()
{
    echo "Init ObjectMap..."
    xsltproc $objxsl $CONFILE | mysql -u $DBUSER -p$DBPASS $DBNAME
    return $?
}

function rotate()
{
    # переносим старые сообщения
    cat $CONFDIR/db_rotate.sql | sed "s/DAYS_AGO/$DAYS_AGO/" | nice -n 20 /usr/bin/mysql --user=$DBUSER --password=$DBPASS
}

function clean()
{
    cat $CONFDIR/db_clean.sql | sed "s/DBNAME/$DBARCHIVENAME/" | sed "s/OLD_DAYS/$OLD_DAYS/" | nice -n 20 /usr/bin/mysql --user=$DBUSER --password=$DBPASS
}


# See how we were called.
case "$1" in

    create)
	create && RETVAL=0
	;;

    repair)
	repair && RETVAL=0
	;;

    restore)
	restore && RETVAL=0
	;;

    rotate)
	rotate && RETVAL=0
	;;

    dump_export)
	dump_export $2 $3 $4 && RETVAL=0
	;;

    dump_import)
	dump_import $2 $3 && RETVAL=0
	;;

    clean)
	clean && RETVAL=0
	;;

    store_init)
	store_init $2 && RETVAL=0
	;;

	objmap_create)
		objmap_init && RETVAL=0
	;;

    -h|--help|help)
	usage
	;;
    *)
	RETVAL=1
esac

exit $RETVAL
