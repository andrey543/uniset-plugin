#!/bin/sh
#
# Управление работой omniNames
# -----------------------------------------------

# Source system function library.
. /etc/init.d/functions

# Source function library.
. ctl-functions-@PACKAGE@.sh

REPDIR=${RUNDIR}/omniORB

# settings
PROGNAME=omninames
PIDFILE=$RUNDIR/$PROGNAME.pid
LOCKFILE=$LOCKDIR/$PROGNAME
SRCREPFILE=omninames-`hostname -s`.log
REPFILE=omninames-$(hostname).dat
REPCHOWN=guest:users
REPCHMOD='a+rw'
RETVAL=0
#OMNIPORT=2809 # задаётся в ctl-functions-@PACKAGE@..sh


#DEBUG=1

check_log_exist()
{
	[ -a $REPDIR/$REPFILE ] && return 0 || return 1
}


#	копирование зараннее созданного репозитория
copy_repository()
{
	msg "copy_repository..."

	RETVAL=0
	mkdir -p -m 777 $REPDIR && cp -f $CONFDIR/$SRCREPFILE $REPDIR/$REPFILE && RETVAL=0 || RETVAL=1

	[ $RETVAL -eq 0 ] && mylog "($PROGNAME): mkdir $REPDIR and copy $REPDIR/$REPFILE [ OK ]"

	if [ $RETVAL -eq 0 ]; then
		chown -R -f $REPCHOWN $REPDIR/$REPFILE && chmod -f $REPCHMOD $REPDIR/* && RETVAL=0 || RETVAL=1
	else
		failure "*** WARNING!!! *** Не удалось скопировать файл репозитория !!!"
		RETVAL=1
	fi

	[ $RETVAL -eq 0 ] && success "copy repository" || failure "copy repository"

#	msg "copy repository return $RETVAL"
	return $RETVAL
}


create_repository()
{
	msg "$PROGNAME: create_repository"
	RETVAL=0
	$BINDIR/${admin} --create --confile ${CONFILE} 2>/dev/null 1>/dev/null
	RETVAL=$?

	[ $RETVAL -eq 0 ] && success "create repository" || failure "create repository"
	return $RETVAL
}

old_start()
{

	# делаем несколько(5) попыток запуска
	for i in `seq 1 5`
	do
		msg "start omninames ($i)..."

		if [ $elog == 1 ]; then
			start_daemon --make-pidfile --pidfile $PIDFILE --lockfile "$LOCKFILE" --no-announce -- \
				$BINDIR/omniNames -logdir $REPDIR -errlog /dev/null
		else
			start_daemon --make-pidfile --pidfile $PIDFILE --lockfile "$LOCKFILE" --no-announce -- \
				$BINDIR/omniNames -start $OMNIPORT -logdir $REPDIR -errlog /dev/null
		fi

		RETVAL=$?
		PID=$(cat $PIDFILE)
		if [ $RETVAL -eq 0 ]; then
			sleep 2 # пауза для корректного запуска 
		fi

		# если запустили успешно
		# то прерываем попытки
		checkpid $PID && break

		# останавливаем все omniNames
		# и пытаемся запустить ещё раз...
		killall -9 omniNames 2>/dev/null && sleep 1
	done;

	[ $RETVAL -eq 0 ] && create_repository &&
			touch $LOCKFILE && success "startup" || failure "start"

	return $RETVAL
}

start()
{
	msg "$PROGNAME: start"

	RETVAL=0
	PID=
	if [ -a "$PIDFILE" ]; then
		PID=$(cat $PIDFILE)
	fi
	
	# проверка может уже запущен
	checkpid $PID && success "startup" && touch $LOCKFILE && return 0 

	if ! [ -a "$CONFDIR/$SRCREPFILE" ]; then
		msg "$CONFDIR/$SRCREPFILE not found. Use old_start function..."
		RETVAL=0
		# старый вариант запуска
		check_log_exist && elog=1 || elog=0
		old_start
		RETVAL=$?
		return $RETVAL
	fi

	# новый вариант запуска (копирование зараннее созданного репозитория)
	copy_repository 
	RETVAL=$?
	if [ $RETVAL -eq 1 ]; then
		failure "start"
		return 1
	fi

	elog=1
	old_start

	return $RETVAL
}

stop()
{
	msg "$PROGNAME: stop"

	PID=
	if [ -a "$PIDFILE" ]; then
		PID=$(cat $PIDFILE)
	fi

	[ -z "$PID" ] && failure "stop (unknown pid)" && return 1

	stop_daemon --pidfile "$PIDFILE" --lockfile "$LOCKFILE" --no-announce $BINDIR/omniNames
	checkpid $PID && kill $PID && RETVAL=$? || RETVAL=0

	# omniNames create $REPFILE and $REPFILE.bak!!!
	# we delete $REPFILE and *.bak-file
	[ $RETVAL -eq 0 ] && rm -f $LOCKFILE && rm -f $PIDFILE \
			&& rm -f $REPDIR/$REPFILE && rm -f $REPDIR/*.bak \
			&& success "stopped" || failure "stop"

	return $RETVAL
}

restart()
{
	stop
	sleep 1
	start
}

status()
{
	check_status
}

# See how we were called.
case "$1" in
	start)
		start
		;;
	stop)
		stop
		;;
	restart)
		restart
		;;
	condstop)
		if [ -e "$LOCKFILE" ]; then
			stop
		fi
		;;
	condrestart)
		if [ -e "$LOCKFILE" ]; then
			restart
		fi
		;;
	status)
		status
		RETVAL=$?
		;;
	*)
		echo "${0##*/} {start|stop|restart|condstop|condrestart|status}"
		RETVAL=1
esac

exit $RETVAL
