#!/bin/sh
# -----------------------------------------------
# $Id: ctl-setclock.sh,v 1.1 2009/06/30 08:23:04 alex Exp $
# -----------------------------------------------
# Формат 
#ЧЧ:ММ:СС ДД:ММ:ГГ
#$1 $2 $3 $4 $5 $6

[ -n "$1" ] || exit 1
[ -n "$2" ] || exit 1
[ -n "$3" ] || exit 1
[ -n "$4" ] || exit 1
[ -n "$5" ] || exit 1
[ -n "$6" ] || exit 1

sudocmd="sudo"
[ $UID \> 0 ] || sudocmd=

#echo "set date time: uid=$UID $sudocmd date $5$4$1$2$6.$3"
$sudocmd date $5$4$1$2$6.$3 && $sudocmd /sbin/service clock sync
