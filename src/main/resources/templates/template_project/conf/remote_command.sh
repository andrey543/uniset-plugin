#!/bin/sh

RETVAL=0
PROG="${0##*/}"
ACTION="system"
CMD="$1"; shift

. ctl-conf.sh

check_key

print_rcmd_usage()
{
    [ "$1" = 0 ] || exec >&2
    cat <<EOF
Usage: $PROG CMD [ $NODES ] | all

EOF
    [ -n "$1" ] && exit "$1" || exit
}

[ -z "$CMD" ] && print_rcmd_usage 1
[ -z "$1" ] && print_rcmd_usage 1

set_list "$*"

init_pssh_vars

exec pssh -o $LOGDIR -e $LOGDIR -t $PSSH_TIMEOUT -p $PSSH_MAX_THREAD -l $SSHUSER -h $hlist $SSHOPT /usr/bin/ctl-$ACTION.sh $CMD
