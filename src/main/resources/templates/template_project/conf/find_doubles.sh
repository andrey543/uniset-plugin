#!/bin/bash

GROUP=$1
CONFILE="configure.xml"

cat $CONFILE | grep tcp_mbreg |  grep -Go ".*\"${GROUP}.*" | grep -Go "mbreg=\"[^\"]*" | grep -Go "[0-9]*" | sort | uniq -d |
while read mbreg
do
    cat $CONFILE | grep tcp_mbreg |  grep -Go ".*\"${GROUP}.*" | grep -Go ".*mbreg=\"${mbreg}\".*"
done
