#!/bin/sh


PROG="${0##*/}"
VM_CONFDIR="vmonit.d"

[ -z "$1" ] && echo "Unknown conffile.." && echo "Use: $PROG conffile [from $VM_CONFDIR]" && exit 1

if [ -a "$VM_CONFDIR/$1" ]; then
	VPARAM=$(cat $VM_CONFDIR/$1)
	uniset2-vmonit $2 $3 $4 $VPARAM
	exit $?
fi

echo "Not found confile $VM_CONFDIR/$1"
exit 1


