#!/bin/sh

PROG="${0##*/}"
[ -z "$1" ] && wait_limit=20 || wait_limit=$1

. ctl-conf.sh
names_hosts=
all_ok=
tab=
attempt=1

echo "Check availability SM"
echo "----------------"

while :;do
	all_ok=1
	for X in $NODES; do
		echo "$X" | grep -q "imitator" && tab="" || tab="	";
		if uniset2-admin -g TestMode_S@"$X" >/dev/null;
		then echo "$X:$tab	SM: OK" 
		else echo "$X:$tab	SM: FAIL	(attempt: $attempt/$wait_limit)" && echo "$error" && all_ok=""
		fi
	done
	echo "----------------"
	[ -n "$all_ok" ] && echo "ALL SM ALREADY" && exit 0;
	[ "$attempt" -eq "$wait_limit" ] && echo "" && echo "Warning: Превышен лимит попыток. Выполение остановлено" && exit 1;
	attempt=$((attempt + 1))
	sleep 3
done
