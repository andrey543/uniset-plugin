#!/bin/bash

#Скрипт, предназначенный для пост-обработки списков датчиков
PROG="${0##*/}"

IN_FILE=$1
# Потом переделаю на нормальные ключи
[ -n "$2" ] && MODE=$2 || MODE= #-d - просто удаление всех настроек

#Переменные для генерирования тэгов
TAGDIR="signals/tag.d"

# Сколько раз рекурсивно искать вложенные тэги
loop_depth=5
count_loop=0

print_help()
{
	cat <<EOF
	Usage: $PROG name_file.xml
EOF
	exit
}

# Функция для рекурсивного раскрытия тэгов
rec_find_tag()
{
	local tag tagval
	tag=$1
	# Если рекурсивность больше заданного уровня - выходим
	#[ $count_loop -qt $loop_depth ] && return ""

	tagval=`eval cat ${TAGDIR}/*.conf | grep -G "^${tag}" | sed "s|$tag||" | tr '\n' ' '`
	#Проверяем на вложенность
	if echo "$tagval" | grep -q "@";
	then
		# Проходим по всем вложенным тэгам
		for inttag in `echo "$tagval" | grep -o "@\w*@"`;
		do
			local inttagval
			inttagval=`rec_find_tag "$inttag"`
			tagval=`echo $tagval | sed "s|$inttag|$inttagval|g"`
		done
	fi

	#Увеличиваем счетчик вложенности
#	count_loop=$((count_loop + 1))
	echo "$tagval"
}

del_tags()
{
	local tag tagval tag_del_replace name
	tag=$1
	tagval=$2
	tag_del_replace=
	name=`echo $tag | sed "s|@||g"`

	# Разбиваем значения тэгов по отдельным настройкам
	for prop in $tagval;
	do
		prop=${prop%%\"*}
		tag_del_replace="${tag_del_replace};s|id=\(.*\) name=\"$name\"\(.*\) ${prop}\"[^\"]*\"|id=\1 name=\"$name\" \2|;"
	done
	echo $tag_del_replace
}

# Проходим по списку всех тэгов в ${TAGDIR}/
prev_TAG=
TAG_REPLACE=
TAG_DEL_REPLACE=


for TAG in `cat ${TAGDIR}/*.conf | grep -o "^@\w*@" | sort`;
do
	unset TAGVAL name_sensor

	# Если повторяется, то пропускаем, т.к. раскрыли все в предыдущий раз
	[[ "$prev_TAG" == "$TAG" ]] && continue

	name_sensor=`echo $TAG | sed "s|@||g"`

	TAGVAL=`rec_find_tag "$TAG"`
	TAG_REPLACE="${TAG_REPLACE};s|id=\(.*\) name=\"${name_sensor}\" \(.*\"\)\(/\?>\)$|id=\1 name=\"${name_sensor}\" \2 ${TAGVAL}\3|;"
	TAG_DEL_REPLACE="${TAG_DEL_REPLACE}`del_tags $TAG "$TAGVAL"`"
	prev_TAG=$TAG

	# Т.к. в итоге получается слишком длинный список замен, буд пока что менять в каждом проходе.
	# Потом добавлю, чтобы правила применялись каждый n-раз
	subst "${TAG_DEL_REPLACE}" $IN_FILE						# Удаляем все настройки
	[ "$MODE" != "-d" ] && subst "$TAG_REPLACE" $IN_FILE	# Генерим заново
	TAG_REPLACE=
	TAG_DEL_REPLACE=
done

# Удаляем все настройки
#echo $TAG_DEL_REPLACE
#subst "${TAG_DEL_REPLACE}" $IN_FILE

# Генерим заново
#echo $TAG_REPLACE
#[ "$MODE" != "-d" ] && subst "$TAG_REPLACE" $IN_FILE
exit 0
