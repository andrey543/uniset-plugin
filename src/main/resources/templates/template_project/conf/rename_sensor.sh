#!/bin/sh

PROJECT_PATH=`dirs -l`
PROJECT_PATH=${PROJECT_PATH}/..

OLDSENSOR=$1
NEWSENSOR=$2

[[ -z $OLDSENSOR || -z $NEWSENSOR ]] && echo "[ERROR]: Bad arguments. Enter rename_sensor.sh OLDSENSOR NEWSENSOR" && exit 1

OLDSENSOR=$(echo $OLDSENSOR | sed "s|\[|\\\[|g;s|\]|\\\]|g;")
NEWSENSOR=$(echo $NEWSENSOR | sed "s|\[|\\\[|g;s|\]|\\\]|g;")

echo -en "\033[37;1;42m $OLDSENSOR Found in these files: \033[0m"
echo

find $PROJECT_PATH -type f -exec grep --color -i -H $OLDSENSOR {} \;

#[ -z $RESULTAT ] && echo -en "\033[37;1;43m Sensor $OLDSENSOR is not found \033[0m" && echo && exit 0
#echo $RESULTAT

echo -en "\033[37;1;42m Rename this sensor? \033[0m"
echo 

read answer

echo $OLDSENSOR " ===> " $NEWSENSOR

find $PROJECT_PATH -type f -exec subst "s|$OLDSENSOR|$NEWSENSOR|g;" {} \;

exit 0
