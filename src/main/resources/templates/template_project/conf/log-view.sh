#!/bin/sh


PROG="${0##*/}"
LV_CONFDIR="logview.d"

if [ -z "$1" ]; then
	echo "Unknown conffile.." && echo "Use: $PROG conffile [from $LV_CONFDIR]"
	echo "List of files:"
    ls $LV_CONFDIR
    exit 1
fi

#[ -z "$1" ] && echo "Unknown conffile.." && echo "Use: $PROG conffile [from $LV_CONFDIR]" && exit 1

if [ -a "$LV_CONFDIR/$1" ]; then
	while read cmd; do
	eval $cmd
	done < $LV_CONFDIR/$1
	exit $?
fi

echo "Not found confile $LV_CONFDIR/$1"
exit 1


