#!/bin/sh

RETVAL=0
PROG="${0##*/}"
ACTION="ping"

. ctl-conf.sh

set_list "$*"

PID=
for HOST in $LIST; do
	init_vars "$ACTION" "$HOST"
	pre_log >/dev/null

	if ping -c 3 -i 0.3 -w 1 "$HOST" 1>>"$LOG" 2>>"$LOG"; then
		ok_log
	else
		fail_log
	fi &

	PID=$!
done
wait

exit $RETVAL
