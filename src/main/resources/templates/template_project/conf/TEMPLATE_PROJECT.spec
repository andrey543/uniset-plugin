%define prj_group gTEMPLATE_PROJECT
%define kernel_flavour asu-def

%define _localdir /var/local
%define _localbindir /usr/local/bin
%define _netdir /etc/net
%define _defnetdir /etc/net/ifaces/default
%define _natdir /etc/net/ifaces/default/fw/iptables/nat

%def_enable omninames
%def_enable unet2
%def_disable io
%def_enable imitators
%def_enable gui
%def_disable vstand
%def_enable vnc

Name: TEMPLATE_PROJECT
Version: 0.1
Release: set1
Summary: TEMPLATE_PROJECT

License: GPL
Group: Development/C++
URL: http://etersoft.ru

Source: %name-%version.tar

ExclusiveArch: %ix86

# Automatically added by buildreq on Thu Oct 02 2014
# optimized out: gcc-c++ libcomedi-devel libcommoncpp2-devel libgpg-error libomniORB-devel libsigc++2-devel libstdc++-devel libuniset2-algorithms libuniset2-devel libuniset2-extension-common libxml2-devel pkg-config xsltproc
BuildRequires: libuniset2-algorithms-devel libuniset2-extension-common-devel libuniset2-utils

BuildRequires: uniset-configurator >= 1.0-alt6
BuildRequires: libuniset2-devel >= 2.6-alt4
#BuildRequires: libuniset2-extension-mysql-devel
BuildRequires: libuniset2-algorithms-devel >= 0.7-alt17

Requires: libuniset2 = %{get_SVR libuniset2}
Requires: libuniset2-algorithms = %{get_SVR libuniset2-algorithms}
Requires: libuniset2-utils
Requires: libuniset2-extension-common
Requires: libuniset2-extension-logicproc
Requires: libomniORB-names
Requires: rdate
Requires: htop

%if_enabled gui
BuildRequires: libuniwidgets2-devel >= 2.0.1-eter24
BuildRequires: libsetwidgets2-devel >= 0.4-set12
%endif

Summary: Common package for TEMPLATE_PROJECT
Group: Development/Other

AutoReq: no
%define findreq_default_method none

%description
This is special internal project for SET Research Instinute

%package conf
Summary: Configuration files for TEMPLATE_PROJECT Project
Group: Development/Other
Requires: %name = %version-%release
AutoReq: no
%description conf
Configuration files for TEMPLATE_PROJECT Project

%if_enabled imitators
%package imitator
Summary: (VM) Imitator Controller
Group: Development/Other
Requires: %name-conf = %version-%release
AutoReq: no
%description imitator
Programs for (VM) Imitator Controller
%endif

%if_enabled gui
%package gui-common
Summary: GUI for TEMPLATE_PROJECT
Group: Development/Other
Requires: %name-conf = %version-%release
Requires: libuniwidgets2 = %{get_SVR libuniwidgets2}
Requires: libuniwidgets2-uniset2-plugin
Requires: libsetwidgets2 = %{get_SVR libsetwidgets2}
Requires: xinit
Requires: autologin
Requires: startup-micro >= 0.3-alt4.6
AutoReq: no
%description gui-common
Common files for GUI for TEMPLATE_PROJECT Project

%package gui
Summary: GUI for TEMPLATE_PROJECT
Group: Development/Other
Requires: %name-gui-common = %version-%release
%if_enabled vnc
Requires: %name-vnc = %version-%release
Requires: x11vnc
%endif
Requires: net-snmp-clients
Requires: fonts-ttf-google-droid-sans
AutoReq: no
%description gui
GUI for TEMPLATE_PROJECT

%package gui1
Summary: GUI for TEMPLATE_PROJECT
Group: Development/Other
Requires: %name-gui = %version-%release
AutoReq: no
%description gui1
GUI1 for TEMPLATE_PROJECT

%if_enabled vnc
%package vnc
Summary: VNC client for GUI1 for TEMPLATE_PROJECT
Group: Development/Other
AutoReq: no
%description vnc
VNC client for GUI for TEMPLATE_PROJECT

%package vnc-client
Summary: VNC client for GUI for TEMPLATE_PROJECT
Group: Development/Other
Requires: %name-vnc = %version-%release
Requires: tigervnc
AutoReq: no
%description vnc-client
VNC client for GUI for TEMPLATE_PROJECT

%package vnc-gate
Summary: VNC gate for GUI for TEMPLATE_PROJECT
Group: Development/Other
Requires: %name-vnc = %version-%release
Requires: novnc
Conflicts: %name-conf
AutoReq: no
%description vnc-gate
VNC gate for GUI for TEMPLATE_PROJECT
%endif
%endif

%prep
%setup

%build
%autoreconf
%configure %{subst_enable gui} %{subst_enable vnc} %{subst_enable vstand} %{subst_enable omninames} %{subst_enable unet2} %{subst_enable io} %{subst_enable imitators} --localstatedir=%{_var}
%make_build
#%make

%install
%makeinstall_std
mkdir -p -m755 %buildroot%_logdir/%name
mkdir -p -m755 %buildroot%_runtimedir/%name
mkdir -p -m755 %buildroot%_lockdir/%name
mkdir -p -m755 %buildroot%_runtimedir/%name/omniORB

%if_enabled vnc
mkdir -p -m755 %buildroot%_localdir/.vnc
cp %buildroot%_sysconfdir/%name/passwd %buildroot%_localdir/.vnc/passwd
%endif

#mkdir -p -m755 %buildroot%_sysconfdir/%name/vmonit.d

mkdir -p -m755 %buildroot%_initdir
mv %buildroot%_bindir/%{name} %buildroot%_initdir/%{name}
mv %buildroot%_bindir/%{name}-novnc-gui %buildroot%_initdir/%{name}-novnc-gui
mkdir -p -m755 %buildroot%_localbindir
mv %buildroot%_bindir/%{name}-novnc-wrapper.sh %buildroot%_localbindir/%{name}-novnc-wrapper.sh

%if_enabled gui
subst 's|\(\./\)*glade/|%_datadir/%name/glade/|' %buildroot%_datadir/%name/glade/*.glade
subst 's|/home.*/glade/|%_datadir/%name/glade/|' %buildroot%_datadir/%name/glade/*.glade
subst 's|\(\./\)*glade/|%_datadir/%name/glade/|' %buildroot%_datadir/%name/glade/dialog/*.glade
subst 's|/home.*/glade/|%_datadir/%name/glade/|' %buildroot%_datadir/%name/glade/dialog/*.glade
subst 's|\./svg|%_datadir/%name/svg|' %buildroot%_datadir/%name/glade/*.glade
subst 's|/home.*/svg|%_datadir/%name/svg|' %buildroot%_datadir/%name/glade/*.glade
subst 's|\./\(.*\)\.glade|%_datadir/%name/glade/\1\.glade|g' %buildroot%_datadir/%name/glade/*.glade
subst 's|\./svg|%_datadir/%name/svg|' %buildroot%_datadir/%name/glade/dialog/*.glade
subst 's|/home.*/svg|%_datadir/%name/svg|' %buildroot%_datadir/%name/glade/dialog/*.glade
subst 's|\./\(.*\)\.glade|%_datadir/%name/glade/\1\.glade|g' %buildroot%_datadir/%name/glade/dialog/*.glade
%endif
# Подмена LocalNode для каждого узла
NLIST="gui1 imitator"
for n in ${NLIST};
do
  	cp -f %buildroot%_sysconfdir/%name/configure.xml %buildroot%_sysconfdir/%name/configure.xml.$n
	subst "s|LocalNode name=\".*\"|LocalNode name=\"$n\"|i" %buildroot%_sysconfdir/%name/configure.xml.$n
	subst "s|omniORB1|omniORB|g" %buildroot%_sysconfdir/%name/configure.xml.$n
	subst "s|r1_host|r1_$n|i" %buildroot%_sysconfdir/%name/configure.xml.$n
	subst "s|r2_host|r2_$n|i" %buildroot%_sysconfdir/%name/configure.xml.$n
	subst "s|base_host|$n|i" %buildroot%_sysconfdir/%name/configure.xml.$n
	%if_enabled vstand
	subst "s|vstand_port|port|i" %buildroot%_sysconfdir/%name/configure.xml.$n
	%else
	subst "s|real_port|port|i" %buildroot%_sysconfdir/%name/configure.xml.$n
	%endif
done

%post conf
ln -sf ctl-admin-%name.sh %_bindir/ctl-admin.sh
ln -sf ctl-smviewer-%name.sh %_bindir/ctl-smviewer.sh
cp -bf %_sysconfdir/%name/hosts %_sysconfdir/hosts
/usr/sbin/groupadd -r -f %prj_group

%if_enabled vstand
cp -bf %_netdir/%name.vstand.sysctl.conf %_netdir/sysctl.conf
cp -bf %_defnetdir/%name.vstand.options %_defnetdir/options
echo "%name-%version-%release vstand" > %_sysconfdir/motd
%else
cp -bf %_netdir/%name.sysctl.conf %_netdir/sysctl.conf
cp -bf %_defnetdir/%name.options %_defnetdir/options
echo %name-%version-%release > %_sysconfdir/motd
%endif

%if_enabled imitators
%post imitator
ln -sf %name-runlist-imitator.sh %_bindir/%name-runlist.sh
ln -sf ctl-monit-imitator.sh %_bindir/ctl-monit-%name.sh
%_bindir/ctl-sethost.sh
ln -sf %_sysconfdir/%name/configure.xml.imitator %_sysconfdir/%name/configure.xml
ln -sf %_sysconfdir/%name/imitator.vmonit.conf %_sysconfdir/%name/vmonit.conf
%endif

%if_enabled gui
%post gui
ln -sf %name-runlist-gui.sh %_bindir/%name-runlist.sh
ln -sf ctl-monit-gui.sh %_bindir/ctl-monit-%name.sh
ln -sf %_bindir/ctl-%name-gui.sh %_bindir/ctl-gui.sh
ln -sf %_bindir/%name-start-gui-xinit.sh %_bindir/%name-start-gui.sh

%post gui1
ln -sf ctl-smemory2-%name-gui1.sh %_bindir/ctl-smemory2-%name.sh
ln -sf ctl-smemory2-%name-gui1.sh %_bindir/ctl-smemory2-%name-gui.sh
ln -sf %_sysconfdir/%name/configure.xml.gui1 %_sysconfdir/%name/configure.xml
#ln -sf %_datadir/%name/glade1 %_datadir/%name/glade
%_bindir/ctl-sethost.sh

%if_enabled vnc
%post vnc
ln -sf %_localdir/.vnc /home/guest/.vnc

%post vnc-gate
cp -bf %_netdir/%name.sysctl.conf %_netdir/sysctl.conf
cp -bf %_defnetdir/%name.options %_defnetdir/options
%_bindir/%name-configure-iptables.sh %_natdir vnc
%endif
%endif

%files -n %name
%config %_initdir/%{name}
%config %_sysconfdir/%name/hosts
%config %_sysconfdir/openssh/authorized_keys2/root
%config %_sysconfdir/apt/sources.list.d/*

%dir %_sysconfdir/%name/vmonit.d
%_sysconfdir/%name/vmonit.d/*

%dir %_sysconfdir/%name/logview.d
%_sysconfdir/%name/logview.d/*

%_bindir/smemory2*
%_bindir/ctl-admin*
%_bindir/ctl-functions*
%_bindir/ctl-omninames*
%_bindir/ctl-script*
%_bindir/ctl-syslog*
%_bindir/ctl-system*
%_bindir/ctl-update*
%_bindir/ctl-install*
%_bindir/ctl-smview*
%_bindir/ctl-smonit*
%_bindir/ctl-vmonit*
%_bindir/ctl-logview*

#%dir %_datadir/%name/
%attr(0775,root,%prj_group) %dir %_logdir/%name
%attr(0775,root,%prj_group) %dir %_runtimedir/%name
%attr(0775,root,%prj_group) %dir %_lockdir/%name
%attr(0775,root,%prj_group) %dir %_runtimedir/%name/omniORB

%files conf
%_bindir/ctl-all*
%_bindir/ctl-monit-func-%name.sh
%_bindir/ctl-synctime.sh
%_bindir/ctl-sethost.sh

%if_enabled vstand
%config %attr(0644,root,root) %_netdir/%name.vstand.sysctl.conf
%config %attr(0644,root,root) %_defnetdir/%name.vstand.options
%else
%config %attr(0644,root,root) %_netdir/%name.sysctl.conf
%config %attr(0644,root,root) %_defnetdir/%name.options
%_bindir/%name-configure-iptables.sh
%endif

%if_enabled imitators
%files imitator
%_sysconfdir/%name/configure.xml.imitator
%config %attr(0600,root,root) %_sysconfdir/%name/%name.imitator.monitrc
%config %_sysconfdir/%name/imitator.vmonit.conf
%config %_bindir/%name-runlist-imitator.sh
%_bindir/ctl-smemory2-%name-imitator.sh
%_bindir/ctl-monit-imitator.sh
%_bindir/im_drk
%_bindir/ctl-drk1-imitator.sh
%_bindir/ctl-drk2-imitator.sh
%endif

%if_enabled gui
%files gui-common
%_libdir/*.so*
%_libdir/libglade/2.0/*.so*
%_sysconfdir/sysconfig/autologin
%attr(0400,root,root) %dir %_sysconfdir/sudoers.d/guest

%files gui
%dir %_datadir/%name/theme/
%dir %_datadir/%name/svg/
%dir %_datadir/%name/cursor/
%_datadir/%name/theme/*
%_datadir/%name/svg/*
%_datadir/%name/cursor/*
%_bindir/%name-gui
%_bindir/ctl-%name-gui.sh
%_bindir/%name-start-gui-xinit.sh
%config %attr(0600,root,root) %_sysconfdir/%name/%name.gui.monitrc
%config %_bindir/%name-runlist-gui.sh
%_bindir/ctl-monit-gui.sh
%_bindir/ctl-setclock.sh

%files gui1
%_bindir/ctl-smemory2-%name-gui1.sh
## %_sysconfdir/%name/omninames-gui1.log
%_sysconfdir/%name/configure.xml.gui1
%dir %_datadir/%name/glade/
%_datadir/%name/glade/*

%if_enabled vnc
%files vnc
%attr(0775,guest,%prj_group) %dir %_localdir/.vnc
%config %attr(0600,guest,guest) %_localdir/.vnc/passwd

%files vnc-client
%_bindir/%name-login-vnc.sh

%files vnc-gate
%config %attr(0644,root,root) %_netdir/%name.sysctl.conf
%config %attr(0644,root,root) %_defnetdir/%name.options
%config %attr(0644,root,root) %_natdir/%name.vnc.OUTPUT
%config %attr(0644,root,root) %_natdir/%name.vnc.POSTROUTING
%config %attr(0644,root,root) %_natdir/%name.vnc.PREROUTING
%_bindir/%name-configure-iptables.sh
%config %attr(0755,root,root) %_initdir/%{name}-novnc-gui
%config %attr(0755,root,root) %_localbindir/%{name}-novnc-wrapper.sh
%endif
%endif
