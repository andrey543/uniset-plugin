#!/bin/sh

RETVAL=0
PROG="${0##*/}"
ACTION="install"

. ctl-conf.sh

print_usage()
{
    [ "$1" = 0 ] || exec >&2
	cat <<EOF

Usage: $PROG <hostname> [<template_host>]

EOF
    [ -n "$1" ] && exit "$1" || exit
}

HOST=$1
DEFAULT=${2-controller}

[ -z "$HOST" ] && print_usage 1

init_vars "$ACTION" "$HOST"
pre_log

if ssh $KSSH -x "$SSHUSER@$DEFAULT" "/usr/bin/ctl-$ACTION.sh" "15310-$HOST" 1>>$LOG 2>>$LOG; then
	ok_log
	echo "'$HOST' reboot..."
	ssh $KSSH -x "$SSHUSER@$DEFAULT" "/usr/bin/ctl-system.sh reboot" 1>>$LOG 2>>$LOG
else
	fail_log
fi

exit $RETVAL
