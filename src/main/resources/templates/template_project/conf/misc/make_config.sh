#!/bin/sh

# before: cp 15310.key ~/.ssh/
# Usage: make_config.sh >> ~/.ssh/config 


cat hosts.list | while read ip name; do
	if [ -n "$ip" ]; then
		echo "Host $name"
		echo "    Hostname $ip"
		echo "    User root"
		echo "    IdentityFile ~/.ssh/TEMPLATE_PROJECT.key"
		echo "    ForwardX11 no"
		echo
	fi
done

