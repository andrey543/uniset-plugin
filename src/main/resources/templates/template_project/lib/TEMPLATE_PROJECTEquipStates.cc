#include "TEMPLATE_PROJECTEquipStates.h"

using namespace std;

namespace TEMPLATE_PROJECT
{
	std::ostream& operator<< (std::ostream& os, const AIOStatus& st)
	{
		switch(st)
		{
			case aioOK:
				os << "aioOK";
			break;

			case aioNotRespond:
				os << "aioNotRespond";
			break;

			case aioUnknown:
				os << "aioUnknown";
			break;

			case aioLowWarning:
				os << "aioLowWarning";
			break;

			case aioHiWarning:
				os << "aioHiWarning";
			break;

			case aioLowAlarm:
				os << "aioLowAlarm";
			break;

			case aioHiAlarm:
				os << "aioHiAlarm";
			break;

			default:
			break;
		}

		return os;
	}
	std::ostream& operator<< (std::ostream& os, const fcState& st)
    {
        switch(st)
        {
			case fcUnknown:
				os <<"fcUnknown";
            break;
            case fcReady1:
                os <<"fcReady1";
            break;
            case fcReady2:
                os <<"fcReady2";
            break;
			case fcWorking:
				os <<"fcWorking";
            break;
			case fcRotation:
				os <<"fcRotation";
			break;
			case fcAlarm:
                os <<"fcAlarm";
            break;
            case fcStopped:
                os <<"fcStopped";
            break;
        }
        return os;
    }

    // -----------------------------------------------------------------------------
    std::ostream& operator<< (std::ostream& os, const fcControlMode& fcm)
    {
        switch(fcm)
        {
            case fcmUndef:
                os << "fcmUndef";
                break;

            case fcmSetRPM:
                os << "fcmSetRPM";
                break;

            case fcmSetPower:
                os << "fcmSetPower";
                break;

            default:
                break;
        }

        return os;
    }
    // -----------------------------------------------------------------------------
	SESPostID getSESPostID( const string& s )
	{
			if( s == "svu" )
				return KSUTSMaster;

			if( s == "ses1" )
				return SES1Master;

			if( s == "ses2" )
				return SES2Master;

			return SESUndefined;
	}
	// -----------------------------------------------------------------------------
	fcControlMode long2fcMode( long val )
	{
		if( val == (long)fcmSetPower )
			return fcmSetPower;

		if( val == (long)fcmSetRPM )
			return fcmSetRPM;

		return fcmUndef;
	}

	long fcMode2long( fcControlMode m )
	{
		if( m == fcmSetPower )
			return (long)fcmSetPower;

		if( m == fcmSetRPM )
			return (long)fcmSetRPM;

		return (long)fcmUndef;
	}

    // -----------------------------------------------------------------------------

}





