// -------------------------------------------------------------------------
#include <UniSetTypes.h>
#include "TEMPLATE_PROJECTConfiguration.h"
#include <sstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include "TEMPLATE_PROJECTEquipStates.h"
#include "TEMPLATE_PROJECTEquipStates.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// -------------------------------------------------------------------------
namespace TEMPLATE_PROJECT
{

	DebugStream dlog;
	// -------------------------------------------------------------------------
	std::string getSaveDir()
	{
		std::string savedir = "/tmp";
		xmlNode *gui = uniset_conf()->getNode("GUI");
		if(gui) {
			UniXML_iterator it(gui);
			savedir = it.getProp("SaveDir");
		}

		savedir += "/" + uniset::dateToString(time(0),".");
		const char *sd = savedir.c_str();
		if(access(sd, F_OK) != 0) {/* Direcrory not exists, we need to create it */
			if(mkdir(sd, 0777) != 0) {
				dlog[Debug::CRIT] << "(getSaveDir): Can't make dir... '" << savedir << endl;
			}
		}

		struct stat status;
		stat(sd, &status);

		if( !(status.st_mode & S_IFDIR) ) {
			dlog[Debug::CRIT] << "(getSaveDir): '" << savedir << "' not a dir..." << endl;
		}

		if( !(status.st_mode & S_IRUSR) || !(status.st_mode & S_IWUSR) || !(status.st_mode & S_IXUSR)) {
			dlog[Debug::CRIT] << "(getSaveDir): Need more rights for dir '" << savedir << endl;
		}

		return savedir;
	}
	//--------------------------------------------------------------------------------------------
	std::string ustring::implode( const std::list<std::string>& arr, const std::string& sep )
	{
		if(arr.empty())
			return "";

		std::ostringstream temp;
		auto arrIt = arr.begin();
		temp << (*arrIt);
		++arrIt;

		for(; arrIt != arr.end(); ++arrIt)
		{
			temp << sep << (*arrIt);
		}

		return temp.str();
	}
	//--------------------------------------------------------------------------------------------
	ustring::list ustring::explode(const string& source, const string& sep, bool ignore_empty)
	{
		ustring::list result;

		if( source.empty() )
			return result;

		string::size_type prev = 0;
		string::size_type pos = 0;

		while( pos != string::npos )
		{
			pos = source.find_first_of(sep, prev);
			string::size_type num = string::npos;

			if( pos != string::npos )
				num = pos - prev;

			string s(source.substr(prev, num));

			if( !s.empty() || !ignore_empty )
				result.emplace_back(s);

			prev = pos + 1;
		}

		return result;
	}
	//--------------------------------------------------------------------------------------------
	ustring::pair ustring::pair_explode(const std::string& source, const std::string& sep, bool ignore_empty)
	{
		string::size_type pos = 0;
		string::size_type prev = 0;

		for(;;)
		{
			pos = source.find_first_of(sep);

			if( pos == string::npos )
				return make_pair(source, "");

			string s(source.substr(prev, pos));

			if( s.empty() && ignore_empty )
				continue;

			string s2(source.substr(pos + 1));
			return make_pair(s, s2);
		}
	}
	//--------------------------------------------------------------------------------------------
	ustring::map ustring::pairs_explode(const std::string& source, const std::string& sep, const std::string& sep2)
	{
		ustring::map result;
		ustring::list l = ustring::explode(source, sep2);
		ustring::list::reverse_iterator it = l.rbegin();

		for(; it != l.rend(); ++it)
			result.insert(pair_explode(*it, sep));

		return result;
	}

}
// end of namespace
// -------------------------------------------------------------------------
