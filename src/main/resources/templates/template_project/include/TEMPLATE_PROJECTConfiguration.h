#ifndef TEMPLATE_PROJECT_CONFIGURATION_H_
#define TEMPLATE_PROJECT_CONFIGURATION_H_
// -------------------------------------------------------------------------
#include <memory>
#include <string>
#include <typeinfo>
#include <Debug.h>
#include <Configuration.h>
#include <UniXML.h>
#include <Exceptions.h>
#include <unordered_map>
#include "TEMPLATE_PROJECTEquipStates.h"
// -------------------------------------------------------------------------
// вспомогательный define для вывода логов..
#define PRELOG myname << "(" << __FUNCTION__ << "): "
#define PRELOG_PF myname << "(" << __PRETTY_FUNCTION__ << "): "
// -------------------------------------------------------------------------
namespace TEMPLATE_PROJECT
{
	// -----------------------------------------------------------------------------
	class ustring
	{
	  public:
		typedef std::list<std::string>			list;
		typedef std::pair<std::string, std::string>	pair;
		typedef std::unordered_map<std::string, std::string>	map;
		static std::string	implode( const std::list<std::string>& arr, const std::string& sep = ",");
		static ustring::list	explode(const std::string& source, const std::string& sep = ",", bool ignore_empty=true);
		static ustring::pair	pair_explode(const std::string& source, const std::string& sep = ",", bool ignore_empty=true);
		static ustring::map	pairs_explode(const std::string& source, const std::string& sep = "=", const std::string& sep2 = ",");
	  protected:
		ustring(){}
	};

	/*! Получить путь к каталогу, в котором сохранять скриншоты и подобную информацию.
	 * Значение настраивается в конфигурационном файле
	 * \code
	 * <GUI saveDir="<dir>"/>
	 * \endcode
	 * Имя конечного каталога получается добавлением даты к <dir>.
	 * Если каталога нет, создает его.
	 * 
	 * @return путь к каталогу 
	 */
	std::string getSaveDir();
	// -----------------------------------------------------------------------------
	inline long sign( const long v )
	{
		if( v < 0 )
			return -1;

		if( v > 0 )
			return 1;

		return 0;
	}
	// -----------------------------------------------------------------------------
	template<typename _T, typename... _Args>
	_T multimin( _T __first )
	{
		return __first;
	}
	template<typename _T, typename... _Args>
	_T multimin( _T __first ,_Args ...__args )
	{
		if( sizeof...(_Args) == 0 )
		{
			return __first;
		}

		_T __tmp = multimin(__args...);
		return __first < __tmp ? __first : __tmp;
	}
	// -----------------------------------------------------------------------------
	extern DebugStream dlog;
}
// -------------------------------------------------------------------------
#endif // TEMPLATE_PROJECT_CONFIGURATION_H_
