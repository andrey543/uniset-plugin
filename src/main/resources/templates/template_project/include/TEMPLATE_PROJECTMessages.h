#ifndef TEMPLATE_PROJECT_MESSAGES_H_
#define TEMPLATE_PROJECT_MESSAGES_H_
// -------------------------------------------------------------------------
#include <UniSetAlgorithmsMessages.h>
// -------------------------------------------------------------------------
namespace TEMPLATE_PROJECT
{
	class TEMPLATE_PROJECTMessage: public UniSetAlgorithms::UniSetAlgorithmsMessage
	{
		public:
			enum TypeOfMessage
			{
				AnyMessage=UniSetAlgorithmsMessage::TheLastFieldOfTypeOfMessage+1,
				DieselInfo,
				GeneratorInfo,
				DGInfo,
				FCInfo,
				GEUAuxInfo,
				GEUInfo
			};
	};

}	// end of name space TEMPLATE_PROJECT
// -----------------------------------------------------------------------------
#endif // TEMPLATE_PROJECT__MESSAGES_H_
