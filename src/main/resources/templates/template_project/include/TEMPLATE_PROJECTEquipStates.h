#ifndef TEMPLATE_PROJECT_EQUIPSTATES_H
#define TEMPLATE_PROJECT_EQUIPSTATES_H
#include <iostream>
#include <limits>

namespace TEMPLATE_PROJECT
{
    // -----------------------------------------------------------------------------
    enum fcState
    {
		fcUnknown = 0, 	/*!< неизвестный режим */
		fcReady1 = 1,	/*!< Готовность N1 */
		fcReady2 = 2,	/*!< Готовность N2 */
		fcStopped = 3,	/*!< Остановлен */
		fcWorking = 4,	/*!< Ход */
		fcRotation = 5,	/*!< Проворот */
		fcAlarm = 6	/*!< Авария */
    };
    // -----------------------------------------------------------------------------
	/*! Режим управления ПЧ */
    enum fcControlMode
    {
		fcmUndef    = -1,  /*!< неопределённое состояние (управление отключено?) */
		fcmSetRPM   = 0,  /*!< режим управления по оборотам ( не менять должно совпадать с протоколом SteerProp!)*/
		fcmSetPower = 1   /*!< режим управления по мощности ( не менять должно совпадать с протоколом SteerProp!) */
    };
    std::ostream& operator<< (std::ostream& os, const fcControlMode& fcm);
	fcControlMode long2fcMode( long val );
	long fcMode2long( fcControlMode m );
    // -----------------------------------------------------------------------------
    /*! статус аналогового канала */
	enum AIOStatus
	{
		aioNotRespond = -1, 	/*!< нет связи */
		aioUnknown = 0, 	/*!< неопределённое состояние */
		aioOK = 1, 	/*!< канал в порядке */
		aioLowWarning = 2, 	/*!< предупредительный по нижнему уровню */
		aioLowAlarm = 3, 	/*!< аварийный по нижнему уровню */
		aioHiWarning = 4, 	/*!< предупредительный по верхнему уровню */
		aioHiAlarm = 5 	/*!< аварийный по верхнему уровню */
	};

	template<typename T>
	AIOStatus make_aiostatus( T val,
				  const T& hiAlarm = std::numeric_limits<T>::max(),
				  const T& hiWarn = std::numeric_limits<T>::max(),
				  const T& lowAlarm = std::numeric_limits<T>::lowest(),
				  const T& lowWarn = std::numeric_limits<T>::lowest(),
				  bool respond = true)
	{
		if( !respond )
			return aioNotRespond;
		
		if( val >= hiAlarm )
			return aioHiAlarm;
		else if( val <= lowAlarm )
			return aioLowAlarm;
		else if( val >= hiWarn )
			return aioHiWarning;
		else if( val <= lowWarn )
			return aioLowWarning;
		
		return aioOK;
	}
    // -----------------------------------------------------------------------------
    /*! Откуда ведется управление СЭС */
    enum SESPostID
    {
        SESUndefined 	= 0,	// неопределён
        SES1Master = 1,	// управление от СЭС1
        SES2Master 	= 2,	// управление от СЭС2
        KSUTSMaster 	= 3,	// управление от КСУ ТС
        SESCntrlTransaction 	= 4,	// переходное состояние
        SESCntrlFailure	= 5		// аварийное состояние алгоритма
    };
	/*! получение идентификатора по строке
	 * \param s - ['svu','ses1','ses2']
	 */
	SESPostID getSESPostID( const std::string& s );
    enum qState
    {
	  qUndefiend=0, /*!< Неопределенное состояние*/
	  qOn=1, /*!< ВКЛ*/
	  qOff=2, /*!< ОТКЛ*/
	  qAlarm=3, /*!< АВАРИЯ*/
	  qProtect=4 /*!< ЗАЩИТНЫЙ РЕЖИМ <- пока не используется*/
    };
    enum DriveState
    {
        DriveUnknown = 0,          //!< Неизвестное состояние
        DriveStopped = 1,          //!< Остановлен
        DriveReadyToStart = 2,     //!< Готовность к пуску
		DriveWork = 3,             //!< Работает
        DriveFault = 4,            //!< Неисправен
        DriveFailure = 5           //!< Авария
    };
    enum GenState
    {
        GenUnknown = 0,    //!< Неизвестное состояние
        GenOff = 1,        //!< Выключен
        GenOn = 2,         //!< Включен
        GenTransaction = 3,//!< Переходное
        GenNeisprWork = 4, //!< Неисправен и включен
        GenNeispr = 5,     //!< Неисправен и выключен
        GenFailure = 6     //!< Авария
    };
    enum GRSHState
    {
        GRSHUnknown = 0,    //!< Неизвестное состояние
        GRSHOff = 1,        //!< Выключен
        GRSHOn = 2,         //!< Включен
    };
    enum GRUState
    {
	GRUUnknown = 0,	    //!< Неизвестное состояние
	GRUOff = 1,	    //!< Выключен
	GRUOn = 2,	    //!< Включен
	GRUFailF = 3,	    //!< Частота не в норме
	GRUFailU = 4,	    //!< Напряжение не в норме
	GRUFailAnother = 5  //!< Другая ошибка
    };
}

#endif // TEMPLATE_PROJECT_EQUIPSTATES_H
