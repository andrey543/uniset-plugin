#ifndef _DYNAMIC_LIBS_H_
#define _DYNAMIC_LIBS_H_
// -------------------------------------------------------------------------
#include <unordered_map>
#include <types.h>
#include <dlfcn.h>
#include <memory>
// -------------------------------------------------------------------------
/*! \namespace UniWidgetsTypes
 *  \brief Пространство имен содержащее различные константы.
*/
class DynamicLib
{
	public:
		typedef void* LibPtr;
		~DynamicLib(){};
		static std::shared_ptr<DynamicLib> open(const std::string& path);
		void* load(const std::string& func);
		
	private:
		DynamicLib(LibPtr lib_) : lib(lib_) {}
		DynamicLib();
		static std::unordered_map<std::string, std::weak_ptr<DynamicLib> > libs;
		LibPtr lib;
		friend struct DynamicLibDeleter;
};

struct DynamicLibDeleter
{
	void operator()(DynamicLib* p) const
	{
		try{ 
			if( p->lib )
				dlclose( p->lib );
			p->lib = nullptr;
		} catch(...) {}
	}
};
// -------------------------------------------------------------------------
#endif // _DYNAMIC_LIBS_H_
// -------------------------------------------------------------------------
