package com.uniset.plugin;

import com.intellij.openapi.vfs.VirtualFile;

import java.util.regex.Pattern;

public class Settings {
    /**
     * Вспомогательные функции
     *
     */

    public static boolean isNumber(String strNumber)
    {
        if(strNumber == null || strNumber.isEmpty())
            return false;

        try {
            Long.parseLong(strNumber);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }
    /**
     * ID-объекта по умолчанию
     *
     * Если не найден id в <objects>
     */
    public final static long DEFAULT_ID_NODE = 60001;

    /**
     * ID-объекта по умолчанию
     *
     * Если не найден id в <objects>
     */
    public final static long DEFAULT_ID_OBJECT = 63003;
    /**
     * Подключенные проекты
     *
     * ПРИМ. PropertiesComponent.getInstance().setValues(Settings.PROP_ATTACH_PROJECTS, values);
     * [0] - String Имя проекта
     * [1] - String Путь проекта
     */
    public final static String PROP_ATTACH_PROJECTS = "PropAttachProjects";

    /**
     * Подключенные проекты
     *
     * ПРИМ. PropertiesComponent.getInstance().setValues(Settings.PROP_ATTACH_PROJECTS, values);
     * [0] - String Имя проекта
     * [1] - String Путь проекта
     */
    public final static String PROP_LAST_FILTER = "PropLastFilter";

    /**
     * Загаловок при замене файлов исходных кодов
     */
    public final static String COMMENT_TITLE_CODE_FILE = "// Created by uniset-plugin from OLDPROJECT project \n\n";

    /**
     * Загаловок при замене файлов xml
     */
    public final static String COMMENT_TITLE_XML_FILE = "<?xml version=\"1.0\" encoding=\"utf-8\"?> \n" +
                                               "<!-- Created by uniset-plugin from OLDPROJECT project --> \n";

    /**
     * Загаловок при замене файлов исходных кодов
     */
    public final static String COMMENT_TITLE_SCRIPT_FILE = "# Created by uniset-plugin from OLDPROJECT project \n\n";

    /**
     * Загаловок при замене файлов исходных кодов
     */
    public final static String COMMENT_TITLE_MAKE_FILE = "# Created by uniset-plugin from OLDPROJECT project \n\n";


    /**
     * Фильтры отбираемых фалов
     * */
    //TODO подумать над отфильтровыванием скриптов и др. файлов
    public static class FilterFiles
    {
        /**
         * Настройки фильтров файлов и папок которые может содержать UnisetObject
         *
         * (Если массив пустой то пропускаются все файлы)
         *
         * */

        // Подходящие виды папок(регулярки)
        static final String filterDir[]  = {"^\\w*$"};

        // Подходящие виды файлов(регулярки)
        static final String filterFile[] = {"^\\w*.h$", "^\\w*.cc$", "^\\w*.src.xml$", "^Makefile.am$", "^.sh.in$"};

        // НЕ должны содержать
        static final String filterFileNotContent[] = {"_SK.cc", "_SK.h"};

        public static boolean checkDir(String name)
        {
            for(String regex : filterDir)
                if(Pattern.matches(regex, name))
                    return true;


            return false;
        }

        public static boolean checkFile(String name)
        {
            for(String regex : filterFile)
                if(Pattern.matches(regex, name)) {
                    for(String content : filterFileNotContent) {
                        if (name.contains(content))
                            return false;
                    }
                    return true;
                }

            return false;
        }

        /**
         * Расширения файлов которые создаються исключительно FileDocumentManager-ом(чтобы работало undo/redo)
         * (в новых версиях можно создавать просто virtualFile.setContent(content.getBytes()) )
         */
        public static final String[] documentExtensions = {".cc", ".h", ".xml"};

        public static boolean isForDocumentManager(VirtualFile file)
        {
            for(String ext : documentExtensions)
            {
                if(file.getName().endsWith(ext))
                    return true;
            }

            return false;
        }
    }


    /** Типы сигналов */
    public static final String IOTYPE_DI = "DI";
    public static final String IOTYPE_DO = "DO";
    public static final String IOTYPE_AI = "AI";
    public static final String IOTYPE_AO = "AO";

    /**
     * Modbus значения по умолчанию
     */
    public static final String MBM_DEVICE_ADDRESS = "0x01";
    public static final String MBM_TIMEOUT = "8000";
    public static final String MBM_POLLTIME = "200";
    public static final String MBM_GATEWAY_PORT = "502";
    public static final String MBM_GATEWAY_IP_ADDRESS = "localhost";
    public static final String MBM_FILTER_FIELD = "tcp";
    public static final String MBM_PREFIX = "tcp_";

    public static final String MBS_PREFIX = "tcp_";
    public static final String MBS_FILTER_FIELD = "tcp";
    public static final String MBS_RTU_DEV = "/dev/ttyS1";
    public static final String MBS_RTU_SPEED = "115200";
    public static final String MBS_TCP_IP_ADDRESS = "localhost";
    public static final String MBS_TCP_PORT = "502";

    public static final String MBMM_DEVICE_ADDRESS = "0x01";
    public static final String MBMM_TIMEOUT = "8000";
    public static final String MBMM_POLLTIME = "200";
    public static final String MBMM_GATE_IP = "localhost";
    public static final String MBMM_GATE_REAL_PORT = "502";
    public static final String MBMM_GATE_VSTAND_PORT = "44000";
    public static final String MBMM_GATE_RESP_INIT_TIMEOUT = "8000";
    public static final String MBMM_GATE_RECV_TIMEOUT = "800";
    public static final String MBMM_FILTER_FIELD = "tcp";
    public static final String MBMM_PREFIX = "tcp_";

    public static final String mbfunc02 = "0x02";
    public static final String mbfunc03 = "0x03";
    public static final String mbfunc04 = "0x04";
    public static final String mbfunc05 = "0x05";
    public static final String mbfunc06 = "0x06";
    public static final String mbfunc10 = "0x10";
    public static final String mbfuncF = "0x0F";

    public static final String[] mbfuncs = {mbfunc02, mbfunc03, mbfunc04, mbfunc05, mbfunc06, mbfunc10, mbfuncF};

    /** Регулярки дефолтных значений(в основном для textfield-ов)*/
    public static final String REGEX_ONLY_NUMBERS      = "[0-9]*";
    public static final String REGEX_ONLY_CHARS        = "[a-zA-Z]+";
    public static final String REGEX_DEVICE_ADDR       = "0x[0-9]*";
    public static final String REGEX_ONLY_NUM_AND_CHAR = "\\w*";
    public static final String REGEX_ALL               = ".*";
    public static final String REGEX_CORRECT_IP        = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

    /**
     * Настройки нового проекта
     *
     */

    public static final String defaultPath = "";


    /**
     * Стандартные пути
     *
     */

    public static final String PROJECT_PATH_TO_ALGORITHMS = "/src/Algorithms/";
    public static final String PROJECT_PATH_TO_IMITATORS = "/src/Imitators/";

    /**
     * Стандартные секции в configure.xml
     *
     * */
    public static final String CONFIGURE_ALGORITHM_SECTION = "Секция алгоритмов";
    public static final String CONFIGURE_MODBUS_SECTION = "Обмен";
    public static final String CONFIGURE_IMITATORS_SECTION = "Секция имитаторов";

}
