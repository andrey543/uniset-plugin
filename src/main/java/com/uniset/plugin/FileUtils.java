package com.uniset.plugin;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.build.objects.UnisetObjectFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedHashMap;
import java.util.Map;

public class FileUtils {

    public static boolean isExists(String path)
    {
        return Files.exists(Paths.get(path));
    }

    public static boolean exists(String path)
    {
        return FileUtil.exists(path);
    }

    public static VirtualFile toVirtualFile(String path)
    {
        if(Files.exists(Paths.get(path)))
            return LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(path));

        return null;
    }

    public static String readContent(VirtualFile file) throws IOException {
        return new String(Files.readAllBytes(VfsUtil.virtualToIoFile(file).toPath()));
    }

    public static boolean containsFileName(String path, String name)
    {
        VirtualFile dir = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(path));
        if(dir == null || !dir.isDirectory())
            return false;

        return containsFileName(dir, name);
    }

    public static boolean containsFileName(VirtualFile dir, String name)
    {
        for(VirtualFile f : dir.getChildren())
        {
            if(f.getName().equals(name))
                return true;
        }
        return false;
    }

    public static VirtualFile getVirtualChild(VirtualFile dir, String name)
    {
        for(VirtualFile f : dir.getChildren())
        {
            if(f.getName().equals(name))
                return f;
        }
        return null;
    }
    public static void replaceLineInFile(VirtualFile file, String oldLine, String newLine) throws IOException {
        setContentToFile(file, new String(readContent(file).getBytes(), StandardCharsets.UTF_8).replaceAll(oldLine, newLine));
    }
    public static void replaceLineInFile(Path file, String oldLine, String newLine) throws IOException {

        Charset charset = StandardCharsets.UTF_8;

        String content = new String(Files.readAllBytes(file), charset).replaceAll(oldLine, newLine);
        Files.write(file, content.getBytes(charset));
    }

    public static void setContentToFile(String pathToFile, String content)
    {
        setContentToFile(LocalFileSystem.getInstance().findFileByIoFile(new File(pathToFile)), content);
    }
    public static void setContentToFile(VirtualFile file, String content)
    {
        Document document = FileDocumentManager.getInstance().getDocument(file);
        document.setText(content);
        FileDocumentManager.getInstance().saveDocument(document);
    }

    public static void addLineToFile(VirtualFile file, int position, String line)
    {
        Document document = FileDocumentManager.getInstance().getDocument(file);
        int lineAndOffset = document.getLineEndOffset(position);
        document.insertString(lineAndOffset, line);
        FileDocumentManager.getInstance().saveDocument(document);
    }

    public static void addContentLast(String file, String content) throws IOException {
        VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(file));
        addContentLast(virtualFile, content);
    }

    public static void addContentLast(VirtualFile file, String content) throws IOException {
        String currentContent = new String(Files.readAllBytes(VfsUtil.virtualToIoFile(file).toPath()));
        if(currentContent.isEmpty())
            currentContent = content;
        else
            currentContent += System.lineSeparator() + content;
        setContentToFile(file, currentContent);
    }
    public static void addContentFirst(VirtualFile file, String content) throws IOException {
        String currentContent = new String(Files.readAllBytes(VfsUtil.virtualToIoFile(file).toPath()));
        if(currentContent.isEmpty())
            currentContent = content;
        else
            currentContent = content + System.lineSeparator() + currentContent;
        setContentToFile(file, currentContent);
    }

    public static void addLineToFileUndo(final Project project, VirtualFile file, final int position, final String line) throws Throwable
    {
        addLineToFileUndo(project, "Add line to file", file, position,line);
    }
    public static void addLineToFileUndo(final Project project, final String actionGroup, VirtualFile file, final int position, final String line) throws Throwable {

        WriteCommandAction
                .writeCommandAction(project)
                .withName(actionGroup)
                .withGroupId(actionGroup)
                .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                .run((ThrowableRunnable<Throwable>) () -> {
                    Document document = FileDocumentManager.getInstance().getDocument(file);
                    final int lineAndOffset = document.getLineEndOffset(position);
                    document.insertString(lineAndOffset, line);
                    FileDocumentManager.getInstance().saveDocument(document);
                });
    }

    public static void setContentToFileUndo(final Project project, VirtualFile file, final String content) throws Throwable
    {
        setContentToFileUndo(project, "Edit File", file, content);
    }
    public static void setContentToFileUndo(final Project project, final String actionGroup, VirtualFile file, final String content) throws Throwable {

        WriteCommandAction
                .writeCommandAction(project)
                .withName(actionGroup)
                .withGroupId(actionGroup)
                .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                .run((ThrowableRunnable<Throwable>) () -> {
                    Document document = FileDocumentManager.getInstance().getDocument(file);
                    document.setText(content);
                    FileDocumentManager.getInstance().saveDocument(document);
                });
    }

    public static void deleteDir(Project project, VirtualFile dir) throws IOException {
        if(!dir.exists())
            throw new FileSystemNotFoundException("Directory " + dir.getName() + " is not found");

        if(!dir.isDirectory())
            throw new NotDirectoryException(dir.getName() + " is not a directory");

        try {
            dir.delete(project);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void deleteRecurs(String path) throws IOException {
        deleteRecurs(Paths.get(path));
    }
    public static void deleteRecurs(Path path) throws IOException {
        if(!isExists(path.toString()))
            return;

        SimpleFileVisitor fileVisitor = new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        };
        Files.walkFileTree(path, fileVisitor);
        Files.delete(path);
    }

    public static VirtualFile createFile(final Project project, String pathToTargetDir, String fileName, String content) throws FileNotFoundException {
        final VirtualFile dir = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(pathToTargetDir));

        return createFile(project, dir, fileName, content);
    }

    public static VirtualFile createFile(final Project project, VirtualFile parentDir, String fileName, String content) throws FileNotFoundException {
        if(parentDir == null)
            throw new FileNotFoundException("Directory " + parentDir.getPath() + " is not found");

        try {
            VirtualFile file = parentDir.createChildData(project, fileName);
            if(!content.isEmpty()) {
                if (Settings.FilterFiles.isForDocumentManager(file)) {
                    Document sketchDocument = FileDocumentManager.getInstance().getDocument(file);
                    sketchDocument.setText(content);
                } else
                    file.setBinaryContent(content.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return parentDir.findChild(fileName);
    }

    public static VirtualFile createDirAndFiles(final Project project, final String parentDir, final String childDir, final LinkedHashMap<String,String> files) throws FileNotFoundException {
        final VirtualFile dir = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(parentDir));
        if(dir == null)
            throw new FileNotFoundException("Directory " + parentDir + " is not found");

        try {
            VirtualFile newDir = dir.createChildDirectory(project, childDir);
            for(Map.Entry<String, String> entry : files.entrySet())
            {
                String name    = entry.getKey();
                String content = entry.getValue();
                VirtualFile file = newDir.createChildData(project, name);
                if(!content.isEmpty())
                    file.setBinaryContent(content.getBytes());
                newDir.findChild(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return dir.findChild(childDir);
    }

    public static VirtualFile createFiles(final Project project, final VirtualFile parentDir, final LinkedHashMap<String,String> files)
    {
        try {
            for (Map.Entry<String, String> entry : files.entrySet()) {
                String name = entry.getKey();
                String content = entry.getValue();

                VirtualFile file = parentDir.createChildData(project, name);
                if (!content.isEmpty()) {
                    file.setBinaryContent(content.getBytes());
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return parentDir;
    }

    public static VirtualFile createDirsAndFiles(final Project project, final VirtualFile parentDir, final UnisetObjectFile object) throws IOException {

        VirtualFile childDir = parentDir.createChildDirectory(project, object.getName());

        if(!object.getSubdirs().isEmpty()) {
            for (UnisetObjectFile subObject : object.getSubdirs()) {
                createDirsAndFiles(project, childDir, subObject);
            }
        }

        for(Map.Entry<String, String> entry : object.getFiles().entrySet())
        {
            String name    = entry.getKey();
            String content = entry.getValue();

            VirtualFile file = childDir.createChildData(project, name);
            if(!content.isEmpty()) {
                if (Settings.FilterFiles.isForDocumentManager(file)) {
                    Document sketchDocument = FileDocumentManager.getInstance().getDocument(file);
                    sketchDocument.setText(content);
                } else
                    file.setBinaryContent(content.getBytes());
            }
        }

        return childDir;
    }

    public static VirtualFile createFileUndo(final Project project, String pathInProject, String fileName, String content) throws Throwable {

        return createFileUndo(project, "Creating File", pathInProject, fileName, content);
    }

    public static void createDirAndFilesUndo(final Project project, final String parentDir, final String childDir, final LinkedHashMap<String,String> files) throws Throwable {

        createDirAndFilesUndo(project, "Creating Files", parentDir, childDir, files);
    }

    public static VirtualFile createFileUndo(final Project project, final String actionGroup, String pathInProject, String fileName, String content) throws Throwable {
        final VirtualFile dir = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(pathInProject));
        if(dir != null)
        {
            ApplicationManager.getApplication().invokeAndWait(() -> {
                try {
                    WriteCommandAction
                            .writeCommandAction(project)
                            .withName(actionGroup)
                            .withGroupId(actionGroup)
                            .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                            .run((ThrowableRunnable<Throwable>) () -> {
                                try {
                                    VirtualFile file = dir.createChildData(project, fileName);
                                    if(!content.isEmpty())
                                        file.setBinaryContent(content.getBytes());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            });
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            });

        }

        return dir.findChild(fileName);
    }

    public static void createDirAndFilesUndo(final Project project, final String actionGroup, final String parentDir, final String childDir, final LinkedHashMap<String,String> files) throws Throwable {

                WriteCommandAction.writeCommandAction(project)
                        .withName(actionGroup)
                        .withGroupId(actionGroup)
                        .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                        .run((ThrowableRunnable<Throwable>) () -> {

                            try {
                                VirtualFile dir = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(parentDir));
                                VirtualFile newDir = dir.createChildDirectory(project, childDir);
                                for(Map.Entry<String, String> entry : files.entrySet())
                                {
                                    String name    = entry.getKey();
                                    String content = entry.getValue();
                                    VirtualFile file = newDir.createChildData(project, name);
                                    if(!content.isEmpty())
                                        file.setBinaryContent(content.getBytes());
                                    newDir.findChild(name);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        });
    }
}

