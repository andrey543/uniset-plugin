package com.uniset.plugin;

import com.uniset.plugin.build.UnisetObjectTransformator;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class R {

    private static final String XML_TAG = "template_project";
    private static R instance;
    private Document xml;

    public static R getInstance() throws IOException, SAXException, ParserConfigurationException {
        if(instance == null)
            instance = new R();

        return instance;
    }

    private R() throws ParserConfigurationException, IOException, SAXException {
        InputStream is = R.class.getClassLoader().getResourceAsStream("resource.xml");
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        xml = domFactory.newDocumentBuilder().parse(is);
    }

    public Document getXml() {
        return xml;
    }

    public TemplateProjectFile getTemplateProject() {
        return createFromXml(xml.getElementsByTagName(XML_TAG).item(0).getChildNodes(),
                new TemplateProjectFile(XML_TAG, "/templates"));
    }

    public TemplateProjectFile getTranfromProject(String newProjectName) throws IOException {
        return createFromXml(newProjectName, xml.getElementsByTagName(XML_TAG).item(0).getChildNodes(),
                new TemplateProjectFile(XML_TAG, "/templates"));
    }

    private TemplateProjectFile createFromXml(NodeList list, TemplateProjectFile root) {
        for (int i = 0; i < list.getLength(); ++i) {
            Node n = list.item(i);
            if (n.getNodeName().equals("dir")) {
                root.addSubDir(createFromXml(n.getChildNodes(),
                        new TemplateProjectFile(n.getAttributes().getNamedItem("name").getTextContent(), root.getPath())));
            } else if (n.getNodeName().equals("file")) {
                String name = n.getTextContent();
                root.addFile(name, readResourceFile(root.getPath() + "/" + name ));
            }
        }

        return root;
    }

    private TemplateProjectFile createFromXml(String projectName, NodeList list, TemplateProjectFile root) throws IOException {
        for (int i = 0; i < list.getLength(); ++i) {
            Node n = list.item(i);
            if (n.getNodeName().equals("dir")) {
                root.addSubDir(createFromXml(projectName, n.getChildNodes(),
                        new TemplateProjectFile(n.getAttributes().getNamedItem("name").getTextContent(), root.getPath())));
            }
            else if(n.getNodeName().equals("link"))
            {
                String path = n.getAttributes().getNamedItem("path").getTextContent();
                String name = n.getAttributes().getNamedItem("name").getTextContent();
                root.addLink(name, path);
            }
            else if (n.getNodeName().equals("file")) {
                String name = n.getTextContent();
                root.addFile(UnisetObjectTransformator.transformTemplateFile(name, projectName),
                        UnisetObjectTransformator.transformTemplateFromResource(root.getPath() + "/" + name, projectName, false, false));
            }
        }

        return root;
    }

    private String readResourceFile(String path)  {
        InputStream is = R.class.getClassLoader().getResourceAsStream(path);
        if(is == null)
        {
            System.out.println(path + " is not found");
            return "";
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String content = "";
        try {
            for(String line = reader.readLine(); line != null; line = reader.readLine())
            {
                content += line + System.lineSeparator();
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return content;
    }

    public static class TemplateProjectFile
    {
        private String path;
        private String dirName;
        private List<TemplateProjectFile> subDirs = new ArrayList<>();
        private LinkedHashMap<String, String> files = new LinkedHashMap<>();
        private LinkedHashMap<String, String> links = new LinkedHashMap<>();

        private TemplateProjectFile(String dirName, String parent)
        {
            this.dirName = dirName;
            this.path = parent + "/" + dirName;
        }

        public String getDirName() {
            return dirName;
        }

        public String getPath() {
            return path;
        }

        public List<TemplateProjectFile> getSubDirs() {
            return new ArrayList<>(subDirs);
        }

        public LinkedHashMap<String, String> getFiles() {
            return new LinkedHashMap<>(files);
        }

        public LinkedHashMap<String, String> getLinks() {
            return new LinkedHashMap<>(links);
        }

        private void addSubDir(TemplateProjectFile subDir)
        {
            subDirs.add(subDir);
        }

        private void addFile(String name, String content)
        {
            files.put(name, content);
        }

        private void addLink(String name, String path)
        {
            links.put(name, path);
        }
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {

    }
}
