package com.uniset.plugin.environment;

import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.modbus.MBMMObject;
import com.uniset.plugin.build.objects.modbus.MBMObject;
import com.uniset.plugin.build.objects.modbus.MBSObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;

public class EnvironmentModbus extends Environment {

    private Smemory smemory;
    private List<ModbusObject> modbusObjects;

    public EnvironmentModbus(final Configure configure, final Smemory smemory) throws IllegalAccessException {
        super(configure);
        this.smemory = smemory;
        this.modbusObjects = configure.getModbusObjectList();

        syncWithSmemory(modbusObjects);
        initStrFields(modbusObjects);
        syncWithSensors(modbusObjects);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EnvironmentModbus)) return false;
        EnvironmentModbus that = (EnvironmentModbus) o;
        return Objects.equals(modbusObjects, that.modbusObjects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modbusObjects);
    }

    public List<ModbusObject> getModbusObjects() {
        return modbusObjects;
    }

    @Override
    public void save(Environment newEnvironment) throws IOException, NodeScript.ExtParamBlockCorrupted, NodeScript.ClosedSymbolNotFound {
        saveModbusObjects((EnvironmentModbus) newEnvironment);
    }

    private void saveModbusObjects(EnvironmentModbus newEnvironment) throws NodeScript.ExtParamBlockCorrupted, IOException, NodeScript.ClosedSymbolNotFound {
        for(int i = 0; i < modbusObjects.size(); ++i)
        {
            ModbusObject oldObject = modbusObjects.get(i);
            ModbusObject newObject = newEnvironment.modbusObjects.get(i);
            if(!oldObject.equals(newObject))
            {
                configure.editModbusSensor(oldObject, newObject);
                configure.editSettingsObject(oldObject, newObject);
                oldObject.getNode().getScript().updateModbusObject(oldObject, newObject);
            }
        }
    }

    private void syncWithSensors(List<ModbusObject> objects)
    {
        for(Sensor sensor : configure.getSensorList()) {
            for (ModbusObject modbusObject : objects) {
                String filterField = modbusObject.getFilterField();
                String filterValue = modbusObject.getFilterValue();
                String prefix = modbusObject.getPrefix();

                String value = sensor.getProperty(filterField);
                if (value != null && value.equals(filterValue)) {

                    if(prefix.isEmpty())
                        prefix = filterField + "_";

                    String addr = sensor.getProperty(prefix + "mbaddr");
                    String reg = sensor.getProperty(prefix + "mbreg");
                    String func = sensor.getProperty(prefix + "mbfunc");
                    String nbit = sensor.getProperty(prefix + "nbit");

                    if(addr == null) addr = "";
                    if(reg == null) reg = "";
                    if(func == null) func = "";
                    if(nbit == null) nbit = "";

                    modbusObject.addSensor(sensor, addr, reg, func, nbit);
                }
            }
        }
    }

    private void syncWithSmemory(List<ModbusObject> objects) {
        for(ModbusObject object : objects) {
            selectNodeOf(object);
        }
    }

    private void selectNodeOf(ModbusObject object){
        for(NodeScript script : smemory.getStartScripts())
        {
            for(NodeScript.ModbusBlock modbusBlock : script.getModbusBlocks())
            {
                if((modbusBlock.getType() + "_" + modbusBlock.getName()).equals(object.getFullName())) {
                    for(Node node : configure.getNodeList()) {
                        if(script.getNodeName().equals(node.getName())) {
                            node.setScript(script);
                            object.setNode(node);
                            object.setModbusBlockOf(modbusBlock);
                        }
                    }
                }
            }
        }
    }

    private void editModbusSensors(ModbusObject oldObject, ModbusObject newObject)
    {
        // Удаляем настройки
        for(ModbusObject.ModbusSensor sensor : oldObject.getSensors())
        {

        }
    }


    /** Рефлексивная стартовая инициализация String полей
     * (для правильной работы отслеживания изменений)*/
    private void initStrFields(List<ModbusObject> objects) throws IllegalAccessException {
        for(ModbusObject object : objects)
        {
            if(object instanceof MBMObject)
            {
                MBMObject mbmObject = (MBMObject) object;
                for(Field field : mbmObject.getClass().getDeclaredFields())
                {
                    if(field.getType() == String.class)
                    {
                        field.setAccessible(true);
                        String value = (String) field.get(mbmObject);
                        if(value == null)
                            field.set(mbmObject, "");
                        field.setAccessible(false);
                    }
                }
            }
            else if(object instanceof MBMMObject)
            {
                MBMMObject mbmmObject = (MBMMObject) object;
                for(Field field : mbmmObject.getClass().getDeclaredFields())
                {
                    if(field.getType() == String.class)
                    {
                        field.setAccessible(true);
                        String value = (String) field.get(mbmmObject);
                        if(value == null)
                            field.set(mbmmObject, "");
                        field.setAccessible(false);
                    }
                }
            }
            else if(object instanceof MBSObject)
            {
                MBSObject mbsObject = (MBSObject) object;

                for(Field field : mbsObject.getClass().getDeclaredFields())
                {
                    if(field.getType() == String.class)
                    {
                        field.setAccessible(true);
                        String value = (String) field.get(mbsObject);
                        if(value == null)
                            field.set(mbsObject, "");
                        field.setAccessible(false);
                    }
                }
            }
        }
    }

}
