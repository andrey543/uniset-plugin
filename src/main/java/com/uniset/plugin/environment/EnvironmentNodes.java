package com.uniset.plugin.environment;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.make.MakeConfiguration;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.smemory.Smemory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;
import java.util.regex.Pattern;

public class EnvironmentNodes extends Environment {

    private Project project;
    private Configure configure;
    private Smemory smemory;
    private LinkedHashMap<String, Node> nodeHashMap = new LinkedHashMap<>();

    public EnvironmentNodes(final Project project, final Configure configure, final Smemory smemory) throws IllegalAccessException {
        super(configure);
        this.project = project;
        this.configure = configure;
        this.smemory = smemory;
        for(Node node : configure.getNodeList())
        {
            nodeHashMap.put(node.getName(), node);
        }

        syncWithNodeScripts();
        syncWithMiscDir();
        syncWithSensors();
        initStrFields(getNodes());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EnvironmentNodes)) return false;
        EnvironmentNodes that = (EnvironmentNodes) o;
        return Objects.equals(nodeHashMap, that.nodeHashMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nodeHashMap);
    }

    private void syncWithNodeScripts()
    {
        for(Node node : getNodes())
        {
            node.setScript(smemory.getStartScript(node.getName()));
        }
    }

    private void syncWithSensors()
    {
        for(Sensor sensor : configure.getSensorList())
        {
            String unet = sensor.getProperty("unet");
            if(unet != null && !unet.isEmpty())
            {
                Node node = nodeHashMap.get(unet);
                if(node != null)
                {
                    node.addSensor(sensor);
                }
            }
        }
    }

    private void syncWithMiscDir()
    {
        for(Map.Entry<String, Node> entry : nodeHashMap.entrySet()) {
            VirtualFile miscNodeDir = LocalFileSystem.getInstance()
                    .findFileByPath(project.getBasePath() + "/conf/misc/" + entry.getValue().getName());
            if(miscNodeDir != null)
                entry.getValue().setMiscDir(miscNodeDir);
        }
    }

    public Configure getConfigure() {
        return configure;
    }

    public List<Node> getNodes() {
        List<Node> nodes = new ArrayList<>();
        for (Map.Entry<String, Node> entry : nodeHashMap.entrySet())
        {
            nodes.add(entry.getValue());
        }
        return nodes;
    }


    @Override
    public void save(Environment newEnvironment) throws Exception {

        for(int i = 0; i < getNodes().size(); ++i) {
            Node oldNode = getNodes().get(i);
            Node newNode = ((EnvironmentNodes)newEnvironment).getNodes().get(i);

            if (!oldNode.equals(newNode)) {
                saveSettings(oldNode, newNode);
                saveSensors(oldNode, newNode);
            }
        }
    }

    private void saveSensors(Node oldNode,  Node newNode)
    {
        LinkedHashMap<String, String> props = new LinkedHashMap<>();
        props.put("unet", oldNode.getName());
        for(Sensor sensor : oldNode.getSensors())
        {
            configure.removeProps(sensor, props);
        }

        props.put("unet", newNode.getName());
        for(Sensor sensor : newNode.getSensors())
        {
            configure.addProps(sensor, props);
        }
    }

    private void saveSettings(Node oldNode,  Node newNode) throws Exception {

        // переименовываем папку в conf/misc
        VirtualFile miscNodeDir = oldNode.getMiscDir();
        if(miscNodeDir != null) {
            for (VirtualFile file : miscNodeDir.getChildren()) {
                // Надеюсь там не будут папки
                if (file.isDirectory())
                    continue;
                FileUtils.replaceLineInFile(file, oldNode.getName(), newNode.getName());
                file.rename(project, file.getName().replaceAll(oldNode.getName(), newNode.getName()));
            }
            miscNodeDir.rename(project, newNode.getName());

            // переименовываем в Makefile.am предыдущего каталога(предположительно в SUBDIRS)
            VirtualFile makeFile = miscNodeDir.getParent().findChild("Makefile.am");
            if(makeFile != null)
                FileUtils.replaceLineInFile(makeFile, oldNode.getName(), newNode.getName());
            // Переименовываем в configure.ac
            VirtualFile configureAcFile = MakeConfiguration.getInstance(project).getConfigure_ac();
            if(configureAcFile != null) {
                String prefix = "conf/misc/";
                for(String line : MakeConfiguration.getInstance(project).getAc_config_files())
                {
                    if(line.matches(prefix + oldNode.getName() + ".*")) {
                        String newLine = line.replaceAll(oldNode.getName(), newNode.getName());
                        FileUtils.replaceLineInFile(configureAcFile, line, newLine);
                    }
                }
            }
        }

        // переименовываем node script
        if(oldNode.getScript() != null)
        {
            VirtualFile script = oldNode.getScript().getVirtualFile();
            FileUtils.replaceLineInFile(script, oldNode.getName(), newNode.getName());
            script.rename(project, script.getName().replaceAll(oldNode.getName(), newNode.getName()));
        }

        // меняем настройки в configure
        configure.editNode(oldNode, newNode);
    }

    /** Рефлексивная стартовая инициализация String полей
     * (для правильной работы отслеживания изменений)*/
    private void initStrFields(List<Node> nodes) throws IllegalAccessException {
        for (Node node : nodes) {
            for (Field field : node.getClass().getDeclaredFields()) {
                if (field.getType() == String.class) {
                    field.setAccessible(true);
                    String value = (String) field.get(node);
                    if (value == null)
                        field.set(node, "");
                    field.setAccessible(false);
                }
            }
        }
    }
}
