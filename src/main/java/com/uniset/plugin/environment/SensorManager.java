package com.uniset.plugin.environment;

import com.uniset.plugin.build.objects.UnisetObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import org.eclipse.lsp4j.jsonrpc.validation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class SensorManager extends Environment{

    private EnvironmentUniset environmentUniset;
    private EnvironmentModbus environmentModbus;

    private List<SyncSensor> syncSensors = new ArrayList<>();

    public SensorManager(@NonNull final Configure configure, @NonNull EnvironmentUniset environmentUniset, @NonNull EnvironmentModbus environmentModbus) {
        super(configure);
        this.configure = configure;
        this.environmentUniset = environmentUniset;
        this.environmentModbus = environmentModbus;
        syncWithEnvironment();
    }

    public List<SyncSensor> getSyncSensors() {
        return syncSensors;
    }

    @Override
    public void save(Environment newEnvironment) throws Exception {

    }

    private void syncWithEnvironment()
    {
        for(Sensor sensor : configure.getSensorList())
        {
            SyncSensor syncSensor = new SyncSensor(sensor);
            defineUnisetObject(syncSensor);
            defineModbusObjects(syncSensor);
            syncSensors.add(syncSensor);
        }
    }

    private void defineModbusObjects(SyncSensor syncSensor)
    {
        for(ModbusObject modbusObject : environmentModbus.getModbusObjects())
        {
            String filterField = modbusObject.getFilterField();
            String filterValue = modbusObject.getFilterValue();
            String prefix = modbusObject.getPrefix();
            if(prefix.isEmpty())
                prefix = modbusObject.getFilterField() + "_";

            String value = syncSensor.sensor.getProperty(filterField);
            if(value != null && value.equals(filterValue)) {
                ModbusInfo modbusInfo = new ModbusInfo(modbusObject);
                modbusInfo.addr = syncSensor.sensor.getProperty(prefix + "mbaddr");
                modbusInfo.reg  = syncSensor.sensor.getProperty(prefix + "mbreg");
                modbusInfo.func = syncSensor.sensor.getProperty(prefix + "mbfunc");
                modbusInfo.nbit = syncSensor.sensor.getProperty(prefix + "nbit");

                syncSensor.modbusInfos.add(modbusInfo);
            }
        }
    }

    private void defineUnisetObject(SyncSensor syncSensor)
    {
        for(UnisetObject unisetObject : environmentUniset.getUnisetObjects()) {
            List<UnisetObject.SMapSensor> sMapSensors = new ArrayList<>();
            for(UnisetObject.SMapSensor sMapSensor : unisetObject.getSmapSensors()) {
                if(sMapSensor.getAttachSensor() == syncSensor.sensor)
                    sMapSensors.add(sMapSensor);
            }
            if(!sMapSensors.isEmpty()) {
                UnisetInfo unisetInfo = new UnisetInfo(unisetObject);
                unisetInfo.sMapSensors.addAll(sMapSensors);
                syncSensor.unisetInfos.add(unisetInfo);
            }
        }
    }

    public class SyncSensor
    {
        private Sensor sensor;
        private List<ModbusInfo> modbusInfos = new ArrayList<>();
        private List<UnisetInfo> unisetInfos = new ArrayList<>();

        private SyncSensor(Sensor sensor)
        {
            this.sensor = sensor;
        }

        public Sensor getSensor() {
            return sensor;
        }

        public List<ModbusInfo> getModbusInfos() {
            return new ArrayList<>(modbusInfos);
        }

        public List<UnisetInfo> getUnisetInfos() {
            return new ArrayList<>(unisetInfos);
        }

        public ModbusInfo getInfo(ModbusObject object)
        {
            for(ModbusInfo info : modbusInfos)
            {
                if(info.getModbusObject().equals(object))
                    return info;
            }
            return null;
        }
    }

    public class ModbusInfo
    {
        private ModbusObject modbusObject;
        private String addr;
        private String reg;
        private String func;
        private String nbit;

        private ModbusInfo(ModbusObject object)
        {
            this.modbusObject = object;
        }

        public ModbusObject getModbusObject() {
            return modbusObject;
        }

        public String getAddr() {
            return addr;
        }

        public String getReg() {
            return reg;
        }

        public String getFunc() {
            return func;
        }

        public String getNbit() {
            return nbit;
        }
    }

    public class UnisetInfo
    {
        private UnisetObject unisetObject;
        private List<UnisetObject.SMapSensor> sMapSensors = new ArrayList<>();

        private UnisetInfo(UnisetObject unisetObject)
        {
            this.unisetObject = unisetObject;
        }

        public UnisetObject getUnisetObject() {
            return unisetObject;
        }

        public List<UnisetObject.SMapSensor> getsMapSensors() {
            return new ArrayList<>(sMapSensors);
        }
    }

}
