package com.uniset.plugin.environment;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.UnisetObjectGenerator;
import com.uniset.plugin.build.objects.UnisetObject;
import com.uniset.plugin.configure.Configure;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EnvironmentUniset extends Environment{

    private Project project;
    private List<UnisetObject> unisetObjects;

    public EnvironmentUniset(final Project project, final Configure configure) {
        super(configure);
        this.project = project;

        unisetObjects = configure.getUnisetObjectList();
        syncSrcXmlOfObjects();
    }

    public List<UnisetObject> getUnisetObjects() {
        return new ArrayList<>(unisetObjects);
    }

    @Override
    public void save(Environment newEnvironmentUniset){
        saveUnisetObjects((EnvironmentUniset) newEnvironmentUniset);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EnvironmentUniset)) return false;
        EnvironmentUniset that = (EnvironmentUniset) o;
        return Objects.equals(unisetObjects, that.unisetObjects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unisetObjects);
    }

    private void saveUnisetObjects(EnvironmentUniset newEnvironmentUniset)
    {
        for(int i = 0; i < unisetObjects.size(); ++i) {
            UnisetObject oldObject = unisetObjects.get(i);
            UnisetObject newObject = newEnvironmentUniset.getUnisetObjects().get(i);
            if(!oldObject.equals(newObject))
            {
                configure.editSettingsObject(newObject);
            }
        }
    }

    private void syncSrcXmlOfObjects()
    {
        List<Path> algorithms;
        List<Path> imitators;
        try {
            algorithms = UnisetObjectGenerator.generateProjectList(project, Settings.PROJECT_PATH_TO_ALGORITHMS);
            imitators = UnisetObjectGenerator.generateProjectList(project, Settings.PROJECT_PATH_TO_IMITATORS);
        } catch (IOException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    "Uniset environment project", Messages.getErrorIcon());
            return;
        }

        for(UnisetObject object : unisetObjects)
        {
            Path srcXml = null;
            if(algorithms != null) {
                srcXml = findSrcXml(object.getTypename(), algorithms, Settings.PROJECT_PATH_TO_ALGORITHMS);
            }
            if(srcXml == null && imitators != null)
            {
                srcXml = findSrcXml(object.getTypename(), imitators, Settings.PROJECT_PATH_TO_IMITATORS);
            }
            if(srcXml != null && Files.exists(srcXml)) {
                try {
                    object.setSrcXml(srcXml, configure);
                } catch (JDOMException e) {
                    e.printStackTrace();
                } catch (ParserConfigurationException e) {
                    // Сообщение(отмена процесса): ошибка "парсинга" хз что это...
                    Messages.showMessageDialog(project,
                            "The " + srcXml.getFileName().toString() +  " has errors of parsing",
                            "Uniset environment project", Messages.getErrorIcon());
                    continue;
                } catch (SAXException e) {
                    // Сообщение(отмена процесса): файл конфигурации поврежден
                    Messages.showMessageDialog(project,
                            "The"+ srcXml.getFileName().toString() + " is corrupted",
                            "Uniset environment project", Messages.getErrorIcon());
                    continue;
                } catch (IOException e) {
                    // Сообщение(отмена процесса): файл конфигурации не найден в проекте
                    Messages.showMessageDialog(project,
                            e.getMessage(),
                            "Uniset environment project", Messages.getErrorIcon());
                    continue;
                }
            }
        }
    }

    private Path findSrcXml(String type, List<Path> list, String path_to){
        for(Path algorithm : list)
        {
            if(algorithm.getFileName().toString().equals(type))
            {
                return Paths.get(project.getBasePath() + path_to + type + "/" + type.toLowerCase() + ".src.xml");
            }
        }

        return null;
    }


}
