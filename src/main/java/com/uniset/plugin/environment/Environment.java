package com.uniset.plugin.environment;

import com.uniset.plugin.configure.Configure;
import org.eclipse.lsp4j.jsonrpc.validation.NonNull;

public abstract class Environment {
    protected Configure configure;

    public Environment(@NonNull final Configure configure)
    {
        this.configure = configure;
    }

    public static class EnvironmentSaveBox
    {
        private Environment oldEnv;
        private Environment newEnv;
        public EnvironmentSaveBox(Environment oldEnv, Environment newEnv)
        {
            this.oldEnv = oldEnv;
            this.newEnv = newEnv;
        }

        public Environment getEnvironment()
        {
            return newEnv;
        }

        public boolean hasChanged()
        {
            return !oldEnv.equals(newEnv);
        }

        public void save() throws Exception {
            oldEnv.save(newEnv);
        }
    }

    public abstract void save(Environment newEnvironment) throws Exception;

}
