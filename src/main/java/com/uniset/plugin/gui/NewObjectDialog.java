package com.uniset.plugin.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.gui.modules.ButtonAllowerOKAdapter;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class NewObjectDialog extends DialogWrapper {
    public final static String TITLE = "New Uniset Object";

    private JPanel mainPanel;
    private JTextField textField1;
    private JCheckBox makeCheckBox;
    private JCheckBox CMakeCheckBox;
    private JCheckBox isCreatingDirCheckBox;
    private JCheckBox createMainCcCheckBox;

    private Project project;
    private Configure configure;

    ButtonAllower okButtonAllower;

    public NewObjectDialog(@Nullable Project project, final Configure configure) {
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.configure = configure;

        JTextField textFields[] = {textField1};
        okButtonAllower = new ButtonAllower(new ButtonAllowerOKAdapter(this), textFields);
    }

    public boolean isDirectory()
    {
        return isCreatingDirCheckBox.isSelected();
    }
    public boolean isMakeConfiguration() {
        return makeCheckBox.isSelected();
    }
    public boolean isCMakeConfiguration() {
        return CMakeCheckBox.isSelected();
    }
    public boolean isCreateMainCc()
    {
        return createMainCcCheckBox.isSelected();
    }

    public String nameOfObject() {
        return textField1.getText();
    }

    @Override
    protected void doOKAction() {
        String name = textField1.getText();

        if(configure.reader.hasElement(name))
        {
            Messages.showMessageDialog(project,
                    "That " + name + " uniset object is already exist in configure.xml",
                    TITLE, Messages.getInformationIcon());
        }
        else
            super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

}
