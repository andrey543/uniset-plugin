package com.uniset.plugin.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.gui.modules.ButtonAllowerOKAdapter;
import com.uniset.plugin.gui.modules.FocusManagerTextField;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.*;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;

public class AddSensorDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JTextField idTextField;
    private JTextField nameTextField;
    private JTextField textnameTextField;
    private JComboBox<String> typeComboBox;
    private JTable propertyTable;
    private JTextField propertyTextField;
    private JTextField valueTextField;
    private JButton addButton;
    private JButton delButton;
    private JTextField debounceTextField;
    private JTable sensorTable;
    private JButton addSensorButton;
    private JButton delSensorButton;

    private Project project;
    private Configure configure;
    private ButtonAllower okButtonAllower;
    private ButtonAllower addSensorButtonAllower;
    private ButtonAllower addButtonAllower;
    private PropertyTableModel propertyTableModel = new PropertyTableModel();
    // ключ - реальное название xml-поля
    private LinkedHashMap<String, JTextField> advancedProps = new LinkedHashMap<>();
    private SensorTableModel sensorTableModel = new SensorTableModel();

    private static final String TITLE = "Add Sensors";

    private final long startId;
    private long newId;

    public AddSensorDialog(@Nullable Project project, final Configure configure) {
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.configure = configure;

        /** Основные параметры */
        startId = getMaxSensorId() + 1;
        newId = startId;
        idTextField.setText(String.valueOf(newId));
        nameTextField.setText("GROUP_SensorName_IOTYPE");
        textnameTextField.setText("GROUP: The description of sensor");
        typeComboBox.addItem(Settings.IOTYPE_DI);
        typeComboBox.addItem(Settings.IOTYPE_DO);
        typeComboBox.addItem(Settings.IOTYPE_AI);
        typeComboBox.addItem(Settings.IOTYPE_AO);
        JTextField[] mainTextFields = {idTextField, nameTextField, textnameTextField};

        /** Дополнительные параметры */
        advancedProps.put("debounce", debounceTextField);

        /** Таблица свойств */
        propertyTable.setDefaultEditor(Object.class, null);
        propertyTable.setModel(propertyTableModel);
        propertyTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(propertyTable, 50, 50);

        propertyTextField.addFocusListener(new FocusManagerTextField(propertyTextField, "prop", Settings.REGEX_ONLY_CHARS));
        valueTextField.addFocusListener(new FocusManagerTextField(propertyTextField, "value", Settings.REGEX_ALL));
        JTextField[] propertyTextFields = {propertyTextField, valueTextField};
        addButtonAllower = new ButtonAllower(addButton, propertyTextFields);

        /** Кнопка add */
        addButton.addActionListener(e -> {
                String prop = propertyTextField.getText();
                String value = valueTextField.getText();
                Property property = getProperty(prop);
                if(property == null)
                    propertyTableModel.addProperty(new Property(prop, value));
                });

        /** Кпонка del */
        delButton.addActionListener(e -> propertyTableModel.remove(propertyTable.getSelectedRow()));

        /**
         * Датчики
         * */
        sensorTable.setDefaultEditor(Object.class, null);
        sensorTable.setModel(sensorTableModel);
        sensorTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setJTableColumnsWidth(sensorTable, 5, 30, 49, 2, 14);

        okButtonAllower = new ButtonAllower(new ButtonAllowerOKAdapter(this));
        okButtonAllower.addEventButton(addSensorButton);
        okButtonAllower.addEventButton(delSensorButton);
        okButtonAllower.addAdditionalCondition(() -> sensorTable.getRowCount() > 0);
        okButtonAllower.updateAllow();

        /** Кнопка "--->" */
        addSensorButtonAllower = new ButtonAllower(addSensorButton, mainTextFields);
        addSensorButton.addActionListener(e -> {
            if(checkSensor()) {
                sensorTableModel.addSensor(getSensor());
                idTextField.setText(String.valueOf(++newId));
            }
        });

        /** Кнопка del */
        delSensorButton.addActionListener(e -> {
            if( getSelectedSensor() == null)
                return;
            sensorTableModel.removeSensor(getSelectedSensor());
            if(newId > startId)
                idTextField.setText(String.valueOf(--newId));
        });

    }

    private Sensor getSelectedSensor()
    {
        if(sensorTable.getSelectedRow() == -1)
            return null;

        return sensorTableModel.getSensor(sensorTable.getSelectedRow());
    }

    private Sensor getSensor()
    {
        // Основные параметры
        String name = getName();
        String iotype = getIOType();
        Sensor sensor = new Sensor(
                Long.parseLong(idTextField.getText()),
                name,
                iotype,
                textnameTextField.getText());

        // Дополнительные
        for(Map.Entry<String, JTextField> advancedProp : advancedProps.entrySet()) {
            String value = advancedProp.getValue().getText();
            if(!value.isEmpty())
                sensor.addProperty(advancedProp.getKey(), advancedProp.getValue().getText());
        }
        // Из таблицы свойств
        for(int row = 0; row < propertyTable.getRowCount(); ++row) {
            Property property = propertyTableModel.getProperty(row);
            sensor.addProperty(property.prop, property.value);
        }

        return sensor;
    }

    public List<Sensor> getSensors()
    {
        return sensorTableModel.getSensors();
    }

    @Override
    protected void doOKAction() {
        super.doOKAction();
    }

    private boolean checkSensor()
    {
        long id = Long.parseLong(idTextField.getText());
        if(configure.getSensorById(id) != null)
        {
            Messages.showMessageDialog(project,
                    "Sensor ID=" + id + " is already exists",
                    TITLE, Messages.getErrorIcon());
            return false;
        }

        if(configure.getSensorByName(getName()) != null)
        {
            Messages.showMessageDialog(project,
                    "Sensor \"" + getName() + "\" is already exists",
                    TITLE, Messages.getErrorIcon());
            return false;
        }

        for(Sensor sensor : sensorTableModel.getSensors()) {
            if (sensor.getId() == id) {
                Messages.showMessageDialog(project,
                        "Sensor ID=" + id + " is already exists in the sensor table",
                        TITLE, Messages.getErrorIcon());
                return false;
            }

            if(sensor.getName().equals(getName()))
            {
                Messages.showMessageDialog(project,
                        "Sensor \"" + getName() + "\" is already exists in the sensor table",
                        TITLE, Messages.getErrorIcon());
                return false;
            }
        }

        return true;
    }



    private String getIOType()
    {
        return (String) typeComboBox.getSelectedItem();
    }

    private String getName()
    {
        return nameTextField.getText();
    }

    private long getMaxSensorId()
    {
        long max = 0;
        for(Sensor sensor : configure.getSensorList())
        {
            if(max < sensor.getId())
                max = sensor.getId();
        }

        return max;
    }

    private Property getProperty(String prop)
    {
        for(int row = 0; row < propertyTable.getRowCount(); ++row)
        {
            Property property = propertyTableModel.getProperty(row);
            if(property.getProp().equals(prop))
                return property;
        }

        return null;
    }

    private class PropertyTableModel extends AbstractTableModel
    {

        private List<Property> properties = new ArrayList<>();
        String[] propertiesHeaders = {"Property", "Value"};

        @Override
        public int getRowCount() {
            return properties.size();
        }

        @Override
        public int getColumnCount() {
            return propertiesHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return propertiesHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Object value = "";
            Property property = properties.get(rowIndex);
            if(property != null)
            {
                switch (columnIndex)
                {
                    case 0:
                        value = property.getProp();
                        break;
                    case 1:
                        value = property.getValue();
                        break;
                }
            }
            return value;
        }

        public void addProperty(final Property property)
        {
            properties.add(property);
            fireTableDataChanged();
        }

        public void addAllProperty(List<Property> list)
        {
            properties.addAll(list);
        }

        public void remove(int row)
        {
            if(row >= 0) {
                properties.remove(row);
            }
            fireTableDataChanged();
        }

        public void clear()
        {
            properties.clear();
            fireTableDataChanged();
        }

        public Property getProperty(int row)
        {
            return properties.get(row);
        }
    }

    private class Property
    {
        private String prop;
        private String value;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Property)) return false;
            Property property = (Property) o;
            return Objects.equals(prop, property.prop) &&
                    Objects.equals(value, property.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(prop, value);
        }

        public Property(String prop, String value) {
            this.prop = prop;
            this.value = value;
        }

        public String getProp() {
            return prop;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    public class SensorTableModel extends AbstractTableModel{

        private final List<Sensor> sensors = new ArrayList<>();
        private final String[] sensorsHeaders = {"id", "name", "textname","type", "other"};

        @Override
        public int getRowCount() {
            return sensors.size();
        }

        @Override
        public int getColumnCount() {
            return sensorsHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return sensorsHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Sensor sensor = sensors.get(rowIndex);
            Object value = "";
            if(sensor != null) {
                switch (columnIndex) {
                    case 0:
                        value = sensor.getId();
                        break;
                    case 1:
                        value = sensor.getName();
                        break;
                    case 2:
                        value = sensor.getTextname();
                        break;
                    case 3:
                        value = sensor.getIotype();
                        break;
                    case 4:
                        for(Map.Entry<String, String> entry : sensor.getProperties().entrySet())
                            value += entry.getKey() + " = " + entry.getValue() + "\n";
                        break;
                }
            }

            return value;
        }
        public void addSensor(final Sensor sensor)
        {
            sensors.add(sensor);
            fireTableDataChanged();
        }

        public void removeSensor(final Sensor sensor)
        {
            sensors.remove(sensor);
            fireTableDataChanged();
        }

        public void addAllSensors(List<Sensor> list)
        {
            sensors.addAll(list);
        }

        public void clear()
        {
            sensors.clear();
            fireTableDataChanged();
        }

        public Sensor getSensor(int row)
        {
            return sensors.get(row);
        }

        public Sensor[] getSensors(int[] rows)
        {
            Sensor[] sensors = new Sensor[rows.length];
            for(int i = 0; i < rows.length; ++i)
                sensors[i] = getSensor(rows[i]);

            return sensors;
        }

        public List<Sensor> getSensors()
        {
            return sensors;
        }
    }
}
