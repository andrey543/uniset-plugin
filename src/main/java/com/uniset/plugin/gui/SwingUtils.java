package com.uniset.plugin.gui;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public class SwingUtils {
    /** Задание ширины колонок в процентах */
    public static void setJTableColumnsWidth(JTable table, double... percentages) {
        double total = 0;
        for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
            total += percentages[i];
        }

        double totalWidth = table.getPreferredSize().getWidth();
        for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
            TableColumn column = table.getColumnModel().getColumn(i);
            double columnWidth = (totalWidth * (percentages[i] / total));
            column.setPreferredWidth((int) columnWidth);
        }
    }
    /** Очистка таблицы */
    public static void clearTable(DefaultTableModel model)
    {
        int count = model.getRowCount();
        for(int i = 0; i < count; i++)
        {
            model.removeRow(0);
        }
    }
}
