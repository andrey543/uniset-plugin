package com.uniset.plugin.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.packageDependencies.ui.TreeModel;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.actions.add.unisetobject.AddUnisetObjectAction;
import com.uniset.plugin.build.objects.ResourceObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.additional.SettingAddUnisetObjectDialog;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.gui.modules.ButtonAllowerOKAdapter;
import org.jdom2.JDOMException;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.List;

public class AddUnisetObjectDialog extends DialogWrapper {
    public static final String NONE = "None";
    public static final String BASE_ALGORITHM = "Base";
    public static final String PROJECT_ALGORITHM = "Project";

    private JPanel mainPanel;
    private JPanel configPane;
    private JCheckBox makeCheckBox;
    private JCheckBox cMakeCheckBox;
    private JTextField nameOfUnisetObjectTextField;
    private JButton addButton;
    private JButton deleteButton;
    private Set<JRadioButton> groupSectionSelected = new HashSet<>();
    private JPanel chooseSectionPanel;
    private JComboBox comboBoxSection;
    private JLabel labelNameOfNewObject;
    private JLabel labelListOfNewObjects;
    private JButton settingsButton;
    private DefaultTreeCellRenderer treeCellRenderer;
    private JTree treeOfUnisetObjects;
    private JTable table1;
    private JCheckBox autogenerateSensorsCheckBox;

    private AddUnisetObjectAction action;
    private Project project;
    private Configure configure;

    private List<Path> projectList;
    private List<ResourceObject> baseList;
    private LinkedHashMap<String, List<Path>> additionalList;

    private DefaultTableModel tableModel = new DefaultTableModel();
    private ButtonAllower addButtonAllower;
    private ButtonAllower okButtonAllower;

    public AddUnisetObjectDialog(final Project project,
                                 final Configure configure,
                                 final AddUnisetObjectAction action)
    {
        super(project);
        this.project = project;
        this.configure = configure;
        this.action = action;
        setTitle("Adding " + action.getName());
        init();

        /** Заголовки */
        labelNameOfNewObject.setText("Name of a new " + action.getName() + " object:");
        labelListOfNewObjects.setText("List of new " + action.getName() + "s objects to configuration:");

        /** Дерево */
        // Для кастомизирования дизайна дерева
        treeCellRenderer = (DefaultTreeCellRenderer) treeOfUnisetObjects.getCellRenderer();
        Icon dirIcon = null;
        Icon leafIcon = null;
        try {
            leafIcon = new ImageIcon(ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("icons/treeIcons/algFile.png")));
            dirIcon = new ImageIcon(ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("icons/treeIcons/dirAlgBase.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        treeCellRenderer.setClosedIcon(dirIcon);
        treeCellRenderer.setOpenIcon(dirIcon);
        treeCellRenderer.setLeafIcon(leafIcon);
        treeOfUnisetObjects.setCellRenderer(treeCellRenderer);
        treeOfUnisetObjects.setRootVisible(false);

        // Заполнение дерева
        updateTree(project, action);

        // При выборе элемента из дерева
        treeOfUnisetObjects.addTreeSelectionListener(e -> {
            updateEnabled();
        });
        updateEnabled();

        /** Таблица объектов*/
        String[] headers = {"Id", "Name"};
        tableModel.setColumnIdentifiers(headers);
        table1.setDefaultEditor(Object.class, null);
        table1.setModel(tableModel);

        /** Кнопка настроек */
        settingsButton.addActionListener(e -> {
            SettingAddUnisetObjectDialog dialog = new SettingAddUnisetObjectDialog(project);
            dialog.setResizable(true);
            dialog.pack();
            dialog.show();
            if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
                // Обновление дерева после применения настроек
                updateTree(project, action);
            }
        });

        //Заполняем имеющимися секциями
        updateComboBox(project, configure, action);

        /** Кнопка ОК */
        okButtonAllower = new ButtonAllower(new ButtonAllowerOKAdapter(this));

        /** Кнопка add */
        JTextField[] textFields = {nameOfUnisetObjectTextField};
        addButtonAllower = new ButtonAllower(addButton, textFields);
        addButton.addActionListener(e -> {
            long id = configure.getNextFreeObjectId();
            String objectName = nameOfUnisetObjectTextField.getText();

            if(configure.reader.hasElement(treeOfUnisetObjects.getLastSelectedPathComponent().toString(), objectName))
            {
                Messages.showMessageDialog(project,
                        "That " + objectName +" object is already exist in configure.xml",
                        action.getTitle(), Messages.getInformationIcon());
                return;
            }

            if(hasObjectInTable(objectName))
                return;

            String[] row = {String.valueOf(nextId(id)), objectName};
            tableModel.addRow(row);
            updateEnabledAutogenerationSensorsCheckBox();
        });

        /** Кнопка delete */
        deleteButton.addActionListener(e -> {
            if (tableModel.getRowCount() > 0) {
                int i = table1.getSelectedRow();
                if(i < 0)
                    return;
                tableModel.removeRow(i);
                updateEnabledAutogenerationSensorsCheckBox();
            }
        });

        /** Завершающее обновление */
        updateEnabled();
    }

    public boolean isAutogenerateSensors()
    {
        return autogenerateSensorsCheckBox.isSelected();
    }
    public boolean isMake()
    {
        return makeCheckBox.isSelected();
    }
    public boolean isCMake()
    {
        return cMakeCheckBox.isSelected();
    }
    public boolean isBase()
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeOfUnisetObjects.getLastSelectedPathComponent();
        if(node == null)
            throw new NullPointerException();

        return node.getParent().toString().equals(BASE_ALGORITHM);
    }

    public boolean isProject()
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeOfUnisetObjects.getLastSelectedPathComponent();
        if(node == null)
            throw new NullPointerException();

        return node.getParent().toString().equals(PROJECT_ALGORITHM);
    }

    public boolean isAdditional()
    {
        return !isProject() && !isBase();
    }

    public LinkedHashMap<String, String> getObjects()
    {
        LinkedHashMap<String, String> map = new LinkedHashMap<>();
        for(int i = 0; i < tableModel.getRowCount(); i++)
        {
            String id   = (String) tableModel.getValueAt(i, 0);
            String name = (String) tableModel.getValueAt(i, 1);

            map.put(id, name);
        }

        return map;
    }


    public String getSelectedName()
    {
        return treeOfUnisetObjects.getLastSelectedPathComponent().toString();
    }

    public String getAdditionalProjectName()
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeOfUnisetObjects.getLastSelectedPathComponent();

        return node.getParent().toString();
    }

    public Path getSelectedProjectObject()
    {
        if(isProject()) {
            for(Path path : projectList)
            {
                if(path.getFileName().toString().equals(getSelectedName()))
                    return path;
            }
        }

        return null;
    }
    public ResourceObject getSelectedBaseObject()
    {
        if(isBase()) {
            for(ResourceObject object : baseList)
                if(object.getName().equals(treeOfUnisetObjects.getLastSelectedPathComponent().toString()))
                    return object;
        }

        return null;
    }

    public Path getSelectedAdditionalObject()
    {
        if(isAdditional()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeOfUnisetObjects.getLastSelectedPathComponent();
            String algorithmName = node.toString();
            String projectName   = node.getParent().toString();

            for (Map.Entry<String, List<Path>> entry : additionalList.entrySet()) {
                if(projectName.equals(entry.getKey()))
                {
                    for(Path path : entry.getValue())
                    {
                        if(algorithmName.equals(path.getFileName().toString()))
                            return path;
                    }
                }
            }
        }

        return null;
    }

    public String getSelectedChooseSection()
    {
        return (String) comboBoxSection.getSelectedItem();
    }

    private void updateTree(Project project, AddUnisetObjectAction action) {

        try {
            projectList = action.getProjectList(project);
        } catch (IOException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    action.getTitle(), Messages.getErrorIcon());
            return;
        }

        try {
            baseList = action.getBaseList();
        } catch (IOException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    action.getTitle(), Messages.getErrorIcon());
            return;
        }

        try {
            additionalList = action.getAdditionalList();
        } catch (IOException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    action.getTitle(), Messages.getErrorIcon());
            return;
        }

        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeOfUnisetObjects.getModel().getRoot();
        root.removeAllChildren();

        DefaultMutableTreeNode baseNode = new DefaultMutableTreeNode(BASE_ALGORITHM);
        for(ResourceObject resourceObject : baseList)
            baseNode.add(new DefaultMutableTreeNode(resourceObject.getName()));
        root.add(baseNode);

        DefaultMutableTreeNode projectNode = new DefaultMutableTreeNode(PROJECT_ALGORITHM);
        if(projectList != null)
            for(Path projectObject : projectList)
                projectNode.add(new DefaultMutableTreeNode(projectObject.getFileName()));
        root.add(projectNode);

        if(additionalList != null) {
            for(Map.Entry<String, List<Path>> entry : additionalList.entrySet())
            {
                String nameProject = entry.getKey();
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(nameProject);

                List<Path> unisetObjects= entry.getValue();
                for (Path path : unisetObjects) {
                    node.add(new DefaultMutableTreeNode(path.getFileName().toString()));
                }

                root.add(node);
            }
        }

        treeOfUnisetObjects.setModel(new TreeModel(root));
    }

    private void updateEnabledAutogenerationSensorsCheckBox()
    {
        boolean srcXmlFound = false;
        if(isBase())
        {
            srcXmlFound = true;
        }
        else if(isProject())
        {
            String srcName = getSelectedName().toLowerCase() + ".src.xml";
            File[] files = getSelectedProjectObject ().toFile().listFiles();
            if(files != null && files.length > 0) {
                for (File file : files) {
                    if (file.getName().equals(srcName)) {
                        srcXmlFound = true;
                        break;
                    }
                }
            }
        }
        else if(isAdditional())
        {
            String srcName = getSelectedName().toLowerCase() + ".src.xml";
            File[] files = getSelectedAdditionalObject ().toFile().listFiles();
            if(files != null && files.length > 0) {
                for (File file : files) {
                    if (file.getName().equals(srcName)) {
                        srcXmlFound = true;
                        break;
                    }
                }
            }
        }

        if(getObjects().size() > 0)
            autogenerateSensorsCheckBox.setEnabled(srcXmlFound);
        else
            autogenerateSensorsCheckBox.setEnabled(false);
    }

    private void updateEnabled()
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeOfUnisetObjects.getLastSelectedPathComponent();
        if(node != null && node.isLeaf() && !node.getParent().toString().equals("JTree"))
        {
            updateEnablePanel(configPane, true);
            addButtonAllower.updateAllow();
            okButtonAllower.updateAllow();

            if(isProject())
            {
                cMakeCheckBox.setEnabled(false);
                makeCheckBox.setEnabled(false);
            }
            updateEnabledAutogenerationSensorsCheckBox();
            //this.setOKActionEnabled(true);
        }
        else
        {
            updateEnablePanel(configPane, false);
            this.setOKActionEnabled(false);
        }
    }
    private void updateComboBox(Project project, Configure configure, AddUnisetObjectAction action)
    {
        comboBoxSection.removeAllItems();
        try {
            comboBoxSection.addItem(NONE);
            for(String section : configure.getSectionList())
            {
                comboBoxSection.addItem(section);
            }

        } catch (JDOMException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    action.getTitle(), Messages.getErrorIcon());
        }
    }

    private void updateEnablePanel(JPanel panel, boolean state)
    {
        for (Component component : panel.getComponents()) {

            if(component instanceof JPanel)
                updateEnablePanel((JPanel) component, state);

            component.setEnabled(state);
        }
    }

    private boolean hasObjectInTable(String name)
    {
        for(int i = 0; i < tableModel.getRowCount(); i++)
        {
            String tableName = (String) tableModel.getValueAt(i, 1);
            if(tableName.equals(name))
                return true;
        }

        return false;
    }

    private long nextId(long currentId)
    {
        if(tableModel.getRowCount() == 0)
            return currentId;

        long maxId = currentId;
        for(int i = 0; i < tableModel.getRowCount(); i++) {
            long tableId = Long.parseLong((String) tableModel.getValueAt(i, 0));
            if(maxId < tableId)
                maxId = tableId;
        }

        return ++maxId;
    }

    @Override
    protected void doOKAction() {
        if(!isProject()) {
            String path = action.getVirtualDir(project).getPath() + "/" + getSelectedName();
            if (FileUtils.isExists(path)) {
                Messages.showMessageDialog(project,
                        "Directory of "
                                + action.getVirtualDir(project).getPath()
                                + "/"
                                + getSelectedName()
                                + " already exists in project",
                        action.getTitle(), Messages.getErrorIcon());
                return;
            }
        }
        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }
}
