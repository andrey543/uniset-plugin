package com.uniset.plugin.gui.modules;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.additional.SelectSensorDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ApplySelectedSensor implements ActionListener {

    private Project project;
    private Configure configure;
    private JTextField textField;

    public ApplySelectedSensor(final Project project, final Configure configure, JTextField textField)
    {
        this.project = project;
        this.configure = configure;
        this.textField = textField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SelectSensorDialog dialog = new SelectSensorDialog(project, configure);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            textField.setText(dialog.getSelectedSensor().getName());
        }
    }
}
