package com.uniset.plugin.gui.modules;

public interface ChangeListener {
    void change();
}
