package com.uniset.plugin.gui.modules;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.regex.Pattern;

public class FocusManagerTextField implements FocusListener {
    private String defaultValue;
    private String regex;
    private JTextField textField;
    private boolean isOn = true;
    private boolean isCorrect;

    public FocusManagerTextField(JTextField textField, String defaultValue, String regex)
    {
        this.textField = textField;
        this.defaultValue = defaultValue;
        this.regex = regex;

        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                isCorrect = Pattern.matches(regex, textField.getText()) && !textField.getText().isEmpty();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                isCorrect = Pattern.matches(regex, textField.getText()) && !textField.getText().isEmpty();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                isCorrect = Pattern.matches(regex, textField.getText()) && !textField.getText().isEmpty();
            }
        });
        if(!isCorrect)
            textField.setText(defaultValue);
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
        textField.setText(defaultValue);
    }

    public void off()
    {
        this.isOn = false;
    }

    public void on()
    {
        this.isOn = true;
    }

    @Override
    public void focusGained(FocusEvent e) {
        if(!isOn)
            return;

        if(textField.getText().equals(defaultValue))
            textField.setText("");
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    @Override
    public void focusLost(FocusEvent e) {
        if(!isOn)
            return;

        if(!isCorrect)
            textField.setText(defaultValue);
    }
}
