package com.uniset.plugin.gui.modules;

import com.uniset.plugin.build.objects.Sensor;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class SensorTableModel extends AbstractTableModel{

    private final List<Sensor> sensors = new ArrayList<>();
    private final String[] sensorsHeaders = {"id", "name", "textname","type"};

    @Override
    public int getRowCount() {
        return sensors.size();
    }

    @Override
    public int getColumnCount() {
        return sensorsHeaders.length;
    }

    @Override
    public String getColumnName(int column) {
        return sensorsHeaders[column];
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Sensor sensor = sensors.get(rowIndex);
        Object value = "";
        if(sensor != null) {
            switch (columnIndex) {
                case 0:
                    value = sensor.getId();
                    break;
                case 1:
                    value = sensor.getName();
                    break;
                case 2:
                    value = sensor.getTextname();
                    break;
                case 3:
                    value = sensor.getIotype();
                    break;
            }
        }

        return value;
    }
    public void addSensor(final Sensor sensor)
    {
        sensors.add(sensor);
        fireTableDataChanged();
    }

    public void removeSensor(final Sensor sensor)
    {
        sensors.remove(sensor);
        fireTableDataChanged();
    }

    public void addAllSensors(List<Sensor> list)
    {
        sensors.addAll(list);
    }

    public void clear()
    {
        sensors.clear();
        fireTableDataChanged();
    }

    public Sensor getSensor(int row)
    {
        return sensors.get(row);
    }

    public Sensor[] getSensors(int[] rows)
    {
        Sensor[] sensors = new Sensor[rows.length];
        for(int i = 0; i < rows.length; ++i)
            sensors[i] = getSensor(rows[i]);

        return sensors;
    }

    public List<Sensor> getSensors()
    {
        return sensors;
    }
}
