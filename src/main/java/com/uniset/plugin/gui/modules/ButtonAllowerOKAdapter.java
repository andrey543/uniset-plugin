package com.uniset.plugin.gui.modules;

import com.intellij.openapi.ui.DialogWrapper;

import javax.swing.*;

public class ButtonAllowerOKAdapter extends JButton {

    private DialogWrapper dialog;

    public ButtonAllowerOKAdapter(DialogWrapper dialog)
    {
        this.dialog = dialog;
    }

    @Override
    public void setEnabled(boolean on)
    {
        dialog.setOKActionEnabled(on);
    }
}
