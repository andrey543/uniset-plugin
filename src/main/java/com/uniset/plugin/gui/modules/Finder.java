package com.uniset.plugin.gui.modules;

import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.environment.SensorManager;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Finder {


    public static List<Sensor> filterSensors(List<Sensor> sensors, String text)
    {
        List<Sensor> list = new ArrayList<>();
        for(Sensor sensor : sensors)
        {
            String filter = "^" + text.replaceAll("\\W", "") + ".*";
            if(Pattern.matches(filter.toLowerCase(), sensor.getName().toLowerCase()))
            {
                list.add(sensor);
            }
        }

        return list;
    }

    public static List<SensorManager.SyncSensor> filterSyncSensors(List<SensorManager.SyncSensor> syncSensors, String text)
    {
        List<SensorManager.SyncSensor> list = new ArrayList<>();
        for(SensorManager.SyncSensor syncSensor : syncSensors)
        {
            String filter = "^" + text.replaceAll("\\W", "") + ".*";
            if(Pattern.matches(filter.toLowerCase(), syncSensor.getSensor().getName().toLowerCase()))
            {
                list.add(syncSensor);
            }
        }

        return list;
    }
}
