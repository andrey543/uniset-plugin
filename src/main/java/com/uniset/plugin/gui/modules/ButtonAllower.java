package com.uniset.plugin.gui.modules;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ButtonAllower implements DocumentListener, ActionListener{
    private JTextField[] textFields;
    private JButton button;
    private List<AdditionalCondition> additionalConditions = new ArrayList<>();
    private List<AbstractButton> additionalEventButtons = new ArrayList<>();
    private boolean isWork = true;

    public ButtonAllower(JButton button, JTextField[] textFields, AdditionalCondition[] conditions, AbstractButton[] eventButtons)
    {
        this(button, textFields);
        additionalConditions.addAll(Arrays.asList(conditions));
        additionalEventButtons.addAll(Arrays.asList(eventButtons));
        for(AbstractButton actionButton : additionalEventButtons)
            actionButton.addActionListener(this);

        updateAllow();
    }

    public ButtonAllower(JButton button, JTextField[] textFields, AdditionalCondition[] conditions) {
        this(button, textFields);
        additionalConditions.addAll(Arrays.asList(conditions));

        updateAllow();
    }

    public ButtonAllower(JButton button, JTextField[] textFields)
    {
        this.textFields = textFields;
        this.button = button;

        for(JTextField textField : textFields)
        {
            textField.getDocument().addDocumentListener(this);
        }
        updateAllow();
    }

    public ButtonAllower(JButton button)
    {
        this.button = button;
        updateAllow();
    }

    public boolean isWork() {
        return isWork;
    }

    public void setWork(boolean work) {
        isWork = work;
        updateAllow();
    }

    public void addAdditionalCondition(AdditionalCondition condition)
    {
        additionalConditions.add(condition);
        updateAllow();
    }

    public void addEventButton(AbstractButton eventButton)
    {
        additionalEventButtons.add(eventButton);
        eventButton.addActionListener(this);
        updateAllow();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateAllow();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateAllow();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updateAllow();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        updateAllow();
    }

    public boolean isAllow()
    {
        if (textFields != null) {
            for (JTextField textField : textFields)
                if (textField.getText().isEmpty()) {
                    return false;
                }
        }

        for (AdditionalCondition condition : additionalConditions)
            if (!condition.check()) {
                return false;
            }

        return true;
    }

    public void updateAllow()
    {
        if(isWork) {
            button.setEnabled(isAllow());
        }
    }

    public interface AdditionalCondition
    {
        boolean check();
    }
}
