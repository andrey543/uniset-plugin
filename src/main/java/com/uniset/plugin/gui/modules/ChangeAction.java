package com.uniset.plugin.gui.modules;

import java.util.ArrayList;
import java.util.List;

public abstract class ChangeAction {
    private List<ChangeListener> listeners = new ArrayList<>();

    public void addChangeListener(ChangeListener listener)
    {
        listeners.add(listener);
    }
    protected void fireChangeEvent()
    {
        for(ChangeListener listener : listeners)
            listener.change();
    }

}
