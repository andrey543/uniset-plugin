package com.uniset.plugin.gui.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.SensorGenerator;
import com.uniset.plugin.build.objects.modbus.Device;
import com.uniset.plugin.build.objects.modbus.MBMObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.gui.modules.*;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.eclipse.lsp4j.jsonrpc.validation.NonNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

public class AddMBMDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JPanel nodeConfig;
    private JComboBox<Node> comboBoxChooseNode;
    private JLabel labelChooseNode;
    private JPanel devicesConfig;
    private JPanel netConfig;
    private JTextField deviceAddrTextField;
    private JTextField deviceTimeoutTextField;
    private JLabel labelDeviceAddr;
    private JLabel labelDeviceForce;
    private JLabel labelDeviceRespSensor;
    private JLabel labelDeviceTimeout;
    private JRadioButton radioButtonDeviceForceYes;
    private JRadioButton radioButtonDeviceForceNo;
    private JButton deviceChooseSensorButton;
    private JTextField deviceRespSensTextField;
    private JButton buttonAddDevice;
    private JLabel labelDeviceInvert;
    private JRadioButton radioButtonDeviceInvertYes;
    private JRadioButton radioButtonDeviceInvertNo;
    private JButton buttonDeleteDevice;
    private JTextField nameTextField;
    private JLabel labelNameMBM;
    private JTextField netGatewayIPAddrTextField;
    private JLabel labelGatewayAddr;
    private JLabel labelGatewayPort;
    private JTextField netGatewayPortTextField;
    private JTextField netPolltimeTextField;
    private JLabel labelPollTime;
    private JLabel labelExchangeSens;
    private JButton netExchChooseSensorButton;
    private JTextField netExchSensTextField;
    private JLabel labelRespSensor;
    private JTextField netRespSensTextField;
    private JButton netRespChooseSensorButton;
    private JLabel labelDeviceList;
    private JPanel forceYesNoPanel;
    private JPanel invertYesNoPanel;
    private JPanel addDelPanel;
    private JTable table1;
    private JTextField netFilterFieldTextField;
    private JTextField netFilterValueTextField;
    private JTextField netPrefixTextField;
    private JRadioButton radioButtonPersistConnectOn;
    private JRadioButton radioButtonPersistConnectOff;
    private JLabel labelFilterField;
    private JLabel labelFilterValue;
    private JLabel labelPrefix;
    private JLabel labelPersistentConnection;
    private JPanel persistConnectPanel;
    private JCheckBox autogenerateSensorsCheckBox;

    private List<Node> nodes = new ArrayList<>();
    private ButtonGroup persistConnectGroup;
    private ButtonAllower allowButtonAddDevice;
    private ButtonAllower allowButtonOk;
    private FocusManagerTextField deviceAddrFocusManager;
    private DefaultTableModel tableModel = new DefaultTableModel();

    private int numDevAddr = 1;

    private static final String REGEX_ONLY_NUMBERS      = "[0-9]*";
    private static final String REGEX_DEVICE_ADDR       = "0x[0-9]*";
    private static final String REGEX_ONLY_NUM_AND_CHAR = "\\w*";
    private static final String REGEX_ALL               = ".*";
    private static final String YES_OPTION              = "on";
    private static final String NO_OPTION               = "off";
    private static final String TITLE                   = "Adding a MBM";
    private static final Node NO_EXIST_NODE             = new Node("", "", "", "");

    private Project project;
    private Configure configure;
    private Smemory smemory;
    private SaveAdapter saveAdapter;

    public AddMBMDialog(@NonNull Project project, @NonNull Configure configure, @NonNull Smemory smemory)
    {
        // Стандартная инициализация
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.configure = configure;
        this.smemory = smemory;

        // Инициализация модели таблицы устройств
        String[] headers = {"Device", "Address", "Respond SMapSensor", "Timeout", "Force", "Invert"};
        tableModel.setColumnIdentifiers(headers);
        table1.setDefaultEditor(Object.class, null);
        table1.setModel(tableModel);

        // Выбор Invert и Force (да/нет)
        radioButtonDeviceForceYes.setSelected(true);
        ButtonGroup forceGroup = new ButtonGroup();
        forceGroup.add(radioButtonDeviceForceYes);
        forceGroup.add(radioButtonDeviceForceNo);

        radioButtonDeviceInvertYes.setSelected(true);
        ButtonGroup invertGroup = new ButtonGroup();
        invertGroup.add(radioButtonDeviceInvertYes);
        invertGroup.add(radioButtonDeviceInvertNo);

        // Выбор Persistent Connection
        radioButtonPersistConnectOn.setSelected(true);
        persistConnectGroup = new ButtonGroup();
        persistConnectGroup.add(radioButtonPersistConnectOn);
        persistConnectGroup.add(radioButtonPersistConnectOff);

        // Обновление доступности кнопки add
        JTextField[] deviceTextFields = {deviceRespSensTextField, deviceTimeoutTextField, deviceAddrTextField}; // От каких textField-ов зависит доступность кнопки
        allowButtonAddDevice = new ButtonAllower(buttonAddDevice, deviceTextFields);

        // Обновление доступности кнопки ok
        JTextField[]  netTextFields = {
                netFilterFieldTextField, netFilterValueTextField, netPrefixTextField,
                netPolltimeTextField, netExchSensTextField, netGatewayIPAddrTextField,
                netGatewayPortTextField, nameTextField, netRespSensTextField};
        JTextField[] allTextFields = Stream.concat(Arrays.stream(deviceTextFields), Arrays.stream(netTextFields)).toArray(JTextField[]::new);
        ButtonAllower.AdditionalCondition[] additionalConditions = {() -> !(tableModel.getRowCount() == 0)};
        JButton[] additionalEventButtons  = {buttonDeleteDevice, buttonAddDevice};
        allowButtonOk = new ButtonAllower(new ButtonAllowerOKAdapter(this), allTextFields, additionalConditions, additionalEventButtons);

        // Выбор датчиков
        deviceChooseSensorButton.addActionListener(new ApplySelectedSensor(project, configure, deviceRespSensTextField));
        netExchChooseSensorButton.addActionListener(new ApplySelectedSensor(project, configure, netExchSensTextField));
        netRespChooseSensorButton.addActionListener(new ApplySelectedSensor(project, configure, netRespSensTextField));

        // Заполнение полей
        deviceAddrFocusManager = new FocusManagerTextField(deviceAddrTextField, formatToDevAddr(numDevAddr), REGEX_DEVICE_ADDR);
        deviceAddrTextField.addFocusListener(deviceAddrFocusManager);
        deviceTimeoutTextField.addFocusListener(new FocusManagerTextField(deviceTimeoutTextField, Settings.MBM_TIMEOUT, REGEX_ONLY_NUMBERS));
        netGatewayIPAddrTextField.addFocusListener(new FocusManagerTextField(netGatewayIPAddrTextField, Settings.MBM_GATEWAY_IP_ADDRESS, REGEX_ALL));
        netGatewayPortTextField.addFocusListener(new FocusManagerTextField(netGatewayPortTextField, Settings.MBM_GATEWAY_PORT, REGEX_ONLY_NUMBERS));
        netPolltimeTextField.addFocusListener(new FocusManagerTextField(netPolltimeTextField, Settings.MBM_POLLTIME, REGEX_ONLY_NUMBERS));
        netFilterFieldTextField.addFocusListener(new FocusManagerTextField(netFilterFieldTextField, Settings.MBM_FILTER_FIELD, REGEX_ALL));
        netPrefixTextField.addFocusListener(new FocusManagerTextField(netPrefixTextField, Settings.MBM_PREFIX, ".*_$"));

        // Интеграция поддержки автогенерации датчиков
        supportAutogenSensors();

        // Кнопка add
        buttonAddDevice.addActionListener(e -> {
            String[] row = {
                    Integer.toString(numDevAddr),
                    deviceAddrTextField.getText(),
                    deviceRespSensTextField.getText(),
                    deviceTimeoutTextField.getText(),
                    (radioButtonDeviceForceYes.isSelected() ? YES_OPTION : NO_OPTION),
                    (radioButtonDeviceInvertYes.isSelected() ? YES_OPTION : NO_OPTION)};
            tableModel.addRow(row);

            deviceAddrFocusManager.setDefaultValue(formatToDevAddr(++numDevAddr));
        });

        // Кнопка delete
        buttonDeleteDevice.addActionListener(e -> {
            if (tableModel.getRowCount() > 0) {
                tableModel.removeRow(tableModel.getRowCount() - 1);
                deviceAddrFocusManager.setDefaultValue(formatToDevAddr(--numDevAddr));
            }
        });

        // Инициализируем доступные узлы
        this.setAllowNodes(syncNodes(configure.getNodeList(), smemory.getStartScripts()));
    }

    public boolean isAutogerateSensors()
    {
        return autogenerateSensorsCheckBox.isSelected();
    }
    public SensorGenerator getSensorGenerator()
    {
        SensorGenerator sensorGenerator;
        if(isAutogerateSensors())
        {
            sensorGenerator = new SensorGenerator(configure);
            sensorGenerator
                    .registrySensor(getNetExchSens(),
                            getName() + ": Датчик обмена",
                    Settings.IOTYPE_DI);
            sensorGenerator
                    .registrySensor(getNetRespSensor(),
                            getName() + ": Датчик состояния связи",
                    Settings.IOTYPE_DI);
            int i = 1;

            for(Device device : getDevices())
            {
                String textname;
                if(device.getRespondSensor().contains("Not"))
                    textname = getName() + ": Нет связи по каналу " + i;
                else
                    textname = getName() + ": Состояние связи по каналу " + i;
                sensorGenerator.registrySensor(device.getRespondSensor(), textname, Settings.IOTYPE_DI);
                i++;
            }

            return sensorGenerator;
        }

        return null;
    }

    public void offFocusManagerTextField()
    {
        JTextField[] textFields = {
                deviceAddrTextField,
                deviceTimeoutTextField,
                netGatewayIPAddrTextField,
                netGatewayPortTextField,
                netPolltimeTextField,
                netFilterFieldTextField,
                netPrefixTextField
        };
        for(JTextField textField : textFields)
        {
            for(FocusListener listener : textField.getFocusListeners())
            {
                if(listener instanceof FocusManagerTextField)
                {
                    ((FocusManagerTextField) listener).off();
                }
            }
        }
    }

    public void setObject(MBMObject object) {
        saveAdapter = new SaveAdapter(this, object);
        comboBoxChooseNode.setEnabled(false);
        autogenerateSensorsCheckBox.setEnabled(false);
    }

    public void addChangeListener(ChangeListener listener)
    {
        saveAdapter.addChangeListener(listener);
    }

    private void setValueTextField(JTextField textField, String value)
    {
        if(value == null || value.isEmpty())
            textField.setText("");
        else
            textField.setText(value);
    }

    private void selectNodeOf(MBMObject object) {
        Node node = object.getNode();
        if(node != null)
            comboBoxChooseNode.setSelectedItem(node);
        else
            comboBoxChooseNode.setSelectedItem(NO_EXIST_NODE);
    }

    public List<Device> getDevices()
    {
        List<Device> devices = new ArrayList<>();
        for(int i = 0; i < tableModel.getRowCount(); i++) {
            String address = (String) tableModel.getValueAt(i, 1);
            String respondSensor = (String) tableModel.getValueAt(i, 2);
            String timeout = (String) tableModel.getValueAt(i, 3);
            String force = (String) tableModel.getValueAt(i, 4);
            String invert = (String) tableModel.getValueAt(i, 5);

            Device device = new Device(address, respondSensor, force.equals(YES_OPTION), invert.equals(YES_OPTION), Long.parseLong(timeout));
            devices.add(device);
        }

        return devices;
    }
    public boolean isPersitentConnection()
    {
        return radioButtonPersistConnectOn.isSelected();
    }

    public String getDeviceAddr() {
        return deviceAddrTextField.getText();
    }

    public String getDeviceTimeout() {
        return deviceTimeoutTextField.getText();
    }

    public String getDeviceRespSens() {
        return deviceRespSensTextField.getText();
    }

    public String getName() {
        return nameTextField.getText();
    }

    public String getNetGatewayIPAddr() {
        return netGatewayIPAddrTextField.getText();
    }

    public String getNetGatewayPort() {
        return netGatewayPortTextField.getText();
    }

    public String getNetPolltime() {
        return netPolltimeTextField.getText();
    }

    public String getNetExchSens() {
        return netExchSensTextField.getText();
    }

    public String getNetRespSensor() {
        return netRespSensTextField.getText();
    }

    public String getNetFilterField() {
        return netFilterFieldTextField.getText();
    }

    public String getNetFilterValue() {
        return netFilterValueTextField.getText();
    }

    public String getNetPrefix() {
        return netPrefixTextField.getText();
    }

    public Node getSelectedNode()
    {
        return (Node) comboBoxChooseNode.getSelectedItem();
    }

    public void setAllowNodes(List<Node> nodes)
    {
        this.nodes = new ArrayList<>(nodes);
        this.nodes.add(0, NO_EXIST_NODE);
        updateNodesComboBox();
        updateEnablePanel(nodeConfig, true);
    }

    private List<Node> syncNodes(List<Node> nodes, List<NodeScript> scripts)
    {
        List<Node> list = new ArrayList<>();
        for(Node node : nodes)
        {
            for(NodeScript script : scripts)
            {
                if(node.getName().equals(script.getNodeName())) {
                    list.add(node);
                    break;
                }
            }
        }

        return list;
    }

    private void updateNodesComboBox()
    {
        comboBoxChooseNode.removeAllItems();
        for(Node node : nodes)
            comboBoxChooseNode.addItem(node);
    }

    private void updateEnablePanel(JPanel panel, boolean state)
    {
        for (Component component : panel.getComponents()) {

            if(component instanceof JPanel)
                updateEnablePanel((JPanel) component, state);

            component.setEnabled(state);
        }

        if(state)
            allowButtonAddDevice.updateAllow();
    }

    private void supportAutogenSensors()
    {
        // Автоген датчиков
        autogenerateSensorsCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                {
                    autogenerateSensors();
                    setEnableSelectButtons(false);
                }
                else
                {
                    setEnableSelectButtons(true);
                    clearTable(tableModel);
                    netExchSensTextField.setText("");
                    netRespSensTextField.setText("");
                    deviceRespSensTextField.setText("");
                    numDevAddr = 1;
                    deviceAddrFocusManager.setDefaultValue(formatToDevAddr(numDevAddr));
                }
            }
        });
        nameTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        radioButtonDeviceInvertYes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        radioButtonDeviceInvertNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });

        buttonAddDevice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        buttonDeleteDevice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
    }

    private void autogenerateSensors()
    {
        String prefix = "MBM_";
        // Устанавливаем в textfield-ы
        String exchangeSensor = prefix + getName() + "_Exchange_S";
        String respondSensor = prefix + getName() + "_Respond_S";
        String respondSensorDev;
        if(radioButtonDeviceInvertYes.isSelected())
            respondSensorDev = prefix + getName() + "_Not_Respond" + numDevAddr + "_S";
        else
            respondSensorDev = prefix + getName() + "_Respond_S" + numDevAddr + "_S";

        autogenerateChangeName(tableModel, prefix, 2);
        netExchSensTextField.setText(exchangeSensor);
        netRespSensTextField.setText(respondSensor);
        deviceRespSensTextField.setText(respondSensorDev);
    }

    private void autogenerateChangeName(DefaultTableModel tableModel, String prefix, int posRespondSensor)
    {
        // Изменяем в таблице
        String[] rows = new String[tableModel.getRowCount()];
        for(int i = 0; i < tableModel.getRowCount(); i++) {
            rows[i] = "";
            for(int j = 0; j < tableModel.getColumnCount(); j++)
                rows[i] += tableModel.getValueAt(i, j) + ":";
        }
        clearTable(tableModel);

        for(int i = 0; i < rows.length; i++)
        {
            String[] row = rows[i].split(":");
            row[posRespondSensor] =
                    row[posRespondSensor].replaceAll(prefix + "[^_]*", prefix + getName());
            tableModel.addRow(row);
        }
    }
    private void setEnableSelectButtons(boolean on)
    {
        netExchChooseSensorButton.setEnabled(on);
        netRespChooseSensorButton.setEnabled(on);
        deviceChooseSensorButton.setEnabled(on);
    }

    private void clearTable(DefaultTableModel model)
    {
        int count = model.getRowCount();
        for(int i = 0; i < count; i++)
        {
            model.removeRow(0);
        }

    }

    private String formatToDevAddr(int numDevAddr)
    {
        if(numDevAddr < 10)
            return "0x0" + numDevAddr;
        return "0x" + numDevAddr;
    }

    @Override
    protected void doOKAction() {
        NodeScript script = smemory.getStartScript(getSelectedNode().getName());
        if(script == null)
        {
            Messages.showMessageDialog(project,
                    "The " + getSelectedNode().getName() + " script is not found",
                    TITLE, Messages.getErrorIcon());
            return;
        }
        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }
    public JComponent getMainPanel()
    {
        return mainPanel;
    }

    public static class SaveAdapter extends ChangeAction
    {
        private MBMObject object;
        private AddMBMDialog dialog;

        public SaveAdapter(AddMBMDialog dialog, MBMObject object) {

            this.object = object;
            this.dialog = dialog;
            init();
        }

        private void addSaveChangeEvent(JTextField textField)
        {
            textField.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEvent(JRadioButton radioButton)
        {
            radioButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEvent(JButton button)
        {
            // Меняем порядок слушателей
            // т.к. первым вызывается последний добавленный,
            // а первым нам необходимо вызывать слушателя который обновляет список devices
            ActionListener[] listeners = Arrays.copyOf(button.getActionListeners(), button.getActionListeners().length);
            for(ActionListener listener : button.getActionListeners())
            {
                button.removeActionListener(listener);
            }

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });

            for(ActionListener listener : listeners)
                button.addActionListener(listener);
        }

        private void addSaveChangeEventNode(JComboBox<Node> comboBox)
        {
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }


        private void init()
        {
            dialog.selectNodeOf(object);
            setValueTextField(dialog.nameTextField, this.object.getName());
            setValueTextField(dialog.netFilterFieldTextField, this.object.getFilterField());
            setValueTextField(dialog.netFilterValueTextField, this.object.getFilterValue());
            setValueTextField(dialog.netPrefixTextField, this.object.getPrefix());
            setValueTextField(dialog.netGatewayIPAddrTextField, this.object.getGatewayAddress());
            setValueTextField(dialog.netGatewayPortTextField, this.object.getGatewayPort());
            setValueTextField(dialog.netExchSensTextField, this.object.getExchangeModeIdSensor());
            setValueTextField(dialog.netRespSensTextField, this.object.getRespondSensor());
            setValueTextField(dialog.netPolltimeTextField, String.valueOf(this.object.getPolltime()));

            dialog.persistConnectGroup.clearSelection();
            dialog.radioButtonPersistConnectOn.setSelected(this.object.isPersistentConnection());
            dialog.radioButtonPersistConnectOff.setSelected(!this.object.isPersistentConnection());

            dialog.clearTable(dialog.tableModel);
            dialog.numDevAddr = 1;
            for(Device device : object.getDeviceList())
            {
                String[] row = {
                        Integer.toString(dialog.numDevAddr++),
                        device.getAddress(),
                        device.getRespondSensor(),
                        String.valueOf(device.getTimeout()),
                        (device.isForce() ? YES_OPTION : NO_OPTION),
                        (device.isInvert() ? YES_OPTION : NO_OPTION)};
                dialog.tableModel.addRow(row);
            }
            dialog.deviceAddrFocusManager.setDefaultValue(dialog.formatToDevAddr(dialog.numDevAddr));

            addSaveChangeEvent(dialog.nameTextField);
            addSaveChangeEvent(dialog.netFilterFieldTextField);
            addSaveChangeEvent(dialog.netFilterValueTextField);
            addSaveChangeEvent(dialog.netPrefixTextField);
            addSaveChangeEvent(dialog.netGatewayIPAddrTextField);
            addSaveChangeEvent(dialog.netGatewayPortTextField);
            addSaveChangeEvent(dialog.netExchSensTextField);
            addSaveChangeEvent(dialog.netRespSensTextField);
            addSaveChangeEvent(dialog.netPolltimeTextField);
            addSaveChangeEvent(dialog.radioButtonPersistConnectOn);
            addSaveChangeEvent(dialog.radioButtonPersistConnectOff);
            addSaveChangeEvent(dialog.buttonAddDevice);
            addSaveChangeEvent(dialog.buttonDeleteDevice);
            addSaveChangeEventNode(dialog.comboBoxChooseNode);

        }

        private void applyChanged()
        {
            if(!dialog.getName().equals(this.object.getName()))
                this.object.setName(dialog.getName());
            if(!dialog.getNetFilterField().equals(this.object.getFilterField()))
                this.object.setFilterField(dialog.getNetFilterField());
            if(!dialog.getNetFilterValue().equals(this.object.getFilterValue()))
                this.object.setFilterValue(dialog.getNetFilterValue());
            if(!dialog.getNetPrefix().equals(this.object.getPrefix()))
                this.object.setPrefix(dialog.getNetPrefix());
            if(!dialog.getNetGatewayIPAddr().equals(this.object.getGatewayAddress()))
                this.object.setGatewayAddress(dialog.getNetGatewayIPAddr());
            if(!dialog.getNetGatewayPort().equals(this.object.getGatewayPort()))
                this.object.setGatewayPort(dialog.getNetGatewayPort());
            if(!dialog.getNetExchSens().equals(this.object.getExchangeModeIdSensor()))
                this.object.setExchangeModeIdSensor(dialog.getNetExchSens());
            if(!dialog.getNetRespSensor().equals(this.object.getRespondSensor()))
                this.object.setRespondSensor(dialog.getNetRespSensor());
            if(!dialog.getNetPolltime().equals(String.valueOf(this.object.getPolltime())))
                this.object.setPolltime(Integer.parseInt(dialog.getNetPolltime()));

            this.object.setPersistentConnection(dialog.isPersitentConnection());

            Node node = dialog.getSelectedNode();
            if(node == NO_EXIST_NODE)
                this.object.setNode(null);
            else
                this.object.setNode(dialog.getSelectedNode());

            this.object.setDeviceList(dialog.getDevices());

            fireChangeEvent();
        }

        private void setValueTextField(JTextField textField, String value) {
            if(value == null || value.isEmpty())
                textField.setText("");
            else
                textField.setText(value);
        }
    }

}
