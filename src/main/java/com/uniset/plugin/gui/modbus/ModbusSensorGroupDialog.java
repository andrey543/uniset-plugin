package com.uniset.plugin.gui.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.gui.modules.ButtonAllowerOKAdapter;
import com.uniset.plugin.gui.modules.FocusManagerTextField;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;

public class ModbusSensorGroupDialog extends DialogWrapper {
    private JTable selectedSensorsTable;
    private JTextField startMbregTextField;
    private JTextField stepMbregTextField;
    private JTextField startNbitTextField;
    private JTextField stepNbitTextField;
    private JPanel mainPanel;
    private JCheckBox nbitCheckBox;
    private JComboBox<String> mbfuncComboBox;
    private JComboBox<String> mbaddrComboBox;

    private static final String TITLE = "Add Modbus Sensor Group";
    private static final String REGEX_ONLY_NUMBERS = "[0-9]*";

    private Project project;
    private Sensor[] sensors;
    private String maxReg;
    private ModbusSensorGroupModel modbusSensorGroupModel = new ModbusSensorGroupModel();
    private ButtonAllower okButtonAllower1;
    private ButtonAllower okButtonAllower2;

    public ModbusSensorGroupDialog(@Nullable Project project, Sensor[] sensors, String maxReg, JComboBox<String> mbaddrComboBox) {
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.sensors = sensors;
        this.maxReg = maxReg;

        /** Таблица датчиков */
        selectedSensorsTable.setDefaultEditor(Object.class, null);
        selectedSensorsTable.setModel(modbusSensorGroupModel);
        selectedSensorsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setJTableColumnsWidth(selectedSensorsTable, 15, 25, 30, 10, 5, 5, 5, 5);

        this.mbaddrComboBox.setEnabled(mbaddrComboBox.isEnabled());
        for(int i = 0; i < mbaddrComboBox.getItemCount(); ++i)
            this.mbaddrComboBox.addItem(mbaddrComboBox.getItemAt(i));
        this.mbaddrComboBox.setSelectedIndex(mbaddrComboBox.getSelectedIndex());

        for(String mbfunc : Settings.mbfuncs)
            mbfuncComboBox.addItem(mbfunc);

        startMbregTextField.addFocusListener(new FocusManagerTextField(startMbregTextField, maxReg, REGEX_ONLY_NUMBERS));
        stepMbregTextField.addFocusListener(new FocusManagerTextField(stepMbregTextField, "1", REGEX_ONLY_NUMBERS));

        startNbitTextField.addFocusListener(new FocusManagerTextField(startNbitTextField, "0", REGEX_ONLY_NUMBERS));
        stepNbitTextField.addFocusListener(new FocusManagerTextField(stepNbitTextField, "1", REGEX_ONLY_NUMBERS));

        nbitCheckBox.addActionListener(e -> updateNbitTextFields());

        JTextField[] allowedTextFieldsWithoutNbits = {startMbregTextField, stepMbregTextField};
        JTextField[] allowedAllTextFiedls = {startMbregTextField, stepMbregTextField, startNbitTextField, stepNbitTextField};
        okButtonAllower1 = new ButtonAllower(new ButtonAllowerOKAdapter(this), allowedTextFieldsWithoutNbits);
        okButtonAllower2 = new ButtonAllower(new ButtonAllowerOKAdapter(this), allowedAllTextFiedls);

        /** Update */
        textFieldsWhoFireEventOfUpdate(startMbregTextField, stepMbregTextField, startNbitTextField, stepNbitTextField);
        comboBoxesWhoFireEventOfUpdate(mbfuncComboBox, this.mbaddrComboBox);
        checkBoxesWhoFireEventOfUpdate(nbitCheckBox);
        updateSensorTable();

    }

    public List<ModbusGroupSensor> getModbusGroupSensors()
    {
        return modbusSensorGroupModel.getModbusSensors();
    }

    private void textFieldsWhoFireEventOfUpdate(JTextField ... textFields)
    {
        for(JTextField textField : textFields)
        {
            textField.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    updateSensorTable();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    updateSensorTable();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    updateSensorTable();
                }
            });
        }
    }

    private void comboBoxesWhoFireEventOfUpdate(JComboBox ... comboBoxes)
    {
        for(JComboBox comboBox : comboBoxes)
            comboBox.addActionListener(e -> updateSensorTable());
    }

    private void checkBoxesWhoFireEventOfUpdate(JCheckBox ... checkBoxes)
    {
        for(JCheckBox checkBox : checkBoxes)
            checkBox.addActionListener(e -> updateSensorTable());
    }

    private void updateSensorTable()
    {

        modbusSensorGroupModel.clear();
        try {
            long mbreg = Long.parseLong(startMbregTextField.getText());
            long mbregstep = Long.parseLong(stepMbregTextField.getText());
            long mbnbit = Long.parseLong(startNbitTextField.getText());
            long mbnbitstep = Long.parseLong(stepNbitTextField.getText());

            for(Sensor sensor : sensors)
            {
                ModbusGroupSensor modbusGroupSensor = new ModbusGroupSensor(sensor);
                modbusGroupSensor.setMbfunc((String) mbfuncComboBox.getSelectedItem());
                modbusGroupSensor.setMbaddr((String) mbaddrComboBox.getSelectedItem());

                if(nbitCheckBox.isSelected()) {
                    if(mbnbit == 15) {
                        mbreg += mbregstep;
                        mbnbit = 0;
                    }

                    modbusGroupSensor.setMbreg(String.valueOf(mbreg));
                    modbusGroupSensor.setNbit(String.valueOf(mbnbit));
                    mbnbit += mbnbitstep;
                }
                else {
                    modbusGroupSensor.setMbreg(String.valueOf(mbreg));
                    mbreg += mbregstep;
                }
                modbusSensorGroupModel.addModbusSensor(modbusGroupSensor);
            }
        }
        catch (NumberFormatException e)
        {
            for(Sensor sensor : sensors) {
                ModbusGroupSensor modbusGroupSensor = new ModbusGroupSensor(sensor);
                modbusGroupSensor.setMbfunc((String) mbfuncComboBox.getSelectedItem());

                modbusSensorGroupModel.addModbusSensor(modbusGroupSensor);
            }
        }
    }
    private void updateNbitTextFields()
    {
        if(nbitCheckBox.isSelected())
        {
            startNbitTextField.setEnabled(true);
            stepNbitTextField.setEnabled(true);
            okButtonAllower1.setWork(false);
            okButtonAllower2.setWork(true);
        }
        else
        {
            startNbitTextField.setEnabled(false);
            stepNbitTextField.setEnabled(false);
            okButtonAllower1.setWork(true);
            okButtonAllower2.setWork(false);
        }
    }
    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    public static class ModbusGroupSensor
    {
        private Sensor sensor;
        private String id;
        private String name;
        private String textname;
        private String type;
        private String mbaddr = "";
        private String mbreg = "";
        private String mbfunc = "";
        private String nbit = "";

        private ModbusGroupSensor(Sensor sensor)
        {
            this.sensor = sensor;
            this.id = String.valueOf(sensor.getId());
            this.name = sensor.getName();
            this.textname = sensor.getTextname();
            this.type = sensor.getIotype();
        }

        public Sensor getSensor() {
            return sensor;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getTextname() {
            return textname;
        }

        public String getType() {
            return type;
        }

        public String getMbaddr() {
            return mbaddr;
        }

        public String getMbreg() {
            return mbreg;
        }

        public String getMbfunc() {
            return mbfunc;
        }

        public String getNbit() {
            return nbit;
        }

        private void setId(String id) {
            this.id = id;
        }

        private void setName(String name) {
            this.name = name;
        }

        private void setTextname(String textname) {
            this.textname = textname;
        }

        private void setType(String type) {
            this.type = type;
        }

        public void setMbaddr(String mbaddr) {
            this.mbaddr = mbaddr;
        }

        private void setMbreg(String mbreg) {
            this.mbreg = mbreg;
        }

        private void setMbfunc(String mbfunc) {
            this.mbfunc = mbfunc;
        }

        private void setNbit(String nbit) {
            this.nbit = nbit;
        }
    }

    private static class ModbusSensorGroupModel extends AbstractTableModel
    {
        private List<ModbusGroupSensor> modbusSensors = new ArrayList<>();
        String[] modbusSensorHeaders = {"id", "name", "textname","type", "mbaddr", "mbreg", "mbfunc", "nbit"};

        @Override
        public int getRowCount() {
            return modbusSensors.size();
        }

        @Override
        public int getColumnCount() {
            return modbusSensorHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return modbusSensorHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            ModbusGroupSensor sensor = modbusSensors.get(rowIndex);
            Object value = "";
            if(sensor != null) {
                switch (columnIndex) {
                    case 0:
                        value = sensor.getId();
                        break;
                    case 1:
                        value = sensor.getName();
                        break;
                    case 2:
                        value = sensor.getTextname();
                        break;
                    case 3:
                        value = sensor.getType();
                        break;
                    case 4:
                        value = sensor.getMbaddr();
                        break;
                    case 5:
                        value = sensor.getMbreg();
                        break;
                    case 6:
                        value = sensor.getMbfunc();
                        break;
                    case 7:
                        value = sensor.getNbit();
                        break;
                }
            }

            return value;
        }
        public void addModbusSensor(final ModbusGroupSensor sensor)
        {
            modbusSensors.add(sensor);
            fireTableDataChanged();
        }

        public void clear()
        {
            modbusSensors.clear();
            fireTableDataChanged();
        }

        public ModbusGroupSensor getModbusSensor(int row)
        {
            return modbusSensors.get(row);
        }

        public List<ModbusGroupSensor> getModbusSensors()
        {
            return modbusSensors;
        }

    }
}
