package com.uniset.plugin.gui.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.SensorGenerator;
import com.uniset.plugin.build.objects.modbus.Device;
import com.uniset.plugin.build.objects.modbus.Gate;
import com.uniset.plugin.build.objects.modbus.MBMMObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.gui.modules.*;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.eclipse.lsp4j.jsonrpc.validation.NonNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.List;
import java.util.*;
import java.util.stream.Stream;

public class AddMBMMDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JPanel nodeConfig;
    private JComboBox<Node> comboBoxChooseNode;
    private JLabel labelChooseNode;
    private JPanel devicesConfig;
    private JPanel netConfig;
    private JTextField deviceAddrTextField;
    private JTextField deviceTimeoutTextField;
    private JLabel labelDeviceAddr;
    private JLabel labelDeviceForce;
    private JLabel labelDeviceRespSensor;
    private JLabel labelDeviceTimeout;
    private JRadioButton radioButtonDeviceForceYes;
    private JRadioButton radioButtonDeviceForceNo;
    private JButton deviceChooseSensorButton;
    private JTextField deviceRespSensTextField;
    private JButton buttonAddDevice;
    private JLabel labelDeviceInvert;
    private JRadioButton radioButtonDeviceInvertYes;
    private JRadioButton radioButtonDeviceInvertNo;
    private JButton buttonDeleteDevice;
    private JTextField nameTextField;
    private JLabel labelNameMBM;
    private JTextField gateIPAddrTextField;
    private JLabel labelGatewayAddr;
    private JLabel labelGatewayPort;
    private JTextField gateRealPortTextField;
    private JTextField netPolltimeTextField;
    private JLabel labelPollTime;
    private JLabel labelExchangeSens;
    private JButton netExchChooseSensorButton;
    private JTextField netExchSensTextField;
    private JLabel labelDeviceList;
    private JPanel forceYesNoPanel;
    private JPanel invertYesNoPanel;
    private JPanel addDevDelPanel;
    private JTable devTable;
    private JTextField netFilterFieldTextField;
    private JTextField netFilterValueTextField;
    private JTextField netPrefixTextField;
    private JRadioButton radioButtonPersistConnectOn;
    private JRadioButton radioButtonPersistConnectOff;
    private JLabel labelFilterField;
    private JLabel labelFilterValue;
    private JLabel labelPrefix;
    private JLabel labelPersistentConnection;
    private JPanel persistConnectPanel;
    private JTable gateTable;
    private JButton buttonAddGate;
    private JButton buttonDeleteGate;
    private JTextField gateVstandPortTextField;
    private JTextField gateRespInitTimeoutTextField;
    private JTextField gateRecvTimeoutTextField;
    private JRadioButton radioButtonGateInvertOn;
    private JRadioButton radioButtonGateInvertOff;
    private JRadioButton radioButtonGateForceOn;
    private JRadioButton radioButtonGateForceOff;
    private JTextField gateRespSensTextField;
    private JButton gateChooseSensorButton;
    private JPanel gateConfig;
    private JCheckBox autogenerateSensorsCheckBox;

    private List<Node> nodes = new ArrayList<>();

    private ButtonGroup persistConnectGroup;
    private ButtonAllower allowButtonAddDevice;
    private ButtonAllower allowButtonAddGate;
    private ButtonAllower allowButtonOk;
    private FocusManagerTextField deviceAddrFocusManager;
    private FocusManagerTextField gateVstandPortFocusManager;

    private DefaultTableModel devTableModel = new DefaultTableModel();
    private DefaultTableModel gateTableModel = new DefaultTableModel();

    private int numDevAddr = 1;
    private int numGate = 1;

    private static final String REGEX_ONLY_NUMBERS      = "[0-9]*";
    private static final String REGEX_DEVICE_ADDR       = "0x[0-9]*";
    private static final String REGEX_ONLY_NUM_AND_CHAR = "\\w*";
    private static final String REGEX_ALL               = ".*";
    private static final String YES_OPTION              = "on";
    private static final String NO_OPTION               = "off";
    private static final String TITLE                   = "Adding a MBMM";
    private static final Node NO_EXIST_NODE             = new Node("", "", "", "");

    private Project project;
    private Configure configure;
    private Smemory smemory;
    private SaveAdapter saveAdapter;

    public AddMBMMDialog(@NonNull Project project, @NonNull Configure configure, @NonNull Smemory smemory)
    {
        // Стандартная инициализация
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.configure = configure;
        this.smemory = smemory;

        // Device
        // Инициализация модели таблицы устройств
        String[] devHeaders = {"Device", "Address", "Respond SMapSensor", "Timeout", "Force", "Invert"};
        devTableModel.setColumnIdentifiers(devHeaders);
        devTable.setDefaultEditor(Object.class, null);
        devTable.setModel(devTableModel);

        // Выбор Invert и Force (да/нет)
        radioButtonDeviceForceYes.setSelected(true);
        ButtonGroup devForceGroup = new ButtonGroup();
        devForceGroup.add(radioButtonDeviceForceYes);
        devForceGroup.add(radioButtonDeviceForceNo);

        radioButtonDeviceInvertYes.setSelected(true);
        ButtonGroup devInvertGroup = new ButtonGroup();
        devInvertGroup.add(radioButtonDeviceInvertYes);
        devInvertGroup.add(radioButtonDeviceInvertNo);

        // Gate
        // Инициализация модели таблицы выходов(gate)
        String[] gateHeaders = {"Ip", "Real port", "Vstand port", "Respond init timeout", "Recv timeout", "Force", "Invert", "Respond sensor"};
        gateTableModel.setColumnIdentifiers(gateHeaders);
        gateTable.setDefaultEditor(Object.class, null);
        gateTable.setModel(gateTableModel);

        // Выбор Invert и Force (да/нет)
        radioButtonGateForceOn.setSelected(true);
        ButtonGroup gateForceGroup = new ButtonGroup();
        gateForceGroup .add(radioButtonGateForceOn);
        gateForceGroup .add(radioButtonGateForceOff);

        radioButtonGateInvertOn.setSelected(true);
        ButtonGroup gateInvertGroup = new ButtonGroup();
        gateInvertGroup.add(radioButtonGateInvertOn);
        gateInvertGroup.add(radioButtonGateInvertOff);

        // Выбор Persistent Connection
        radioButtonPersistConnectOn.setSelected(true);
        persistConnectGroup = new ButtonGroup();
        persistConnectGroup.add(radioButtonPersistConnectOn);
        persistConnectGroup.add(radioButtonPersistConnectOff);

        // Обновление доступности кнопки add
        JTextField[] deviceTextFields = {deviceRespSensTextField, deviceTimeoutTextField, deviceAddrTextField}; // От каких textField-ов зависит доступность кнопки
        allowButtonAddDevice = new ButtonAllower(buttonAddDevice, deviceTextFields);

        // Обновление доступности кнопки add
        JTextField[] gateTextFields = {gateIPAddrTextField, gateRealPortTextField, gateVstandPortTextField,
                gateRespInitTimeoutTextField, gateRecvTimeoutTextField, gateRespSensTextField}; // От каких textField-ов зависит доступность кнопки
        allowButtonAddGate = new ButtonAllower(buttonAddGate, gateTextFields);

        // Обновление доступности кнопки ok
        JTextField[]  netTextFields = {
                netFilterFieldTextField, netFilterValueTextField, netPrefixTextField,
                netPolltimeTextField, netExchSensTextField, nameTextField};
        JTextField[] allTextFields = Stream.concat(Arrays.stream(deviceTextFields), Arrays.stream(netTextFields)).toArray(JTextField[]::new);
        ButtonAllower.AdditionalCondition[] additionalConditions = {() -> !(devTableModel.getRowCount() == 0), () -> !(gateTableModel.getRowCount() == 0)};
        JButton[] additionalEventButtons  = {buttonDeleteDevice, buttonAddDevice, buttonAddGate, buttonDeleteGate};
        allowButtonOk = new ButtonAllower(new ButtonAllowerOKAdapter(this), allTextFields, additionalConditions, additionalEventButtons);

        // Выбор датчиков
        deviceChooseSensorButton.addActionListener(new ApplySelectedSensor(project, configure, deviceRespSensTextField));
        gateChooseSensorButton.addActionListener(new ApplySelectedSensor(project, configure, gateRespSensTextField));
        netExchChooseSensorButton.addActionListener(new ApplySelectedSensor(project, configure, netExchSensTextField));

        // Заполнение полей
        deviceAddrFocusManager = new FocusManagerTextField(deviceAddrTextField, formatToDevAddr(numDevAddr), REGEX_DEVICE_ADDR);
        deviceAddrTextField.addFocusListener(deviceAddrFocusManager);
        deviceTimeoutTextField.addFocusListener(new FocusManagerTextField(deviceTimeoutTextField, Settings.MBMM_TIMEOUT, REGEX_ONLY_NUMBERS));
        gateIPAddrTextField.addFocusListener(new FocusManagerTextField(gateIPAddrTextField, Settings.MBMM_GATE_IP, REGEX_ALL));
        gateRealPortTextField.addFocusListener(new FocusManagerTextField(gateRealPortTextField, Settings.MBMM_GATE_REAL_PORT, REGEX_ONLY_NUMBERS));
        gateVstandPortFocusManager = new FocusManagerTextField(gateVstandPortTextField, Settings.MBMM_GATE_VSTAND_PORT + numGate, REGEX_ONLY_NUMBERS);
        gateVstandPortTextField.addFocusListener(gateVstandPortFocusManager);
        gateRespInitTimeoutTextField.addFocusListener(new FocusManagerTextField(gateRespInitTimeoutTextField, Settings.MBMM_GATE_RESP_INIT_TIMEOUT, REGEX_ONLY_NUMBERS));
        gateRecvTimeoutTextField.addFocusListener(new FocusManagerTextField(gateRecvTimeoutTextField, Settings.MBMM_GATE_RECV_TIMEOUT, REGEX_ONLY_NUMBERS));
        netPolltimeTextField.addFocusListener(new FocusManagerTextField(netPolltimeTextField, Settings.MBMM_POLLTIME, REGEX_ONLY_NUMBERS));
        netFilterFieldTextField.addFocusListener(new FocusManagerTextField(netFilterFieldTextField, Settings.MBMM_FILTER_FIELD, REGEX_ALL));
        netPrefixTextField.addFocusListener(new FocusManagerTextField(netPrefixTextField, Settings.MBMM_PREFIX, ".*_$"));

        // Интеграция поддержки автогенерации датчиков
        supportAutogenSensors();

        // Кнопка add
        buttonAddDevice.addActionListener(e -> {
            String[] row = {
                    Integer.toString(numDevAddr),
                    deviceAddrTextField.getText(), deviceRespSensTextField.getText(),
                    deviceTimeoutTextField.getText(),
                    (radioButtonDeviceForceYes.isSelected() ? YES_OPTION : NO_OPTION),
                    (radioButtonDeviceInvertYes.isSelected() ? YES_OPTION : NO_OPTION)};
            devTableModel.addRow(row);

            deviceAddrFocusManager.setDefaultValue(formatToDevAddr(++numDevAddr));
        });

        // Кнопка delete
        buttonDeleteDevice.addActionListener(e -> {
            if (devTableModel.getRowCount() > 0) {
                devTableModel.removeRow(devTableModel.getRowCount() - 1);
                deviceAddrFocusManager.setDefaultValue(formatToDevAddr(--numDevAddr));
            }
        });

        buttonAddGate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] row = {
                        gateIPAddrTextField.getText(),
                        gateRealPortTextField.getText(),
                        gateVstandPortTextField.getText(),
                        gateRespInitTimeoutTextField.getText(),
                        gateRecvTimeoutTextField.getText(),
                        (radioButtonGateForceOn.isSelected()) ? YES_OPTION : NO_OPTION,
                        (radioButtonGateInvertOn.isSelected()) ? YES_OPTION : NO_OPTION,
                        gateRespSensTextField.getText()
                };
                gateTableModel.addRow(row);
                gateVstandPortFocusManager.setDefaultValue(Settings.MBMM_GATE_VSTAND_PORT + (++numGate));
            }
        });

        buttonDeleteGate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(gateTableModel.getRowCount() > 0) {
                    gateTableModel.removeRow(gateTableModel.getRowCount() - 1);
                    gateVstandPortFocusManager.setDefaultValue(Settings.MBMM_GATE_VSTAND_PORT + (--numGate));
                }
            }
        });

        // Инициализируем доступные узлы
        this.setAllowNodes(syncNodes(configure.getNodeList(), smemory.getStartScripts()));
    }

    public boolean isAutogerateSensors()
    {
        return autogenerateSensorsCheckBox.isSelected();
    }

    public SensorGenerator getSensorGenerator()
    {
        SensorGenerator sensorGenerator;
        if(isAutogerateSensors())
        {
            sensorGenerator = new SensorGenerator(configure);
            sensorGenerator
                    .registrySensor(getNetExchSens(),
                            getName() + ": Датчик обмена",
                            Settings.IOTYPE_DI);
            int i = 1;
            for(Device device : getDevices())
            {
                String textname;
                if(device.getRespondSensor().contains("Not"))
                    textname = getName() + ": Нет связи по каналу " + i;
                else
                    textname = getName() + ": Состояние связи по каналу " + i;
                sensorGenerator.registrySensor(device.getRespondSensor(), textname, Settings.IOTYPE_DI);
                i++;
            }
            i = 1;
            for(Gate gate : getGates())
            {
                String textname;
                if(gate.getRespondSensor().contains("Not"))
                    textname = getName() + ": Нет связи по каналу " + i;
                else
                    textname = getName() + ": Состояние связи по каналу " + i;

                sensorGenerator.registrySensor(gate.getRespondSensor(), textname, Settings.IOTYPE_DI);
                i++;
            }

            return sensorGenerator;
        }

        return null;
    }

    public void offFocusManagerTextField()
    {
        JTextField[] textFields = {
                netPolltimeTextField,
                netFilterFieldTextField,
                netPrefixTextField
        };
        for(JTextField textField : textFields)
        {
            for(FocusListener listener : textField.getFocusListeners())
            {
                if(listener instanceof FocusManagerTextField)
                {
                    ((FocusManagerTextField) listener).off();
                }
            }
        }
    }

    public void setObject(MBMMObject object) {
        saveAdapter = new SaveAdapter(this, object);
        comboBoxChooseNode.setEnabled(false);
        autogenerateSensorsCheckBox.setEnabled(false);
    }

    public void addChangeListener(ChangeListener listener)
    {
        saveAdapter.addChangeListener(listener);
    }

    private List<Node> syncNodes(List<Node> nodes, List<NodeScript> scripts)
    {
        List<Node> list = new ArrayList<>();
        for(Node node : nodes)
        {
            for(NodeScript script : scripts)
            {
                if(node.getName().equals(script.getNodeName())) {
                    list.add(node);
                    break;
                }
            }
        }

        return list;
    }

    private void setValueTextField(JTextField textField, String value)
    {
        if(value == null || value.isEmpty())
            textField.setText("");
        else
            textField.setText(value);
    }

    private void selectNodeOf(MBMMObject object) {
        Node node = object.getNode();
        if(node != null)
            comboBoxChooseNode.setSelectedItem(node);
        else
            comboBoxChooseNode.setSelectedItem(NO_EXIST_NODE);
    }

    public List<Device> getDevices()
    {
        List<Device> devices = new ArrayList<>();
        for(int i = 0; i < devTableModel.getRowCount(); i++) {
            String address = (String) devTableModel.getValueAt(i, 1);
            String respondSensor = (String) devTableModel.getValueAt(i, 2);
            String timeout = (String) devTableModel.getValueAt(i, 3);
            String force = (String) devTableModel.getValueAt(i, 4);
            String invert = (String) devTableModel.getValueAt(i, 5);

            Device device = new Device(address, respondSensor, force.equals(YES_OPTION), invert.equals(YES_OPTION), Long.parseLong(timeout));
            devices.add(device);
        }

        return devices;
    }

    public List<Gate> getGates()
    {
        List<Gate> gates = new ArrayList<>();
        for(int i = 0; i < gateTableModel.getRowCount(); i++) {
            String ip = (String) gateTableModel.getValueAt(i, 0);
            String realPort = (String) gateTableModel.getValueAt(i, 1);
            String vstandPort = (String) gateTableModel.getValueAt(i, 2);
            String respondInitTimeout = (String) gateTableModel.getValueAt(i, 3);
            String recvTimeout = (String) gateTableModel.getValueAt(i, 4);
            String force = (String) gateTableModel.getValueAt(i, 5);
            String invert = (String) gateTableModel.getValueAt(i, 6);
            String respondSensor = (String) gateTableModel.getValueAt(i, 7);

            Gate gate = new Gate(
                    ip,
                    respondSensor,
                    realPort,
                    vstandPort,
                    Long.parseLong(respondInitTimeout),
                    Long.parseLong(recvTimeout),
                    force.equals(YES_OPTION),
                    invert.equals(YES_OPTION));
            gates.add(gate);
        }

        return gates;
    }

    public boolean isPersitentConnection()
    {
        return radioButtonPersistConnectOn.isSelected();
    }

    public String getDeviceAddr() {
        return deviceAddrTextField.getText();
    }

    public String getDeviceTimeout() {
        return deviceTimeoutTextField.getText();
    }

    public String getDeviceRespSens() {
        return deviceRespSensTextField.getText();
    }

    public String getName() {
        return nameTextField.getText();
    }

    public String getGateIPAddr() {
        return gateIPAddrTextField.getText();
    }

    public String getGateRealPort() {
        return gateRealPortTextField.getText();
    }

    public String getNetPolltime() {
        return netPolltimeTextField.getText();
    }

    public String getNetExchSens() {
        return netExchSensTextField.getText();
    }


    public String getNetFilterField() {
        return netFilterFieldTextField.getText();
    }

    public String getNetFilterValue() {
        return netFilterValueTextField.getText();
    }

    public String getNetPrefix() {
        return netPrefixTextField.getText();
    }

    public String getGateVstandPort() {
        return gateVstandPortTextField.getText();
    }

    public String getGateRespInitTimeout() {
        return gateRespInitTimeoutTextField.getText();
    }

    public String getGateRecvTimeout() {
        return gateRecvTimeoutTextField.getText();
    }

    public String getGateRespSens() {
        return gateRespSensTextField.getText();
    }

    public Node getSelectedNode()
    {
        return (Node) comboBoxChooseNode.getSelectedItem();
    }

    public void setAllowNodes(List<Node> nodes)
    {
        this.nodes = new ArrayList<>(nodes);
        this.nodes.add(0, NO_EXIST_NODE);
        updateNodesComboBox();
        updateEnablePanel(nodeConfig, true);
    }

    private void supportAutogenSensors()
    {
        // Автоген датчиков
        autogenerateSensorsCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                {
                    clearTable(devTableModel);
                    clearTable(gateTableModel);
                    autogenerateSensors();
                    setEnableSelectButtons(false);
                }
                else
                {
                    setEnableSelectButtons(true);
                    clearTable(devTableModel);
                    clearTable(gateTableModel);
                    netExchSensTextField.setText("");
                    deviceRespSensTextField.setText("");
                    gateRespSensTextField.setText("");
                    numDevAddr = 1;
                    numGate = 1;
                    deviceAddrFocusManager.setDefaultValue(formatToDevAddr(numDevAddr));
                    gateVstandPortFocusManager.setDefaultValue(Settings.MBMM_GATE_VSTAND_PORT + numGate);
                }
            }
        });
        nameTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        radioButtonDeviceInvertYes.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        radioButtonDeviceInvertNo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });

        buttonAddDevice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        buttonDeleteDevice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });

        buttonAddGate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
        buttonDeleteGate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(autogenerateSensorsCheckBox.isSelected())
                    autogenerateSensors();
            }
        });
    }


    private void autogenerateSensors()
    {
        // Устанавливаем в textfield-ы
        String prefix = "MBMM_";
        String exchangeSensor = prefix + getName() + "_Exchange_S";
        String respondSensorDev;
        String respondSensorGate;
        if(radioButtonDeviceInvertYes.isSelected())
            respondSensorDev = prefix  + getName() + "_Device" + numDevAddr + "_Not_Respond_S";
        else
            respondSensorDev = prefix  + getName() + "_Device" + numDevAddr + "_Respond_S";

        if(radioButtonGateInvertOn.isSelected())
            respondSensorGate = prefix  + getName() + "_Gate" + numGate + "_Not_Respond_S";
        else
            respondSensorGate = prefix  + getName() + "_Gate" + numGate + "_Respond_S";

        autogenerateChangeName(devTableModel, prefix, 2);
        autogenerateChangeName(gateTableModel, prefix, 7);
        netExchSensTextField.setText(exchangeSensor);
        deviceRespSensTextField.setText(respondSensorDev);
        gateRespSensTextField.setText(respondSensorGate);

    }

    private void autogenerateChangeName(DefaultTableModel tableModel, String prefix, int posRespondSensor)
    {
        // Изменяем в таблице
        String[] rows = new String[tableModel.getRowCount()];
        for(int i = 0; i < tableModel.getRowCount(); i++) {
            rows[i] = "";
            for(int j = 0; j < tableModel.getColumnCount(); j++)
                rows[i] += tableModel.getValueAt(i, j) + ":";
        }
        clearTable(tableModel);

        for(int i = 0; i < rows.length; i++)
        {
            String[] row = rows[i].split(":");
            row[posRespondSensor] =
                    row[posRespondSensor].replaceAll(prefix + "[^_]*", prefix + getName());
            tableModel.addRow(row);
        }
    }

    private void setEnableSelectButtons(boolean on)
    {
        netExchChooseSensorButton.setEnabled(on);
        gateChooseSensorButton.setEnabled(on);
        deviceChooseSensorButton.setEnabled(on);
    }


    private void updateNodesComboBox()
    {
        comboBoxChooseNode.removeAllItems();
        for(Node node : nodes)
            comboBoxChooseNode.addItem(node);
    }

    private void updateEnablePanel(JPanel panel, boolean state)
    {
        for (Component component : panel.getComponents()) {

            if(component instanceof JPanel)
                updateEnablePanel((JPanel) component, state);

            component.setEnabled(state);
        }

        if(state)
            allowButtonAddDevice.updateAllow();
    }

    private void clearTable(DefaultTableModel model)
    {
        int count = model.getRowCount();
        for(int i = 0; i < count; i++)
        {
            model.removeRow(0);
        }

    }

    private String formatToDevAddr(int numDevAddr)
    {
        if(numDevAddr < 10)
            return "0x0" + numDevAddr;
        return "0x" + numDevAddr;
    }

    @Override
    protected void doOKAction() {
        NodeScript script = smemory.getStartScript(getSelectedNode().getName());
        if(script == null)
        {
            Messages.showMessageDialog(project,
                    "The " + getSelectedNode().getName() + " script is not found",
                    TITLE, Messages.getErrorIcon());
            return;
        }
        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    public JComponent getMainPanel()
    {
        return mainPanel;
    }

    public static class SaveAdapter extends ChangeAction
    {
        private MBMMObject object;
        private AddMBMMDialog dialog;

        public SaveAdapter(AddMBMMDialog dialog, MBMMObject object) {

            this.object = object;
            this.dialog = dialog;
            init();
        }

        private void addSaveChangeEvent(JTextField textField)
        {
            textField.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEvent(JRadioButton radioButton)
        {
            radioButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEvent(JButton button)
        {
            // Меняем порядок слушателей
            // т.к. первым вызывается последний добавленный,
            // а первым нам необходимо вызывать слушателя который обновляет список devices
            ActionListener[] listeners = Arrays.copyOf(button.getActionListeners(), button.getActionListeners().length);
            for(ActionListener listener : button.getActionListeners())
            {
                button.removeActionListener(listener);
            }

            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });

            for(ActionListener listener : listeners)
                button.addActionListener(listener);
        }

        private void addSaveChangeEventNode(JComboBox<Node> comboBox)
        {
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void init()
        {
            dialog.selectNodeOf(object);
            setValueTextField(dialog.nameTextField, this.object.getName());
            setValueTextField(dialog.netFilterFieldTextField, this.object.getFilterField());
            setValueTextField(dialog.netFilterValueTextField, this.object.getFilterValue());
            setValueTextField(dialog.netPrefixTextField, this.object.getPrefix());
            setValueTextField(dialog.netPolltimeTextField, String.valueOf(this.object.getPolltime()));
            setValueTextField(dialog.netExchSensTextField, this.object.getExchangeSensor());

            dialog.persistConnectGroup.clearSelection();
            dialog.radioButtonPersistConnectOn.setSelected(this.object.isPersistentConnection());
            dialog.radioButtonPersistConnectOff.setSelected(!this.object.isPersistentConnection());

            dialog.clearTable(dialog.devTableModel);
            dialog.numDevAddr = 1;
            for(Device device : object.getDeviceList())
            {
                String[] row = {
                        Integer.toString(dialog.numDevAddr++),
                        device.getAddress(),
                        device.getRespondSensor(),
                        String.valueOf(device.getTimeout()),
                        (device.isForce() ? YES_OPTION : NO_OPTION),
                        (device.isInvert() ? YES_OPTION : NO_OPTION)};
                dialog.devTableModel.addRow(row);
            }
            dialog.deviceAddrFocusManager.setDefaultValue(dialog.formatToDevAddr(dialog.numDevAddr));

            dialog.clearTable(dialog.gateTableModel);
            for(Gate gate : object.getGatesList())
            {
                String[] row = {
                        gate.getIp(),
                        gate.getRealPort(),
                        gate.getVstandPort(),
                        String.valueOf(gate.getRespondInitTimeout()),
                        String.valueOf(gate.getRecvTimeout()),
                        (gate.isForce()) ? YES_OPTION : NO_OPTION,
                        (gate.isInvert()) ? YES_OPTION : NO_OPTION,
                        gate.getRespondSensor()
                };
                dialog.gateTableModel.addRow(row);
            }

            addSaveChangeEvent(dialog.nameTextField);
            addSaveChangeEvent(dialog.netFilterFieldTextField);
            addSaveChangeEvent(dialog.netFilterValueTextField);
            addSaveChangeEvent(dialog.netPrefixTextField);
            addSaveChangeEvent(dialog.netPolltimeTextField);
            addSaveChangeEvent(dialog.netExchSensTextField);
            addSaveChangeEvent(dialog.radioButtonPersistConnectOn);
            addSaveChangeEvent(dialog.radioButtonPersistConnectOff);
            addSaveChangeEvent(dialog.buttonAddDevice);
            addSaveChangeEvent(dialog.buttonDeleteDevice);
            addSaveChangeEvent(dialog.buttonAddGate);
            addSaveChangeEvent(dialog.buttonDeleteGate);
            addSaveChangeEventNode(dialog.comboBoxChooseNode);

        }

        private void applyChanged()
        {
            if(!dialog.getName().equals(this.object.getName()))
                this.object.setName(dialog.getName());
            if(!dialog.getNetFilterField().equals(this.object.getFilterField()))
                this.object.setFilterField(dialog.getNetFilterField());
            if(!dialog.getNetFilterValue().equals(this.object.getFilterValue()))
                this.object.setFilterValue(dialog.getNetFilterValue());
            if(!dialog.getNetPrefix().equals(this.object.getPrefix()))
                this.object.setPrefix(dialog.getNetPrefix());
            if(!dialog.getNetPolltime().equals(String.valueOf(this.object.getPolltime())))
                this.object.setPolltime(Integer.parseInt(dialog.getNetPolltime()));
            if(!dialog.getNetExchSens().equals(this.object.getExchangeSensor()))
                this.object.setExchangeModeIdSensor(dialog.getNetExchSens());

            this.object.setPersistentConnection(dialog.isPersitentConnection());

            Node node = dialog.getSelectedNode();
            if(node == NO_EXIST_NODE)
                this.object.setNode(null);
            else
                this.object.setNode(dialog.getSelectedNode());

            this.object.setDeviceList(dialog.getDevices());
            this.object.setGatesList(dialog.getGates());

            fireChangeEvent();
        }

        private void setValueTextField(JTextField textField, String value) {
            if(value == null || value.isEmpty())
                textField.setText("");
            else
                textField.setText(value);
        }
    }
}
