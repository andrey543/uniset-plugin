package com.uniset.plugin.gui.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.modules.ChangeAction;
import com.uniset.plugin.gui.modules.ChangeListener;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;
import static java.awt.event.KeyEvent.VK_DELETE;

public class ModbusSensorsDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JButton addButton;
    private JButton delButton;
    private JTable sensorTable;
    private ChangeNotificator changeNotificator = new ChangeNotificator();
    private ModbusSensorsModel modbusSensorsModel = new ModbusSensorsModel();
    private ModbusObject modbusObject;


    public ModbusSensorsDialog(final Project project, final Configure configure)
    {
        super(project);
        setTitle("Modbus sensors");
        init();

        /** Таблица датчиков */
        sensorTable.setDefaultEditor(Object.class, null);
        sensorTable.setModel(modbusSensorsModel);
        sensorTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setJTableColumnsWidth(sensorTable, 15, 25, 30, 10, 5, 5, 5, 5);

        /** Обработка add */
        addButton.addActionListener(e -> {
            EditorModbusSensorDialog dialog = new EditorModbusSensorDialog(project, configure, modbusObject);
            dialog.setResizable(true);
            dialog.pack();
            dialog.show();
            if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
            {
                Sensor[] sensors = dialog.getSelectedSensors();
                if(sensors.length > 1)
                {
                    for(ModbusSensorGroupDialog.ModbusGroupSensor modbusGroupSensor : dialog.getModbusGroupSensors())
                    {
                        modbusObject.addSensor(
                                modbusGroupSensor.getSensor(),
                                modbusGroupSensor.getMbaddr(),
                                modbusGroupSensor.getMbreg(),
                                modbusGroupSensor.getMbfunc(),
                                modbusGroupSensor.getNbit());
                    }
                }
                else {
                    Sensor sensor = dialog.getSelectedSensors()[0];
                    modbusObject.addSensor(sensor, dialog.getMbaddr(), dialog.getMbreg(), dialog.getMbfunc(), dialog.getNbit());
                }
                updateSensorInfo();
                changeNotificator.fireChangeEvent();
            }
        });

        /** Обработка del */
        sensorTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == VK_DELETE )
                {
                    deleteSensors();
                    /** Отслеживание изменений */
                    changeNotificator.fireChangeEvent();
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        delButton.addActionListener(e -> {
            deleteSensors();
        });
    }

    public JComponent getMainPanel()
    {
        return mainPanel;
    }

    public void setModbusObject(final ModbusObject modbusObject)
    {
        this.modbusObject = modbusObject;
        updateSensorInfo();
    }

    public void addChangeListener(ChangeListener listener)
    {
        changeNotificator.addChangeListener(listener);
    }

    private void deleteSensors()
    {
        for(int it : sensorTable.getSelectedRows())
            modbusObject.removeSensor(modbusSensorsModel.getModbusSensor(it));
        updateSensorInfo();
        changeNotificator.fireChangeEvent();
    }
    private void updateSensorInfo()
    {
        modbusSensorsModel.clear();
        for(ModbusObject.ModbusSensor modbusSensor : modbusObject.getSensors())
        {
            modbusSensorsModel.addModbusSensor(modbusSensor);
        }
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    private static class ModbusSensorsModel extends AbstractTableModel
    {
        private List<ModbusObject.ModbusSensor> modbusSensors = new ArrayList<>();
        String[] modbusSensorHeaders = {"id", "name", "textname","type", "mbaddr", "mbreg", "mbfunc", "nbit"};

        @Override
        public int getRowCount() {
            return modbusSensors.size();
        }

        @Override
        public int getColumnCount() {
            return modbusSensorHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return modbusSensorHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            ModbusObject.ModbusSensor sensor = modbusSensors.get(rowIndex);
            Object value = "";
            if(sensor != null) {
                switch (columnIndex) {
                    case 0:
                        value = sensor.getSensor().getId();
                        break;
                    case 1:
                        value = sensor.getSensor().getName();
                        break;
                    case 2:
                        value = sensor.getSensor().getTextname();
                        break;
                    case 3:
                        value = sensor.getSensor().getIotype();
                        break;
                    case 4:
                        value = sensor.getMbaddr();
                        break;
                    case 5:
                        value = sensor.getMbreg();
                        break;
                    case 6:
                        value = sensor.getMbfunc();
                        break;
                    case 7:
                        value = sensor.getNbit();
                        break;
                }
            }

            return value;
        }
        public void addModbusSensor(final ModbusObject.ModbusSensor sensor)
        {
            modbusSensors.add(sensor);
            fireTableDataChanged();
        }

        public void clear()
        {
            modbusSensors.clear();
            fireTableDataChanged();
        }

        public ModbusObject.ModbusSensor getModbusSensor(int row)
        {
            return modbusSensors.get(row);
        }
    }

    private class ChangeNotificator extends ChangeAction
    {
        @Override
        protected void fireChangeEvent() {
            super.fireChangeEvent();
        }
    }

}
