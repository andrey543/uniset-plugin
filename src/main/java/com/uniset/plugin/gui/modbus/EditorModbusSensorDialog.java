package com.uniset.plugin.gui.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.modbus.Device;
import com.uniset.plugin.build.objects.modbus.MBMMObject;
import com.uniset.plugin.build.objects.modbus.MBMObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.additional.SelectSensorDialog;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.gui.modules.ButtonAllowerOKAdapter;
import com.uniset.plugin.gui.modules.FocusManagerTextField;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

//TODO Сделать это окно с сохранением предыдущих настроек
public class EditorModbusSensorDialog extends DialogWrapper {
    private JPanel mainPanel;
    private JPanel selectSensorPanel;
    private JTextField mbregTextField;
    private JTextField nbitTextField;
    private JComboBox<String> mbfuncComboBox;
    private JComboBox<String> mbaddrComboBox;
    private JTextField sensorTextField;
    private ModbusObject modbusObject;

    private SelectSensorDialog selectSensorDialog;
    private ButtonAllower buttonAllowerOk;

    private static final String TITLE= "Editor Modbus Sensor";
    private static final String REGEX_ONLY_NUMBERS = "[0-9]*";

    private Project project;
    private List<ModbusSensorGroupDialog.ModbusGroupSensor> modbusGroupSensors;

    public EditorModbusSensorDialog(@Nullable Project project, final Configure configure, final ModbusObject modbusObject) {
        super(project);
        setTitle(TITLE);
        init();
        this.project = project;
        this.modbusObject = modbusObject;

        selectSensorDialog = new SelectSensorDialog(project, configure, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        selectSensorPanel.add(selectSensorDialog.getMainPanel());
        selectSensorPanel.updateUI();
        selectSensorDialog
                .getSensorTable()
                .addMouseMotionListener(new MouseMotionListener()
                {
                    @Override
                    public void mouseDragged(MouseEvent e) {
                        buttonAllowerOk.updateAllow();
                    }

                    @Override
                    public void mouseMoved(MouseEvent e) {
                        buttonAllowerOk.updateAllow();
                    }
                });
        selectSensorDialog.getSensorTable().addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                buttonAllowerOk.updateAllow();
                updateSensorTextField();
            }
        });

        for(String mbfunc : Settings.mbfuncs)
            mbfuncComboBox.addItem(mbfunc);

        mbfuncComboBox.addActionListener(e -> {
            updateNbitTextField();
        });
        mbfuncComboBox.setSelectedIndex(0);

        if(modbusObject instanceof MBMObject)
        {
            mbaddrComboBox.setEnabled(true);
            for(Device device : ((MBMObject)modbusObject).getDeviceList())
            {
                mbaddrComboBox.addItem(device.getAddress());
            }
        }
        else if(modbusObject instanceof MBMMObject)
        {
            mbaddrComboBox.setEnabled(true);
            for(Device device : ((MBMMObject)modbusObject).getDeviceList())
            {
                mbaddrComboBox.addItem(device.getAddress());
            }
        }
        else {
            mbaddrComboBox.setEnabled(false);
            mbaddrComboBox.addItem("0x01");
            mbaddrComboBox.setSelectedIndex(0);
        }

        mbregTextField.addFocusListener(new FocusManagerTextField(mbregTextField, getMaxReg(), REGEX_ONLY_NUMBERS));
        nbitTextField.addFocusListener(new FocusManagerTextField(nbitTextField, getMaxNbit(), REGEX_ONLY_NUMBERS));

        JTextField[] allowOkTextFields = {mbregTextField};
        buttonAllowerOk = new ButtonAllower(new ButtonAllowerOKAdapter(this), allowOkTextFields);
        buttonAllowerOk.addAdditionalCondition(() -> selectSensorDialog.getSensorTable().getSelectedRow() != -1);
    }

    private void updateNbitTextField()
    {
        String func = (String) mbfuncComboBox.getSelectedItem();
        if(func != null && (func.equals("0x02") || func.equals("0x03")))
        {
            nbitTextField.setText(getMaxNbit());
            nbitTextField.setEnabled(true);
        }
        else {
            nbitTextField.setText("");
            nbitTextField.setEnabled(false);
        }
    }

    @Override
    protected void doOKAction() {

        Sensor[] sensors = getSelectedSensors();
        if(getSelectedSensors().length > 1)
        {
            ModbusSensorGroupDialog dialog = new ModbusSensorGroupDialog(project, sensors, getMaxReg(), mbaddrComboBox);
            dialog.setResizable(true);
            dialog.pack();
            dialog.show();
            if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
            {
                this.modbusGroupSensors = dialog.getModbusGroupSensors();
            }
            else
                return;
        }

        List<ModbusObject.ModbusSensor> alreadySelected = new ArrayList<>();
        for(Sensor selectedSensor : getSelectedSensors()) {
            for (ModbusObject.ModbusSensor modbusSensor : modbusObject.getSensors()) {
                if (modbusSensor.getSensor().equals(selectedSensor)) {
                    alreadySelected.add(modbusSensor);
                }
            }
        }

        if(alreadySelected.size() > 0)
        {
            String sensorNames = "";
            for(ModbusObject.ModbusSensor sensor : alreadySelected)
                sensorNames += sensor.getSensor().getName() + System.lineSeparator();
            Messages.showMessageDialog(project,
                    "Selected sensors is already added:" + System.lineSeparator() + "- " + sensorNames,
                    TITLE, Messages.getErrorIcon());
            return;
        }

        super.doOKAction();

    }

    private void updateSensorTextField()
    {
        if(getSelectedSensors().length > 1)
            sensorTextField.setText("");
        else
            sensorTextField.setText(getSelectedSensors()[0].getName());
    }

    public List<ModbusSensorGroupDialog.ModbusGroupSensor> getModbusGroupSensors()
    {
        return modbusGroupSensors;
    }
    public Sensor[] getSelectedSensors()
    {
        return selectSensorDialog.getSelectedSensors();
    }

    public String getMbreg() {
        return mbregTextField.getText();
    }

    public String getNbit() {
        return nbitTextField.getText();
    }

    public String getMbfunc() {
        return (String) mbfuncComboBox.getSelectedItem();
    }

    public String getMbaddr() {
        return (String) mbaddrComboBox.getSelectedItem();
    }

    private String getMaxReg()
    {
        int max = -1;
        for(ModbusObject.ModbusSensor sensor : modbusObject.getSensors())
        {
            String value = sensor.getMbreg();
            if(value.isEmpty())
                continue;

            int reg = Integer.parseInt(value);
            if(max < reg)
                max = reg;
        }
        if(max == -1)
            return "0";

        return String.valueOf(++max);
    }

    private String getMaxNbit()
    {
        int max = 0;
        for(ModbusObject.ModbusSensor sensor : modbusObject.getSensors())
        {
            String value = sensor.getNbit();
            if(value.isEmpty())
                continue;

            int nbit = Integer.parseInt(value);
            if(max < nbit)
                max = nbit;
        }
        if(max == 0)
            return "";

        return String.valueOf(++max);
    }
    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }
}
