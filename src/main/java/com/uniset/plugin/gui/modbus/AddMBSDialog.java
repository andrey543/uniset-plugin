package com.uniset.plugin.gui.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.modbus.MBSObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.gui.modules.*;
import com.uniset.plugin.gui.modules.ChangeListener;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.eclipse.lsp4j.jsonrpc.validation.NonNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.List;
import java.util.*;
import java.util.stream.Stream;

public class AddMBSDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JPanel nodeConfig;
    private JComboBox<Node> comboBoxChooseNode;
    private JLabel labelChooseNode;
    private JPanel panelConfigTCP;
    private JTextField nameTextField;
    private JLabel labelNameMBM;
    private JTextField IPAddrTextField;
    private JLabel labelNetAddr;
    private JLabel labelNetPort;
    private JTextField portTextField;
    private JTextField filterFieldTextField;
    private JTextField filterValueTextField;
    private JTextField prefixTextField;
    private JRadioButton radioButtonCheckMbfuncOn;
    private JRadioButton radioButtonCheckMbfuncOff;
    private JLabel labelFilterField;
    private JLabel labelFilterValue;
    private JLabel labelPrefix;
    private JLabel labelCheckMbfunc;
    private JPanel panelCheckMbfunc;
    private JTabbedPane tabbedPane1;
    private JComboBox<String> comboBoxType;
    private JPanel panelConfigRTU;
    private JPanel cardLayoutTypes;
    private JTextField devTextField;
    private JTextField speedTextField;
    private JRadioButton radioButtonForceOn;
    private JRadioButton radioButtonForceOff;
    private ButtonGroup forceGroup;
    private ButtonGroup checkMbfuncGroup;

    private List<Node> nodes = new ArrayList<>();
    private ButtonAllower allowButtonOkTCP;
    private ButtonAllower allowButtonOkRTU;
    private ButtonAllower allowButtonOkNone;

    private static final String REGEX_ONLY_NUMBERS      = "[0-9]*";
    private static final String REGEX_DEVICE_ADDR       = "0x[0-9]*";
    private static final String REGEX_ONLY_NUM_AND_CHAR = "\\w*";
    private static final String REGEX_ALL               = ".*";
    private static final String YES_OPTION              = "on";
    private static final String NO_OPTION               = "off";
    private static final String TITLE                   = "Adding a MBS";
    private static final Node NO_EXIST_NODE             = new Node("", "", "", "");
    private static final String NO_ITEM                 = "None";

    private Project project;
    private Configure configure;
    private Smemory smemory;
    private SaveAdapter saveAdapter;

    public AddMBSDialog(@NonNull Project project, @NonNull Configure configure, @NonNull Smemory smemory)
    {
        // Стандартная инициализация
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.configure = configure;
        this.smemory = smemory;

        // Выбор check-mbfunc
        radioButtonCheckMbfuncOn.setSelected(true);
        checkMbfuncGroup = new ButtonGroup();
        checkMbfuncGroup.add(radioButtonCheckMbfuncOn);
        checkMbfuncGroup.add(radioButtonCheckMbfuncOff);

        // Выбор check-mbfunc
        radioButtonForceOn.setSelected(true);
        forceGroup = new ButtonGroup();
        forceGroup.add(radioButtonForceOn);
        forceGroup.add(radioButtonForceOff);

        // Выбор узла
        comboBoxChooseNode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateTypePanels();
            }
        });

        // Панель выбора типов слэйва
        // Для добавления новой панели в дизайнере обязательно задать поле name
        // иначе панель не добавится
        Component[] components =  cardLayoutTypes.getComponents();
        cardLayoutTypes.removeAll();
        comboBoxType.addItem(NO_ITEM);
        for(Component component : components) {
            JPanel panel = (JPanel) component;
            cardLayoutTypes.add(panel, panel.getName());
            comboBoxType.addItem(panel.getName());
        }
        comboBoxType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateTypePanels();
            }
        });

        // Обновление доступности кнопки ok если выбран TCP
        JTextField[]  commonTextFields = {
                filterFieldTextField, filterValueTextField, prefixTextField, nameTextField};
        filterFieldTextField.addFocusListener(new FocusManagerTextField(filterFieldTextField, Settings.MBS_FILTER_FIELD, REGEX_ALL));
        prefixTextField.addFocusListener(new FocusManagerTextField(prefixTextField, Settings.MBS_PREFIX, ".*_$"));

        JTextField[] TCPTextFields = {IPAddrTextField, portTextField};
        IPAddrTextField.addFocusListener(new FocusManagerTextField(IPAddrTextField, Settings.MBS_TCP_IP_ADDRESS, REGEX_ALL));
        portTextField.addFocusListener(new FocusManagerTextField(portTextField, Settings.MBS_TCP_PORT, REGEX_ONLY_NUMBERS));
        JTextField[] commonTCPTextFields = Stream.concat(Arrays.stream(commonTextFields), Arrays.stream(TCPTextFields)).toArray(JTextField[]::new);
        allowButtonOkTCP = new ButtonAllower(new ButtonAllowerOKAdapter(this), commonTCPTextFields);

        JTextField[] RTUTextFields = {devTextField, speedTextField};
        devTextField.addFocusListener(new FocusManagerTextField(devTextField, Settings.MBS_RTU_DEV, REGEX_ALL));
        speedTextField.addFocusListener(new FocusManagerTextField(speedTextField, Settings.MBS_RTU_SPEED, REGEX_ONLY_NUMBERS));
        JTextField[] commonRTUTextFields = Stream.concat(Arrays.stream(commonTextFields), Arrays.stream(RTUTextFields)).toArray(JTextField[]::new);
        allowButtonOkRTU = new ButtonAllower(new ButtonAllowerOKAdapter(this), commonRTUTextFields);

        allowButtonOkNone = new ButtonAllower(new ButtonAllowerOKAdapter(this), commonTextFields);
        allowButtonOkNone.addAdditionalCondition(() -> !getType().equals(NO_ITEM));
        // Инициализируем доступные узлы
        this.setAllowNodes(syncNodes(configure.getNodeList(), smemory.getStartScripts()));

        updateTypePanels();
    }

    public void offFocusManagerTextField()
    {
        JTextField[] textFields = {
                nameTextField,
                filterFieldTextField,
                filterValueTextField,
                prefixTextField,
                IPAddrTextField,
                portTextField,
                devTextField,
                speedTextField
        };
        for(JTextField textField : textFields)
        {
            for(FocusListener listener : textField.getFocusListeners())
            {
                if(listener instanceof FocusManagerTextField)
                {
                    ((FocusManagerTextField) listener).off();
                }
            }
        }
    }

    public void setObject(MBSObject object) {
        saveAdapter = new SaveAdapter(this, object);
        updateTypePanels();
        comboBoxChooseNode.setEnabled(false);
    }

    public void addChangeListener(ChangeListener listener)
    {
        saveAdapter.addChangeListener(listener);
    }

    private void selectNodeOf(MBSObject object) {
        Node node = object.getNode();
        if(node != null)
            comboBoxChooseNode.setSelectedItem(node);
        else
            comboBoxChooseNode.setSelectedItem(NO_EXIST_NODE);
    }

    public boolean isCheckMbFunc()
    {
        return radioButtonCheckMbfuncOn.isSelected();
    }

    public boolean isForce()
    {
        return radioButtonForceOn.isSelected();
    }

    public String getName() {
        return nameTextField.getText();
    }

    public String getIPAddr() {
        return IPAddrTextField.getText();
    }

    public String getPort() {
        return portTextField.getText();
    }

    public String getFilterField() {
        return filterFieldTextField.getText();
    }

    public String getFilterValue() {
        return filterValueTextField.getText();
    }

    public String getPrefix() {
        return prefixTextField.getText();
    }

    public String getDev() {
        return devTextField.getText();
    }

    public String getSpeed() {
        return speedTextField.getText();
    }

    public String getType()
    {
        return comboBoxType.getSelectedItem().toString();
    }

    public Node getSelectedNode()
    {
        return (Node) comboBoxChooseNode.getSelectedItem();
    }

    public void setAllowNodes(List<Node> nodes)
    {
        this.nodes = new ArrayList<>(nodes);
        this.nodes.add(0, NO_EXIST_NODE);
        updateNodesComboBox();
        updateEnablePanel(nodeConfig, true);
    }

    private List<Node> syncNodes(List<Node> nodes, List<NodeScript> scripts)
    {
        List<Node> list = new ArrayList<>();
        for(Node node : nodes)
        {
            for(NodeScript script : scripts)
            {
                if(node.getName().equals(script.getNodeName())) {
                    list.add(node);
                    break;
                }
            }
        }

        return list;
    }

    private void selectType(String name)
    {

        if(name != null && name.equals("TCP"))
        {
            allowButtonOkTCP.setWork(true);
            allowButtonOkRTU.setWork(false);
            allowButtonOkNone.setWork(false);
        }
        else if(name != null && name.equals("RTU"))
        {
            allowButtonOkTCP.setWork(false);
            allowButtonOkRTU.setWork(true);
            allowButtonOkNone.setWork(false);
        }
        else
        {
            allowButtonOkTCP.setWork(false);
            allowButtonOkRTU.setWork(false);
            allowButtonOkNone.setWork(true);
            updateEnablePanel(cardLayoutTypes, false);
            name = NO_ITEM;
        }

        comboBoxType.setSelectedItem(name);
        CardLayout cardLayout = (CardLayout) cardLayoutTypes.getLayout();
        cardLayout.show(cardLayoutTypes, name);
    }

    private void updateNodesComboBox()
    {
        comboBoxChooseNode.removeAllItems();
        for(Node node : nodes)
            comboBoxChooseNode.addItem(node);
    }

    private void updateEnablePanel(JPanel panel, boolean state)
    {
        for (Component component : panel.getComponents()) {

            if(component instanceof JPanel)
                updateEnablePanel((JPanel) component, state);

            component.setEnabled(state);
        }
    }

    private void updateTypePanels()
    {
        String name = comboBoxType.getSelectedItem().toString();
        CardLayout cardLayout = (CardLayout) cardLayoutTypes.getLayout();
        cardLayout.show(cardLayoutTypes, name);
        updateEnablePanel(cardLayoutTypes, true);

        if(name.equals("TCP")) {
            allowButtonOkTCP.setWork(true);
            allowButtonOkRTU.setWork(false);
            allowButtonOkNone.setWork(false);

        }
        else if(name.equals("RTU")){
            allowButtonOkTCP.setWork(false);
            allowButtonOkRTU.setWork(true);
            allowButtonOkNone.setWork(false);
        }
        else
        {
            allowButtonOkTCP.setWork(false);
            allowButtonOkRTU.setWork(false);
            allowButtonOkNone.setWork(true);
            updateEnablePanel(cardLayoutTypes, false);
        }
    }

    @Override
    protected void doOKAction() {
        NodeScript script = smemory.getStartScript(getSelectedNode().getName());
        if(script == null)
        {
            Messages.showMessageDialog(project,
                    "The " + getSelectedNode().getName() + " script is not found",
                    TITLE, Messages.getErrorIcon());
            return;
        }

        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }
    public JComponent getMainPanel()
    {
        return mainPanel;
    }

    public static class SaveAdapter extends ChangeAction
    {
        private MBSObject object;
        private AddMBSDialog dialog;

        public SaveAdapter(AddMBSDialog dialog, MBSObject object) {

            this.object = object;
            this.dialog = dialog;
            init();
        }

        private void addSaveChangeEvent(JTextField textField)
        {
            textField.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEvent(JRadioButton radioButton)
        {
            radioButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEventString(JComboBox<String> comboBox)
        {
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEventNode(JComboBox<Node> comboBox)
        {
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void init()
        {
            dialog.selectNodeOf(object);
            setValueTextField(dialog.nameTextField, this.object.getName());
            setValueTextField(dialog.filterFieldTextField, this.object.getFilterField());
            setValueTextField(dialog.filterValueTextField, this.object.getFilterValue());
            setValueTextField(dialog.prefixTextField, this.object.getPrefix());
            setValueTextField(dialog.IPAddrTextField, this.object.getTcpIpAddr());
            setValueTextField(dialog.portTextField, this.object.getTcpPort());
            setValueTextField(dialog.devTextField, this.object.getRtuDev());
            setValueTextField(dialog.speedTextField, this.object.getRtuSpeed());

            dialog.forceGroup.clearSelection();
            dialog.radioButtonForceOn.setSelected(this.object.isForce());
            dialog.radioButtonForceOff.setSelected(!this.object.isForce());
            dialog.checkMbfuncGroup.clearSelection();
            dialog.radioButtonCheckMbfuncOn.setSelected(this.object.isCheckMbFunc());
            dialog.radioButtonCheckMbfuncOff.setSelected(!this.object.isCheckMbFunc());
            dialog.selectType(this.object.getType());

            addSaveChangeEvent(dialog.nameTextField);
            addSaveChangeEvent(dialog.filterFieldTextField);
            addSaveChangeEvent(dialog.filterValueTextField);
            addSaveChangeEvent(dialog.prefixTextField);
            addSaveChangeEvent(dialog.IPAddrTextField);
            addSaveChangeEvent(dialog.portTextField);
            addSaveChangeEvent(dialog.devTextField);
            addSaveChangeEvent(dialog.speedTextField);
            addSaveChangeEvent(dialog.radioButtonForceOn);
            addSaveChangeEvent(dialog.radioButtonForceOff);
            addSaveChangeEvent(dialog.radioButtonCheckMbfuncOn);
            addSaveChangeEvent(dialog.radioButtonCheckMbfuncOff);
            addSaveChangeEventString(dialog.comboBoxType);
            addSaveChangeEventNode(dialog.comboBoxChooseNode);
        }

        private void applyChanged()
        {
            if(!dialog.getName().equals(this.object.getName()))
                this.object.setName(dialog.getName());
            if(!dialog.getFilterField().equals(this.object.getFilterField()))
                this.object.setFilterField(dialog.getFilterField());
            if(!dialog.getFilterValue().equals(this.object.getFilterValue()))
                this.object.setFilterValue(dialog.getFilterValue());
            if(!dialog.getPrefix().equals(this.object.getPrefix()))
                this.object.setPrefix(dialog.getPrefix());
            if(!dialog.getIPAddr().equals(this.object.getTcpIpAddr()))
                this.object.setTcpIpAddr(dialog.getIPAddr());
            if(!dialog.getPort().equals(this.object.getTcpPort()))
                this.object.setTcpPort(dialog.getPort());
            if(!dialog.getDev().equals(this.object.getRtuDev()))
                this.object.setRtuDev(dialog.getDev());
            if(!dialog.getSpeed().equals(this.object.getRtuSpeed()))
                this.object.setRtuSpeed(dialog.getSpeed());

            this.object.setForce(dialog.isForce());
            this.object.setCheckMbFunc(dialog.isCheckMbFunc());

            String type = (String) dialog.comboBoxType.getSelectedItem();
            if(type != null && type.equals("TCP"))
                this.object.setType(MBSObject.Type.TCP);
            else if(type != null && type.equals("RTU"))
                this.object.setType(MBSObject.Type.RTU);
            else if(type != null && type.equals(NO_ITEM))
                this.object.setType(MBSObject.Type.NONE);

            Node node = dialog.getSelectedNode();
            if(node == NO_EXIST_NODE)
                this.object.setNode(null);
            else
                this.object.setNode(dialog.getSelectedNode());

            fireChangeEvent();
        }

        private void setValueTextField(JTextField textField, String value) {
            if(value == null || value.isEmpty())
                textField.setText("");
            else
                textField.setText(value);
        }
    }


}
