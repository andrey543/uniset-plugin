package com.uniset.plugin.gui.additional;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.fileChooser.FileChooser;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.Settings;
import org.bouncycastle.util.Arrays;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class SettingAddUnisetObjectDialog extends DialogWrapper {
    private JPanel mainPanel;
    private JButton addButton;
    private JButton deleteButton;
    private JTable table1;
    private DefaultTableModel model = new DefaultTableModel();

    public SettingAddUnisetObjectDialog(final Project project) {
        super(project);
        setTitle("Settings of adding uniset object");
        init();

        String[] headers = {"Name", "Path"};
        model.setColumnIdentifiers(headers);
        table1.setModel(model);


        addButton.addActionListener(e -> {
            FileChooserDescriptor descriptor = new FileChooserDescriptor(
                    false,
                    true,
                    false,
                    false,
                    false,
                    false);
            VirtualFile file = FileChooser.chooseFile(descriptor, project, null);


            if(file != null)
            {
                if(isExists(file))
                    return;

                String [] row = {file.getName(), file.getPath()};
                model.addRow(row);
            }
        });

        deleteButton.addActionListener(e -> {
            if (model.getRowCount() > 0) {

                int row = table1.getSelectedRow();
                if(row == -1)
                    return;
                model.removeRow(row);
            }
        });

        updateTable();
    }

    private void updateTable()
    {
        PropertiesComponent component = PropertiesComponent.getInstance();
        String [] values = component.getValues(Settings.PROP_ATTACH_PROJECTS);

        clearTable();
        if(values != null) {
            for (String s : values) {
                String name = s.split(" ")[0];
                String path = s.split(" ")[1];

                String[] row = {name, path};
                model.addRow(row);
            }
        }
        table1.updateUI();

    }
    private void clearTable()
    {
        int count = model.getRowCount();
        for(int i = 0; i < count; i++)
        {
            model.removeRow(0);
        }
    }

    private boolean isExists(VirtualFile file)
    {
        for(int i = 0; i < table1.getRowCount(); i++)
        {
            String name = (String) model.getValueAt(i, 0);
            String path = (String) model.getValueAt(i, 1);

            if(name.equals(file.getName()) && path.equals(file.getPath()))
                return true;
        }

        return false;
    }

    private void save()
    {
        String [] values = null;
        for(int i = 0; i < table1.getRowCount(); i++)
        {
            String name = (String) model.getValueAt(i, 0);
            String path = (String) model.getValueAt(i, 1);
            String value = name + " " + path;
            values = Arrays.append(values, value);
        }
        PropertiesComponent.getInstance().setValues(Settings.PROP_ATTACH_PROJECTS, values);
    }

    @Override
    protected void doOKAction ()
    {
        save();
        super.doOKAction();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }
}
