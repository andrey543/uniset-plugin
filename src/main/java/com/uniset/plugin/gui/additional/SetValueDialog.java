package com.uniset.plugin.gui.additional;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class SetValueDialog extends DialogWrapper {
    private JPanel mainPanel;
    private JTextField enterValueTextField;

    private Project project;

    public SetValueDialog(final Project project) {
        super(project);
        this.project = project;
        setTitle("Select sensor");
        init();
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        enterValueTextField.requestFocusInWindow();
        return mainPanel;
    }

    @Override
    public @Nullable JComponent getPreferredFocusedComponent() {
        return enterValueTextField;
    }

    public String getValue()
    {
        return enterValueTextField.getText();
    }
}
