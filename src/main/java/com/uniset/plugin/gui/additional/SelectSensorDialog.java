package com.uniset.plugin.gui.additional;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.modules.Finder;
import com.uniset.plugin.gui.modules.SensorTableModel;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.MouseEvent;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;

public class SelectSensorDialog extends DialogWrapper {

    private JPanel mainPanel;
    private JTable sensorTable;
    private JTextField textFieldFind;
    private JLabel labelFind;
    private SensorTableModel sensorTableModel = new SensorTableModel();

    private Configure configure;

    public SelectSensorDialog(final Project project, Configure configure, int LIST_SELECTION_MODEL)
    {
        this(project, configure);
        sensorTable.setSelectionMode(LIST_SELECTION_MODEL);
    }
    public SelectSensorDialog(final Project project, Configure configure)
    {
        super(project);
        this.configure = configure;

        setTitle("Select sensor");
        init();

        /** Таблица датчиков */
        sensorTable.setModel(sensorTableModel);
        sensorTable.setDefaultEditor(Object.class, null);
        sensorTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(sensorTable, 10, 39, 49, 2);

        String lastTextFind = PropertiesComponent.getInstance().getValue(Settings.PROP_LAST_FILTER);
        textFieldFind.setText(lastTextFind);
        textFieldFind.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateTable();
            }
        });


        sensorTable.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getClickCount() >= 2)
                {
                    SelectSensorDialog.this.doOKAction();
                }
            }
        });

        updateTable();

    }

    public JTable getSensorTable() {
        return sensorTable;
    }

    public Sensor getSelectedSensor()
    {
        return sensorTableModel.getSensor(sensorTable.getSelectedRow());
    }

    public Sensor[] getSelectedSensors()
    {
        return sensorTableModel.getSensors(sensorTable.getSelectedRows());
    }

    public void addSelectionListener(ListSelectionListener listSelectionListener)
    {
        sensorTable.getSelectionModel().addListSelectionListener(listSelectionListener);
    }

    private void updateTable()
    {
        sensorTableModel.clear();
        sensorTableModel.addAllSensors(Finder.filterSensors(configure.getSensorList(), textFieldFind.getText()));
        PropertiesComponent.getInstance().setValue(Settings.PROP_LAST_FILTER, textFieldFind.getText());
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    public JComponent getMainPanel()
    {
        return mainPanel;
    }
}
