package com.uniset.plugin.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.Settings;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.gui.modules.ButtonAllowerOKAdapter;
import com.uniset.plugin.gui.modules.FocusManagerTextField;

import javax.swing.*;

public class NewNodeDialog extends DialogWrapper {
    private JTextField nameTextField;
    private JTextField textnameTextField;
    private JTextField IPAddressTextField;
    private JPanel mainPanel;

    private Project project;
    private Configure configure;

    private ButtonAllower okButtonAllower;
    private FocusManagerTextField focusManagerTextField;

    private final String TITLE;
    public NewNodeDialog(String title, final Project project, final Configure configure)
    {
        super(project);
        setTitle(title);
        init();

        this.project = project;
        this.configure = configure;
        this.TITLE = title;

        JTextField[] textFields = {nameTextField, textnameTextField, IPAddressTextField};
        okButtonAllower = new ButtonAllower(new ButtonAllowerOKAdapter(this), textFields);

        focusManagerTextField =
                new FocusManagerTextField(
                        IPAddressTextField,
                        "127.0.0.1",
                        Settings.REGEX_CORRECT_IP);
        IPAddressTextField
                .addFocusListener(focusManagerTextField);
    }

    @Override
    protected void doOKAction() {
        for(Node node : configure.getNodeList()) {
            if (node.getName().equals(nameTextField.getText())) {
                Messages.showMessageDialog(project,
                        "The node " + node.getName() + " is already exist.",
                        TITLE, Messages.getErrorIcon());
                return;
            }
        }
        if(!focusManagerTextField.isCorrect()) {
            Messages.showMessageDialog(project,
                    "IP address " + IPAddressTextField.getText() + " is not correct.",
                    TITLE, Messages.getErrorIcon());
            return;
        }

        super.doOKAction();
    }

    @Override
    protected JComponent createCenterPanel() {
        return mainPanel;
    }

    public String getName() {
        return nameTextField.getText();
    }

    public String getTextname() {
        return textnameTextField.getText();
    }

    public String getIPAddress() {
        return IPAddressTextField.getText();
    }
}
