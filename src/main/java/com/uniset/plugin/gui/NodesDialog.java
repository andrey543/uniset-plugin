package com.uniset.plugin.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.environment.EnvironmentNodes;
import com.uniset.plugin.gui.additional.SelectSensorDialog;
import com.uniset.plugin.gui.modules.ChangeAction;
import com.uniset.plugin.gui.modules.ChangeListener;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;

public class NodesDialog extends DialogWrapper {
    private JPanel mainPanel;
    private JTable nodesTable;
    private JTabbedPane tabbedPane1;
    private JTable sensorTable;
    private JButton delButton;
    private JButton addButton;
    private JTextField respondSensor1TextField;
    private JTextField nameTextField;
    private JTextField respondSensor2TextField;
    private JTextField IPAddressTextField;
    private JTextField respondSensorTextField;
    private JTextField textnameTextField;
    private JButton selectRespondSensor;
    private JButton selectRespondSensor1;
    private JButton selectRespondSensor2;
    private JTextField channelSensorTextField;
    private JTextField lostPacketsSensor1TextField;
    private JTextField lostPacketsSensor2TextField;
    private JButton selectChanelSensor;
    private JButton selectLostPacketsSensor1;
    private JButton selectLostPacketsSensor2;
    private NodesTableModel nodesTableModel = new NodesTableModel();
    private UnetSensorsModel unetSensorsModel = new UnetSensorsModel();
    private ChangeNotificator changeNotificator = new ChangeNotificator();
    private Configure configure;
    private Project project;
    private EnvironmentNodes environmentNodes;
    private SaveAdapter saveAdapter;

    private static final String TITLE = "Nodes";

    public NodesDialog(@Nullable Project project, final EnvironmentNodes environmentNodes) {
        super(project);
        setTitle(TITLE);
        init();

        this.project = project;
        this.configure = environmentNodes.getConfigure();
        this.environmentNodes = environmentNodes;

        /** Таблица узлов */
        nodesTable.setDefaultEditor(Object.class, null);
        nodesTable.setModel(nodesTableModel);
        nodesTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(nodesTable, 50,50);

        /** Обработка нажатий в таблице nodes-объектов */
        saveAdapter = new SaveAdapter(this);
        nodesTable.getSelectionModel().addListSelectionListener(e -> {
            if(e.getValueIsAdjusting())
                return;
            DefaultListSelectionModel selectedObject = (DefaultListSelectionModel) e.getSource();
            if(selectedObject == null)
                return;

            saveAdapter.setNode(getSelectedNode());
            updateUnetSensors(getSelectedNode());
        });

        /** Таблица датчиков */
        sensorTable.setDefaultEditor(Object.class, null);
        sensorTable.setModel(unetSensorsModel);
        sensorTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        setJTableColumnsWidth(sensorTable, 20, 35, 45);

        /** Кнопки add и del*/
        addButton.addActionListener(e -> {
            SelectSensorDialog dialog = new SelectSensorDialog(project, configure, ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            dialog.setResizable(true);
            dialog.pack();
            dialog.show();
            if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
                for(Sensor sensor : dialog.getSelectedSensors())
                    getSelectedNode().addSensor(sensor);
                updateUnetSensors(getSelectedNode());
                changeNotificator.fireChangeEvent();
            }
        });

        sensorTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                deleteSelectedSensors();
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        delButton.addActionListener(e -> {
            deleteSelectedSensors();
        });

        /** Выбор датчиков */
        HashMap<JButton, JTextField> map = new HashMap<>();
        map.put(selectRespondSensor, respondSensorTextField);
        map.put(selectRespondSensor1, respondSensor1TextField);
        map.put(selectRespondSensor2, respondSensor2TextField);
        map.put(selectChanelSensor, channelSensorTextField);
        map.put(selectLostPacketsSensor1, lostPacketsSensor1TextField);
        map.put(selectLostPacketsSensor2, lostPacketsSensor2TextField);
        addListenerForSelectButtons(map);

        updateNodes();
    }

    public void addChangeListener(ChangeListener listener)
    {
        changeNotificator.addChangeListener(listener);
        saveAdapter.addChangeListener(listener);
    }

    public JComponent getMainPanel()
    {
        return mainPanel;
    }

    public Node getSelectedNode()
    {
        return nodesTableModel.getNode(nodesTable.getSelectedRow());
    }

    public Sensor getSelectedSensor()
    {
        return unetSensorsModel.getUnetSensor(sensorTable.getSelectedRow());
    }

    public Sensor[] getSelectedSensors()
    {
        return unetSensorsModel.getUnetSensors(sensorTable.getSelectedRows());
    }

    private void addListenerForSelectButtons(HashMap<JButton, JTextField> map)
    {
        for(Map.Entry<JButton, JTextField> entry : map.entrySet())
        {
            entry.getKey().addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SelectSensorDialog dialog = new SelectSensorDialog(project, configure);
                    dialog.setResizable(true);
                    dialog.pack();
                    dialog.show();
                    if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
                        entry.getValue().setText(dialog.getSelectedSensor().getName());
                    }
                }
            });
        }
    }
    private void deleteSelectedSensors()
    {
        Sensor[] sensors = getSelectedSensors();
        for(Sensor sensor : sensors)
            getSelectedNode().removeSensor(sensor);
        updateUnetSensors(getSelectedNode());
        changeNotificator.fireChangeEvent();
    }
    private void updateNodes() {

        nodesTableModel.clear();
        for (Node node : environmentNodes.getNodes())
        {
            nodesTableModel.addNode(node);
        }
    }

    private void updateNodeSettings(Node node)
    {
        // Main
        nameTextField.setText(node.getName());
        textnameTextField.setText(node.getTextname());
        IPAddressTextField.setText(node.getIp());

        // Advanced
        respondSensorTextField.setText(node.getNotRespondSensor());
        respondSensor1TextField.setText(node.getNotRespondSensor1());
        respondSensor2TextField.setText(node.getNotRespondSensor2());
        channelSensorTextField.setText(node.getNumChanelSensor());
        lostPacketsSensor1TextField.setText(node.getLostPacketsSensor1());
        lostPacketsSensor2TextField.setText(node.getLostPacketsSensor2());

    }
    private void updateUnetSensors(Node node)
    {
        unetSensorsModel.clear();
        unetSensorsModel.addUnetSensors(node.getSensors());
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    private static class NodesTableModel extends AbstractTableModel
    {
        private List<Node> nodes = new ArrayList<>();
        String[] nodeHeaders = {"id", "name"};

        @Override
        public int getRowCount() {
            return nodes.size();
        }

        @Override
        public int getColumnCount() {
            return nodeHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return nodeHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Node node = nodes.get(rowIndex);
            Object value = "";
            if(node != null) {
                switch (columnIndex) {
                    case 0:
                        value = node.getId();
                        break;
                    case 1:
                        value = node.getName();
                        break;
                }
            }

            return value;
        }

        public void addNode(final Node node)
        {
            nodes.add(node);
            fireTableDataChanged();
        }

        public void clear()
        {
            nodes.clear();
            fireTableDataChanged();
        }

        public Node getNode(int row)
        {
            return nodes.get(row);
        }
    }

    private static class UnetSensorsModel extends AbstractTableModel
    {
        private List<Sensor> unetSensors = new ArrayList<>();
        String[] unetSensorHeaders = {"id", "name", "textname"};

        @Override
        public int getRowCount() {
            return unetSensors.size();
        }

        @Override
        public int getColumnCount() {
            return unetSensorHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return unetSensorHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            Sensor sensor = unetSensors.get(rowIndex);
            Object value = "";
            if(sensor != null) {
                switch (columnIndex) {
                    case 0:
                        value = sensor.getId();
                        break;
                    case 1:
                        value = sensor.getName();
                        break;
                    case 2:
                        value = sensor.getTextname();
                        break;
                }
            }

            return value;
        }

        public void addUnetSensor(final Sensor sensor) {
            unetSensors.add(sensor);
            fireTableDataChanged();
        }

        public void addUnetSensors(final List<Sensor> sensors)
        {
            unetSensors.addAll(sensors);
            fireTableDataChanged();
        }

        public void clear() {
            unetSensors.clear();
            fireTableDataChanged();
        }

        public Sensor getUnetSensor(int row) {
            return unetSensors.get(row);
        }

        public Sensor[] getUnetSensors(int[] rows) {
            Sensor[] selectedSensors = new Sensor[rows.length];
            for(int i = 0; i < rows.length; ++i)
                selectedSensors[i] = getUnetSensor(rows[i]);

            return selectedSensors;
        }
    }

    private class ChangeNotificator extends ChangeAction
    {
        @Override
        protected void fireChangeEvent() {
            super.fireChangeEvent();
        }
    }

    public static class SaveAdapter extends ChangeAction
    {
        private Node node;
        private NodesDialog dialog;
        private LinkedHashMap<JTextField, DocumentListener> jTextFieldDocumentListeners = new LinkedHashMap<>();

        public SaveAdapter(NodesDialog dialog) {

            this.dialog = dialog;
        }

        public void setNode(Node node)
        {
            removeListeners();
            this.node = node;
            init();
        }

        private void removeListener(JTextField textField)
        {
            DocumentListener listener = jTextFieldDocumentListeners.get(textField);
            if(listener != null)
            {
                textField.getDocument().removeDocumentListener(listener);
            }
        }

        private void removeListeners()
        {
            for(Map.Entry<JTextField, DocumentListener> entry : jTextFieldDocumentListeners.entrySet())
                entry.getKey().getDocument().removeDocumentListener(entry.getValue());
        }

        private void addSaveChangeEvent(JTextField textField)
        {

            DocumentListener documentListener = new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    applyChanged();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    applyChanged();
                }
            };
            textField.getDocument().addDocumentListener(documentListener);
            jTextFieldDocumentListeners.put(textField, documentListener);
        }

        private void addSaveChangeEvent(JRadioButton radioButton)
        {
            radioButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEventString(JComboBox<String> comboBox)
        {
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void addSaveChangeEventNode(JComboBox<Node> comboBox)
        {
            comboBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    applyChanged();
                }
            });
        }

        private void init()
        {
            dialog.updateNodeSettings(node);

            addSaveChangeEvent(dialog.nameTextField);
            addSaveChangeEvent(dialog.textnameTextField);
            addSaveChangeEvent(dialog.IPAddressTextField);
            addSaveChangeEvent(dialog.respondSensorTextField);
            addSaveChangeEvent(dialog.respondSensor1TextField);
            addSaveChangeEvent(dialog.respondSensor2TextField);
            addSaveChangeEvent(dialog.channelSensorTextField);
            addSaveChangeEvent(dialog.lostPacketsSensor1TextField);
            addSaveChangeEvent(dialog.lostPacketsSensor2TextField);
        }

        private void applyChanged()
        {
            if(!dialog.nameTextField.getText().equals(this.node.getName()))
                this.node.setName(dialog.nameTextField.getText());
            if(!dialog.textnameTextField.getText().equals(this.node.getTextname()))
                this.node.setTextname(dialog.textnameTextField.getText());
            if(!dialog.IPAddressTextField.getText().equals(this.node.getIp()))
                this.node.setIp(dialog.IPAddressTextField.getText());
            if(!dialog.respondSensorTextField.getText().equals(this.node.getNotRespondSensor()))
                this.node.setNotRespondSensor(dialog.respondSensorTextField.getText());
            if(!dialog.respondSensor1TextField.getText().equals(this.node.getNotRespondSensor1()))
                this.node.setNotRespondSensor1(dialog.respondSensor1TextField.getText());
            if(!dialog.respondSensor2TextField.getText().equals(this.node.getNotRespondSensor2()))
                this.node.setNotRespondSensor2(dialog.respondSensor2TextField.getText());
            if(!dialog.channelSensorTextField.getText().equals(this.node.getNumChanelSensor()))
                this.node.setNumChanelSensor(dialog.channelSensorTextField.getText());
            if(!dialog.lostPacketsSensor1TextField.getText().equals(this.node.getLostPacketsSensor1()))
                this.node.setLostPacketsSensor1(dialog.lostPacketsSensor1TextField.getText());
            if(!dialog.lostPacketsSensor2TextField.getText().equals(this.node.getLostPacketsSensor2()))
                this.node.setLostPacketsSensor2(dialog.lostPacketsSensor2TextField.getText());

            fireChangeEvent();
        }
    }
}
