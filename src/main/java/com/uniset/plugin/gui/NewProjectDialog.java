package com.uniset.plugin.gui;

import com.intellij.ide.RecentProjectsManager;
import com.intellij.openapi.fileChooser.FileChooserDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.TextFieldWithBrowseButton;
import com.intellij.openapi.vfs.VfsUtil;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NewProjectDialog extends DialogWrapper {

    private JPanel mainPanel;
    private TextFieldWithBrowseButton projectPathTextField;
    private JTextField projectNameTextField;
    private JPanel projectPanel;
    private JLabel resultLabel;

    private String resultPath;

    private static final String TITLE = "New Uniset Project";

    public NewProjectDialog(Project project, String defaultProjectName, String defaultProjectPath)
    {
        super(project);
        setTitle(TITLE);
        init();

        this.projectNameTextField.setText(defaultProjectName);
        this.projectPathTextField.setText(defaultProjectPath);

        projectPathTextField.addBrowseFolderListener("Select project directory",
                null,
                null,
                new FileChooserDescriptor(false,
                        true,
                        false,
                        false,
                        false,
                        false));

        projectNameTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                syncNameAndPath();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                syncNameAndPath();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                syncNameAndPath();
            }
        });

        projectPathTextField.getTextField().getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                syncNameAndPath();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                syncNameAndPath();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                syncNameAndPath();
            }
        });
    }

    protected boolean tryFinish() {
        String projectPath = getProjectPath();
        String projectName = getProjectName();
        resultPath  = getResultPath();

        Path newProjectPath = Paths.get(resultPath);
        if(projectName.isEmpty()) {
            Messages.showMessageDialog("Enter a file name to create a new Uniset project",
                    "New Uniset Project",
                    Messages.getErrorIcon());
            return false;
        }

        if(!Files.exists(Paths.get(projectPath)))
        {
            Messages.showMessageDialog("That path" + projectPath +" is not found",
                    "New Uniset Project",
                    Messages.getErrorIcon());
            return false;
        }

        if(Files.exists(newProjectPath))
        {
            Messages.showMessageDialog("That path" + projectPath +" is already exist",
                    "New Uniset Project",
                    Messages.getErrorIcon());
            return false;
        }

        try {
            VfsUtil.createDirectories(resultPath);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        String parentDir = newProjectPath.getParent().toString();
        if(!parentDir.isEmpty()) {
            RecentProjectsManager.getInstance().setLastProjectCreationLocation(parentDir);
            return true;
        }

        return false;
    }

    @Override
    protected void doOKAction() {
        if(tryFinish())
            super.doOKAction();
    }

    private void syncNameAndPath()
    {
        if(!getProjectPath().isEmpty())
        {
            resultPath = getProjectPath() + "/" + getProjectName();
            resultLabel.setText(resultPath);
        }
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    public String getProjectPath()
    {
        return projectPathTextField.getText();
    }

    public String getProjectName()
    {
        return projectNameTextField.getText();
    }

    public String getResultPath() {
        return resultPath;
    }
}
