package com.uniset.plugin.gui;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.packageDependencies.ui.TreeModel;
import com.uniset.plugin.build.objects.UnisetObject;
import com.uniset.plugin.environment.SensorManager;
import com.uniset.plugin.gui.modules.Finder;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;

public class SensorManagerDialog extends DialogWrapper {
    private JPanel mainPanel;
    private JTable sensorTable;
    private JButton addSensorButton;
    private JButton delSensorButton;
    private JPanel modbusPanel;
    private JPanel unisetPanel;
    private JComboBox unetComboBox;
    private JPanel unetPanel;
    private JTable modbusPropertyTable;
    private JTextField findTextField;
    private JTree unisetObjectTree;

    private SensorTableModel sensorTableModel = new SensorTableModel();
    private ModbusPropertyTableModel modbusPropertyTableModel = new ModbusPropertyTableModel();

    private SensorManager sensorManager;

    public SensorManagerDialog(@Nullable Project project, final SensorManager sensorManager) {
        super(project);
        setTitle("Sensor Manager");
        init();

        this.sensorManager = sensorManager;

        /** Таблица датчиков */
        sensorTable.setDefaultEditor(Object.class, null);
        sensorTable.setModel(sensorTableModel);
        sensorTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(sensorTable, 20, 30, 40, 10);

        /** Таблица modbus-параметров */
        modbusPropertyTable.setDefaultEditor(Object.class, null);
        modbusPropertyTable.setModel(modbusPropertyTableModel);
        modbusPropertyTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(modbusPropertyTable, 40, 12, 12, 12, 12, 12);

        /** Дерево */
        // Для кастомизирования дизайна дерева
        DefaultTreeCellRenderer treeCellRenderer = (DefaultTreeCellRenderer) unisetObjectTree.getCellRenderer();
        Icon dirIcon = null;
        Icon leafIcon = null;
        try {
            leafIcon = new ImageIcon(ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("icons/treeIcons/algFile.png")));
            dirIcon = new ImageIcon(ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("icons/treeIcons/dirAlgBase.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
        treeCellRenderer.setClosedIcon(dirIcon);
        treeCellRenderer.setOpenIcon(dirIcon);
        treeCellRenderer.setLeafIcon(leafIcon);
        unisetObjectTree.setCellRenderer(treeCellRenderer);
        unisetObjectTree.setRootVisible(false);

        /** Строка поиска */
        findTextField.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void insertUpdate(DocumentEvent e) {
                updateSensorTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updateSensorTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updateSensorTable();
            }
        });

        /** Обработка нажатий в таблице */
        sensorTable.getSelectionModel().addListSelectionListener(e -> {
            if(e.getValueIsAdjusting())
                return;
            DefaultListSelectionModel selectedObject = (DefaultListSelectionModel) e.getSource();
            if(selectedObject == null)
                return;

            updateSelectedSensor(sensorTableModel.getSensor(selectedObject.getAnchorSelectionIndex()));
        });

        updateSensorTable();
        sensorTable.setRowSelectionInterval(0, 0);
    }

    private void updateSensorTable()
    {
        sensorTableModel.clear();
        for(SensorManager.SyncSensor syncSensor :
                Finder.filterSyncSensors(sensorManager.getSyncSensors(), findTextField.getText()))
        {
            sensorTableModel.addSensor(syncSensor);
        }
    }

    private void updateModbusTable(SensorManager.SyncSensor syncSensor)
    {
        modbusPropertyTableModel.clear();
        for(SensorManager.ModbusInfo info : syncSensor.getModbusInfos()) {
            modbusPropertyTableModel.addInfo(info);
        }
    }

    private void updateUnisetObjectTable(SensorManager.SyncSensor syncSensor)
    {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) unisetObjectTree.getModel().getRoot();
        root.removeAllChildren();
        for(SensorManager.UnisetInfo info : syncSensor.getUnisetInfos())
        {
            DefaultMutableTreeNode unisetObjectNode = new DefaultMutableTreeNode(info.getUnisetObject().getName());
            for(UnisetObject.SMapSensor sMapSensor : info.getsMapSensors())
            {
                DefaultMutableTreeNode sMapSensorNode = new DefaultMutableTreeNode(sMapSensor.getName() + " " +
                        sMapSensor.getComment() + " " + sMapSensor.getIotype());
                unisetObjectNode.add(sMapSensorNode);
            }
            root.add(unisetObjectNode);
        }
        unisetObjectTree.setModel(new TreeModel(root));
    }

    private void updateSelectedSensor(SensorManager.SyncSensor syncSensor)
    {
        updateModbusTable(syncSensor);
        updateUnisetObjectTable(syncSensor);
    }

    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }

    private static class SensorTableModel extends AbstractTableModel
    {
        private List<SensorManager.SyncSensor> syncSensors = new ArrayList<>();
        String[] syncSensorsHeaders = {"id", "Name", "Textname","Type"};

        @Override
        public int getRowCount() {
            return syncSensors.size();
        }

        @Override
        public int getColumnCount() {
            return syncSensorsHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return syncSensorsHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            SensorManager.SyncSensor syncSensor = syncSensors.get(rowIndex);
            Object value = "";
            if(syncSensor != null) {
                switch (columnIndex) {
                    case 0:
                        value = syncSensor.getSensor().getId();
                        break;
                    case 1:
                        value = syncSensor.getSensor().getName();
                        break;
                    case 2:
                        value = syncSensor.getSensor().getTextname();
                        break;
                    case 3:
                        value = syncSensor.getSensor().getIotype();
                        break;
                }
            }

            return value;
        }
        public void addSensor(final SensorManager.SyncSensor sensor)
        {
            syncSensors.add(sensor);
            fireTableDataChanged();
        }

        public void addAllSensors(List<SensorManager.SyncSensor> list)
        {
            syncSensors.addAll(list);
        }

        public void clear()
        {
            syncSensors.clear();
            fireTableDataChanged();
        }

        public SensorManager.SyncSensor getSensor(int row)
        {
            return syncSensors.get(row);
        }
    }

    private static class ModbusPropertyTableModel extends AbstractTableModel
    {
        private List<SensorManager.ModbusInfo> infos = new ArrayList<>();
        String[] infosHeaders = {"modbus", "mbaddr", "mbreg", "mbfunc", "nbit"};

        @Override
        public int getRowCount() {
            return infos.size();
        }

        @Override
        public int getColumnCount() {
            return infosHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return infosHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            SensorManager.ModbusInfo info = infos.get(rowIndex);
            Object value = "";
            if(info != null) {
                switch (columnIndex) {
                    case 0:
                        value = info.getModbusObject().getFullName();
                        break;
                    case 1:
                        value = info.getAddr();
                        break;
                    case 2:
                        value = info.getReg();
                        break;
                    case 3:
                        value = info.getFunc();
                        break;
                    case 4:
                        value = info.getNbit();
                        break;
                }
            }

            return value;
        }
        public void addInfo(final SensorManager.ModbusInfo info)
        {
            infos.add(info);
            fireTableDataChanged();
        }

        public void clear()
        {
            infos.clear();
            fireTableDataChanged();
        }

        public SensorManager.ModbusInfo getSensor(int row)
        {
            return infos.get(row);
        }
    }
}
