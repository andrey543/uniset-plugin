package com.uniset.plugin.gui.environment;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.ComboBox;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.UnisetObject;
import com.uniset.plugin.build.objects.modbus.MBMMObject;
import com.uniset.plugin.build.objects.modbus.MBMObject;
import com.uniset.plugin.build.objects.modbus.MBSObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.environment.Environment;
import com.uniset.plugin.environment.EnvironmentModbus;
import com.uniset.plugin.environment.EnvironmentNodes;
import com.uniset.plugin.environment.EnvironmentUniset;
import com.uniset.plugin.gui.NodesDialog;
import com.uniset.plugin.gui.additional.SelectSensorDialog;
import com.uniset.plugin.gui.additional.SetValueDialog;
import com.uniset.plugin.gui.modbus.AddMBMDialog;
import com.uniset.plugin.gui.modbus.AddMBMMDialog;
import com.uniset.plugin.gui.modbus.AddMBSDialog;
import com.uniset.plugin.gui.modbus.ModbusSensorsDialog;
import com.uniset.plugin.gui.modules.ButtonAllower;
import com.uniset.plugin.smemory.Smemory;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static com.uniset.plugin.gui.SwingUtils.setJTableColumnsWidth;
import static java.awt.event.KeyEvent.VK_DELETE;

public class EnvironmentDialog extends DialogWrapper {
    private static final String YES_OPTION = "on";
    private static final String NO_OPTION  = "off";
    private static final String TITLE      = "Environment";

    private JPanel mainPanel;
    private JTabbedPane tabbedPane1;
    private JTable unisetObjectTable;
    private JTabbedPane tabbedPane2;
    private JTable smapSensorTable;
    private JLabel labelStructFound;
    private JTable settingsTable;
    private JButton selectSensorButton;
    private JTable paramsTable;
    private JButton paramsSetValueButton;
    private JTable modbusObjectsTable;
    private JPanel modbusDialogPanel;
    private JLabel wasChangeLabel;
    private JTabbedPane tabbedPane3;
    private JPanel sensorsDialogPanel;
    private JPanel nodesPanel;

    private Project project;
    private Configure configure;
    private Smemory smemory;

    // Uniset-objects
    private UnisetObjectTableModel unisetObjectTableModel = new UnisetObjectTableModel();
    private SmapSensorTableModel smapSensorTableModel = new SmapSensorTableModel();
    private ParamsTableModel paramsTableModel = new ParamsTableModel();
    private DefaultTableModel settingsTableModel = new DefaultTableModel();

    //Modbus-objects
    private ModbusObjectTableModel modbusObjectTableModel = new ModbusObjectTableModel();
    private AddMBMDialog mbmDialog;
    private AddMBMMDialog mbmmDialog;
    private AddMBSDialog mbsDialog;
    private ModbusSensorsDialog sensorsDialog;

    private Environment.EnvironmentSaveBox environmentNodes;
    private Environment.EnvironmentSaveBox environmentUniset;
    private Environment.EnvironmentSaveBox environmentModbus;


    private boolean isEnvironmentToSave = false;

    public EnvironmentDialog(@Nullable Project project,
                             final Configure configure,
                             final Smemory smemory,
                             final Environment.EnvironmentSaveBox environmentNodes,
                             final Environment.EnvironmentSaveBox environmentUniset,
                             final Environment.EnvironmentSaveBox environmentModbus)
    {
        super(project);
        setTitle("Uniset project environment ");
        init();

        this.project = project;
        this.configure = configure;
        this.smemory = smemory;
        this.environmentNodes  = environmentNodes;
        this.environmentUniset = environmentUniset;
        this.environmentModbus = environmentModbus;

        /**
         *
         * Uniset-объекты
         *
         * */

        /** Таблица uniset-объектов */
        unisetObjectTable.setDefaultEditor(Object.class, null);
        unisetObjectTable.setModel(unisetObjectTableModel);
        unisetObjectTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(unisetObjectTable, 18, 40, 42);

        /** Таблица smap-датчиков */
        smapSensorTable.setModel(smapSensorTableModel);
        smapSensorTable.setDefaultEditor(Object.class, null);
        smapSensorTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(smapSensorTable, 1, 35, 1, 1, 9, 51, 1, 1);

        /** Таблица настроек */
        String[] settingsHeaders = {"Property", "Value"};
        settingsTableModel.setColumnIdentifiers(settingsHeaders);
        settingsTable.setDefaultEditor(Object.class, null);
        settingsTable.setModel(settingsTableModel);
        settingsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        /** Таблица параметров */
        paramsTable.setModel(paramsTableModel);
        paramsTable.setDefaultEditor(Object.class, null);
        paramsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(paramsTable, 7, 3, 60, 15, 15);

        updateUnisetObjectTable();

        /** Обработка нажатий в таблице датчиков */
        ButtonAllower selectButtonAllower = new ButtonAllower(selectSensorButton);
        selectButtonAllower.addAdditionalCondition(() -> smapSensorTable.getSelectedRow() >= 0);
        smapSensorTable.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                selectButtonAllower.updateAllow();
                if (e.getClickCount() >= 2)
                {
                    applySelectedSensor();
                }
            }
        });
        smapSensorTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == VK_DELETE )
                {
                    smapSensorTableModel.deleteAttachSensor(getSelectedSMapSensor());
                    /** Отслеживание изменений */
                    wasChangeLabel.setVisible(hasChanged());
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        selectSensorButton.addActionListener(e -> applySelectedSensor());

        /** Обработка нажатий в таблице параметров */
        ButtonAllower paramsSetValueButtonAllower = new ButtonAllower(paramsSetValueButton);
        paramsSetValueButtonAllower.addAdditionalCondition(() -> paramsTable.getSelectedRow() >= 0);
        paramsSetValueButton.addActionListener(e -> applySettedValue());

        paramsTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == VK_DELETE )
                {
                    paramsTableModel.deleteParam(getSelectedParam());
                    /** Отслеживание изменений */
                    wasChangeLabel.setVisible(hasChanged());
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        paramsTable.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                paramsSetValueButtonAllower.updateAllow();
                if(e.getClickCount() >= 2)
                {
                    applySettedValue();
                }
            }
        });

        /** Обработка нажатий в таблице uniset-объектов */
        unisetObjectTable.getSelectionModel().addListSelectionListener(e -> {
            if(e.getValueIsAdjusting())
                return;
            DefaultListSelectionModel selectedObject = (DefaultListSelectionModel) e.getSource();
            if(selectedObject == null)
                return;

            updateSelectedUnisetObjectStructure(unisetObjectTableModel.getObject(selectedObject.getAnchorSelectionIndex()));
        });
        unisetObjectTable.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                selectButtonAllower.updateAllow();
                paramsSetValueButtonAllower.updateAllow();
            }
        });

        /**
         *
         * Modbus-объекты
         *
         * */

        /** Таблица modbus-объектов */
        modbusObjectsTable.setDefaultEditor(Object.class, null);
        modbusObjectsTable.setModel(modbusObjectTableModel);
        modbusObjectsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setJTableColumnsWidth(modbusObjectsTable, 18, 40, 42);

        /** Обработка нажатий в таблице modbus-объектов */
        modbusObjectsTable.getSelectionModel().addListSelectionListener(e -> {
            if(e.getValueIsAdjusting())
                return;
            DefaultListSelectionModel selectedObject = (DefaultListSelectionModel) e.getSource();
            if(selectedObject == null)
                return;

            updateSelectedModbusObjectStructure(modbusObjectTableModel.getObject(selectedObject.getAnchorSelectionIndex()));
        });
        updateModbusObjectsTable();

        /**
         *
         * Nodes
         *
         * */
        NodesDialog nodesDialog = new NodesDialog(project, (EnvironmentNodes) environmentNodes.getEnvironment());
        nodesDialog.addChangeListener(() -> wasChangeLabel.setVisible(hasChanged()));
        nodesPanel.add(nodesDialog.getMainPanel());

        /** Отслеживание изменений */
        wasChangeLabel.setVisible(hasChanged());
    }

    public boolean isEnvironmentToSave() {
        return isEnvironmentToSave;
    }

    public boolean hasChanged()
    {
        return environmentNodes.hasChanged() || environmentUniset.hasChanged() || environmentModbus.hasChanged();
    }

    private void updateUnisetObjectTable()
    {
        unisetObjectTableModel.clear();
        for(UnisetObject object : ((EnvironmentUniset) environmentUniset.getEnvironment()).getUnisetObjects())
            unisetObjectTableModel.addObject(object);
    }

    private void updateModbusObjectsTable()
    {
        modbusObjectTableModel.clear();
        for(ModbusObject object :((EnvironmentModbus) environmentModbus.getEnvironment()).getModbusObjects())
        {
            modbusObjectTableModel.addObject(object);
        }
    }

    private void applySelectedSensor()
    {
        int row = smapSensorTable.getSelectedRow();
        SelectSensorDialog dialog = new SelectSensorDialog(project, configure);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            UnisetObject selectedObject = getSelectedObject();

            if(selectedObject != null) {
                smapSensorTableModel.setAttachSensor(
                        smapSensorTableModel.getSensor(smapSensorTable.getSelectedRow()),
                        dialog.getSelectedSensor());
                smapSensorTable.setRowSelectionInterval(row, row);
            }
        }
        /** Отслеживание изменений */
        wasChangeLabel.setVisible(hasChanged());

    }

    private void applySettedValue()
    {
        int row = smapSensorTable.getSelectedRow();
        SetValueDialog dialog = new SetValueDialog(project);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
        {
            paramsTableModel.setValue(paramsTableModel.getParam(paramsTable.getSelectedRow()), dialog.getValue());
            paramsTable.setRowSelectionInterval(row, row);
        }
        /** Отслеживание изменений */
        wasChangeLabel.setVisible(hasChanged());
    }

    private void updateSelectedModbusObjectStructure(ModbusObject object)
    {
        if(object == null)
            return;

        /** Sensors */
        sensorsDialogPanel.removeAll();
        if(sensorsDialog == null) {
            sensorsDialog = new ModbusSensorsDialog(project, configure);
            sensorsDialog.addChangeListener(() -> wasChangeLabel.setVisible(hasChanged()));
        }
        sensorsDialogPanel.add(sensorsDialog.getMainPanel());
        sensorsDialog.setModbusObject(object);
        sensorsDialogPanel.updateUI();

        /** Settings */
        modbusDialogPanel.removeAll();
        if(object instanceof MBMObject)
        {

            mbmDialog = new AddMBMDialog(project, configure, smemory);
            mbmDialog.offFocusManagerTextField();
            modbusDialogPanel.add(mbmDialog.getMainPanel());
            mbmDialog.setObject((MBMObject) object);
            mbmDialog.addChangeListener(() -> wasChangeLabel.setVisible(hasChanged()));
        }
        else if (object instanceof MBMMObject)
        {

            mbmmDialog = new AddMBMMDialog(project, configure, smemory);
            mbmmDialog.offFocusManagerTextField();
            modbusDialogPanel.add(mbmmDialog.getMainPanel());
            mbmmDialog.setObject((MBMMObject) object);
            mbmmDialog.addChangeListener(() -> wasChangeLabel.setVisible(hasChanged()));

        }
        else if(object instanceof MBSObject)
        {
            mbsDialog = new AddMBSDialog(project, configure, smemory);
            mbsDialog.offFocusManagerTextField();
            modbusDialogPanel.add(mbsDialog.getMainPanel());
            mbsDialog.setObject((MBSObject) object);
            mbsDialog.addChangeListener(() -> wasChangeLabel.setVisible(hasChanged()));

        }

        modbusDialogPanel.updateUI();
    }

    private void updateSelectedUnisetObjectStructure(UnisetObject object)
    {
        if(object == null)
            return;

        smapSensorTableModel.clear();
        paramsTableModel.clear();
        clearTable(settingsTableModel);

        if(object.getSrcXml() == null || !Files.exists(object.getSrcXml()))
        {
            labelStructFound.setText("File " + object.getTypename().toLowerCase() + ".src.xml is not found");
            labelStructFound.setVisible(true);
            return;
        }
        else {
            labelStructFound.setVisible(false);
            labelStructFound.setText("");
        }

        /** Настройки */
        settingsTableModel.addRow(new String[] {"Uniset manager", object.getSetting().isUnisetManager() ? YES_OPTION : NO_OPTION});
        settingsTableModel.addRow(new String[] {"msg-count", String.valueOf(object.getSetting().getMsgCount())});
        settingsTableModel.addRow(new String[] {"sleep-msec", String.valueOf(object.getSetting().getSleepMsec())});

        /** Параметры */
        for(UnisetObject.Param param : object.getParams())
            paramsTableModel.addParam(param);

        /** Датчики */
        for(UnisetObject.SMapSensor sMapSensor : object.getSmapSensors())
            smapSensorTableModel.addSensor(sMapSensor);

    }

    @Override
    protected void doOKAction() {
        if(hasChanged())
        {
            int answer = JOptionPane.showConfirmDialog(this.getWindow(), "Do you want to save the changes?", "Save changes", JOptionPane.YES_NO_CANCEL_OPTION);
            if(answer == JOptionPane.YES_OPTION)
            {
                isEnvironmentToSave = true;
                super.doOKAction();
            }
            else if(answer == JOptionPane.NO_OPTION)
            {
                super.doOKAction();
            }
        }
        else
        {
            super.doOKAction();
        }
    }

    private UnisetObject.Param getSelectedParam()
    {
        return paramsTableModel.getParam(paramsTable.getSelectedRow());
    }

    private UnisetObject.SMapSensor getSelectedSMapSensor()
    {
        return smapSensorTableModel.getSensor(smapSensorTable.getSelectedRow());
    }

    private UnisetObject getSelectedObject() {
        return unisetObjectTableModel.getObject(unisetObjectTable.getSelectedRow());
    }

    private void createOnOffTableColumn(TableColumn column)
    {
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.addItem(YES_OPTION);
        comboBox.addItem(NO_OPTION);
        column.setCellEditor(new DefaultCellEditor(comboBox));
    }

    private void clearTable(DefaultTableModel model)
    {
        int count = model.getRowCount();
        for(int i = 0; i < count; i++)
        {
            model.removeRow(0);
        }

    }


    @Override
    protected @Nullable JComponent createCenterPanel() {
        return mainPanel;
    }
    private static class ModbusObjectTableModel extends AbstractTableModel
    {
        private List<ModbusObject> objects = new ArrayList<>();
        String[] objectHeaders = {"Id", "Type", "Name"};


        @Override
        public int getRowCount() {
            return objects.size();
        }

        @Override
        public int getColumnCount() {
            return objectHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return objectHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            ModbusObject object = objects.get(rowIndex);
            Object value = "";
            if(object != null) {
                switch (columnIndex) {
                    case 0:
                        value = String.valueOf(object.getId());
                        break;
                    case 1: {
                        if(object instanceof MBMObject)
                            value = "MBM";
                        else if(object instanceof MBMMObject)
                            value = "MBMM";
                        else if(object instanceof MBSObject)
                            value = "MBS";
                        break;
                    }
                    case 2:
                        value = object.getName();
                        break;
                }
            }

            return value;
        }
        public void addObject(final ModbusObject object)
        {
            objects.add(object);
            fireTableDataChanged();
        }

        public void clear()
        {
            objects.clear();
            fireTableDataChanged();
        }

        public ModbusObject getObject(int row)
        {
            return objects.get(row);
        }
    }


    private static class UnisetObjectTableModel extends AbstractTableModel
    {
        private List<UnisetObject> objects = new ArrayList<>();
        String[] objectHeaders = {"Id", "Type", "Name"};


        @Override
        public int getRowCount() {
            return objects.size();
        }

        @Override
        public int getColumnCount() {
            return objectHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return objectHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            UnisetObject object = objects.get(rowIndex);
            Object value = "";
            if(object != null) {
                switch (columnIndex) {
                    case 0:
                        value = object.getId();
                        break;
                    case 1:
                        value = object.getTypename();
                        break;
                    case 2:
                        value = object.getName();
                        break;
                }
            }

            return value;
        }
        public void addObject(final UnisetObject object)
        {
            objects.add(object);
            fireTableDataChanged();
        }

        public void clear()
        {
            objects.clear();
            fireTableDataChanged();
        }

        public UnisetObject getObject(int row)
        {
            return objects.get(row);
        }
    }

    private static class SmapSensorTableModel extends AbstractTableModel {
        private List<UnisetObject.SMapSensor> sMapSensors = new ArrayList<>();
        private String[] columnIdentifies = {"Id", "Attached sensor", "Type", "IOType", "Name", "Comment", "No check id", "Init from SM"};

        public SmapSensorTableModel()
        {

        }
        public SmapSensorTableModel(List<UnisetObject.SMapSensor> sMapSensors)
        {
            this.sMapSensors = new ArrayList<>(sMapSensors);
        }

        @Override
        public int getRowCount() {
            return sMapSensors.size();
        }

        @Override
        public int getColumnCount() {
            return columnIdentifies.length;
        }

        @Override
        public String getColumnName(int column) {
            return columnIdentifies[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            UnisetObject.SMapSensor sMapSensor = sMapSensors.get(rowIndex);
            Object value = "";
            if(sMapSensor != null) {
                Sensor attachSensor;
                switch (columnIndex) {
                    case 0:
                         attachSensor = sMapSensor.getAttachSensor();
                        if(attachSensor != null)
                            value = attachSensor.getId();
                        break;
                    case 1:
                        attachSensor = sMapSensor.getAttachSensor();
                        if(attachSensor != null)
                            value = attachSensor.getName();
                        break;
                    case 2:
                        value = sMapSensor.getVarType();
                        break;
                    case 3:
                        value = sMapSensor.getIotype();
                        break;
                    case 4:
                        value = sMapSensor.getName();
                        break;
                    case 5:
                        value = sMapSensor.getComment();
                        break;
                    case 6:
                        value = sMapSensor.isNoCheckId() ? YES_OPTION : NO_OPTION;
                        break;
                    case 7:
                        value = sMapSensor.isInitFromSm() ? YES_OPTION : NO_OPTION;
                        break;
                }
            }

            return value;
        }

        public void addSensor(final UnisetObject.SMapSensor sensor)
        {
            sMapSensors.add(sensor);
            fireTableDataChanged();
        }

        public void setAttachSensor(UnisetObject.SMapSensor sMapSensor, Sensor attachSensor)
        {
            sMapSensor.setAttachSensor(attachSensor);
            fireTableDataChanged();
        }

        public void deleteAttachSensor(UnisetObject.SMapSensor sMapSensor)
        {
            sMapSensor.setAttachSensor(null);
            fireTableDataChanged();
        }

        public void clear()
        {
            sMapSensors.clear();
            fireTableDataChanged();
        }

        public UnisetObject.SMapSensor getSensor(int row)
        {
            return sMapSensors.get(row);
        }

    }

    private static class ParamsTableModel extends AbstractTableModel
    {
        private List<UnisetObject.Param> params = new ArrayList<>();
        String[] paramsHeaders = {"Name", "Type", "Comment","Value", "Default value"};

        @Override
        public int getRowCount() {
            return params.size();
        }

        @Override
        public int getColumnCount() {
            return paramsHeaders.length;
        }

        @Override
        public String getColumnName(int column) {
            return paramsHeaders[column];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            UnisetObject.Param param = params.get(rowIndex);
            Object value = "";
            if(param != null) {
                switch (columnIndex) {
                    case 0:
                        value = param.getName();
                        break;
                    case 1:
                        value = param.getType();
                        break;
                    case 2:
                        value = param.getComment();
                        break;
                    case 3:
                        value = param.getSettedValue();
                        break;
                    case 4:
                        value = param.getDefaultValue();
                        break;
                }
            }

            return value;
        }
        public void addParam(final UnisetObject.Param param)
        {
            params.add(param);
            fireTableDataChanged();
        }

        public void deleteParam(UnisetObject.Param param)
        {
            param.setSettedValue(null);
            fireTableDataChanged();
        }

        public void clear()
        {
            params.clear();
            fireTableDataChanged();
        }

        public void setValue(UnisetObject.Param param, String value)
        {
            param.setSettedValue(value);
            fireTableDataChanged();
        }

        public UnisetObject.Param getParam(int row)
        {
            return params.get(row);
        }
    }

}
