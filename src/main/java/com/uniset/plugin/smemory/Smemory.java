package com.uniset.plugin.smemory;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.configure.Configure;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Smemory {
    private static Smemory instance;
    private Project project;
    private VirtualFile smemoryFile;
    private List<NodeScript> scripts;

    private static final String PATH_TO_SMEMORY = "/src/SharedMemory2/smemory2.cc";

    public static Smemory getInstance(final Project project) throws IOException, NodeScript.ExtParamBlockCorrupted, NodeScript.ClosedSymbolNotFound {
        if(instance == null)
            instance = new Smemory(project);
        else
            instance.initialize(project);

        return instance;
    }

    private Smemory(final Project project) throws IOException, NodeScript.ExtParamBlockCorrupted, NodeScript.ClosedSymbolNotFound {
        initialize(project);
    }

    private void initialize(final Project project) throws IOException, NodeScript.ExtParamBlockCorrupted, NodeScript.ClosedSymbolNotFound {
        this.project     = project;
        this.smemoryFile = LocalFileSystem.getInstance().findFileByIoFile(new File(project.getBasePath() + PATH_TO_SMEMORY));

        if(smemoryFile == null)
            throw new FileNotFoundException("File " + PATH_TO_SMEMORY + " is not found");

        if(FileUtils.containsFileName(smemoryFile.getParent(), "configure.xml"))
        {
            Configure.getInstance(project).createLink(smemoryFile.getParent().getPath());
        }

        scripts = initStartScripts();
    }

    public VirtualFile getSmemoryFile() {
        return smemoryFile;
    }

    public NodeScript getStartScript(String nodeName) {
        for(NodeScript script : scripts)
        {
            if(script.getNodeName().equals(nodeName))
                return script;
        }
        return null;
    }

    public List<NodeScript> getStartScripts() {
        return scripts;
    }

    private List<NodeScript> initStartScripts() throws NodeScript.ExtParamBlockCorrupted, IOException, NodeScript.ClosedSymbolNotFound {
        List<NodeScript> list = new ArrayList<>();
        for(VirtualFile file : smemoryFile.getParent().getChildren())
        {
            String regex = "ctl-smemory2-" + project.getName() + "-" + "\\w*" + "\\.sh\\.in";
            String regexLower = "ctl-smemory2-" + project.getName().toLowerCase() + "-" + "\\w*" + "\\.sh\\.in";
            String fileName = file.getName();
            if(Pattern.matches(regex,fileName)
            || Pattern.matches(regexLower,fileName))
            {
                String name = file.getName()
                        .replaceAll("ctl-smemory2-" + project.getName() + "-", "")
                        .replaceAll("ctl-smemory2-" + project.getName().toLowerCase() + "-", "")
                        .replaceAll(".sh.in", "");
                try {
                    list.add(new NodeScript(project, name));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }

    private boolean hasBlock(String regexBlock) throws IOException {
        String content  = new String(Files.readAllBytes(Paths.get(smemoryFile.getPath())));
        boolean has = Pattern.compile(regexBlock).matcher(content).find();

        return has;
    }

    public void updateContent(String newContent)
    {
        FileUtils.setContentToFile(smemoryFile, newContent);
    }

    /**
     * Добавление ModbusMaster в вектор mbm_vector если такой есть
     *
     * @param name - Имя добавляемого MBM
     *
     * @return обновленное содержимое(после добавления MBM) файла
     *
     * @throws IOException - файл не найден
     * @throws ModbusBlockIsNotFound - не найден блок(std::vector<string>) с MBM именами
     *
     */
    public String addMBM(String name) throws IOException, ModbusBlockIsNotFound {

        String vectorName = "mbm_vector";
        String content  = new String(Files.readAllBytes(Paths.get(smemoryFile.getPath())));
        String regexMbmBlock = ".*vector<std::string> " + vectorName + "[^;]*\\{[^;]*\\};";

        if(!hasBlock(regexMbmBlock))
            return content;

        return addToBlock(content, vectorName, name);
    }

    /**
     * Добавление ModbusSlave в вектор mbm_vector если такой есть
     *
     * @param name - Имя добавляемого MBS
     *
     * @return обновленное содержимое(после добавления MBS) файла
     *
     * @throws IOException - файл не найден
     * @throws ModbusBlockIsNotFound - не найден блок(std::vector<string>) с MBS именами
     *
     */
    public String addMBS(String name) throws IOException, ModbusBlockIsNotFound {

        String vectorName = "mbs_vector";
        String content  = new String(Files.readAllBytes(Paths.get(smemoryFile.getPath())));
        String regexMbmBlock = ".*vector<std::string> " + vectorName + "[^;]*\\{[^;]*\\};";

        if(!hasBlock(regexMbmBlock))
            return content;

        return addToBlock(content, vectorName, name);
    }

    /**
     * Добавление ModbusMultiMaster в вектор mbm_vector если такой есть
     *
     * @param name - Имя добавляемого MBMM
     *
     * @return обновленное содержимое(после добавления MBMM) файла
     *
     * @throws IOException - файл не найден
     * @throws ModbusBlockIsNotFound - не найден блок(std::vector<string>) с MBMM именами
     *
     */
    public String addMBMM(String name) throws IOException, ModbusBlockIsNotFound {

        String vectorName = "mbmm_vector";
        String content  = new String(Files.readAllBytes(Paths.get(smemoryFile.getPath())));
        String regexMbmBlock = ".*vector<std::string> " + vectorName + "[^;]*\\{[^;]*\\};";

        if(!hasBlock(regexMbmBlock))
            return content;

        return addToBlock(content, vectorName, name);
    }

    private String addToBlock(String content, String vectorName, String name) throws ModbusBlockIsNotFound {

        String regexSpace = "^\\s*";
        String regexName = "\"[^\"]*\"";
        String regexBlock = ".*vector<std::string> " + vectorName + "[^;]*;";
        Matcher matcher = Pattern.compile(regexBlock).matcher(content);

        String found = "";
        String space = "";
        List<String> array = new ArrayList<>();
        if (matcher.find()) {
            found = matcher.group();
            Matcher matcher1 = Pattern.compile(regexName).matcher(found.replaceAll("[\\s\\n]", ""));
            Matcher matcher2 = Pattern.compile(regexSpace).matcher(found);
            if (matcher2.find())
                space = matcher2.group();

            while (matcher1.find())
                array.add(matcher1.group().replaceAll("\\\"", ""));
        }
        array.add(name);

        String newStr = space + "std::vector<std::string> " + vectorName + " = \n" + space + "{\n";
        for (int i = 0; i < array.size(); i++) {
            String s = array.get(i);
            if (i == array.size() - 1)
                newStr += space + "\t" + "\"" + s + "\"" + "\n";
            else
                newStr += space + "\t" + "\"" + s + "\"" + ",\n";
        }
        newStr += space + "};";

        if (!found.isEmpty()) {
            content = content.replace(found, newStr);
            return content;
        }

        throw new ModbusBlockIsNotFound(name);
    }

    public static class ModbusBlockIsNotFound extends Exception
    {
        private String message;
        ModbusBlockIsNotFound()
        {
            super();
            this.message = "Modbus block \"vector<std::string> mbm_vector.. \" is not found or corrupted";
        }

        ModbusBlockIsNotFound(String name)
        {
            super();
            this.message = name + " block \"vector<std::string> mbm_vector.. \" is not found or corrupted";
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

}
