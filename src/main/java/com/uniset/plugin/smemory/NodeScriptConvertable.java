package com.uniset.plugin.smemory;

public interface NodeScriptConvertable {
    String getAsPartOfScript(String space);
}
