package com.uniset.plugin.smemory;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.objects.modbus.MBMMObject;
import com.uniset.plugin.build.objects.modbus.MBMObject;
import com.uniset.plugin.build.objects.modbus.MBSObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NodeScript {

    private Project project;
    private String nodeName;
    private VirtualFile script;
    private String regexExtBlock = "EXTPARAM\\s*=\\s*\\\"[^\\\"]*--smemory-id SharedMemory1[^\\\"]*\\\"";
    private String regexEndExtBlock = ".*\\$\\*\\\"";
    private List<ModbusBlock> modbusBlocks;

    private final static String MODBUS_TAG  = "====MODBUS_START_FROM_HERE====";

    NodeScript(final Project project, String nodeName) throws IOException, ExtParamBlockCorrupted, ClosedSymbolNotFound {
        String path  = project.getBasePath() + "/src/SharedMemory2/ctl-smemory2-" + project.getName() + "-" + nodeName + ".sh.in";
        this.nodeName = nodeName;
        this.project = project;
        this.script  = LocalFileSystem.getInstance().findFileByIoFile(new File(path));

        if(script == null) {
            path  = project.getBasePath() + "/src/SharedMemory2/ctl-smemory2-" + project.getName().toLowerCase() + "-" + nodeName + ".sh.in";
            this.script  = LocalFileSystem.getInstance().findFileByIoFile(new File(path));
            if(script == null)
                throw new FileNotFoundException("Script " + path + " is not found");
        }

        this.modbusBlocks = initModbusBlocks();
    }

//    public String addModbusObject(ModbusObject modbusObject) throws IOException, ExtParamBlockCorrupted, ClosedSymbolNotFound {
//        String content  = new String(Files.readAllBytes(Paths.get(script.getPath())));
//        Matcher matcher = Pattern.compile(regexExtBlock).matcher(content);
//        if(matcher.find())
//        {
//            String space = getSpace();
//
//            if(space == null)
//                throw new ClosedSymbolNotFound();
//
//            String oldBlock = matcher.group();
//            String newBlock = oldBlock.replaceAll(regexEndExtBlock, modbusObject.getAsPartOfScript(space)) + space + "$*\"";
//            content = content.replace(oldBlock, newBlock);
//
//            return content;
//        }
//        else
//            throw new ExtParamBlockCorrupted();
//    }

    public String addModbusObject(ModbusObject modbusObject) throws IOException, ModbusBlockAlreadyExist, ExtParamBlockCorrupted, ClosedSymbolNotFound {
        if(isModbusExist(modbusObject))
            throw new ModbusBlockAlreadyExist(modbusObject.getFullName());

        ModbusBlock modbusBlock = null;
        if(modbusObject instanceof MBMObject)
            modbusBlock = new ModbusBlock("MBM", modbusObject.getName());
        else if(modbusObject instanceof MBMMObject)
            modbusBlock = new ModbusBlock("MBMM", modbusObject.getName());
        else if(modbusObject instanceof MBSObject)
            modbusBlock = new ModbusBlock("MBS", modbusObject.getName());

        if(modbusBlock != null) {
            for (Map.Entry<String, String> entry : modbusObject.getMobusBlockProperties().entrySet())
                modbusBlock.properties.put(entry.getKey(), entry.getValue());
            modbusBlocks.add(modbusBlock);
        }

        return updateContent();
    }

    public void updateModbusObject(ModbusObject oldObject, ModbusObject newObject) throws IOException, ExtParamBlockCorrupted, ClosedSymbolNotFound {
        if(isModbusExist(oldObject)) {
            ModbusBlock modbusBlock = getModbusBlock(oldObject);
            modbusBlock.name = newObject.getName();
            modbusBlock.properties.clear();
            for (Map.Entry<String, String> entry : newObject.getMobusBlockProperties().entrySet()) {
                modbusBlock.properties.put(entry.getKey(), entry.getValue());
            }
        }

        updateContent(updateContent());
    }

    public VirtualFile getVirtualFile() {
        return script;
    }

    public boolean isModbusExist(ModbusObject modbusObject)
    {
        return getModbusBlock(modbusObject) != null;
    }

    public ModbusBlock getModbusBlock(ModbusObject modbusObject)
    {
        for(ModbusBlock modbusBlock : modbusBlocks)
        {
            if(modbusObject instanceof MBMObject)
            {
                if(modbusBlock.getName().equals(modbusObject.getName()))
                    return modbusBlock;
            }
            else if(modbusObject instanceof MBMMObject)
            {
                if(modbusBlock.getName().equals(modbusObject.getName()))
                    return modbusBlock;
            }
            else if(modbusObject instanceof MBSObject)
            {
                if(modbusBlock.getName().equals(modbusObject.getName()))
                    return modbusBlock;
            }
        }

        return null;
    }

    public List<ModbusBlock> getModbusBlocks()
    {
        return new ArrayList<>(modbusBlocks);
    }

    public String updateContent() throws ExtParamBlockCorrupted, ClosedSymbolNotFound, IOException {
        String content  = new String(Files.readAllBytes(Paths.get(script.getPath())));
        Matcher matcher = Pattern.compile(regexExtBlock).matcher(content);
        if(matcher.find())
        {
            String space = getSpace();

            if(space == null)
                throw new ClosedSymbolNotFound();

            String extOldBlock = matcher.group();
            String extNewBlock = updateAllModbus(extOldBlock);
            content = content.replace(extOldBlock, extNewBlock);

            return content;
        }
        else
            throw new ExtParamBlockCorrupted();
    }

    public void updateContent(String newContent)
    {
        FileUtils.setContentToFile(script, newContent);
    }

    public String getNodeName() {
        return nodeName;
    }

    private String getSpace() throws IOException {
        String content  = new String(Files.readAllBytes(Paths.get(script.getPath())));
        Matcher matcher = Pattern.compile(regexExtBlock).matcher(content);

        if(matcher.find())
        {
            String block = matcher.group();
            Matcher endMatch = Pattern.compile(regexEndExtBlock).matcher(block);
            if(endMatch.find()){
                Matcher spaceMatch = Pattern.compile("^\\s*").matcher(endMatch.group());
                if(spaceMatch.find())
                    return spaceMatch.group();
            }
            else
                return null;
        }

        return null;
    }

    private String getEndExtBlock() throws IOException {
        String content  = new String(Files.readAllBytes(Paths.get(script.getPath())));
        Matcher matcher = Pattern.compile(regexExtBlock).matcher(content);

        if(matcher.find())
        {
            String block = matcher.group();
            Matcher endMatch = Pattern.compile(regexEndExtBlock).matcher(block);
            if(endMatch.find()){
                return endMatch.group();
            }
            else
                return null;
        }

        return null;
    }

    private String getSpaceOfLine(String regex) throws IOException {
        String content  = new String(Files.readAllBytes(Paths.get(script.getPath())));
        Matcher matcher = Pattern.compile(regexExtBlock).matcher(content);
        if(matcher.find())
        {
            String block = matcher.group();
            Matcher lineMatch = Pattern.compile(regex).matcher(block);
            if(lineMatch.find()){
                Matcher spaceMatch = Pattern.compile("^\\s*").matcher(lineMatch.group());
                if(spaceMatch.find())
                    return spaceMatch.group();
            }
            else
                return null;
        }

        return null;
    }

    private List<ModbusBlock> initModbusBlocks() throws IOException, ExtParamBlockCorrupted, ClosedSymbolNotFound {
        List<ModbusBlock> list = new ArrayList<>();
        String content  = new String(Files.readAllBytes(Paths.get(script.getPath())));
        Matcher matcher = Pattern.compile(regexExtBlock).matcher(content);
        if(matcher.find())
        {
            String space = getSpace();

            if(space == null)
                throw new ClosedSymbolNotFound();

            String block = matcher.group();

            if(block != null) {
                list.addAll(getModbusObjects(block));

            }
        }
        else
            throw new ExtParamBlockCorrupted();

        return list;
    }

    //TODO поправить обработку - если есть решетки(#)в тексте скрипта, то некорректно парсит ModbusBlock-и
    private List<ModbusBlock> getModbusObjects(String block) throws IOException {
        List<ModbusBlock> list = new ArrayList<>();
        String find_type = "mb(s|mm|m)";
        String find = "--add-" + find_type + "[^\\s]*";
        Matcher matcher = Pattern.compile(find).matcher(block);
        while(matcher.find()) {
            String header = matcher.group();
            String name = header.replaceAll("--add-" + find_type, "");
            String type = header.replaceAll(name, "").replaceAll("--add-", "");
            String space = getSpaceOfLine(".*" + header);
            if(space != null)
                ModbusBlock.modbusSpace = space;

            ModbusBlock object = new ModbusBlock(type.toUpperCase(), name);
            String propertyRegex = "--" + type + name + "-[^\\s]* [^\\s]*";
            Matcher propertyMatcher = Pattern.compile(propertyRegex).matcher(block);
            while (propertyMatcher.find()) {
                String line = propertyMatcher.group();
                String property = line
                        .replaceAll("^--" + type.toLowerCase() + name + "-", "")
                        .replaceAll(" .*$", "");
                String value = line.replaceAll("^--" + type.toLowerCase() + name + "-[^\\s]* ", "");
                object.properties.put(property, value);
            }
            list.add(object);
        }

        return list;
    }

    private String updateAllModbus(String block) throws IOException {

        // Удаляем все modbus-ы и оставляем тэг для будущей замены
        boolean isStartBlockFound = false;
        String find_type = "mb(s|mm|m)";
        String find = "--add-" + find_type + "[^\\s]*";
        Matcher matcher = Pattern.compile(find).matcher(block);
        if(matcher.find()) {
            do{
                String header = matcher.group();
                String name = header.replaceAll("--add-" + find_type, "");
                String type = header.replaceAll(name, "").replaceAll("--add-", "");

                if (!isStartBlockFound) {
                    block = block.replaceAll("\\s*" + header + "\\s.*", "\n" + MODBUS_TAG);
                    isStartBlockFound = true;
                } else
                    block = block.replaceAll("\\s*" + header + ".*", "");


                String propertyRegex = "--" + type + name + "-[^\\s]* [^\\s]*";
                Matcher propertyMatcher = Pattern.compile(propertyRegex).matcher(block);
                while (propertyMatcher.find()) {
                    String line = propertyMatcher.group();
                    block = block.replaceAll("\\s*" + line + ".*", "");
                }
            }
            while (matcher.find());
        }
        else {
            String end = getEndExtBlock();
            if(end != null) {
                block = block.replace(end, MODBUS_TAG + "\n" + end);
            }

        }

        // Записываем modbus-ы в EXTPARAM-блок
        String modbus = "";
        for(ModbusBlock modbusBlock : modbusBlocks)
        {
            modbus += modbusBlock;
        }
        // Удаляем последний переход на новую строку
        modbus = modbus.substring(0,modbus.lastIndexOf("\n"));
        block = block.replace(MODBUS_TAG, modbus);

        return block;
    }

    public static class ModbusBlock
    {
        private String type;
        private String name;
        private LinkedHashMap<String, String> properties = new LinkedHashMap<>();
        private static String modbusSpace = "\t";

        private ModbusBlock(String type, String name) throws IOException {
            this.type = type;
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public String getName() {
            return name;
        }

        public String getProperty(String propertyName)
        {
            String val = null;
            try {
                val = properties.get(propertyName);
            }
            catch (NullPointerException e)
            {
                return val;
            }
            return val;
        }

        @Override
        public String toString() {
            String result = modbusSpace + "--add-" + type.toLowerCase() + name + " \\" + "\n";
            for(Map.Entry<String, String> entry : properties.entrySet())
            {
                String prop = entry.getKey();
                String val  = entry.getValue();
                if(val != null && !val.isEmpty() && !val.equals("0"))
                    result += modbusSpace + "--" + type.toLowerCase() + name + "-" + prop + " " + val + " \\" + "\n";
            }

            return result;
        }
    }

    public static class ExtParamBlockCorrupted extends Exception
    {
        private String message;
        ExtParamBlockCorrupted()
        {
            super();
            this.message = "EXTPARAM variable is corrupted";
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

    public static class ClosedSymbolNotFound extends Exception
    {
        private String message;
        ClosedSymbolNotFound()
        {
            super();
            this.message = "Closed symbol $*\" for EXTPARAM is not found";
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

    public static class ModbusBlockAlreadyExist extends Exception
    {
        private String message;
        ModbusBlockAlreadyExist(String blockName)
        {
            super();
            this.message = blockName + " block is already exist.";
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

}
