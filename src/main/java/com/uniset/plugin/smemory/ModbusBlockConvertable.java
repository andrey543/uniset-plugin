package com.uniset.plugin.smemory;

import java.util.LinkedHashMap;

public interface ModbusBlockConvertable {
    LinkedHashMap<String,String> getMobusBlockProperties();
}
