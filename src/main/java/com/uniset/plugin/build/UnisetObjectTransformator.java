package com.uniset.plugin.build;

import com.intellij.openapi.project.Project;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.ResourceObject;
import com.uniset.plugin.build.objects.UnisetObjectFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class UnisetObjectTransformator {

    /**
     * Преобразование шаблона в конкретный UnisetObject-файл с заменой имени объекта
     *
     * @param fileName       - Имя файла
     * @param projectName    - Имя проекта(на это имя заменяется имя файла-шаблона)
     *
     * @return - Новое имя файла
     * */
    public static String transformTemplateFile(String fileName, String projectName) throws IOException {

        return fileName.replaceAll("TEMPLATE_PROJECT", projectName);
    }

    /**
     * Преобразование шаблона в конкретный UnisetObject-файл с заменой имени объекта
     *
     * @param pathToTemplate - Путь к шаблону в ресурсах проекта плагина
     * @param projectName    - Имя проекта(на это имя заменяется имя шаблона)
     * @param nameOfObject   - Имя объекта(на это имя заменяется имя шаблона)
     *
     * @return - Содержимое файла
     * */
    public static String transformTemplateFromResource(String pathToTemplate, String projectName, String nameOfObject) throws IOException {
        InputStream s = UnisetObjectTransformator.class.getResourceAsStream(pathToTemplate);

        String content = "";
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(s));
        for (String line = fileReader.readLine(); line != null; line = fileReader.readLine()) {
            line = line.replaceAll("TEMPLATE_PROJECT", projectName);
            line = line.replaceAll("TemplateObject", nameOfObject);
            line = line.replaceAll("templateobject", nameOfObject.toLowerCase());
            line = line.replaceAll("USER", System.getProperty("user.name"));
            line = line.replaceAll("DATE", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));

            content += (line + System.lineSeparator());
        }
        fileReader.close();

        return content;
    }

    /**
     * Преобразование шаблона в конкретный файл
     *
     * @param pathToTemplate - Путь к шаблону в ресурсах проекта плагина
     * @param replaceMap    - Набор замен(1 - ЧТО 2 - НА ЧТО )
     *
     * @return - Содержимое файла
     * */
    public static String transformTemplateFromResource(String pathToTemplate, HashMap<String, String> replaceMap) throws IOException {
        InputStream s = UnisetObjectTransformator.class.getResourceAsStream(pathToTemplate);

        BufferedInputStream bis = new BufferedInputStream(s);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        for(int result = bis.read(); result != -1; result = bis.read())
            baos.write((byte) result);
        String content = baos.toString("UTF-8");
        baos.close();
        bis.close();

        content = content.replaceAll("USER", System.getProperty("user.name"));
        content = content.replaceAll("DATE", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));

        for(Map.Entry<String, String> replace : replaceMap.entrySet())
        {
            content = content.replaceAll(replace.getKey(), replace.getValue());
        }

        return content;
    }

    /**
     * Преобразование нескольких шаблонов в конкретные UnisetObject-файлы
     *
     * @param project   - Проект
     * @param templates - Список шаблонов(их путей)
     *
     * @return - Имена путей и их содержимое
     * */
    public static LinkedHashMap<String, String> transformTemplatesFromResource(final Project project, final List<String> templates) throws Throwable {

        LinkedHashMap<String, String> files = new LinkedHashMap<>();
        for(String pathToTemplate : templates) {
            String templateName = pathToTemplate.substring(pathToTemplate.lastIndexOf("/") + 1);

            files.put(templateName, transformTemplateFromResource(pathToTemplate, project.getName()));
        }

        return files;
    }

    /**
     * Преобразование нескольких шаблонов в конкретные UnisetObject-файлы
     *
     * @param project      - Проект
     * @param unisetObject - По сути директория UnisetObject-а
     *
     * @return - Имена файлов и их содержимое
     * */
    public static LinkedHashMap<String, String> transformTemplatesFromResource(final Project project, final ResourceObject unisetObject) throws IOException {

        LinkedHashMap<String, String> files = new LinkedHashMap<>();

        for(File file : unisetObject.listFiles())
            files.put(file.getName(), transformTemplateFromResource(file.getPath(), project.getName()));

        return files;
    }

    /**
     * Преобразование шаблона в конкретный UnisetObject-файл
     *
     * @param pathToTemplate - Путь к шаблону в ресурсах проекта плагина
     * @param projectName    - Имя проекта(на это имя заменяется имя шаблона)
     *
     * @return - Содержимое файла
     * */
    public static String transformTemplateFromResource(String pathToTemplate, String projectName) throws IOException {
        InputStream s = UnisetObjectTransformator.class.getResourceAsStream(pathToTemplate);

        String content = "";
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(s));
        for (String line = fileReader.readLine(); line != null; line = fileReader.readLine()) {
            line = line.replaceAll("TEMPLATE_PROJECT", projectName);
            line = line.replaceAll("USER", System.getProperty("user.name"));
            line = line.replaceAll("DATE", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));

            content += (line + System.lineSeparator());
        }
        fileReader.close();

        return content;
    }

    /**
     * Преобразование шаблона в конкретный UnisetObject-файл
     *
     * @param pathToTemplate - Путь к шаблону в ресурсах проекта плагина
     * @param projectName    - Имя проекта(на это имя заменяется имя шаблона)
     *
     * @return - Содержимое файла
     * */
    public static String transformTemplateFromResource(String pathToTemplate, String projectName, boolean date, boolean user) throws IOException {
        InputStream s = UnisetObjectTransformator.class.getResourceAsStream(pathToTemplate);

        String content = "";
        BufferedReader fileReader = new BufferedReader(new InputStreamReader(s));
        for (String line = fileReader.readLine(); line != null; line = fileReader.readLine()) {
            line = line.replaceAll("TEMPLATE_PROJECT", projectName);
            if(user) line = line.replaceAll("USER", System.getProperty("user.name"));
            if(date) line = line.replaceAll("DATE", new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime()));

            content += (line + System.lineSeparator());
        }
        fileReader.close();

        return content;
    }

    /**
     * Преобразование исходного UnisetObject в новый UnisetObject под текущий проект
     *
     * @param project      - Текущий проект
     * @param projectName  - Имя проекта(на это имя заменяется имя шаблона)
     * @param unisetObject - Исходный объект из которого преобразуется объект под конкретный проект
     *
     * @return - Преображенный под проект объект
     * */
    public static UnisetObjectFile transformTemplatesFromProject(final Project project, final String projectName, final UnisetObjectFile unisetObject)
    {
        return transformTemplatesFromProject(projectName, project.getName(), unisetObject);
    }

    /**
     * Замена старого имени проекта на новое имя проекта в UnisetObject
     *
     * @param oldProjectName - Старое имя в объекте
     * @param newProjectName - Новое имя которое появится в объекте
     * @param unisetObject   - Исходный объект из которого преобразуется объект под конкретный проект
     *
     * @return - Преображенный под проект объект
     * */
    public static UnisetObjectFile transformTemplatesFromProject(final String oldProjectName, final String newProjectName, final UnisetObjectFile unisetObject)
    {

        UnisetObjectFile newUnisetObject = UnisetObjectFile.createFromObject(unisetObject);
        List<UnisetObjectFile> queue = new ArrayList<>();
        queue.add(newUnisetObject);

        while (!queue.isEmpty())
        {
            UnisetObjectFile objectFile = queue.remove(0);

            if(!objectFile.getSubdirs().isEmpty())
                queue.addAll(objectFile.getSubdirs());

            LinkedHashMap<String, String> newFiles = new LinkedHashMap<>();
            for(Map.Entry<String, String> entry : objectFile.getFiles().entrySet())
            {
                String name    = entry.getKey();
                String content = entry.getValue();

                content = content
                        .replaceAll(oldProjectName, newProjectName)
                        .replaceAll(oldProjectName.toLowerCase(), newProjectName)
                        .replaceAll(oldProjectName.toUpperCase(), newProjectName);
                if(name.endsWith(".xml"))
                {
                    String title = Settings.COMMENT_TITLE_XML_FILE.replaceAll("OLDPROJECT", oldProjectName);
                    content = title + content.replaceAll(".*<\\?xml version=.*","");
                }
                else if(name.endsWith(".h") || name.endsWith(".cc")) {
                    String title = Settings.COMMENT_TITLE_CODE_FILE.replaceAll("OLDPROJECT", oldProjectName);
                    content = title + content;
                }
                else if(name.endsWith(".sh.in"))
                {
                    String title = Settings.COMMENT_TITLE_SCRIPT_FILE.replaceAll("OLDPROJECT", oldProjectName);
                    content = title + content;
                }
                else if(name.endsWith("Makefile.am"))
                {
                    String title = Settings.COMMENT_TITLE_SCRIPT_FILE.replaceAll("OLDPROJECT", oldProjectName);
                    content = title + content;
                }

                newFiles.put(name, content);
            }

            objectFile.setFiles(newFiles);
        }
        return newUnisetObject;
    }

}
