package com.uniset.plugin.build.make;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.sun.istack.NotNull;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.UnisetObjectTransformator;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class MakeConfiguration {

    public  static final String MESSAGE_TITLE = MakeConfiguration.class.getName() + ":";
    private static MakeConfiguration instance;

    private Project project;
    private VirtualFile configureAc;
    private VirtualFile mainMakeFile;
    private List<String> ac_config_files = new LinkedList<>(); // секция AC_CONFIG_FILES в configure.ac

    public static MakeConfiguration getInstance(final Project project) throws FileNotFoundException {
        if(instance == null)
            instance = new MakeConfiguration(project);
        else
            instance.initialize(project);

        return instance;
    }

    private MakeConfiguration(final Project project) throws FileNotFoundException {
       initialize(project);
    }
    private void initialize(Project project) throws FileNotFoundException {
        this.project = project;

        configureAc = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(project.getBasePath() + "/configure.ac"));
        if(configureAc == null)
            throw new FileNotFoundException("File configure.ac is not found");

        mainMakeFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(project.getBasePath() + "/Makefile.am"));
    }

    public List<String> getAc_config_files() throws Exception {
        initAcConfigFiles();
        return ac_config_files;
    }

    public VirtualFile getMainMakeFile() {
        return mainMakeFile;
    }

    public VirtualFile getConfigure_ac() {
        return configureAc;
    }

    public void configureSubdirs(Project project, String pathToLocalMakefile) throws Throwable {


        Path root = Paths.get(project.getBasePath());
        Path toMakeFile = Paths.get(pathToLocalMakefile);
        if(!Files.isDirectory(toMakeFile))
            toMakeFile = toMakeFile.getParent();
        Path path = root.resolve(root.relativize(toMakeFile));
        recursiveConfigureSubdirs(project, path);
    }

    public void configureMainFile(Project project, String pathToFile) throws Throwable {
        Path root = Paths.get(project.getBasePath());
        Path path = Paths.get(pathToFile);
        String line = root.relativize(path).toString()
                .replaceAll(".am$", "")
                .replaceAll(".in$", "");

        addAcConfigFile(line);
    }

    public void configureLocalFile(String pathToMakefile, String pathToTemplate, String nameOfObject, String projectName) throws IOException {
        List<String> currentContentFile = new ArrayList<>();
        currentContentFile.addAll(Files.readAllLines(Paths.get(pathToMakefile)));

        String newContent = UnisetObjectTransformator.transformTemplateFromResource(pathToTemplate, projectName, nameOfObject);
        newContent += System.lineSeparator();

        for(String line : currentContentFile)
            newContent += line + System.lineSeparator();

        FileUtils.setContentToFile(pathToMakefile, newContent);
    }

    public void createNewLocalFile(Project project, String pathToMakefile, String pathToTemplate, String nameOfObject) throws IOException {
        FileUtils.createFile(project, pathToMakefile, "Makefile.am", UnisetObjectTransformator.transformTemplateFromResource(pathToTemplate, project.getName(), nameOfObject));
    }

    private void initAcConfigFiles() throws Exception {
        List<String> list = new ArrayList<>();
        list.addAll(Files.readAllLines(Paths.get(configureAc.getPath())));
        boolean isSection = false;
        ac_config_files.clear();
        for(String line : list)
        {
            if(line.equals("AC_CONFIG_FILES([Makefile")) {
                isSection = true;
                ac_config_files.add(line);
                continue;
            }

            if(isSection && Pattern.matches(".*]\\)$", line))
            {
                isSection = false;
                line = line.replaceAll("\\s*","");
                ac_config_files.add(line);
                break;
            }

            if(isSection)
            {
                line = line.replaceAll("\\s*","");
                ac_config_files.add(line);
            }
        }

        if(isSection)
        {
            throw new Exception("File configure.ac is corrupted. Section 'AC_CONFIG_FILES' is not closed.");
        }

        if(ac_config_files.isEmpty())
        {
            throw new Exception("File configure.ac is corrupted. Section 'AC_CONFIG_FILES' is not exist");
        }
    }

    private static void recursiveConfigureSubdirs(Project project, Path path) throws Throwable {

        if(Paths.get(project.getBasePath()).equals(path))
            return;

        String subDirName = path.getFileName().toString();
        Path prevDir = path.getParent();

        Path amFile = prevDir.resolve("Makefile.am");
        VirtualFile file = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(amFile.toFile());

        if(Files.exists(amFile))
        {
            boolean isFound = false;
            List<String> content = Files.readAllLines(Paths.get(file.getPath()));
            for(int i = 0; i < content.size(); i++)
            {
                String line = content.get(i);
                if(Pattern.matches("^SUBDIRS.*", line))
                {
                    String regex = "[^\\w]" + subDirName + "([^\\w]|$)";
                    if(Pattern
                            .compile(regex)
                            .matcher(line)
                            .find())
                    {
                        isFound = true;
                        break;
                    }
                    String dirs = line.replaceAll("SUBDIRS\\s?=\\s?", "");
                    dirs = dirs.replaceAll("\\s+", " ");
                    dirs = subDirName + " " + dirs;
                    String newLine = "SUBDIRS = " + dirs;
                    content.set(i, newLine);

                    String newContent = "";
                    for(String l : content)
                        newContent += l + System.lineSeparator();
                    FileUtils.setContentToFile(file, newContent);
                    isFound = true;
                    break;
                }
            }
            if(!isFound)
            {
                content.add(0, "SUBDIRS = " + subDirName);
                String newContent = "";
                for(String l : content)
                    newContent += l + System.lineSeparator();

                FileUtils.setContentToFile(file, newContent);
            }
        }
        else
        {
            FileUtils.createFile(project, prevDir.toString(), "Makefile.am", "SUBDIRS = " + subDirName);
        }

        recursiveConfigureSubdirs(project, prevDir);
    }

    private String updateAcConfigFiles1() throws Exception {
        List<String> list = new ArrayList<>();
        list.addAll(Files.readAllLines(Paths.get(configureAc.getPath())));
        boolean isSection = false;
        String replace_tag = "AC_CONFIG_FILES_REPLACE_PLACE";
        for(int i = 0; i < list.size(); i++)
        {
            String line = list.get(i);
            if(line.equals("AC_CONFIG_FILES([Makefile")) {
                isSection = true;
                list.set(i, replace_tag);
                continue;
            }

            if(isSection && Pattern.matches(".*]\\)$", line))
            {
                isSection = false;
                list.remove(i);
                break;
            }

            if(isSection)
            {
                list.remove(i--);
            }
        }

        if(isSection)
        {
            throw new Exception("File configure.ac is corrupted. Section 'AC_CONFIG_FILES' is not closed.");
        }

        if(ac_config_files.isEmpty())
        {
            throw new Exception("File configure.ac is corrupted. Section 'AC_CONFIG_FILES' is not exist");
        }

        for(int i = 0; i < list.size(); i++)
        {
            String line = list.get(i);

            if(line.equals(replace_tag))
            {
                Iterator<String> it = ac_config_files.listIterator();
                list.set(i, it.next());
                while (it.hasNext())
                {
                    String l = it.next();
                    int pos = ++i;
                    if(l.equals(""))
                        list.add(pos, l);
                    else
                        list.add(pos, "\t\t\t\t " + l);
                }
                break;
            }
        }

        String content = "";
        for(String line : list)
        {
            content += (line + System.lineSeparator());
        }

        return content;
    }
    private void updateAcConfigFiles(String actionGroup) throws Throwable {

        FileUtils.setContentToFileUndo(project, actionGroup, configureAc, updateAcConfigFiles1());
    }
    private void updateAcConfigFiles() throws Throwable {

        FileUtils.setContentToFileUndo(project, configureAc, updateAcConfigFiles1());
    }

    private void addAcConfigFile1(String line) throws Exception {
        initAcConfigFiles();

        if(ac_config_files.contains(line))
            return;
        if(line.equals("Makefile"))
            return;

        if(!line.contains("/"))
        {
            int pos = findInsert(line);
            ac_config_files.add(pos,line);
        }
        else {

            String fileName = line.substring(line.lastIndexOf("/") + 1);
            String filePath = line.substring(0, line.lastIndexOf("/"));
            int pos = findInsert(filePath);
            ac_config_files.add(pos, filePath + "/" + fileName);
        }
    }
    private void addAcConfigFile(String actionGroup, String line) throws Throwable {
        addAcConfigFile1(line);
        updateAcConfigFiles(actionGroup);
    }
    private void addAcConfigFile(String line) throws Throwable {
        addAcConfigFile1(line);
        updateAcConfigFiles();
    }

    private int findInsert(String findLine)
    {
        if(findLine.equals(""))
            return 1;

        boolean isFind = false;
        for(int i = 0; i < ac_config_files.size(); i++) {
            if (Pattern.matches(findLine + ".*", ac_config_files.get(i)))
            {
                isFind = true;
                continue;
            }
            if(isFind)
                return i;
        }

        if(!findLine.contains("/"))
            return ac_config_files.size() - 1;
        return findInsert(findLine.substring(0, findLine.lastIndexOf("/")));
    }

}
