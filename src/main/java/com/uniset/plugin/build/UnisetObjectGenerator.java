package com.uniset.plugin.build;

import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.project.Project;
import com.uniset.plugin.R;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.ResourceObject;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

public class UnisetObjectGenerator {

    /**
     * Генерация списка UnisetObject-ов из ресурсов плагина(uniset-plugin/src/main/resources)
     *
     * @param pathToTemplateResourceList - Путь к файлу *.list в папке resources
     *
     * @return - Список UnisetObject-ов
     * */
    public static List<ResourceObject> generateBaseList(String pathToTemplateResourceList) throws IOException {
        List<ResourceObject> list = new ArrayList<>();
        String pathToDir = pathToTemplateResourceList.substring(0, pathToTemplateResourceList.lastIndexOf("/"));
        InputStream is = UnisetObjectGenerator.class.getClassLoader().getResourceAsStream(pathToTemplateResourceList);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        for (String line = reader.readLine(); line != null; line = reader.readLine())
        {
            if(line.startsWith("#"))
            {
                String path = pathToDir + "/" + line.replaceAll("#", "");
                list.add(new ResourceObject(path));
            }
            else
            {
                //TODO тут алгоритм работает хреново, стоит подумать как переделать
                for(ResourceObject uObject : list)
                {
                    if(line.startsWith("--" + uObject.getName()))
                        uObject.files.add(new File(pathToDir + "/" + line.replaceAll("--", "")));
                }
            }
        }

        return list;
    }

    /**
     * Генерация списка UnisetObject-ов существующих в открытом проекте
     *
     * @param pathInProject - Путь к папке с алгоритмами
     *
     * @return - Список UnisetObject-ов
     * */
    public static List<String> generateProjectList(String pathInProject) throws IOException {
        Path path = Paths.get(pathInProject);
        List<String> list = null;
        if(Files.exists(path)) {
            list = new ArrayList<>();
            File dir = new File(pathInProject);
            for(File f : dir.listFiles())
                if(f.isDirectory())
                    list.add(f.getName());
        }

        return list;
    }

    /**
     * Генерация списка UnisetObject-ов существующих в открытом проекте
     *
     * @param project - Проект
     * @param pathInProject - Путь к папке с алгоритмами
     *
     * @return - Список UnisetObject-ов
     * */
    public static List<Path> generateProjectList(Project project, String pathInProject) throws IOException {
        Path path = Paths.get(project.getBasePath() + pathInProject);
        List<Path> list = null;
        if(Files.exists(path)) {
            list = new ArrayList<>();
            File dir = new File(path.toString());
            for(File f : dir.listFiles())
                if(f.isDirectory())
                    list.add(f.toPath());
        }

        return list;
    }

    /**
     * Генерация списка UnisetObject-ов существующих в открытом проекте
     *
     * @param unisetObjectDirectory - Путь к папке с алгоритмами
     *
     * @return - Список UnisetObject-ов
     * */
    public static LinkedHashMap<String, List<Path>> generateAdditionalList(String unisetObjectDirectory) throws IOException {
        String [] values = PropertiesComponent.getInstance().getValues(Settings.PROP_ATTACH_PROJECTS);
        LinkedHashMap<String, List<Path>> list = null;

        if(values != null) {
            list = new LinkedHashMap<>();
            for (String s : values) {
                String name = s.split(" ")[0];
                String path = s.split(" ")[1];

                List<Path> projectUnisetObjects = new ArrayList<>();
                File dir = new File(path + "/" + unisetObjectDirectory);
                File[] files = dir.listFiles();

                if(files == null)
                    continue;

                for(File f : files)
                {
                    if(f.isDirectory())
                        projectUnisetObjects.add(f.toPath());
                }
                list.put(name, projectUnisetObjects);
            }
        }

        return list;
    }

    /**
     * Генерация списка файлов UnisetObjec-а по указанному пути к директории
     *
     * @param pathToDir - Путь к папке с алгоритмом
     *
     * @return - Список файлов(путей)
     * */
    public static List<String> generateFiles(String pathToDir) throws IOException {
        Path path = Paths.get(pathToDir);
        List<String> list = null;
        if(Files.exists(path)) {
            list = new ArrayList<>();
            Files.list(path)
                    .filter(i -> !Files.isDirectory(i))
                    .filter(i -> i.toString().endsWith(".h")
                            || i.toString().endsWith(".cc")
                            || i.toString().endsWith(".src.xml")
                            || i.toString().endsWith("Makefile.am"))
                    .filter(i -> !i.toString().contains("_SK."))
                    .map(i -> i.getFileName().toString())
                    .forEach(list::add);
        }

        return list;
    }

    /**
     * Генерация списка файлов UnisetObjec-а по указанному пути к директории
     *
     * @param pathToDir - Путь к папке с алгоритмом
     *
     * @return - Список файлов(путей)
     * */
    public static List<String> generateFiles(File pathToDir) throws IOException {
        return generateFiles(pathToDir.getPath());
    }

    public static void main(String[] args) throws IOException {

    }
}
