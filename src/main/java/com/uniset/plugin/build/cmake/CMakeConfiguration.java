package com.uniset.plugin.build.cmake;

import com.intellij.history.core.Paths;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.UnisetObjectTransformator;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Pattern;

public class CMakeConfiguration {
    private static CMakeConfiguration instance;
    private Project project;
    private VirtualFile mainCMakeFile;
    private String pathToTemplateMainCc = "/templates/main_make/CMakeLists.txt";
    private String pathToTemplate = "/templates/CMakeLists.txt";

    public static CMakeConfiguration getInstance(final Project project) throws FileNotFoundException {
        if(instance == null)
            instance = new CMakeConfiguration(project);
        else
            instance.initialize(project);

        return instance;
    }

    private CMakeConfiguration(final Project project) throws FileNotFoundException {
        initialize(project);
    }

    private void initialize(@NotNull final Project project) throws FileNotFoundException {
        this.project = project;
        this.mainCMakeFile = FileUtils.toVirtualFile(project.getBasePath() + "/CMakeLists.txt");
        if(this.mainCMakeFile == null)
            throw new FileNotFoundException("File CMakeLists.txt is not found in root of project");
    }


    public void configureSubdirsAt(String path) throws IOException {
        VirtualFile dir = FileUtils.toVirtualFile(path);
        if(!Paths.isParent(project.getBasePath(), path))
        {
            return;
        }

        VirtualFile prev_cmake = FileUtils.getVirtualChild(dir.getParent(), "CMakeLists.txt");
        if(prev_cmake == null){
            FileUtils.createFile(project, dir.getParent(), "CMakeLists.txt", "add_subdirectory(" + dir.getName() + ")");
        }
        else
        {
            String content = FileUtils.readContent(prev_cmake);
            String regex = "\\s*add_subdirectory\\s*\\(\\s*" + dir.getName() + "\\s*\\)\\s*";
            if(!Pattern.compile(regex).matcher(content).find())
            {
                if(dir.getParent().getPath().equals(project.getBasePath()))
                {
                    FileUtils.addContentLast(prev_cmake,"add_subdirectory(" + dir.getName() + ")");
                    return;
                }
                FileUtils.addContentFirst(prev_cmake, "add_subdirectory(" + dir.getName() + ")");
            }
        }

        configureSubdirsAt(dir.getParent().getPath());
    }

    public void configureFileAt(String path, String nameOfObject, boolean main) throws IOException {
        String currentFile = path + "/CMakeLists.txt";
        String newContent;
        if(main)
            newContent = UnisetObjectTransformator.transformTemplateFromResource(pathToTemplateMainCc, project.getName(), nameOfObject);
        else
            newContent  = UnisetObjectTransformator.transformTemplateFromResource(pathToTemplate, project.getName(), nameOfObject);
        if(FileUtils.isExists(currentFile)) {
            FileUtils.addContentLast(currentFile, newContent);
        }
        else {
            FileUtils.createFile(project, path, "CMakeLists.txt", newContent);
        }
    }

}
