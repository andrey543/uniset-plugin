package com.uniset.plugin.build.objects.modbus;

import java.util.Objects;

public class Gate {
    public static final String GATE_PROPERTY_IP = "ip";
    public static final String GATE_PROPERTY_REAL_PORT = "real_port";
    public static final String GATE_PROPERTY_VSTAND_PORT = "vstand_port";
    public static final String GATE_PROPERTY_RESPOND_INIT_TIMEOUT = "respondInitTimeout";
    public static final String GATE_PROPERTY_RECV_TIMEOUT = "recv_timeout";
    public static final String GATE_PROPERTY_RESPOND_SENSOR = "respondSensor";
    public static final String GATE_PROPERTY_FORCE = "force";
    public static final String GATE_PROPERTY_INVERT = "invert";

    private String ip;
    private String respondSensor;
    private String realPort;
    private String vstandPort;
    private long respondInitTimeout;
    private long recv_timeout;
    private boolean force;
    private boolean invert;

    public Gate(String ip, String port)
    {
        this.ip = ip;
        this.realPort = port;
    }
    public Gate(String ip, String respondSensor, String realPort, String vstandPort, long respondInitTimeout, long recv_timeout, boolean force, boolean invert) {
        this.ip = ip;
        this.respondSensor = respondSensor;
        this.realPort = realPort;
        this.vstandPort = vstandPort;
        this.respondInitTimeout = respondInitTimeout;
        this.recv_timeout = recv_timeout;
        this.force = force;
        this.invert = invert;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Gate)) return false;
        Gate gate = (Gate) o;
        return respondInitTimeout == gate.respondInitTimeout &&
                recv_timeout == gate.recv_timeout &&
                force == gate.force &&
                invert == gate.invert &&
                Objects.equals(ip, gate.ip) &&
                Objects.equals(respondSensor, gate.respondSensor) &&
                Objects.equals(realPort, gate.realPort) &&
                Objects.equals(vstandPort, gate.vstandPort);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ip, respondSensor, realPort, vstandPort, respondInitTimeout, recv_timeout, force, invert);
    }

    public String getIp() {
        return ip;
    }

    public String getRespondSensor() {
        return respondSensor;
    }

    public String getRealPort() {
        return realPort;
    }

    public String getVstandPort() {
        return vstandPort;
    }

    public long getRespondInitTimeout() {
        return respondInitTimeout;
    }

    public long getRecvTimeout() {
        return recv_timeout;
    }

    public boolean isForce() {
        return force;
    }

    public boolean isInvert() {
        return invert;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setRespondSensor(String respondSensor) {
        this.respondSensor = respondSensor;
    }

    public void setRealPort(String realPort) {
        this.realPort = realPort;
    }

    public void setVstandPort(String vstandPort) {
        this.vstandPort = vstandPort;
    }

    public void setRespondInitTimeout(long respondInitTimeout) {
        this.respondInitTimeout = respondInitTimeout;
    }

    public void setRecv_timeout(long recv_timeout) {
        this.recv_timeout = recv_timeout;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public void setInvert(boolean invert) {
        this.invert = invert;
    }
}
