package com.uniset.plugin.build.objects.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlTag;
import com.uniset.plugin.gui.modbus.AddMBSDialog;
import com.uniset.plugin.smemory.NodeScript;

import java.util.LinkedHashMap;
import java.util.Objects;

public class MBSObject extends ModbusObject {
    private String fullname;
    private String tcpIpAddr;
    private String tcpPort;
    private String rtuDev;
    private String rtuSpeed;
    private String filterField;
    private String filterValue;
    private String prefix;
    private boolean force;
    private boolean checkMbFunc;
    private Type type = Type.NONE;

    public enum Type
    {
        NONE("None"),
        TCP("TCP"),
        RTU("RTU");

        String type;
        Type(String type)
        {
            this.type = type;
        }
    }

    public MBSObject(long id, String name)
    {
        super(id, name);
        fullname    = "MBS_" + name;
    }
    public MBSObject(long id, final AddMBSDialog dialog) throws MBSFailedTypeException {
        super(id, dialog.getName());

        if(dialog.getType().equals(Type.TCP.toString()))
        {
            type = Type.TCP;
            tcpIpAddr = dialog.getIPAddr();
            tcpPort   = dialog.getPort();
        }
        else if(dialog.getType().equals(Type.RTU.toString()))
        {
            type = Type.RTU;
            rtuDev   = dialog.getDev();
            rtuSpeed = dialog.getSpeed();
        }
        else if(dialog.getType().equals(Type.NONE.toString()))
        {
            type = Type.NONE;
        }
        else
            throw new MBSFailedTypeException(dialog.getType());

        fullname    = "MBS_" + dialog.getName();
        name        = dialog.getName();
        filterField = dialog.getFilterField();
        filterValue = dialog.getFilterValue();
        prefix      = dialog.getPrefix();
        force       = dialog.isForce();
        checkMbFunc = dialog.isCheckMbFunc();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        this.fullname = "MBS_" + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MBSObject)) return false;
        if (!super.equals(o)) return false;
        MBSObject mbsObject = (MBSObject) o;
        return force == mbsObject.force &&
                checkMbFunc == mbsObject.checkMbFunc &&
                Objects.equals(tcpIpAddr, mbsObject.tcpIpAddr) &&
                Objects.equals(tcpPort, mbsObject.tcpPort) &&
                Objects.equals(rtuDev, mbsObject.rtuDev) &&
                Objects.equals(rtuSpeed, mbsObject.rtuSpeed) &&
                Objects.equals(filterField, mbsObject.filterField) &&
                Objects.equals(filterValue, mbsObject.filterValue) &&
                Objects.equals(prefix, mbsObject.prefix) &&
                type == mbsObject.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tcpIpAddr, tcpPort, rtuDev, rtuSpeed, filterField, filterValue, prefix, force, checkMbFunc, type);
    }

    @Override
    public void setModbusBlockOf(NodeScript.ModbusBlock block)
    {
        this.filterField   = block.getProperty("filter-field");
        this.filterValue   = block.getProperty("filter-value");
        this.prefix        = block.getProperty("set-prop-prefix");
        String force       = block.getProperty("force");
        this.force = (force != null && force.equals("1"));
        String checkMbfunc = block.getProperty("check-mbfunc");
        this.checkMbFunc   = (checkMbfunc != null && checkMbfunc.equals("1"));
        String type = block.getProperty("type");
        if(type.equals(Type.TCP.toString()))
            this.type = Type.TCP;
        else if(type.equals(Type.RTU.toString()))
            this.type = Type.RTU;
        if(this.type == Type.TCP)
        {
            this.tcpIpAddr = block.getProperty("inet-addr");
            this.tcpPort   = block.getProperty("inet-port");
        }
        else if(this.type == Type.RTU)
        {
            this.rtuDev   = block.getProperty("dev");
            this.rtuSpeed = block.getProperty("speed");
        }
    }

    public String getTcpIpAddr() {
        return tcpIpAddr;
    }

    public String getTcpPort() {
        return tcpPort;
    }

    public String getRtuDev() {
        return rtuDev;
    }

    public String getRtuSpeed() {
        return rtuSpeed;
    }

    @Override
    public String getFilterField() {
        return filterField;
    }

    @Override
    public String getFilterValue() {
        return filterValue;
    }

    @Override
    public String getPrefix() {
        return prefix;
    }

    public boolean isForce() {
        return force;
    }

    public boolean isCheckMbFunc() {
        return checkMbFunc;
    }

    public String getType() {
        if(type == null)
            return null;

        return type.toString();
    }

    public void setTcpIpAddr(String tcpIpAddr) {
        this.tcpIpAddr = tcpIpAddr;
    }

    public void setTcpPort(String tcpPort) {
        this.tcpPort = tcpPort;
    }

    public void setRtuDev(String rtuDev) {
        this.rtuDev = rtuDev;
    }

    public void setRtuSpeed(String rtuSpeed) {
        this.rtuSpeed = rtuSpeed;
    }

    public void setFilterField(String filterField) {
        this.filterField = filterField;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public void setCheckMbFunc(boolean checkMbFunc) {
        this.checkMbFunc = checkMbFunc;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String getFullName() {
        return fullname;
    }

    @Override
    public XmlTag getSettingsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<" + fullname + "/>");
        mainTag.setAttribute("name", fullname);

        return mainTag;
    }

    @Override
    public XmlTag getObjectsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
        mainTag.setAttribute("id", String.valueOf(id));
        mainTag.setAttribute("name", fullname);

        return mainTag;
    }

    @Override
    public String getAsPartOfScript(String space) {
        if(type == Type.RTU)
        {
            return  space + "--add-mbs" + name + " \\\\" + "\n" +
                    space + "--mbs" + name + "-name " + fullname + " \\\\" + "\n" +
                    space + "--mbs" + name + "-type " + type + " \\\\" + "\n" +
                    space + "--mbs" + name + "-dev " + rtuDev + " \\\\" + "\n" +
                    space + "--mbs" + name + "-speed " + rtuSpeed + " \\\\" + "\n" +
                    space + "--mbs" + name + "-filter-field " + filterField + " \\\\" + "\n" +
                    space + "--mbs" + name + "-filter-value " + filterValue + " \\\\" + "\n" +
                    space + "--mbs" + name + "-set-prop-prefix " + prefix + " \\\\" + "\n" +
                    space + "--mbs" + name + "-force " + (force ? "1" : "0") + " \\\\" + "\n" +
                    space + "--mbs" + name + "-check-mbfunc " + (checkMbFunc ? "1" : "0") + " \\\\" + "\n";
        }

        return  space + "--add-mbs" + name + " \\\\" + "\n" +
                space + "--mbs" + name + "-name " + fullname + " \\\\" + "\n" +
                space + "--mbs" + name + "-type " + type + " \\\\" + "\n" +
                space + "--mbs" + name + "-inet-addr " + tcpIpAddr + " \\\\" + "\n" +
                space + "--mbs" + name + "-inet-port " + tcpPort + " \\\\" + "\n" +
                space + "--mbs" + name + "-filter-field " + filterField + " \\\\" + "\n" +
                space + "--mbs" + name + "-filter-value " + filterValue + " \\\\" + "\n" +
                space + "--mbs" + name + "-set-prop-prefix " + prefix + " \\\\" + "\n" +
                space + "--mbs" + name + "-force " + (force ? "1" : "0") + " \\\\" + "\n" +
                space + "--mbs" + name + "-check-mbfunc " + (checkMbFunc ? "1" : "0") + " \\\\" + "\n";
    }

    @Override
    public LinkedHashMap<String, String> getMobusBlockProperties() {
        LinkedHashMap<String, String> properties = new LinkedHashMap<>();
        properties.put("name", fullname);
        properties.put("type", type.toString());
        properties.put("filter-field", filterField);
        properties.put("filter-value", filterValue);
        properties.put("set-prop-prefix", prefix);
        properties.put("inet-addr", tcpIpAddr);
        properties.put("inet-port", tcpPort);
        properties.put("dev", rtuDev);
        properties.put("speed", rtuSpeed);
        properties.put("force", (force ? "1" : "0"));
        properties.put("check-mbfunc", (checkMbFunc ? "1" : "0"));

        return properties;
    }

    public static class MBSFailedTypeException extends Exception
    {
        private String message = "Undefined type of MBS ";

        MBSFailedTypeException(String type)
        {
            super();
            message += type;
        }

        @Override
        public String getMessage() {
            return message;
        }
    }
}
