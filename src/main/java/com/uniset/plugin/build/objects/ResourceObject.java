package com.uniset.plugin.build.objects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ResourceObject {

    private File resourceFile;
    public List<File> files = new ArrayList<>();

    public ResourceObject(String path)
    {
        this.resourceFile = new File(path);
    }
    public ResourceObject(String path, List<File> files)
    {
        this.resourceFile = new File(path);
        this.files = files;
    }

    public String getName()
    {
        return resourceFile.getName();
    }

    public String getPath()
    {
        return resourceFile.getPath();
    }

    public void setDirectory(String path) {

        this.resourceFile = new File(path);
    }

    public File[] listFiles()
    {
        File[] array = new File[files.size()];
        for(int i = 0; i < files.size(); i++)
            array[i] = files.get(i);
        return array;
    }
}
