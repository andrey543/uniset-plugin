package com.uniset.plugin.build.objects.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlTag;
import com.uniset.plugin.gui.modbus.AddMBMMDialog;
import com.uniset.plugin.smemory.NodeScript;

import java.util.*;

public class MBMMObject extends ModbusObject {

    public static final String MBMM_PROPERTY_POLLTIME = "polltime";
    public static final String MBMM_PROPERTY_EXCHANGEMODE_ID_SENSOR = "exchangeModeID";

    private String fullname;
    private String exchangeSensor;
    private String filterField;
    private String filterValue;
    private String prefix;
    private long polltime;
    private boolean isPersistentConnection;
    private List<Device> deviceList;
    private List<Gate> gatesList;

    public MBMMObject(long id, String name)
    {
        super(id, name);
        this.fullname = "MBMM_" + name;
        deviceList    = new ArrayList<>();
        gatesList     = new ArrayList<>();
    }

    public MBMMObject(long id, final AddMBMMDialog dialog)
    {
        super(id, dialog.getName());

        this.fullname               = "MBMM_" + name;
        this.exchangeSensor         = dialog.getNetExchSens();
        this.filterField            = dialog.getNetFilterField();
        this.filterValue            = dialog.getNetFilterValue();
        this.prefix                 = dialog.getNetPrefix();
        this.polltime               = Long.parseLong(dialog.getNetPolltime());
        this.isPersistentConnection = dialog.isPersitentConnection();
        this.deviceList             = dialog.getDevices();
        this.gatesList              = dialog.getGates();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        this.fullname = "MBMM_" + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MBMMObject)) return false;
        if (!super.equals(o)) return false;
        MBMMObject that = (MBMMObject) o;
        return polltime == that.polltime &&
                isPersistentConnection == that.isPersistentConnection &&
                Objects.equals(exchangeSensor, that.exchangeSensor) &&
                Objects.equals(filterField, that.filterField) &&
                Objects.equals(filterValue, that.filterValue) &&
                Objects.equals(prefix, that.prefix) &&
                Objects.equals(deviceList, that.deviceList) &&
                Objects.equals(gatesList, that.gatesList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), exchangeSensor, filterField, filterValue, prefix, polltime, isPersistentConnection, deviceList, gatesList);
    }

    @Override
    public String getFullName() {
        return fullname;
    }

    public List<Device> getDeviceList() {
        return new ArrayList<>(deviceList);
    }

    public List<Gate> getGatesList() {
        return new ArrayList<>(gatesList);
    }

    public String getExchangeSensor() {
        return exchangeSensor;
    }

    @Override
    public String getFilterField() {
        return filterField;
    }

    @Override
    public String getFilterValue() {
        return filterValue;
    }
    @Override
    public String getPrefix() {
        return prefix;
    }

    public long getPolltime() {
        return polltime;
    }

    public boolean isPersistentConnection() {
        return isPersistentConnection;
    }

    public void setExchangeModeIdSensor(String exchangeSensor) {
        this.exchangeSensor = exchangeSensor;
    }

    public void setPolltime(long polltime) {
        this.polltime = polltime;
    }

    public void setExchangeSensor(String exchangeSensor) {
        this.exchangeSensor = exchangeSensor;
    }

    public void setFilterField(String filterField) {
        this.filterField = filterField;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setPersistentConnection(boolean persistentConnection) {
        isPersistentConnection = persistentConnection;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = new ArrayList<>(deviceList);
    }

    public void setGatesList(List<Gate> gatesList) {
        this.gatesList = new ArrayList<>(gatesList);
    }

    public void addDevice(final Device device)
    {
        deviceList.add(device);
    }

    public void addGate(final Gate gate)
    {
        gatesList.add(gate);
    }

    @Override
    public void setModbusBlockOf(NodeScript.ModbusBlock block)
    {
        this.filterField = block.getProperty("filter-field");
        this.filterValue = block.getProperty("filter-value");
        this.prefix      = block.getProperty("set-prop-prefix");
        String persistentConnection = block.getProperty("persistent-connection");
        this.isPersistentConnection = (persistentConnection != null && persistentConnection.equals("1"));
    }

    @Override
    public XmlTag getSettingsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<" + fullname + "/>");
        mainTag.setAttribute("name", fullname);
        mainTag.setAttribute(MBMM_PROPERTY_EXCHANGEMODE_ID_SENSOR, exchangeSensor);
        mainTag.setAttribute(MBMM_PROPERTY_POLLTIME, String.valueOf(polltime));

        XmlTag deviceListTag = XmlElementFactory.getInstance(project).createTagFromText("<DeviceList>\n</DeviceList>");
        List<Device> devices = getDeviceList();
        Collections.reverse(devices);
        for(Device device : devices)
        {
            XmlTag itemTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
            itemTag.setAttribute("addr", device.getAddress());
            itemTag.setAttribute("timeout", String.valueOf(device.getTimeout()));
            if(device.isForce())  itemTag.setAttribute("force", "1");
            if(device.isInvert()) itemTag.setAttribute("invert", "1");
            itemTag.setAttribute("respondSensor", device.getRespondSensor());

            deviceListTag.addSubTag(itemTag, true);
        }

        XmlTag gateListTag = XmlElementFactory.getInstance(project).createTagFromText("<GateList>\n</GateList>");
        List<Gate> gates = getGatesList();
        Collections.reverse(gates);
        for(Gate gate : gates)
        {
            XmlTag itemTag = XmlElementFactory.getInstance(project).createHTMLTagFromText("<item/>");
            itemTag.setAttribute("ip", gate.getIp());
            itemTag.setAttribute("real_port", gate.getRealPort());
            itemTag.setAttribute("vstand_port", gate.getVstandPort());
            itemTag.setAttribute("respondInitTimeout", String.valueOf(gate.getRespondInitTimeout()));
            itemTag.setAttribute("recv_timeout", String.valueOf(gate.getRecvTimeout()));
            if(gate.isForce()) itemTag.setAttribute("force", "1");
            if(gate.isInvert()) itemTag.setAttribute("invert", "1");
            itemTag.setAttribute("respondSensor", gate.getRespondSensor());

            gateListTag.addSubTag(itemTag, true);
        }
        mainTag.addSubTag(deviceListTag, true);
        mainTag.addSubTag(gateListTag, true);

        return mainTag;
    }

    @Override
    public XmlTag getObjectsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
        mainTag.setAttribute("id", String.valueOf(id));
        mainTag.setAttribute("name", fullname);

        return mainTag;
    }

    @Override
    public String getAsPartOfScript(String space) {
        return space + "--add-mbmm" + name + " \\\\" + "\n" +
                space + "--mbmm" + name + "-name " + fullname + " \\\\" + "\n" +
                space + "--mbmm" + name + "-filter-field " + filterField + " \\\\" + "\n" +
                space + "--mbmm" + name + "-filter-value " + filterValue + " \\\\" + "\n" +
                space + "--mbmm" + name + "-persistent-connection " + (isPersistentConnection ? "1" : "0") + " \\\\" + "\n";
    }

    @Override
    public LinkedHashMap<String, String> getMobusBlockProperties() {
        LinkedHashMap<String, String> properties = new LinkedHashMap<>();
        properties.put("name", fullname);
        properties.put("filter-field", filterField);
        properties.put("filter-value", filterValue);
        properties.put("set-prop-prefix", prefix);
        properties.put("persistent-connection", (isPersistentConnection) ? "1" : "0");

        return properties;
    }
}
