package com.uniset.plugin.build.objects;

import com.uniset.plugin.Settings;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UnisetObjectFile {

    private String name;
    private List<UnisetObjectFile> subObjects = new ArrayList<>();
    private LinkedHashMap<String, String> files = new LinkedHashMap<>();

    public UnisetObjectFile(String name)
    {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<UnisetObjectFile> getSubdirs()
    {
        return subObjects;
    }

    public LinkedHashMap<String, String> getFiles()
    {
        return files;
    }

    public void addSubObject(UnisetObjectFile object)
    {
        subObjects.add(object);
    }

    public void addFile(String name, String content)
    {
        files.put(name, content);
    }

    public void setFiles(LinkedHashMap<String, String> files)
    {
        this.files = new LinkedHashMap<>(files);
    }

    public String getContentFile(String fileName)
    {
        for(Map.Entry<String, String> entry : getFiles().entrySet())
        {
            if(entry.getKey().equals(fileName))
                return entry.getValue();
        }

        return null;
    }

    public static UnisetObjectFile createFromPath(Path path) throws IOException {

        UnisetObjectFile root = new UnisetObjectFile(path.getFileName().toString());

        File[] listFiles = path.toFile().listFiles();
        if(listFiles != null) {
            for (File f : listFiles) {
                if (f.isFile()) {
                    if(Settings.FilterFiles.checkFile(f.getName()))
                        root.files.put(f.getName(), new String(Files.readAllBytes(f.toPath())));
                }
                else if (f.isDirectory())
                    if(Settings.FilterFiles.checkDir(f.getName()))
                        root.subObjects.add(createFromPath(f.toPath()));
            }
        }

        return root;
    }

    public static UnisetObjectFile createFromObject(UnisetObjectFile unisetObjectFile)
    {
        UnisetObjectFile newUnisetObjectFile = new UnisetObjectFile(unisetObjectFile.getName());
        if(!unisetObjectFile.getSubdirs().isEmpty())
        {
            for (UnisetObjectFile subObject : unisetObjectFile.getSubdirs())
                newUnisetObjectFile.addSubObject(createFromObject(subObject));
        }

        for(Map.Entry<String, String> entry : unisetObjectFile.getFiles().entrySet())
            newUnisetObjectFile.addFile(entry.getKey(), entry.getValue());

        return newUnisetObjectFile;
    }



}
