package com.uniset.plugin.build.objects.modbus;

import java.util.Objects;

public class Device {
    public static final String DEVICE_PROPERTY_ADDR = "addr";
    public static final String DEVICE_PROPERTY_TIMEOUT = "timeout";
    public static final String DEVICE_PROPERTY_FORCE = "force";
    public static final String DEVICE_PROPERTY_INVERT = "invert";
    public static final String DEVICE_PROPERTY_RESPOND_SENSOR = "respondSensor";

    private String address;
    private String respondSensor;
    private boolean force;
    private boolean invert;
    private long timeout;

    public Device(String address)
    {
        this.address = address;
    }
    public Device(String address, String respondSensor, boolean force, boolean invert, long timeout) {
        this.address = address;
        this.respondSensor = respondSensor;
        this.force = force;
        this.invert = invert;
        this.timeout = timeout;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Device)) return false;
        Device device = (Device) o;
        return force == device.force &&
                invert == device.invert &&
                timeout == device.timeout &&
                Objects.equals(address, device.address) &&
                Objects.equals(respondSensor, device.respondSensor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, respondSensor, force, invert, timeout);
    }

    public String getAddress() {
        return address;
    }

    public String getRespondSensor() {
        return respondSensor;
    }

    public boolean isForce() {
        return force;
    }

    public boolean isInvert() {
        return invert;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setRespondSensor(String respondSensor) {
        this.respondSensor = respondSensor;
    }

    public void setForce(boolean force) {
        this.force = force;
    }

    public void setInvert(boolean invert) {
        this.invert = invert;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }
}
