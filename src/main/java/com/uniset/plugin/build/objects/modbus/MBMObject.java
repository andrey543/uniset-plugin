package com.uniset.plugin.build.objects.modbus;

import com.intellij.openapi.project.Project;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlTag;
import com.uniset.plugin.gui.modbus.AddMBMDialog;
import com.uniset.plugin.smemory.NodeScript;

import java.util.*;

public class MBMObject extends ModbusObject{

    public static final String MBM_PROPERTY_GATEWAY_IADDR = "gateway_iaddr";
    public static final String MBM_PROPERTY_GATEWAY_REAL_PORT = "gateway_real_port";
    public static final String MBM_PROPERTY_POLLTIME = "polltime";
    public static final String MBM_PROPERTY_EXCHANGEMODE_ID_SENSOR = "exchangeModeID";
    public static final String MBM_PROPERTY_RESPOND_ID_SENSOR = "respondSensor";

    private String fullname;
    private String gatewayAddress;
    private String gatewayPort;
    private String exchangeModeIdSensor;
    private String respondSensor;
    private String filterField;
    private String filterValue;
    private String prefix;
    private boolean isPersistentConnection;
    private int polltime;
    private List<Device> deviceList;

    public MBMObject(long id, final AddMBMDialog dialog) {
        super(id, dialog.getName());
        this.fullname = "MBM_" + name;
        this.gatewayAddress = dialog.getNetGatewayIPAddr();
        this.gatewayPort = dialog.getNetGatewayPort();
        this.exchangeModeIdSensor = dialog.getNetExchSens();
        this.respondSensor = dialog.getNetRespSensor();
        this.filterField = dialog.getNetFilterField();
        this.filterValue = dialog.getNetFilterValue();
        this.prefix = dialog.getNetPrefix();
        this.isPersistentConnection = dialog.isPersitentConnection();
        this.polltime = Integer.parseInt(dialog.getNetPolltime());
        this.deviceList = new ArrayList<>(dialog.getDevices());
    }

    public MBMObject(long id, final String name)
    {
        super(id, name);
        this.fullname = "MBM_" + name;
        this.deviceList = new ArrayList<>();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        this.fullname = "MBM_" + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MBMObject)) return false;
        if (!super.equals(o)) return false;
        MBMObject mbmObject = (MBMObject) o;
        return isPersistentConnection == mbmObject.isPersistentConnection &&
                polltime == mbmObject.polltime &&
                Objects.equals(gatewayAddress, mbmObject.gatewayAddress) &&
                Objects.equals(gatewayPort, mbmObject.gatewayPort) &&
                Objects.equals(exchangeModeIdSensor, mbmObject.exchangeModeIdSensor) &&
                Objects.equals(respondSensor, mbmObject.respondSensor) &&
                Objects.equals(filterField, mbmObject.filterField) &&
                Objects.equals(filterValue, mbmObject.filterValue) &&
                Objects.equals(prefix, mbmObject.prefix) &&
                Objects.equals(deviceList, mbmObject.deviceList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gatewayAddress, gatewayPort, exchangeModeIdSensor, respondSensor, filterField, filterValue, prefix, isPersistentConnection, polltime, deviceList);
    }

    @Override
    public String getFullName() {
        return fullname;
    }
    @Override
    public String getPrefix() {
        return prefix;
    }

    public String getRespondSensor() {
        return respondSensor;
    }

    public int getPolltime() {
        return polltime;
    }

    public String getGatewayAddress() {
        return gatewayAddress;
    }

    public String getGatewayPort() {
        return gatewayPort;
    }

    public String getExchangeModeIdSensor() {
        return exchangeModeIdSensor;
    }

    @Override
    public String getFilterField() {
        return filterField;
    }

    @Override
    public String getFilterValue() {
        return filterValue;
    }

    public boolean isPersistentConnection() {
        return isPersistentConnection;
    }

    public List<Device> getDeviceList() {
        return new ArrayList<>(deviceList);
    }

    public void setGatewayAddress(String gatewayAddress) {
        this.gatewayAddress = gatewayAddress;
    }

    public void setGatewayPort(String gatewayPort) {
        this.gatewayPort = gatewayPort;
    }

    public void setExchangeModeIdSensor(String exchangeModeIdSensor) {
        this.exchangeModeIdSensor = exchangeModeIdSensor;
    }

    public void setRespondSensor(String respondSensor) {
        this.respondSensor = respondSensor;
    }

    public void setPolltime(int polltime) {
        this.polltime = polltime;
    }

    public void setFilterField(String filterField) {
        this.filterField = filterField;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setPersistentConnection(boolean persistentConnection) {
        isPersistentConnection = persistentConnection;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = new ArrayList<>(deviceList);
    }

    @Override
    public void setModbusBlockOf(NodeScript.ModbusBlock block)
    {
        this.filterField = block.getProperty("filter-field");
        this.filterValue = block.getProperty("filter-value");
        this.prefix      = block.getProperty("set-prop-prefix");
        String persistentConnection = block.getProperty("persistent-connection");
        this.isPersistentConnection = (persistentConnection != null && persistentConnection.equals("1"));
    }

    public void addDevice(final Device device)
    {
        deviceList.add(device);
    }

    @Override
    public XmlTag getSettingsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<" + fullname + "/>");
        mainTag.setAttribute("name", fullname);
        mainTag.setAttribute(MBM_PROPERTY_GATEWAY_IADDR, gatewayAddress);
        mainTag.setAttribute(MBM_PROPERTY_GATEWAY_REAL_PORT, gatewayPort);
        mainTag.setAttribute(MBM_PROPERTY_POLLTIME, String.valueOf(polltime));
        mainTag.setAttribute(MBM_PROPERTY_EXCHANGEMODE_ID_SENSOR, exchangeModeIdSensor);
        mainTag.setAttribute(MBM_PROPERTY_RESPOND_ID_SENSOR, respondSensor);

        XmlTag deviceListTag = XmlElementFactory.getInstance(project).createTagFromText("<DeviceList>\n</DeviceList>");
        List<Device> devices = getDeviceList();
        Collections.reverse(devices);
        for(Device device : devices)
        {
            XmlTag itemTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
            itemTag.setAttribute(Device.DEVICE_PROPERTY_ADDR, device.getAddress());
            itemTag.setAttribute(Device.DEVICE_PROPERTY_TIMEOUT, String.valueOf(device.getTimeout()));
            if(device.isForce())  itemTag.setAttribute(Device.DEVICE_PROPERTY_FORCE, "1");
            if(device.isInvert()) itemTag.setAttribute(Device.DEVICE_PROPERTY_INVERT, "1");
            itemTag.setAttribute(Device.DEVICE_PROPERTY_RESPOND_SENSOR, device.getRespondSensor());

            deviceListTag.addSubTag(itemTag, true);
        }
        mainTag.addSubTag(deviceListTag, true);

        return mainTag;
    }

    @Override
    public XmlTag getObjectsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
        mainTag.setAttribute("id", String.valueOf(id));
        mainTag.setAttribute("name", fullname);

        return mainTag;
    }

    @Override
    public String getAsPartOfScript(String space) {
        return space + "--add-mbm" + name + " \\\\" + "\n" +
                space + "--mbm" + name + "-name " + fullname + " \\\\" + "\n" +
                space + "--mbm" + name + "-set-prop-prefix " + prefix + " \\\\" + "\n" +
                space + "--mbm" + name + "-filter-field " + filterField + " \\\\" + "\n" +
                space + "--mbm" + name + "-filter-value " + filterValue + " \\\\" + "\n" +
                space + "--mbm" + name + "-persistent-connection " + (isPersistentConnection ? "1" : "0") + " \\\\" + "\n";
    }

    @Override
    public LinkedHashMap<String, String> getMobusBlockProperties() {
        LinkedHashMap<String, String> properties = new LinkedHashMap<>();
        properties.put("name", fullname);
        properties.put("filter-field", filterField);
        properties.put("filter-value", filterValue);
        properties.put("set-prop-prefix", prefix);
        properties.put("persistent-connection", (isPersistentConnection) ? "1" : "0");

        return properties;
    }
}
