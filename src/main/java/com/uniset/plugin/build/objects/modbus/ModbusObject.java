package com.uniset.plugin.build.objects.modbus;

import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.configure.XmlTagUnisetObject;
import com.uniset.plugin.smemory.ModbusBlockConvertable;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.NodeScriptConvertable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class ModbusObject implements XmlTagUnisetObject, NodeScriptConvertable, ModbusBlockConvertable {
    protected String name;
    protected long id;
    protected Node node;
    protected List<ModbusSensor> sensors = new ArrayList<>();

    public ModbusObject(long id, String name) {
        this.name = name;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModbusObject that = (ModbusObject) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(node, that.node) &&
                Objects.equals(sensors, that.sensors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, node, sensors);
    }

    public abstract String getFullName();
    public abstract String getFilterField();
    public abstract String getFilterValue();
    public abstract String getPrefix();
    public abstract void setModbusBlockOf(NodeScript.ModbusBlock block);

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public List<ModbusSensor> getSensors() {
        return sensors;
    }

    public void addSensor(final Sensor sensor, String addr, String mbreg, String mbfunc, String nbit)
    {
        sensors.add(new ModbusSensor(addr, mbreg, mbfunc, nbit, sensor));
    }

    public void removeSensor(final ModbusSensor sensor)
    {
        sensors.remove(sensor);
    }

    public class ModbusSensor
    {
        private final String mbaddr;
        private final String mbreg;
        private final String mbfunc;
        private final String nbit;
        private final Sensor sensor;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof ModbusSensor)) return false;
            ModbusSensor that = (ModbusSensor) o;
            return Objects.equals(mbreg, that.mbreg) &&
                    Objects.equals(mbfunc, that.mbfunc) &&
                    Objects.equals(nbit, that.nbit) &&
                    Objects.equals(sensor, that.sensor);
        }

        @Override
        public int hashCode() {
            return Objects.hash(mbreg, mbfunc, nbit, sensor);
        }

        private ModbusSensor(String mbaddr, String mbreg, String mbfunc, String nbit, Sensor sensor) {

            this.mbaddr = mbaddr;
            this.mbreg = mbreg;
            this.mbfunc = mbfunc;
            this.nbit = nbit;
            this.sensor = sensor;
        }

        public ModbusObject getModbusObject()
        {
            return ModbusObject.this;
        }

        public String getMbaddr() {
            return mbaddr;
        }

        public String getMbreg() {
            return mbreg;
        }

        public String getMbfunc() {
            return mbfunc;
        }

        public String getNbit() {
            return nbit;
        }

        public Sensor getSensor() {
            return sensor;
        }
    }

}
