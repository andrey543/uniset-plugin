package com.uniset.plugin.build.objects;

import com.intellij.openapi.project.Project;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlTag;
import com.uniset.plugin.configure.XmlTagSensor;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class Sensor implements Cloneable, XmlTagSensor {
    private long id;
    private String name;
    private String iotype;
    private String textname;
    private LinkedHashMap<String, String> properties = new LinkedHashMap<>();

    public Sensor(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Sensor(long id, String name, String iotype, String textname) {
        this.id = id;
        this.name = name;
        this.iotype = iotype;
        this.textname = textname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(name, sensor.name) && Objects.equals(iotype, sensor.iotype) && Objects.equals(textname, sensor.textname) && Objects.equals(properties, sensor.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, iotype, textname, properties);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public XmlTag getSensorTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
        mainTag.setAttribute("id", String.valueOf(id));
        mainTag.setAttribute("name", name);
        mainTag.setAttribute("textname", textname);
        mainTag.setAttribute("iotype", iotype);

        for(Map.Entry<String, String> prop : properties.entrySet())
            mainTag.setAttribute(prop.getKey(), prop.getValue());

        return mainTag;
    }

    public String getIotype() {
        return iotype;
    }

    public String getTextname() {
        return textname;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addProperty(String name, String value)
    {
        properties.put(name, value);
    }

    public String getProperty(String name)
    {
        return properties.get(name);
    }

    public LinkedHashMap<String, String> getProperties() {
        return properties;
    }
}
