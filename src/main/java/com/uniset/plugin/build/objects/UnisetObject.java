package com.uniset.plugin.build.objects;

import com.intellij.openapi.project.Project;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlTag;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.XmlTagUnisetObject;
import com.uniset.plugin.configure.xml.XMLReader;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class UnisetObject implements XmlTagUnisetObject {

    private static final String DEFAULT_MSG_COUNT = "30";
    private static final String DEFAULT_MSEC      = "200";

    private String id;
    private String typename;
    private String name;
    private String textname;
    private Path srcXml;
    /* TODO Стоит сделать синхронизацию с параметрами в .src.xml
     * т.к есть баг при сохранении - если нету совпадающего файла .src.xml с алгоритмом
      * в секции settings, то удаляются все настройки у алгоритма*/
    private LinkedHashMap<String, String> property = new LinkedHashMap<>();

    private Setting setting;
    private List<Param> params = new ArrayList<>();
    private List<SMapSensor> smapSensors = new ArrayList<>();

    public UnisetObject(String id, String typename, String name) {
        this.id = id;
        this.typename = typename;
        this.name = name;
    }

    public UnisetObject(String id, String typename, String name, String textname) {
        this.id = id;
        this.typename = typename;
        this.name = name;
        this.textname = textname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UnisetObject)) return false;
        UnisetObject object = (UnisetObject) o;
        return Objects.equals(params, object.params) &&
                Objects.equals(smapSensors, object.smapSensors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(params, smapSensors);
    }

    public Path getSrcXml() {
        return srcXml;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTypename() {
        return typename;
    }

    public Setting getSetting() {
        return setting;
    }

    public List<Param> getParams() {
        return params;
    }

    public List<SMapSensor> getSmapSensors() {
        return smapSensors;
    }

    public void addProperty(String field, String value)
    {
        property.put(field, value);
    }

    public void setSmapSensor(String smapSensorName, Sensor sensor)
    {
        SMapSensor sMapSensor = getSMapSensorByName(smapSensorName);
        if(sMapSensor != null)
            sMapSensor.setAttachSensor(sensor);
    }
    public void setSrcXml(Path srcXml, Configure configure) throws JDOMException, ParserConfigurationException, SAXException, IOException {
        this.srcXml = srcXml;
        updatePropertyFromSrcXml(configure);
    }

    public void setInputStreamSrcXml(InputStream is, Configure configure) throws JDOMException, ParserConfigurationException, SAXException, IOException {
        updatePropertyFromSrcXml(is, configure);
    }
    public void updatePropertyFromSrcXml(InputStream is, Configure configure) throws IOException, JDOMException, ParserConfigurationException, SAXException {

        XMLReader reader = new XMLReader(is);
        updateSettingsSrcXml(reader);
        updateParamsSrcXml(reader);
        updateSensorsSrcXml(reader, configure);
    }

    public void updatePropertyFromSrcXml(Configure configure) throws IOException, JDOMException, ParserConfigurationException, SAXException {
        if(srcXml == null)
            throw new FileNotFoundException("File src.xml is not found for this object");

        XMLReader reader = new XMLReader(srcXml);
        updateSettingsSrcXml(reader);
        updateParamsSrcXml(reader);
        updateSensorsSrcXml(reader, configure);
    }

    private void updateSettingsSrcXml(final XMLReader reader)
    {
        String msgCount = getValueFromSettings(reader, "msg-count");
        String sleepCount = getValueFromSettings(reader, "sleep-msec");

        setting = new Setting(
                isUnisetManager(reader),
                typename,
                msgCount == null ? Integer.parseInt(DEFAULT_MSG_COUNT) : Integer.parseInt(msgCount),
                sleepCount == null ? Integer.parseInt(DEFAULT_MSEC) : Integer.parseInt(sleepCount));
    }

    private void updateParamsSrcXml(final XMLReader reader)
    {
        Element variables = reader.stax.findFirtsChild("variables");
        if(variables == null)
            return;
        for(Element item : variables.getChildren())
        {
            String name     = item.getAttributeValue("name");
            String type     = item.getAttributeValue("type");
            String comment  = item.getAttributeValue("comment");
            String defValue = item.getAttributeValue("default");

            String settedValue = property.get(name);

            params.add(new Param(settedValue, name, type, comment, defValue));
        }
    }

    private void updateSensorsSrcXml(final XMLReader reader, Configure configure)
    {
        Element smap = reader.stax.findFirtsChild("smap");
        if(smap == null)
            return;
        for(Element item : smap.getChildren())
        {
            String name     = item.getAttributeValue("name");
            String vartype     = item.getAttributeValue("vartype");
            String iotype     = item.getAttributeValue("iotype");
            String comment  = item.getAttributeValue("comment");
            String initFromSM = item.getAttributeValue("initFromSM");
            String noCheckId = item.getAttributeValue("no_check_id");

            Sensor sensor = null;
            String sensorName = property.get(name);
            if(sensorName != null)
                sensor = configure.getSensorByName(sensorName);

            smapSensors.add(
                    new SMapSensor(
                            sensor,
                            name,
                            vartype,
                            iotype,
                            comment,
                            noCheckId != null && noCheckId.equals("1"),
                            initFromSM != null && initFromSM.equals("1")));
        }
    }

    private boolean isUnisetManager(XMLReader reader)
    {
        Element settings = reader.stax.findFirtsChild("settings");
        if(settings == null)
            return false;
        for(Element item : settings.getChildren()) {
            String name = item.getAttributeValue("name");
            String val  = item.getAttributeValue("val");

            if(name.equals("base-class") && val.equals("UniSetManager"))
            {
                return true;
            }
        }
        return false;
    }

    private String getValueFromSettings(XMLReader reader, String key)
    {
        Element settings = reader.stax.findFirtsChild("settings");
        if(settings == null)
            return null;
        for(Element item : settings.getChildren()) {
            String name = item.getAttributeValue("name");
            String val  = item.getAttributeValue("val");

            if(name.equals(key))
            {
                return val;
            }
        }
        return null;
    }

    @Override
    public XmlTag getSettingsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<" + typename + "/>");
        mainTag.setAttribute("name", name);
        for(Param param : params)
        {
            if(param.getSettedValue() != null && !param.getSettedValue().isEmpty())
            {
                mainTag.setAttribute(param.getName(), param.getSettedValue());
            }
        }
        for(SMapSensor sMapSensor : smapSensors)
        {
            if(sMapSensor.getAttachSensor() != null)
                mainTag.setAttribute(sMapSensor.getName(), sMapSensor.getAttachSensor().getName());
        }

        return mainTag;
    }

    @Override
    public XmlTag getObjectsTag(Project project) {
        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
        mainTag.setAttribute("id", String.valueOf(id));
        mainTag.setAttribute("name", name);

        return mainTag;
    }


    public class SMapSensor
    {
        private final String name;
        private final String varType;
        private final String iotype;
        private final String comment;
        private final boolean noCheckId;
        private final boolean initFromSm;

        private Sensor attachSensor;

        private SMapSensor(Sensor attachSensor, String name, String varType, String iotype, String comment, boolean noCheckId, boolean initFromSm) {
            this.attachSensor = attachSensor;
            this.name = name;
            this.varType = varType;
            this.iotype  = iotype;
            this.comment = comment;
            this.noCheckId = noCheckId;
            this.initFromSm = initFromSm;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof SMapSensor)) return false;
            SMapSensor that = (SMapSensor) o;
            return Objects.equals(attachSensor, that.attachSensor);
        }

        @Override
        public int hashCode() {
            return Objects.hash(attachSensor);
        }

        public Sensor getAttachSensor() {
            return attachSensor;
        }

        public String getName() {
            return name;
        }

        public String getVarType() {
            return varType;
        }

        public String getIotype() {
            return iotype;
        }

        public String getComment() {
            return comment;
        }

        public boolean isNoCheckId() {
            return noCheckId;
        }

        public boolean isInitFromSm() {
            return initFromSm;
        }

        public void setAttachSensor(Sensor attachSensor) {
            this.attachSensor = attachSensor;
        }
    }

    public static class Setting
    {
        private final boolean isUnisetManager;
        private final String className;
        private final int msgCount;
        private final int sleepMsec;

        private Setting(boolean isUnisetManager, String className, int msgCount, int sleepMsec) {
            this.isUnisetManager = isUnisetManager;
            this.className = className;
            this.msgCount = msgCount;
            this.sleepMsec = sleepMsec;
        }

        public boolean isUnisetManager() {
            return isUnisetManager;
        }

        public String getClassName() {
            return className;
        }

        public int getMsgCount() {
            return msgCount;
        }

        public int getSleepMsec() {
            return sleepMsec;
        }
    }

    public static class Param
    {
        private final String name;
        private final String type;
        private final String comment;
        private final String defaultValue;

        private String settedValue;

        private Param(String settedValue, String name, String type, String comment, String defaultValue) {
            this.settedValue = settedValue;
            this.name = name;
            this.type = type;
            this.comment = comment;
            this.defaultValue = defaultValue;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Param)) return false;
            Param param = (Param) o;
            return Objects.equals(settedValue, param.settedValue);
        }

        @Override
        public int hashCode() {
            return Objects.hash(settedValue);
        }

        public String getSettedValue() {
            return settedValue;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getComment() {
            return comment;
        }

        public String getDefaultValue() {
            return defaultValue;
        }

        public void setSettedValue(String settedValue) {
            this.settedValue = settedValue;
        }
    }

    private SMapSensor getSMapSensorByName(String name)
    {
        for(SMapSensor sMapSensor : smapSensors)
        {
            if(sMapSensor.getName().equals(name))
                return sMapSensor;
        }

        return null;
    }
}
