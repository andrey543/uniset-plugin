package com.uniset.plugin.build;

import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SensorGenerator {
    private Configure configure;
    private List<SensorRequest> sensorRequests = new ArrayList<>();
    private List<Sensor> sensors;

    public SensorGenerator(final Configure configure) {
        this.configure = configure;
    }

    public void registrySensor(String name, String textname, String iotype)
    {
        sensorRequests.add(new SensorRequest(name, textname, iotype));
    }

    public void registrySensor(String name, String textname, String iotype, LinkedHashMap<String, String> advancedProps)
    {
        sensorRequests.add(new SensorRequest(name, textname, iotype, advancedProps));
    }

    public List<Sensor> getSensors()
    {
        if (sensors == null) {

            sensors = new ArrayList<>();
            long id = configure.getNextFreeSensorId();
            for (SensorRequest sensorRequest : sensorRequests) {
                Sensor sensor = new Sensor(id, sensorRequest.name, sensorRequest.iotype, sensorRequest.textname);
                if (sensorRequest.advancedProps != null) {
                    for (Map.Entry<String, String> props : sensorRequest.advancedProps.entrySet())
                        sensor.addProperty(props.getKey(), props.getValue());
                }
                sensors.add(sensor);
                id++;
            }
        }

        return sensors;
    }

    private class SensorRequest
    {
        private String name;
        private String textname;
        private String iotype;
        private LinkedHashMap<String, String> advancedProps;

        private SensorRequest(String name, String textname, String iotype) {
            this.name = name;
            this.textname = textname;
            this.iotype = iotype;
        }

        public SensorRequest(String name, String textname, String iotype, LinkedHashMap<String, String> advancedProps) {
            this.name = name;
            this.textname = textname;
            this.iotype = iotype;
            this.advancedProps = advancedProps;
        }
    }
}
