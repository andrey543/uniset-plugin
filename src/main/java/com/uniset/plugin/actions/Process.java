package com.uniset.plugin.actions;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.project.Project;

public class Process {

    private Project project;
    private String processName;
    private Runnable runnable;
    private boolean b;

    public Process(final Project project, String processName, boolean b, Runnable runnable)
    {
        this.project = project;
        this.processName = processName;
        this.b = b;
        this.runnable = runnable;
    }

    public void run()
    {

        ApplicationManager.getApplication().invokeAndWait(new Runnable() {
            @Override
            public void run() {
                ProgressManager.getInstance().runProcessWithProgressSynchronously(new Runnable() {
                    @Override
                    public void run() {
                        ProgressIndicator indicator = ProgressManager.getInstance().getProgressIndicator();
                        indicator.setText(processName);
                        runnable.run();
                    }
                }, processName, b, project);
            }
        });
    }



}
