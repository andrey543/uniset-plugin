package com.uniset.plugin.actions.newactions;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.actions.Process;
import com.uniset.plugin.build.UnisetObjectTransformator;
import com.uniset.plugin.build.cmake.CMakeConfiguration;
import com.uniset.plugin.build.make.MakeConfiguration;
import com.uniset.plugin.gui.NewObjectDialog;
import com.uniset.plugin.configure.Configure;
import org.jdom2.JDOMException;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class NewObjectAction extends AnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if(view == null) {
            return;
        }

        Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        PsiDirectory directory = view.getOrChooseDirectory();
        if(directory == null) {
            return;
        }

        // Инициализация configure.xml
        Configure configure = Configure.getInstance(project);
        if(configure == null)
            return;

        NewObjectDialog dialog = new NewObjectDialog(project, configure);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();

        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            String fileName = dialog.nameOfObject();

            if (fileName.equals("")) {
                Messages.showMessageDialog(project, "Name of object is empty", "Uniset UnisetObject", Messages.getInformationIcon());
                return;
            }

            VirtualFile targetDir = directory.getVirtualFile();
            if(dialog.isDirectory()){
                Path path = Paths.get(targetDir.getPath() + "/" + fileName);
                if (Files.exists(path)) {
                    Messages.showMessageDialog(project, "This uniset object is already exist. " +
                            "Please, remove this file:" +
                            " " + fileName, "Uniset UnisetObject", Messages.getInformationIcon());
                    return;
                }
            }
            else {
                for (String ext : new String[]{".cc", ".h", ".src.xml"}) {
                    Path path = Paths.get(targetDir.getPath() + "/" + fileName + ext);
                    if (Files.exists(path)) {
                        Messages.showMessageDialog(project, "This uniset object or part is already exist. " +
                                "Please, remove this file:" +
                                " " + fileName + ext, "Uniset UnisetObject", Messages.getInformationIcon());
                        return;
                    }
                }
            }

            final String[] hFile = new String[1];
            final String[] ccFile = new String[1];
            final String[] srcFile = new String[1];
            final String[] mainFile = new String[1];
            final String[] startScript = new String[1];
            Process processPreparing = new Process(project, "Preparing files...", true, new Runnable() {
                @Override
                public void run() {
                    try {
                        hFile[0] = UnisetObjectTransformator.transformTemplateFromResource("/templates/TemplateObject.h", project.getName(), fileName);
                        ccFile[0] = UnisetObjectTransformator.transformTemplateFromResource("/templates/TemplateObject.cc", project.getName(), fileName);
                        if(dialog.isCreateMainCc()) {
                            srcFile[0] = UnisetObjectTransformator.transformTemplateFromResource("/templates/um_templateobject.src.xml", project.getName(), fileName);
                            mainFile[0] = UnisetObjectTransformator.transformTemplateFromResource("/templates/templateobject.cc", project.getName(), fileName);
                            startScript[0] = UnisetObjectTransformator.transformTemplateFromResource("/templates/start_fg.sh", project.getName(), fileName);
                        }
                        else
                            srcFile[0] = UnisetObjectTransformator.transformTemplateFromResource("/templates/templateobject.src.xml", project.getName(), fileName);
                    } catch (IOException e1) {
                        Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                e1.getMessage(), "Exception", Messages.getInformationIcon());
                    }
                }
            });
            processPreparing.run();


            Process processCreating = new Process(project, "Creating files...", true, new Runnable() {
                @Override
                public void run() {
                    try {
                        WriteCommandAction
                                .writeCommandAction(project)
                                .withGlobalUndo()
                                .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                                .withName("Creating " + fileName)
                                .run(new ThrowableRunnable<Throwable>() {
                                    @Override
                                    public void run() throws Throwable {
                                        VirtualFile finalDir = targetDir;
                                        if(dialog.isDirectory()) {
                                            try {
                                                finalDir = targetDir.createChildDirectory(project, fileName);
                                                configure.createLink(finalDir.getPath());
                                            } catch (IOException e1) {
                                                Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                                        e1.getMessage(), "Exception", Messages.getInformationIcon());
                                            }
                                        }

                                        FileUtils.createFile(project, finalDir.getPath(), fileName + ".h", hFile[0]);
                                        FileUtils.createFile(project, finalDir.getPath(), fileName + ".cc", ccFile[0]);
                                        FileUtils.createFile(project, finalDir.getPath(), fileName.toLowerCase() + ".src.xml", srcFile[0]);
                                        if(dialog.isCreateMainCc()) {
                                            FileUtils.createFile(project, finalDir.getPath(), fileName.toLowerCase() + ".cc", mainFile[0]);
                                            FileUtils.createFile(project, finalDir.getPath(), "start_fg_" + fileName.toLowerCase() +".sh", startScript[0]);
                                        }

                                        if(dialog.isMakeConfiguration())
                                        {
                                            // Инициализация Make-конфигурации
                                            MakeConfiguration makeConfiguration;
                                            try {
                                                makeConfiguration = MakeConfiguration.getInstance(project);
                                            } catch (FileNotFoundException e1) {
                                                // Сообщение(отмена процесса): configure.ac не найден
                                                Messages.showMessageDialog(project,
                                                        e1.getMessage(),
                                                        "Uniset algorithm", Messages.getErrorIcon());
                                                return;
                                            }

                                            String pathToMakefile = finalDir.getPath() + "/" + "Makefile.am";
                                            if(Files.exists(Paths.get(pathToMakefile)))
                                            {
                                                try {
                                                    if(dialog.isCreateMainCc())
                                                    {
                                                        makeConfiguration.configureLocalFile(
                                                                pathToMakefile,
                                                                "/templates/main_make/Makefile.am",
                                                                fileName,
                                                                finalDir.getName());
                                                    }
                                                    else
                                                        makeConfiguration.configureLocalFile(
                                                            pathToMakefile,
                                                            "/templates/Makefile.am",
                                                            fileName,
                                                            finalDir.getName());
                                                } catch (IOException e1) {
                                                    Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                                            e1.getMessage(), "Exception", Messages.getInformationIcon());
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                try {
                                                    if(dialog.isCreateMainCc())
                                                        makeConfiguration.createNewLocalFile(project,
                                                                finalDir.getPath() + "/",
                                                                "/templates/main_make/Makefile.am",
                                                                fileName);

                                                    else
                                                        makeConfiguration.createNewLocalFile(project,
                                                            finalDir.getPath() + "/",
                                                            "/templates/Makefile.am",
                                                            fileName);
                                                } catch (Throwable e1) {
                                                    Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                                            e1.getMessage(), "Exception", Messages.getInformationIcon());
                                                    return;
                                                }
                                            }

                                            try {
                                                makeConfiguration.configureMainFile(project, pathToMakefile);
                                            } catch (Throwable e1) {
                                                Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                                        e1.getMessage(), "Exception", Messages.getInformationIcon());
                                                return;
                                            }

                                            try {
                                                makeConfiguration.configureSubdirs(project, pathToMakefile);
                                            } catch (Throwable e1) {
                                                Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                                        e1.getMessage(), "Exception", Messages.getInformationIcon());
                                                return;
                                            }
                                        }
                                        if(dialog.isCMakeConfiguration())
                                        {
                                            CMakeConfiguration cMakeConfiguration;
                                            try {
                                                cMakeConfiguration = CMakeConfiguration.getInstance(project);
                                            } catch (FileNotFoundException e1) {
                                                Messages.showMessageDialog(project,
                                                        e1.getMessage(),
                                                        "Uniset algorithm", Messages.getErrorIcon());
                                                return;
                                            }

                                            cMakeConfiguration.configureFileAt(finalDir.getPath(), fileName, dialog.isCreateMainCc());
                                            cMakeConfiguration.configureSubdirsAt(finalDir.getPath());
                                        }
                                    }
                                });

                    } catch (Throwable e1) {
                        Messages.showMessageDialog(project, MakeConfiguration.MESSAGE_TITLE +
                                e1.getMessage(), "Exception", Messages.getInformationIcon());
                    }
                }
            });
            processCreating.run();

            targetDir.refresh(true,false);

        }
    }
}
