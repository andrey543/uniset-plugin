package com.uniset.plugin.actions.newactions.node;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.UnisetObjectTransformator;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Hosts;
import com.uniset.plugin.configure.Node;
import com.uniset.plugin.configure.XmlNode;
import com.uniset.plugin.gui.NewNodeDialog;
import com.uniset.plugin.build.SensorGenerator;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class NewNodeImpl implements NewNode{

    private Node node;
    private Project project;
    private Configure configure;
    private NewNodeDialog dialog;
    private SensorGenerator sensorGenerator;
    private Hosts hosts;

    public NewNodeImpl(Project project, Configure configure, Hosts hosts,  NewNodeDialog dialog) {
        this.project = project;
        this.configure = configure;
        this.dialog = dialog;
        this.sensorGenerator = new SensorGenerator(configure);
        this.node = new Node(dialog.getName(), String.valueOf(configure.getNextFreeNodeId()), dialog.getTextname(), dialog.getIPAddress());
        this.hosts = hosts;
    }

    @Override
    public XmlNode addToConfigure() {
        return node;
    }

    @Override
    public void addIPAddressToHosts() throws IOException {
        String ipaddress = dialog.getIPAddress();
        String hostname  = dialog.getName();
        hosts.add(hostname, ipaddress);
    }

    @Override
    public VirtualFile createScript() throws IOException {
        HashMap<String, String> replaceMap = new HashMap<>();
        replaceMap.put("NODE", dialog.getName());
        // Преображаем содержимое файла
        String content = UnisetObjectTransformator
                .transformTemplateFromResource("/templates/newnode/ctl-smemory2.sh.in", replaceMap);

        return FileUtils.createFile(project,
                project.getBasePath() + "/src/SharedMemory2/",
                "ctl-smemory2-" + project.getName() + "-" + dialog.getName() + ".sh.in", content);
    }

    @Override
    public VirtualFile createMiscDir() throws IOException {
        HashMap<String,String> replaceMap = new HashMap<>();
        replaceMap.put("node", dialog.getName());

        String[] templatesNames = {
                "ctl-monit-node.sh.in",
                "Makefile.am",
                "TEMPLATE_PROJECT.node.monitrc.in",
                "TEMPLATE_PROJECT-runlist-node.sh.in"};
        LinkedHashMap<String, String> files = new LinkedHashMap<>();
        for(String templateName : templatesNames)
        {
            String content = UnisetObjectTransformator
                    .transformTemplateFromResource("/templates/newnode/misc/" + templateName, replaceMap);
            String fileName = templateName
                    .replaceAll("node", dialog.getName())
                    .replaceAll("TEMPLATE_PROJECT", project.getName());
            files.put(fileName, content);
        }

        return FileUtils.createDirAndFiles(project, project.getBasePath() + "/conf/misc", dialog.getName(), files);
    }

    @Override
    public List<Sensor> autogenerateSensors() {

        node.setNotRespondSensor(requestNotRespondSensor(
                dialog.getName(),
                "_UNET_Not_Respond",
                " (UNET): Нет связи по обоим каналам",
                "2"));
        node.setNotRespondSensor1(requestNotRespondSensor(
                dialog.getName(),
                "_UNET_Not_Respond1",
                " (UNET): Нет связи по каналу N1",
                "1"));
        node.setNotRespondSensor2(requestNotRespondSensor(
                dialog.getName(),
                "_UNET_Not_Respond2",
                " (UNET): Нет связи по каналу N2",
                "1"));
        node.setNumChanelSensor(requestAnalogSensor(
                dialog.getName(),
                "_UNET_NumChannel",
                " (UNET): Номер текущего рабочего канала"));
        node.setLostPacketsSensor1(requestAnalogSensor(
                dialog.getName(),
                "_UNET_LostPackets1",
                " (UNET): Количество потерянных пакетов по каналу N1"));
        node.setLostPacketsSensor2(requestAnalogSensor(
                dialog.getName(),
                "_UNET_LostPackets2",
                " (UNET): Количество потерянных пакетов по каналу N2"));

        return sensorGenerator.getSensors();
    }

    private String requestAnalogSensor(String name, String nameComment, String textnameComment)
    {
        String final_name = name.toUpperCase() + nameComment + "_AS";
        String final_textname = name.toUpperCase() + textnameComment;
        String iotype = Settings.IOTYPE_AI;
        sensorGenerator.registrySensor(final_name, final_textname, iotype);

        return final_name;
    }
    private String requestNotRespondSensor(String name, String nameComment, String textnameComment, String mtype)
    {
        String final_name = name.toUpperCase() + nameComment + "_S";
        String final_textname = name.toUpperCase() + textnameComment;
        String iotype = Settings.IOTYPE_DI;
        LinkedHashMap<String, String> props = new LinkedHashMap<>();
        props.put("mtype", mtype);
        sensorGenerator.registrySensor(final_name, final_textname, iotype, props);

        return final_name;
    }

}
