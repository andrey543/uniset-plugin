package com.uniset.plugin.actions.newactions;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.fileEditor.OpenFileDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ex.ProjectManagerEx;
import com.intellij.openapi.project.impl.ProjectManagerImpl;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.R;
import com.uniset.plugin.actions.Process;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.NewProjectDialog;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.jdom.JDOMException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class NewProjectWizard {

    public NewProjectWizard() {
    }

    public void doRunWizard(String projectName, String resultPath) {
        if(resultPath == null)
        {
            return;
        }

        VirtualFile projectRoot = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(resultPath));
        Process processCreating = new Process(ProjectManagerEx.getInstanceEx().getDefaultProject(), "Creating " + projectName, true, new Runnable() {
            @Override
            public void run() {
                createProject(ProjectManagerEx.getInstanceEx().getDefaultProject(), projectName, projectRoot);
            }
        });
        processCreating.run();
        Project project = null;
        try {
            project = ProjectManagerImpl.getInstance().loadAndOpenProject(resultPath);
        } catch (IOException | JDOMException e) {
            e.printStackTrace();
            return;
        }
        if(project == null)
            return;

        ProjectManagerEx.getInstanceEx().openProject(project);

        Configure configure = Configure.getInstance(project);
        Smemory smemory;
        try {
            smemory = Smemory.getInstance(project);
        } catch (IOException | NodeScript.ExtParamBlockCorrupted | NodeScript.ClosedSymbolNotFound e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    "New Uniset Project", Messages.getErrorIcon());
            return;
        }

        VirtualFile confFile = configure.getConfigureVirtualFile();
        VirtualFile smemoryFile = smemory.getSmemoryFile();
        VirtualFile cMakeFile = projectRoot.findChild("CMakeLists.txt");

        new OpenFileDescriptor(project, confFile).navigate(false);
        new OpenFileDescriptor(project, cMakeFile).navigate(false);
        new OpenFileDescriptor(project, smemoryFile).navigate(true);
    }

    private void createProject(Project project, String projectName, VirtualFile projectFile)
    {
        R.TemplateProjectFile templateProjectFile = null;
        try {
            templateProjectFile = R.getInstance().getTranfromProject(projectName);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        try {
            R.TemplateProjectFile finalTemplateProjectFile = templateProjectFile;
            WriteCommandAction
                    .writeCommandAction(project)
                    .run(new ThrowableRunnable<Throwable>() {
                        @Override
                        public void run() throws Throwable {
                            creatingFiles(project, finalTemplateProjectFile, projectFile);
                        }
                    });
        }
        catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }

    private void creatingFiles(Project project, R.TemplateProjectFile root, VirtualFile dir) throws IOException {
        List<R.TemplateProjectFile> list = root.getSubDirs();
        if(!list.isEmpty())
        {
            for(R.TemplateProjectFile d : list)
            {
                VirtualFile newDir = dir.createChildDirectory(project, d.getDirName());
                creatingFiles(project, d, newDir);
            }
        }

        for (Map.Entry<String, String> entry : root.getFiles().entrySet()) {
            String name = entry.getKey();
            String content = entry.getValue();

            if(name.equals("configure.xml") && !dir.getPath().endsWith("conf"))
                continue;

            VirtualFile file = dir.createChildData(project, name);
            if (!content.isEmpty()) {
                file.setBinaryContent(content.getBytes());
            }
            File f = VfsUtil.virtualToIoFile(file);
            if(Pattern.matches(".*\\.sh$", f.getName()) || f.getName().equals("configure"))
            {
                f.setExecutable(true);
            }
        }

        for(Map.Entry<String, String> entry : root.getLinks().entrySet())
        {
            String name = entry.getKey();
            String path = entry.getValue();

            Files.createSymbolicLink(Paths.get(dir.getPath() + "/" + name), Paths.get(path));
        }

    }
}
