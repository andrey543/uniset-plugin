package com.uniset.plugin.actions.newactions.node;

import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.XmlNode;

import java.io.IOException;
import java.util.List;

public interface NewNode {
    XmlNode addToConfigure();
    void addIPAddressToHosts() throws IOException;
    VirtualFile createScript() throws IOException;
    VirtualFile createMiscDir() throws IOException;
    List<Sensor> autogenerateSensors();
}
