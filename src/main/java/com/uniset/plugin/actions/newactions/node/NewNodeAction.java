package com.uniset.plugin.actions.newactions.node;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.io.FileUtil;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.make.MakeConfiguration;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.configure.Hosts;
import com.uniset.plugin.gui.NewNodeDialog;
import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;

public class NewNodeAction extends AnAction {

    private static final String TITLE = "New Node";

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if(view == null) {
            return;
        }

        Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        PsiDirectory directory = view.getOrChooseDirectory();
        if(directory == null) {
            return;
        }


        if(!FileUtils.exists(project.getBasePath() + "/conf/misc"))
        {
            Messages.showMessageDialog(project,
                    "The project haven't '/conf/misc' for a node directory",
                    TITLE, Messages.getErrorIcon());
            return;
        }

        // Инициализация configure.xml
        Configure configure = Configure.getInstance(project);
        if(configure == null)
            return;

        MakeConfiguration makeConfiguration;
        try {
             makeConfiguration = MakeConfiguration.getInstance(project);
        } catch (FileNotFoundException fileNotFoundException) {
            Messages.showMessageDialog(project,
                    fileNotFoundException.getMessage(),
                    TITLE, Messages.getErrorIcon());
            return;
        }

        Hosts hosts;
        try {
            hosts = Hosts.getInstance(project);
        } catch (FileNotFoundException fileNotFoundException) {
            Messages.showMessageDialog(project,
                    fileNotFoundException.getMessage(),
                    TITLE, Messages.getErrorIcon());
            return;
        }


        NewNodeDialog dialog = new NewNodeDialog(TITLE, project, configure);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
        {
            NewNode newNode = new NewNodeImpl(project, configure, hosts, dialog);
            try {
                WriteCommandAction
                        .writeCommandAction(project)
                        .withName("New node " + dialog.getName())
                        .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                        .withGlobalUndo()
                        .run(() -> {
                            // Генерируем необходимые датчики
                            for(Sensor sensor : newNode.autogenerateSensors())
                                configure.addToSensorsLast(sensor);
                            // Добавляем xml-ноду
                            configure.addToNodeLast(newNode.addToConfigure());

                            // Создаем скрипт в SharedMemory2
                            VirtualFile scriptFile = newNode.createScript();
                            // Прописываем Make настройки для созданного скрипта
                            makeConfiguration.configureMainFile(project,
                                    scriptFile.getPath());
                            makeConfiguration.configureSubdirs(project, scriptFile.getPath());

                            // Создаем папку ноды в conf/misc
                            VirtualFile nodeDir = newNode.createMiscDir();
                            // Прописываем в configure.ac Makefile.am
                            makeConfiguration.configureMainFile(
                                    project,
                                    nodeDir.getPath() + "/Makefile.am");
                            // Прописываем в configure.ac .in скрипты
                            for(VirtualFile virtualFile : nodeDir.getChildren()) {
                                if (virtualFile.getName().endsWith(".in"))
                                    makeConfiguration.configureMainFile(project,
                                            virtualFile.getPath());
                            }
                            // Прописываем Makefile-путь до папки
                            makeConfiguration.configureSubdirs(project,
                                    nodeDir.getPath() + "/Makefile.am");

                            newNode.addIPAddressToHosts();
                        });
            } catch (Throwable throwable) {
                Messages.showMessageDialog(project,
                        "Creating new node is failed",
                        TITLE, Messages.getErrorIcon());
            }
        }

    }
}
