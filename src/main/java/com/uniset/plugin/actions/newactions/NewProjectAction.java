package com.uniset.plugin.actions.newactions;

import com.intellij.ide.RecentProjectsManager;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.gui.NewProjectDialog;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Optional;

public class NewProjectAction extends AnAction {

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        NewProjectWizard wizard = new NewProjectWizard();
        String lastDir = Optional.ofNullable(RecentProjectsManager.getInstance().getLastProjectCreationLocation()).orElse("");
        NewProjectDialog dialog = new NewProjectDialog(e.getProject(),"", new File(lastDir).getPath());
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
        {
            wizard.doRunWizard(dialog.getProjectName(), dialog.getResultPath());
        }
    }
}
