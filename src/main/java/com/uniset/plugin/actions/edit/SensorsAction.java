package com.uniset.plugin.actions.edit;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiDirectory;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.environment.EnvironmentModbus;
import com.uniset.plugin.environment.EnvironmentUniset;
import com.uniset.plugin.environment.SensorManager;
import com.uniset.plugin.gui.SensorManagerDialog;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class SensorsAction extends AnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if(view == null) {
            return;
        }

        Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        PsiDirectory directory = view.getOrChooseDirectory();
        if(directory == null) {
            return;
        }

        Configure configure = Configure.getInstance(project);
        if(configure == null)
            return;

        Smemory smemory;
        try {
            smemory = Smemory.getInstance(project);
        } catch (IOException | NodeScript.ExtParamBlockCorrupted | NodeScript.ClosedSymbolNotFound e1) {
            Messages.showMessageDialog(project,
                    e1.getMessage(),
                    "EnvironmentUniset", Messages.getErrorIcon());
            return;
        }
        if(smemory == null)
            return;

        /** Uniset objects */
        EnvironmentUniset environmentUniset = new EnvironmentUniset(project, configure);

        /** Modbus objects */
        EnvironmentModbus environmentModbus;
        try {
            environmentModbus = new EnvironmentModbus(configure, smemory);
        } catch (IllegalAccessException e1) {
            Messages.showMessageDialog(project,
                    "Unknown error",
                    "Environment modbus", Messages.getErrorIcon());
            return;
        }

        SensorManager sensorManager = new SensorManager(configure, environmentUniset, environmentModbus);
        SensorManagerDialog dialog = new SensorManagerDialog(project, sensorManager);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
    }
}
