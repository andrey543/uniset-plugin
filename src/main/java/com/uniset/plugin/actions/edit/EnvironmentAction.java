package com.uniset.plugin.actions.edit;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiDirectory;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.environment.Environment;
import com.uniset.plugin.environment.EnvironmentModbus;
import com.uniset.plugin.environment.EnvironmentNodes;
import com.uniset.plugin.environment.EnvironmentUniset;
import com.uniset.plugin.gui.environment.EnvironmentDialog;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class EnvironmentAction extends AnAction {
    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if(view == null) {
            return;
        }

        Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        PsiDirectory directory = view.getOrChooseDirectory();
        if(directory == null) {
            return;
        }

        // Инициализация configure.xml
        Configure configure = Configure.getInstance(project);
        if(configure == null)
            return;

        Smemory smemory;
        try {
            smemory = Smemory.getInstance(project);
        } catch (IOException | NodeScript.ExtParamBlockCorrupted | NodeScript.ClosedSymbolNotFound e1) {
            Messages.showMessageDialog(project,
                    e1.getMessage(),
                    "EnvironmentUniset", Messages.getErrorIcon());
            return;
        }
        if(smemory == null)
            return;

        /** Nodes */
        EnvironmentNodes oldEnvironmentNodes;
        EnvironmentNodes newEnvironmentNodes;
        try {
            oldEnvironmentNodes = new EnvironmentNodes(project, configure, smemory);
            newEnvironmentNodes = new EnvironmentNodes(project, configure, smemory);
        } catch (IllegalAccessException e1) {
            Messages.showMessageDialog(project,
                    "Unknown error",
                    "Environment modbus", Messages.getErrorIcon());
            return;
        }


        /** Uniset objects */
        EnvironmentUniset oldEnviromentUniset = new EnvironmentUniset(project, configure);
        EnvironmentUniset newEnviromentUniset = new EnvironmentUniset(project, configure);


        /** Modbus objects */
        EnvironmentModbus oldEnviromentModbus;
        EnvironmentModbus newEnviromentModbus;
        try {
            oldEnviromentModbus = new EnvironmentModbus(configure, smemory);
            newEnviromentModbus = new EnvironmentModbus(configure, smemory);
        } catch (IllegalAccessException e1) {
            Messages.showMessageDialog(project,
                    "Unknown error",
                    "Environment modbus", Messages.getErrorIcon());
            return;
        }


        Environment.EnvironmentSaveBox nodesEnvironment  = new Environment.EnvironmentSaveBox(oldEnvironmentNodes, newEnvironmentNodes);
        Environment.EnvironmentSaveBox unisetEnvironment = new Environment.EnvironmentSaveBox(oldEnviromentUniset, newEnviromentUniset);
        Environment.EnvironmentSaveBox modbusEnvironment = new Environment.EnvironmentSaveBox(oldEnviromentModbus, newEnviromentModbus);

        EnvironmentDialog dialog = new EnvironmentDialog(project, configure, smemory, nodesEnvironment, unisetEnvironment, modbusEnvironment);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();

        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
        {
            if(dialog.isEnvironmentToSave())
            {
                try {
                    WriteCommandAction
                            .writeCommandAction(project)
                            .withName("Edit Configure")
                            .withGlobalUndo()
                            .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                            .run((ThrowableRunnable<Throwable>) () -> {
                                if(unisetEnvironment.hasChanged())
                                    unisetEnvironment.save();
                                if(modbusEnvironment.hasChanged())
                                    modbusEnvironment.save();
                                if(nodesEnvironment.hasChanged())
                                    nodesEnvironment.save();
                            });
                } catch (Throwable throwable) {
                    Messages.showMessageDialog(project,
                            "Editing configure is failed",
                            "Edit Configure", Messages.getErrorIcon());
                }
            }
        }
    }
}
