package com.uniset.plugin.actions.add.unisetobject;

//TODO Доделать вставку в конфигурь по секциям

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.cmake.CMakeConfiguration;
import com.uniset.plugin.build.make.MakeConfiguration;
import com.uniset.plugin.build.UnisetObjectTransformator;
import com.uniset.plugin.build.objects.ResourceObject;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.UnisetObject;
import com.uniset.plugin.build.objects.UnisetObjectFile;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.AddUnisetObjectDialog;
import com.uniset.plugin.actions.Process;
import org.jdom2.JDOMException;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public abstract class AddUnisetObjectAction extends AnAction {
    protected Project project;
    protected Configure configure;
    protected String name;
    protected String title;
    protected AddUnisetObjectDialog dialog;

    public abstract String getName();

    public abstract List<Path> getProjectList(Project project) throws IOException;

    public abstract List<ResourceObject> getBaseList() throws IOException;

    public abstract LinkedHashMap<String, List<Path>> getAdditionalList() throws IOException;

    public abstract VirtualFile getVirtualDir(Project project);

    public abstract String getDefaultSection();

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {

        /**
         * Инициализация
         */

        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if (view == null) {
            return;
        }
        project = PlatformDataKeys.PROJECT.getData(dataContext);

        name = getName();
        title = "Adding " + name;

        // Инициализация configure.xml
        configure = Configure.getInstance(project);
        if(configure == null)
            return;

        dialog = new AddUnisetObjectDialog(project, configure, this);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();

        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {

            UnisetObjectFile[] objectFile = new UnisetObjectFile[1];
            List<UnisetObject> objects = new ArrayList<>();
            Process processCreating = new Process(project, "Preparing files...", false, new Runnable() {
                @Override
                public void run() {
                    objectFile[0] = createUnisetObjectFile(dialog);
                    for(Map.Entry<String, String> entry : dialog.getObjects().entrySet())
                    {
                        UnisetObject object = new UnisetObject(entry.getKey(), dialog.getSelectedName(), entry.getValue());
                        objects.add(object);
                    }
                }
            });
            processCreating.run();

            //TODO Плохо работает undo/redo из за ссылки на configure.xml
            final VirtualFile[] newDir = new VirtualFile[1];
            try {
                WriteCommandAction
                        .writeCommandAction(project)
                        .withGlobalUndo()
                        .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                        .withName("Adding " + name + " " + objectFile[0].getName())
                        .run((ThrowableRunnable<Throwable>) () -> {
                            if(dialog.isAutogenerateSensors())
                                autogenerationSensors(objectFile[0], objects);
                            addToConfigure(objects);
                            if(!dialog.isProject()) {
                                newDir[0] = createFiles(objectFile[0]);
                                if(dialog.isMake())
                                    addMakeConfiguration(objectFile[0]);
                                if(dialog.isCMake())
                                    addCMakeConfiguration(objectFile[0]);
                                configure.createLink(getVirtualDir(project).getPath() + "/" + objectFile[0].getName());
                            }
                        });
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            configure.getConfigureVirtualFile().refresh(false, true);
        }
    }

    public String getTitle() {
        return title;
    }

    private UnisetObjectFile createUnisetObjectFile(AddUnisetObjectDialog dialog) {
        UnisetObjectFile object = null;
        if(dialog.isProject())
        {
            try {
                object = UnisetObjectFile.createFromPath(dialog.getSelectedProjectObject());
            } catch (IOException e) {
                Messages.showMessageDialog(project,
                        "Transforming file of " + name + " is failed",
                        title, Messages.getErrorIcon());
            }

        }
        if (dialog.isBase()) {
            try {
                ResourceObject resourceObject = dialog.getSelectedBaseObject();
                object = new UnisetObjectFile(resourceObject.getName());
                LinkedHashMap<String, String> files = UnisetObjectTransformator.transformTemplatesFromResource(project, resourceObject);
                for (Map.Entry<String, String> entry : files.entrySet()) {
                    object.addFile(entry.getKey(), entry.getValue());
                }

            } catch (IOException e) {
                Messages.showMessageDialog(project,
                        "Transforming file of " + name + " is failed",
                        title, Messages.getErrorIcon());
            }
        } else if (dialog.isAdditional()) {
            try {
                Path path = dialog.getSelectedAdditionalObject();
                object = UnisetObjectFile.createFromPath(path);
                object = UnisetObjectTransformator.transformTemplatesFromProject(project, dialog.getAdditionalProjectName(), object);
            } catch (IOException e) {
                Messages.showMessageDialog(project,
                        "Transforming file of " + name + " is failed",
                        title, Messages.getErrorIcon());
            }
        }

        return object;
    }

    private void addToConfigure(List<UnisetObject> objects) {

        for(UnisetObject object : objects) {
            if(!dialog.getSelectedChooseSection().equals(AddUnisetObjectDialog.NONE))
            {
                String sectionName = dialog.getSelectedChooseSection();
                configure.addToSettingsLast(object.getSettingsTag(project), sectionName);
                configure.addToObjectsLast(object.getObjectsTag(project), sectionName);
            }
            else {
                configure.addToSettingsLast(object.getSettingsTag(project), getDefaultSection());
                configure.addToObjectsLast(object.getObjectsTag(project), getDefaultSection());
            }
        }
    }

    public boolean canToAutogenerateSensors(Path srcXml, List<UnisetObject> objectList)
    {
        if(objectList.size() == 0)
            return false;

        return Files.exists(srcXml);
    }

    private void autogenerationSensors(UnisetObjectFile objectFile, List<UnisetObject> objectList) throws IOException, JDOMException, ParserConfigurationException, SAXException {
        String content = objectFile.getContentFile(objectFile.getName().toLowerCase() + ".src.xml");
        for(UnisetObject object : objectList) {
            InputStream is = new ByteArrayInputStream(content.getBytes("UTF-8"));
            object.setInputStreamSrcXml(is, configure);
        }

        generateSensors(objectList);
    }

    private void generateSensors(List<UnisetObject> objectList)
    {
        long id = configure.getNextFreeSensorId();
        List<Sensor> sensors = new ArrayList<>();
        for(UnisetObject object : objectList)
        {
            for(UnisetObject.SMapSensor sMapSensor : object.getSmapSensors())
            {
                String name = object.getName() + "_" +
                        sMapSensor.getName().substring(0, sMapSensor.getName().lastIndexOf("_")) +
                        sMapSensor.getName().substring(sMapSensor.getName().lastIndexOf("_")).toUpperCase();
                String textname = object.getName() + ": " + sMapSensor.getComment();
                String iotype = sMapSensor.getIotype();
                Sensor sensor = new Sensor(id, name, iotype, textname);
                sMapSensor.setAttachSensor(sensor);
                sensors.add(sensor);
                id++;
            }
        }

        for(Sensor sensor : sensors)
            configure.addToSensorsLast(sensor);
    }

    private void addCMakeConfiguration(UnisetObjectFile object)
    {
        CMakeConfiguration cMakeConfiguration;
        try {
            cMakeConfiguration = CMakeConfiguration.getInstance(project);
        } catch (FileNotFoundException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    title, Messages.getErrorIcon());
            return;
        }

        try {
            cMakeConfiguration.configureSubdirsAt(getVirtualDir(project).getPath() + "/" + object.getName());
        } catch (IOException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    title, Messages.getErrorIcon());
        }
    }

    private void addMakeConfiguration(UnisetObjectFile object) {
        MakeConfiguration makeConfiguration;
        try {
            makeConfiguration = MakeConfiguration.getInstance(project);
        } catch (FileNotFoundException e1) {
            Messages.showMessageDialog(project,
                    e1.getMessage(),
                    title, Messages.getErrorIcon());
            return;
        }

        String makeFileName = "Makefile.am";

        // Формируем путь до Makefile.am
        String pathToMakefile = getVirtualDir(project).getPath() + "/" + object.getName() + "/" + makeFileName;

        // Правим configure.ac
        try {
            makeConfiguration.configureMainFile(project, pathToMakefile);
        } catch (Throwable throwable) {
            Messages.showMessageDialog(project,
                    "Configuration main Makefile of " + name + " is failed",
                    title, Messages.getErrorIcon());
        }

        // Прописываем SUBDIRS
        try {
            makeConfiguration.configureSubdirs(project, pathToMakefile);
        } catch (Throwable throwable) {
            Messages.showMessageDialog(project,
                    "Configuration make subdirs of " + name + " is failed",
                    title, Messages.getErrorIcon());
        }
    }

    private VirtualFile createFiles(UnisetObjectFile object) {

        try {
            return FileUtils.createDirsAndFiles(project, getVirtualDir(project), object);
        } catch (IOException e) {
            Messages.showMessageDialog(project,
                    "Creating file of " + name + " is failed",
                    title, Messages.getErrorIcon());
        }

        return null;
    }


}
