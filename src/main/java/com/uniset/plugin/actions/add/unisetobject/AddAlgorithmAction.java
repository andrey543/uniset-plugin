package com.uniset.plugin.actions.add.unisetobject;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.UnisetObjectGenerator;
import com.uniset.plugin.build.objects.ResourceObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedHashMap;
import java.util.List;

public class AddAlgorithmAction extends AddUnisetObjectAction {

    private String name = "algorithm";
    private String pathInProject = Settings.PROJECT_PATH_TO_ALGORITHMS;
    private String pathToTemplates = "/templates/Algorithms/Algorithms.list";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Path> getProjectList(Project project) throws IOException {
        return UnisetObjectGenerator.generateProjectList(project, pathInProject);
    }

    @Override
    public List<ResourceObject> getBaseList() throws IOException {
        return UnisetObjectGenerator.generateBaseList(pathToTemplates);
    }

    @Override
    public LinkedHashMap<String, List<Path>> getAdditionalList() throws IOException {
        return UnisetObjectGenerator.generateAdditionalList(pathInProject);
    }

    @Override
    public VirtualFile getVirtualDir(Project project) {
        return LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(project.getBasePath() + pathInProject));
    }

    @Override
    public String getDefaultSection() {
        return Settings.CONFIGURE_ALGORITHM_SECTION;
    }
}
