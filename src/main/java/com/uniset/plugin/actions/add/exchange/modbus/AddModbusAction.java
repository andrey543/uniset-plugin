package com.uniset.plugin.actions.add.exchange.modbus;

//TODO Возможно стоит сделать проверку на существование modbus-объектов в проекте

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.Settings;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.build.SensorGenerator;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public abstract class AddModbusAction extends AnAction {
    protected Project project;
    protected DialogWrapper dialog;
    protected Configure configure;
    protected Smemory smemory;
    protected NodeScript script;
    protected String title = "Modbus exchange";

    protected abstract DialogWrapper initDialog();
    protected abstract ModbusObject createModbusObject();
    protected abstract String addToStartScript() throws NodeScript.ExtParamBlockCorrupted, IOException, NodeScript.ClosedSymbolNotFound, NodeScript.ModbusBlockAlreadyExist;
    protected abstract String addToSmemory() throws IOException, Smemory.ModbusBlockIsNotFound;
    protected abstract boolean isAutogerateSensors();
    protected abstract SensorGenerator getSensorGenerator();

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {

        /**
         * Инициализация
         */

        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if(view == null) {
            return;
        }
        project = PlatformDataKeys.PROJECT.getData(dataContext);

        // Инициализация configure.xml
        configure = Configure.getInstance(project);
        if(configure == null)
            return;

        try {
            smemory = Smemory.getInstance(project);
        } catch (IOException | NodeScript.ExtParamBlockCorrupted | NodeScript.ClosedSymbolNotFound e1) {
            Messages.showMessageDialog(project,
                    e1.getMessage(),
                    title, Messages.getErrorIcon());
            return;
        }

        /**
         * Инициализация GUI-окна
         */
        dialog = initDialog();
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();

        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {

            /** Создаем объект */
            ModbusObject object = createModbusObject();
            if(object == null)
            {
                Messages.showMessageDialog(project,
                        "Creating modbus object is failed",
                        title, Messages.getErrorIcon());
                return;
            }

            /** Добавляем настройки в стартовый скрипт */
            String newScriptContent;
            try {
                newScriptContent = addToStartScript();
            } catch (NodeScript.ExtParamBlockCorrupted | IOException | NodeScript.ClosedSymbolNotFound | NodeScript.ModbusBlockAlreadyExist extParamBlockCorrupted) {
                // Сообщение(отмена процесса): Поврежден(не правильно заполнен) блок переменной EXTPARAM в скрипте
                Messages.showMessageDialog(project,
                        extParamBlockCorrupted.getMessage(),
                        title, Messages.getErrorIcon());
                return;
            }

            if(newScriptContent == null || newScriptContent.isEmpty()) {
                Messages.showMessageDialog(project,
                        "Unknown error of edit start-script",
                        title, Messages.getErrorIcon());
                return;
            }

            /** Добавляем настройки в smemory.cc*/
            String newSmemoryContent = null;
            try {
                newSmemoryContent = addToSmemory();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Smemory.ModbusBlockIsNotFound modbusBlockIsNotFound) {
                // Сообщение(отмена процесса): Блок с modbus-вектором не найден или с ошибкой
//                Messages.showMessageDialog(project,
//                        modbusBlockIsNotFound.getMessage(),
//                        title, Messages.getErrorIcon());
            }
            if(newSmemoryContent == null || newSmemoryContent.isEmpty()) {
                Messages.showMessageDialog(project,
                        "Unknown error of edit smemory.cc",
                        title, Messages.getErrorIcon());
                return;
            }

            try {
                String finalNewSmemoryContent = newSmemoryContent;
                WriteCommandAction
                        .writeCommandAction(project)
                        .withName("Adding " + object.getFullName())
                        .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                        .withGlobalUndo()
                        .run((ThrowableRunnable<Throwable>) () -> {
                            /** Добавляем настройки в configure.xml */
                            if(addToConfigure(object)) {
                                smemory.updateContent(finalNewSmemoryContent);
                                script.updateContent(newScriptContent);
                            }
                        });
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }

    private boolean addToConfigure(ModbusObject modbusObject)
    {
        try {
            configure.addToSettingsLast(modbusObject.getSettingsTag(project), Settings.CONFIGURE_MODBUS_SECTION);
            configure.addToObjectsLast(modbusObject.getObjectsTag(project), Settings.CONFIGURE_MODBUS_SECTION);
            if(isAutogerateSensors())
            {
                for(Sensor sensor : getSensorGenerator().getSensors())
                {
                    configure.addToSensorsLast(sensor);
                }
            }
        }
        catch (Exception e)
        {
            Messages.showMessageDialog(project,
                    "Error of adding " + modbusObject.getFullName() + " to configure.xml",
                    "AddModbusAction", Messages.getErrorIcon());
            return false;
        }

        return true;
    }
}
