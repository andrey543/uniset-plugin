package com.uniset.plugin.actions.add.exchange.modbus;

import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.build.objects.modbus.MBMObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.gui.modbus.AddMBMDialog;
import com.uniset.plugin.build.SensorGenerator;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;

import java.io.IOException;

public class AddMBMAction extends AddModbusAction {
    private AddMBMDialog dialog;
    private MBMObject mbmObject;

    public AddMBMAction()
    {
        super();
        this.title = "MBM";
    }

    @Override
    protected DialogWrapper initDialog() {
        // Создаем соответствующее окно
        this.dialog = new AddMBMDialog(project, configure, smemory);
        return dialog;
    }

    @Override
    protected ModbusObject createModbusObject() {
        this.mbmObject = new MBMObject(configure.getNextFreeObjectId(), this.dialog);
        return mbmObject;
    }

    @Override
    protected String addToStartScript() throws NodeScript.ExtParamBlockCorrupted, IOException, NodeScript.ClosedSymbolNotFound, NodeScript.ModbusBlockAlreadyExist {
        String nodeName = dialog.getSelectedNode().getName();
        script = smemory.getStartScript(nodeName);

        return script.addModbusObject(this.mbmObject);
    }

    @Override
    protected String addToSmemory() throws IOException, Smemory.ModbusBlockIsNotFound {

        return smemory.addMBM(this.mbmObject.getName());
    }

    @Override
    protected boolean isAutogerateSensors() {
        return dialog.isAutogerateSensors();
    }

    @Override
    protected SensorGenerator getSensorGenerator() {
        return dialog.getSensorGenerator();
    }
}
