package com.uniset.plugin.actions.add.exchange.modbus;

import com.intellij.openapi.ui.DialogWrapper;
import com.uniset.plugin.build.objects.modbus.MBMMObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.gui.modbus.AddMBMMDialog;
import com.uniset.plugin.build.SensorGenerator;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;

import java.io.IOException;

public class AddMBMMAction extends AddModbusAction {
    private AddMBMMDialog dialog;
    private MBMMObject mbmmObject;

    @Override
    protected DialogWrapper initDialog() {
        this.dialog = new AddMBMMDialog(project, configure, smemory);
        return dialog;
    }

    @Override
    protected ModbusObject createModbusObject() {
        this.mbmmObject = new MBMMObject(configure.getNextFreeObjectId(), this.dialog);
        return mbmmObject;
    }

    @Override
    protected String addToStartScript() throws NodeScript.ExtParamBlockCorrupted, IOException, NodeScript.ClosedSymbolNotFound, NodeScript.ModbusBlockAlreadyExist {
        String nodeName = dialog.getSelectedNode().getName();
        script = smemory.getStartScript(nodeName);

        return script.addModbusObject(this.mbmmObject);
    }

    @Override
    protected String addToSmemory() throws IOException, Smemory.ModbusBlockIsNotFound {
        return smemory.addMBMM(this.mbmmObject.getName());
    }

    @Override
    protected boolean isAutogerateSensors() {
        return dialog.isAutogerateSensors();
    }

    @Override
    protected SensorGenerator getSensorGenerator() {
        return dialog.getSensorGenerator();
    }
}
