package com.uniset.plugin.actions.add;

import com.intellij.ide.IdeView;
import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.command.UndoConfirmationPolicy;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiDirectory;
import com.intellij.util.ThrowableRunnable;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.configure.Configure;
import com.uniset.plugin.gui.AddSensorDialog;
import org.jetbrains.annotations.NotNull;

public class AddSensorsAction extends AnAction {
    private static final String TITLE = "Add Sensor";

    @Override
    public void actionPerformed(@NotNull AnActionEvent e) {
        DataContext dataContext = e.getDataContext();
        IdeView view = LangDataKeys.IDE_VIEW.getData(dataContext);
        if(view == null) {
            return;
        }

        Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        PsiDirectory directory = view.getOrChooseDirectory();
        if(directory == null) {
            return;
        }

        Configure configure = Configure.getInstance(project);
        if(configure == null)
            return;

        AddSensorDialog dialog = new AddSensorDialog(project, configure);
        dialog.setResizable(true);
        dialog.pack();
        dialog.show();
        if(dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE)
        {
            try {
                WriteCommandAction
                        .writeCommandAction(project)
                        .withGlobalUndo()
                        .withName("Add Sensors")
                        .withUndoConfirmationPolicy(UndoConfirmationPolicy.REQUEST_CONFIRMATION)
                        .run((ThrowableRunnable<Throwable>) () -> {
                            for (Sensor sensor : dialog.getSensors())
                                configure.addToSensorsLast(sensor);
                        });
            } catch (Throwable throwable) {
                Messages.showMessageDialog(project,
                        "Adding sensor is failed",
                        TITLE, Messages.getErrorIcon());
                return;
            }

        }

    }
}
