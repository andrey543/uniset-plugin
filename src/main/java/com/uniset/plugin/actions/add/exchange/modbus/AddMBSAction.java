package com.uniset.plugin.actions.add.exchange.modbus;

import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.Messages;
import com.uniset.plugin.build.objects.modbus.MBSObject;
import com.uniset.plugin.build.objects.modbus.ModbusObject;
import com.uniset.plugin.gui.modbus.AddMBSDialog;
import com.uniset.plugin.build.SensorGenerator;
import com.uniset.plugin.smemory.NodeScript;
import com.uniset.plugin.smemory.Smemory;

import java.io.IOException;

public class AddMBSAction extends AddModbusAction {
    private AddMBSDialog dialog;
    private MBSObject mbsObject;

    public AddMBSAction()
    {
        super();
        this.title = "MBS";
    }

    @Override
    protected DialogWrapper initDialog() {
        this.dialog = new AddMBSDialog(project, configure, smemory);
        return dialog;
    }

    @Override
    protected ModbusObject createModbusObject() {
        try {
            mbsObject = new MBSObject(configure.getNextFreeObjectId(), this.dialog);
        } catch (MBSObject.MBSFailedTypeException e) {
            Messages.showMessageDialog(project,
                    e.getMessage(),
                    title, Messages.getErrorIcon());
        }
        return mbsObject;
    }

    @Override
    protected String addToStartScript() throws NodeScript.ExtParamBlockCorrupted, IOException, NodeScript.ClosedSymbolNotFound, NodeScript.ModbusBlockAlreadyExist {
        String nodeName = dialog.getSelectedNode().getName();
        script = smemory.getStartScript(nodeName);

        return script.addModbusObject(this.mbsObject);
    }

    @Override
    protected String addToSmemory() throws IOException, Smemory.ModbusBlockIsNotFound {
        return smemory.addMBS(this.mbsObject.getName());
    }

    @Override
    protected boolean isAutogerateSensors() {
        return false;
    }

    @Override
    protected SensorGenerator getSensorGenerator() {
        return null;
    }
}
