package com.uniset.plugin.configure;

import com.intellij.openapi.project.Project;
import com.intellij.psi.xml.XmlTag;

public interface XmlTagUnisetObject {
    XmlTag getSettingsTag(Project project);
    XmlTag getObjectsTag(Project project);
}
