package com.uniset.plugin.configure;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.XmlElementFactory;
import com.intellij.psi.xml.XmlTag;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.smemory.NodeScript;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Node implements XmlNode{
    private String name;
    private String id;
    private String textname;
    private String ip;

    private String notRespondSensor;
    private String notRespondSensor1;
    private String notRespondSensor2;
    private String numChanelSensor;
    private String lostPacketsSensor1;
    private String lostPacketsSensor2;

    private VirtualFile miscDir;

    private NodeScript script;
    private List<Sensor> sensors = new ArrayList<>();

    public Node(String name, String id, String textname, String ip)
    {
        this.name     = name;
        this.id       = id;
        this.textname = textname;
        this.ip       = ip;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return Objects.equals(name, node.name) &&
                Objects.equals(textname, node.textname) &&
                Objects.equals(ip, node.ip) &&
                Objects.equals(notRespondSensor, node.notRespondSensor) &&
                Objects.equals(notRespondSensor1, node.notRespondSensor1) &&
                Objects.equals(notRespondSensor2, node.notRespondSensor2) &&
                Objects.equals(numChanelSensor, node.numChanelSensor) &&
                Objects.equals(lostPacketsSensor1, node.lostPacketsSensor1) &&
                Objects.equals(lostPacketsSensor2, node.lostPacketsSensor2) &&
                Objects.equals(sensors, node.sensors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, textname, ip, notRespondSensor, notRespondSensor1, notRespondSensor2, numChanelSensor, lostPacketsSensor1, lostPacketsSensor2, sensors);
    }

    public VirtualFile getMiscDir() {
        return miscDir;
    }

    public void setMiscDir(VirtualFile miscDir) {
        this.miscDir = miscDir;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getTextname() {
        return textname;
    }

    public String getIp() {
        return ip;
    }

    public NodeScript getScript() {
        return script;
    }

    public String getNotRespondSensor() {
        return notRespondSensor;
    }

    public void setNotRespondSensor(String notRespondSensor) {
        this.notRespondSensor = notRespondSensor;
    }

    public String getNotRespondSensor1() {
        return notRespondSensor1;
    }

    public void setNotRespondSensor1(String notRespondSensor1) {
        this.notRespondSensor1 = notRespondSensor1;
    }

    public String getNotRespondSensor2() {
        return notRespondSensor2;
    }

    public void setNotRespondSensor2(String notRespondSensor2) {
        this.notRespondSensor2 = notRespondSensor2;
    }

    public String getNumChanelSensor() {
        return numChanelSensor;
    }

    public void setNumChanelSensor(String numChanelSensor) {
        this.numChanelSensor = numChanelSensor;
    }

    public String getLostPacketsSensor1() {
        return lostPacketsSensor1;
    }

    public void setLostPacketsSensor1(String lostPacketsSensor1) {
        this.lostPacketsSensor1 = lostPacketsSensor1;
    }

    public String getLostPacketsSensor2() {
        return lostPacketsSensor2;
    }

    public void setLostPacketsSensor2(String lostPacketsSensor2) {
        this.lostPacketsSensor2 = lostPacketsSensor2;
    }

    public void setScript(NodeScript script) {
        this.script = script;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTextname(String textname) {
        this.textname = textname;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void addSensor(final Sensor sensor)
    {
        sensors.add(sensor);
    }

    public void removeSensor(final Sensor sensor)
    {
        sensors.remove(sensor);
    }

    @Override
    public String toString()
    {
        id = id == null || id.isEmpty() ? "?" : id;
        name = name == null || name.isEmpty() ? "?" : name;
        textname = textname == null || textname.isEmpty() ? "?" : textname;
        ip = ip == null || ip.isEmpty() ? "?" : ip;

        if(id.equals("?") && name.equals("?") && textname.equals("?") && ip.equals("?"))
            return "None";

        return "[" + id + "] " + name + "(" + textname + ") " + "ip:" + ip;
    }

    @Override
    public XmlTag getNodeTag(Project project) {

        XmlTag mainTag = XmlElementFactory.getInstance(project).createTagFromText("<item/>");
        mainTag.setAttribute("id", String.valueOf(id));
        mainTag.setAttribute("name", name);
        mainTag.setAttribute("textname", textname);
        mainTag.setAttribute("ip", name); // В ноде ставим hostname
        mainTag.setAttribute("select_unet", "real");
        if(!notRespondSensor.isEmpty()) mainTag.setAttribute("unet_respond_id", notRespondSensor);
        if(!notRespondSensor1.isEmpty()) mainTag.setAttribute("unet_respond1_id", notRespondSensor1);
        if(!notRespondSensor2.isEmpty()) mainTag.setAttribute("unet_respond2_id", notRespondSensor2);
        mainTag.setAttribute("unet_respond_invert","1");
        if(!numChanelSensor.isEmpty()) mainTag.setAttribute("unet_numchannel_id", numChanelSensor);
        if(!lostPacketsSensor1.isEmpty()) mainTag.setAttribute("unet_lostpackets1_id", lostPacketsSensor1);
        if(!lostPacketsSensor2.isEmpty()) mainTag.setAttribute("unet_lostpackets2_id", lostPacketsSensor2);

        return mainTag;
    }
}
