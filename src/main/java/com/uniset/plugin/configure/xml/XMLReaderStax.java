package com.uniset.plugin.configure.xml;

import com.uniset.plugin.FileUtils;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class XMLReaderStax {

    Document document;
    String file;

    public XMLReaderStax(String file) throws IOException, JDOMException{

        this.file = file;

        SAXBuilder saxBuilder = new SAXBuilder();
        document = saxBuilder.build(new File(file));
    }

    public XMLReaderStax(InputStream is) throws JDOMException, IOException {
        SAXBuilder saxBuilder = new SAXBuilder();
        document = saxBuilder.build(is);
    }

    public void addObjectInTagSection(String tagSection, Content newElement) throws IOException, JDOMException {
        Element tag = findFirtsChild(tagSection);

        if(tag == null)
            throw new JDOMException("The tag \"" + tagSection + "\" is not found in configure.xml");

        tag.addContent(newElement);
        updateXML();
    }

    public void addObjectInCommentSection(String tagSection, String startSectionName, String endSectionName, Element newElement) throws IOException, JDOMException {

        Element tag = findFirtsChild(tagSection);

        if(tag == null)
            throw new JDOMException("The tag \"" + tagSection + "\" is not found in configure.xml");

        List<Content> contents = tag.getContent();

        boolean sectionAlgIsFound = false;
        boolean sectionAlgIsClose = false;
        for(int i = 0; i < contents.size(); i++)
        {
            Content content = contents.get(i);
            String type = content.getCType().toString();

            if(type.equals("Comment"))
            {
                String text = content.getValue();
                if(!sectionAlgIsFound && Pattern.matches(".*" + startSectionName + ".*", text))
                {
                    sectionAlgIsFound = true;
                }
                else if(sectionAlgIsFound && Pattern.matches(".*" + endSectionName + ".*", text)) {

                    sectionAlgIsClose = true;
                }
            }

            if(sectionAlgIsClose)
            {
                tag.addContent(--i,newElement);
                updateXML();
                return;
            }
        }

        if(!sectionAlgIsFound) {
            tag.addContent(new Comment(startSectionName));
        }
        tag.addContent(newElement);
        updateXML();
    }

    public List<String> getCommentsFromSection(String tagSection) throws JDOMException {
        Element tag = findFirtsChild(tagSection);

        if(tag == null)
            throw new JDOMException("The tag \"" + tagSection + "\" is not found in configure.xml");

        List<Content> contents = tag.getContent();

        List<String> comments = new ArrayList<>();
        for(int i = 0; i < contents.size(); i++)
        {
            Content content = contents.get(i);
            String type = content.getCType().toString();

            if(type.equals("Comment"))
            {
                comments.add(content.getValue());
            }
        }

        return comments;
    }

    // Рекурсивный поиск элемента
    public Element findFirtsChild(String name)
    {
        Element root = document.getRootElement();
        List<Element> queue = new ArrayList<>();
        queue.addAll(root.getChildren());

        while (!queue.isEmpty())
        {
            Element child = queue.remove(0);

            if(child.getName().equals(name))
                return child;
            else
            {
                queue.addAll(child.getChildren());
            }
        }

        return null;
    }

    public void updateXML() throws IOException {

        XMLOutputter xmlOutput = new XMLOutputter();

        Format format = Format.getPrettyFormat();
        format.setIndent("\t");
        xmlOutput.setFormat(format);

        FileWriter writer = new FileWriter(file);
        xmlOutput.output(document, writer);
        writer.close();

        FileUtils.replaceLineInFile(Paths.get(file), " />", "/>");
    }

    public static void main(String[] args) throws JDOMException, IOException {
        XMLReaderStax readerStax = new XMLReaderStax("templates/configure1.xml");
        //readerStax.addObjectInTagSection("settings", new Comment("Тестовый комментарий"));
    }


}
