package com.uniset.plugin.configure.xml;

import com.uniset.plugin.Settings;
import org.jdom2.JDOMException;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class XMLReader{

    private Path file;
    private Document xml;

    public XMLReaderStax stax;

    public XMLReader(Path file) throws IOException, ParserConfigurationException, SAXException, JDOMException {

        this.file = file;
        this.stax = new XMLReaderStax(file.toString());

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setSchema(null);
        domFactory.setValidating(false);
        domFactory.setNamespaceAware(true);
        domFactory.setExpandEntityReferences(false);
        domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        xml = domFactory.newDocumentBuilder().parse(Files.newInputStream(file));
    }

    public XMLReader(InputStream is) throws ParserConfigurationException, IOException, JDOMException, SAXException {
        this.stax = new XMLReaderStax(is);

        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setSchema(null);
        domFactory.setValidating(false);
        domFactory.setNamespaceAware(true);
        domFactory.setExpandEntityReferences(false);
        domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        is.reset();
        xml = domFactory.newDocumentBuilder().parse(is);
    }

    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, JDOMException {
        XMLReader reader = new XMLReader(Paths.get("/home/andrey/packages/uniset-plugin/src/main/resources/templates/configure1.xml"));
        reader.hasElement("objects");
    }

    public String getRootName()
    {
        return xml.getDocumentElement().getTagName();
    }

    public long findNewObjectIdAtSection(String regSection)
    {
        List<Long> numbers = new ArrayList<>();
        long id = 0;
        boolean algSectionIsFound = false;

        NodeList list = xml.getElementsByTagName("objects");

        if(list.getLength() > 0)
        {
            Node node = list.item(0);
            NodeList nodeList = node.getChildNodes();
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                Node n = nodeList.item(i);
                if(algSectionIsFound && n.getNodeName().equals("item"))
                {
                    numbers.add(Long.parseLong(n.getAttributes().getNamedItem("id").getNodeValue()));
                }
                else if(!algSectionIsFound && n.getNodeName().equals("#comment"))
                {
                    if(Pattern.matches("\\s?" + regSection + "\\s?", n.getTextContent()))
                        algSectionIsFound = true;
                }
                else if(algSectionIsFound && n.getNodeName().equals("#comment"))
                {
                    break;
                }
            }
        }
        else
            return -1;

        if(algSectionIsFound)
        {
            if(numbers.size() > 0) {
                id = Collections.max(numbers) + 1;
                if (!hasObjectId(id))
                    return id;
            }
            else
                return Settings.DEFAULT_ID_OBJECT;
        }

        return -1;

    }

    public long findNewObjectId()
    {
        List<Long> numbers = new ArrayList<>();

        NodeList list = xml.getElementsByTagName("objects");
        if(list.getLength() > 0)
        {
            Node node = list.item(0);
            NodeList nodeList = node.getChildNodes();
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                Node n = nodeList.item(i);
                if(n.getNodeName().equals("item"))
                    numbers.add(Long.parseLong(n.getAttributes().getNamedItem("id").getNodeValue()));
            }
        }

        return (numbers.size() > 0 ) ? Collections.max(numbers) + 1 : Settings.DEFAULT_ID_OBJECT;
    }

    public boolean hasObjectId(long id)
    {
        NodeList list = xml.getElementsByTagName("objects");
        if(list.getLength() > 0)
        {
            Node node = list.item(0);
            NodeList nodeList = node.getChildNodes();
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                Node n = nodeList.item(i);
                if (n.getNodeName().equals("item") && id == Long.parseLong(n.getAttributes().getNamedItem("id").getNodeValue()))
                    return true;
            }
        }
        return false;
    }

    public boolean hasElement(String tagName)
    {
        NodeList list = xml.getElementsByTagName(tagName);

        return list.getLength() > 0;
    }

    public boolean hasElement(String tagName, String nameAttribute)
    {
        NodeList list = xml.getElementsByTagName(tagName);

        if(list.getLength() > 0)
        {
            for(int i = 0; i < list.getLength(); i++)
            {
                Node node = list.item(i);
                if(nameAttribute.equals(node.getAttributes().getNamedItem("name").getTextContent()))
                    return true;
            }
        }
        return false;
    }

    @Deprecated
    public boolean addONewObject(String sectionName, String className, String objectName, String id) throws Exception {

        List<Node> algList = new ArrayList<>();
        boolean algSectionIsFound = false;
        boolean algSectionIsClose = false;
        NodeList list = xml.getElementsByTagName("settings");

        if(list.getLength() > 0)
        {
            Node node = list.item(0);
            NodeList nodeList = node.getChildNodes();
            for(int i = 0; i < nodeList.getLength(); i++)
            {
                Node n = nodeList.item(i);
                if(!algSectionIsFound && n.getNodeName().equals("#comment"))
                {
                    if(Pattern.matches("\\s*" + sectionName + "\\s*", n.getTextContent()))
                        algSectionIsFound = true;
                }
                else if(algSectionIsFound)
                {
                    if(n.getNodeName().equals("#comment")){
                        if(Pattern.matches("\\s?=*\\s?", n.getTextContent())) {
                            if(algSectionIsClose)
                                break;
                            else
                                algSectionIsClose = true;
                        }
                    }
                    else if(!n.getNodeName().equals("#text"))
                        algList.add(n);
                }
            }
        }
        if(!algSectionIsFound)
            throw new Exception("UnisetObject section is not found");

        Node n = algList.get(algList.size() - 1);
        Element newElement = xml.createElement(className);
        newElement.setAttribute("name", objectName);
        n.getParentNode().insertBefore(newElement, n.getNextSibling());
        n.getParentNode().insertBefore(xml.createTextNode("\n\t\t"), newElement);

        /* Save the editing */
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        Path tmp = Files.createTempFile("configure.xml", null);
        FileOutputStream fos = new FileOutputStream(tmp.toString());
        StreamResult result = new StreamResult(fos);
        DOMSource source = new DOMSource(xml);
        transformer.transform(source, result);
        Files.move(tmp, file, StandardCopyOption.REPLACE_EXISTING);

        return true;
    }

}
