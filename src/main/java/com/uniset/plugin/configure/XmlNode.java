package com.uniset.plugin.configure;

import com.intellij.openapi.project.Project;
import com.intellij.psi.xml.XmlTag;

public interface XmlNode {
    XmlTag getNodeTag(Project project);
}
