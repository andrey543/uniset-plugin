package com.uniset.plugin.configure;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.impl.source.xml.XmlCommentImpl;
import com.intellij.psi.xml.*;
import com.uniset.plugin.Settings;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.build.objects.Sensor;
import com.uniset.plugin.build.objects.UnisetObject;
import com.uniset.plugin.build.objects.modbus.*;
import com.uniset.plugin.configure.xml.XMLReader;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

public class Configure {

    private Project project;
    private VirtualFile configure;
    private XmlFile file;
    private XmlTag nodes;
    private XmlTag sensors;
    private XmlTag settings;
    private XmlTag objects;
    private XmlTag root;

    private XmlTag modbus;

    private List<Sensor> sensorList;

    public XMLReader reader;

    public static Configure getInstance(final Project project) {

        Configure instance = null;
        try {
            instance = new Configure(project);
        }
        catch (IOException | Configure.MainTagNotFound | Configure.RootTagNotFound e1) {
            // Сообщение(отмена процесса): файл конфигурации не найден в проекте
            Messages.showMessageDialog(project,
                    e1.getMessage(),
                    "Configuration", Messages.getErrorIcon());
        } catch (SAXException e1) {
            // Сообщение(отмена процесса): файл конфигурации поврежден
            Messages.showMessageDialog(project,
                    "The configure.xml is corrupted",
                    "Configuration", Messages.getErrorIcon());
        } catch (ParserConfigurationException | JDOMException e1) {
            // Сообщение(отмена процесса): ошибка "парсинга" хз что это...
            Messages.showMessageDialog(project,
                    "The configure.xml has errors of parsing",
                    "Configuration", Messages.getErrorIcon());
        } catch (ClassCastException e1) {
            Messages.showMessageDialog(project,
                    "The configure.xml is too large",
                    "Configuration", Messages.getErrorIcon());
        }

        return instance;
    }

    private Configure(final Project project) throws IOException, SAXException, ParserConfigurationException, JDOMException, MainTagNotFound, RootTagNotFound, ClassCastException {
        this.project = project;

        reader = new XMLReader(Paths.get(project.getBasePath() + "/conf/configure.xml"));

        configure = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(project.getBasePath() + "/conf/configure.xml"));
        if(configure == null)
            throw new FileNotFoundException("File conf/configure.xml is not found");

        file = (XmlFile) PsiManager.getInstance(project).findFile(configure);

        XmlDocument document;
        try {
            document = file.getDocument();
            root     = document.getRootTag();
        }
        catch (NullPointerException e) {
            throw new IOException("File " + configure.getPath() + " is corrupted");
        }
        if(root == null)
            throw new RootTagNotFound();

        String nodesSection = "nodes";

        try
        {
            nodes = findFirstChildRecurs(root, nodesSection);
        }
        catch (NullPointerException e)
        {
            throw new MainTagNotFound(nodesSection);
        }

        String objectsSection = "objects";
        try
        {
            objects = findFirstChildRecurs(root, objectsSection);
        }
        catch (NullPointerException e)
        {
            throw new MainTagNotFound(objectsSection);
        }

        String settingsSection = "settings";
        try
        {
            settings = findFirstChildRecurs(root, settingsSection);
        }
        catch (NullPointerException e)
        {
            throw new MainTagNotFound(settingsSection);
        }

        String sensorsSection = "sensors";

        try
        {
            sensors = findFirstChildRecurs(root, sensorsSection);
        }
        catch (NullPointerException e)
        {
            throw new MainTagNotFound(sensorsSection);
        }

        if(nodes == null)
            throw new MainTagNotFound(nodesSection);

        if(sensors == null)
            throw new MainTagNotFound(sensorsSection);

        if(objects == null)
            throw new MainTagNotFound(objectsSection);

        if(settings == null)
            throw new MainTagNotFound(settingsSection);

        modbus = findFirstChildRecurs(settings, "modbus");

    }

    public Project getProject() {
        return project;
    }

    public VirtualFile getConfigureVirtualFile()
    {
        return configure;
    }

    public long getNextFreeNodeId()
    {
        long id = getMaxNodeId();

        if(id == 0)
            return Settings.DEFAULT_ID_NODE;

        return ++id;
    }

    public long getNextFreeObjectId()
    {
        long id = getMaxObjectId();

        if(id == 0)
            return Settings.DEFAULT_ID_OBJECT;

        return ++id;
    }

    public long getNextFreeSensorId()
    {
        long id = getMaxSensorId();

        return ++id;
    }

    public List<Node> getNodeList() {

        // Инициализируем доступные узлы
        List<Node> nodeList = new ArrayList<>();
        Element nodes = reader.stax.findFirtsChild("nodes");
        for(Element item : nodes.getChildren()) {
            String name     = item.getAttributeValue("name");
            String id       = item.getAttributeValue("id");
            String textname = item.getAttributeValue("textname");
            String ip       = item.getAttributeValue("ip");
            Node node = new Node(name, id, textname, ip);
            node.setNotRespondSensor(item.getAttributeValue("unet_respond_id"));
            node.setNotRespondSensor1(item.getAttributeValue("unet_respond1_id"));
            node.setNotRespondSensor2(item.getAttributeValue("unet_respond2_id"));
            node.setNumChanelSensor(item.getAttributeValue("unet_numchannel_id"));
            node.setLostPacketsSensor1(item.getAttributeValue("unet_lostpackets1_id"));
            node.setLostPacketsSensor2(item.getAttributeValue("unet_lostpackets2_id"));
            nodeList.add(node);
        }

        return nodeList;
    }


    private long findObjectIdByName(String name)
    {
        Element objects = reader.stax.findFirtsChild("objects");
        for(Element item : objects.getChildren()) {
            String next_name = item.getAttributeValue("name");
            if (name != null && name.equals(next_name))
                return Long.parseLong(item.getAttributeValue("id"));
        }
        return -1;
    }

    private long findSensorIdByName(String name)
    {
        Element sensors = reader.stax.findFirtsChild("sensors");
        for(Element item : sensors.getChildren()) {
            String next_name = item.getAttributeValue("name");
            if (name.equals(next_name))
                return Long.parseLong(item.getAttributeValue("id"));
        }

        return -1;
    }

    public Sensor getSensorByName(String name)
    {
        for(Sensor sensor : getSensorList()) {
            if(name.equals(sensor.getName()))
                return sensor;
        }

        return null;
    }

    public Sensor getSensorById(long id)
    {
        for(Sensor sensor : getSensorList()) {
            if(sensor.getId() == id)
                return sensor;
        }

        return null;
    }

    public List<Sensor> getSensorList()
    {
        if(sensorList == null) {
            sensorList = new ArrayList<>();
            Element sensors = reader.stax.findFirtsChild("sensors");
            for (Element item : sensors.getChildren()) {
                String name = item.getAttributeValue("name");
                String textname = item.getAttributeValue("textname");
                String iotype = item.getAttributeValue("iotype");
                String id = item.getAttributeValue("id");
                Sensor sensor = new Sensor(Long.parseLong(id), name, iotype, textname);

                for (Attribute attribute : item.getAttributes()) {
                    if (attribute.getName().equals("name")
                            || attribute.getName().equals("textname")
                            || attribute.getName().equals("iotype")
                            || attribute.getName().equals("id"))
                        continue;
                    sensor.addProperty(attribute.getName(), attribute.getValue());
                }
                sensorList.add(sensor);
            }
        }

        return sensorList;
    }

    public List<UnisetObject> getUnisetObjectList()
    {

        // Инициализируем доступные объекты
        List<UnisetObject> unisetObjectList = new ArrayList<>();
        Element objects = reader.stax.findFirtsChild("settings");
        for (Element item : objects.getChildren()) {
            String type = item.getName();
            if (Pattern.matches("MBM_.*", type)
                    || Pattern.matches("MBMM_.*", type)
                    || Pattern.matches("MBS_.*", type)
                    || Pattern.matches("UNetExchange", type)) {
                continue;
            }

            String name = item.getAttributeValue("name");
            long id = findObjectIdByName(name);
            if (id == -1)
                continue;

            UnisetObject object = new UnisetObject(String.valueOf(id), type, name);
            for (Attribute attribute : item.getAttributes()) {
                if (attribute.getName().equals("name"))
                    continue;
                object.addProperty(attribute.getName(), attribute.getValue());
            }
            unisetObjectList.add(object);
        }

        return unisetObjectList;
    }

    public List<ModbusObject> getModbusObjectList()
    {
            // Инициализируем доступные объекты
            List<ModbusObject> modbusObjectList = new ArrayList<>();
            List<String> sections = new ArrayList<>();
            sections.add("settings");
            if(modbus != null)
                sections.add("modbus");
            for(String section : sections) {
                Element objects = reader.stax.findFirtsChild(section);
                for (Element item : objects.getChildren()) {
                    String type = item.getName();
                    if (Pattern.matches("MBM_.*", type)) {
                        String fullname = item.getAttributeValue("name");
                        String name = fullname.replaceAll("^MBM_", "");
                        long id = findObjectIdByName(fullname);
                        if (id == -1)
                            continue;

                        MBMObject object = new MBMObject(id, name);
                        object.setGatewayAddress(item.getAttributeValue(MBMObject.MBM_PROPERTY_GATEWAY_IADDR));
                        object.setGatewayPort(item.getAttributeValue(MBMObject.MBM_PROPERTY_GATEWAY_REAL_PORT));
                        String polltime = item.getAttributeValue(MBMObject.MBM_PROPERTY_POLLTIME);
                        if (Settings.isNumber(polltime))
                            object.setPolltime(Integer.parseInt(polltime));
                        object.setExchangeModeIdSensor(item.getAttributeValue(MBMObject.MBM_PROPERTY_EXCHANGEMODE_ID_SENSOR));
                        object.setRespondSensor(item.getAttributeValue(MBMObject.MBM_PROPERTY_RESPOND_ID_SENSOR));

                        for (Device device : getDeviceList(item)) {
                            object.addDevice(device);
                        }

                        modbusObjectList.add(object);
                    } else if (Pattern.matches("MBMM_.*", type)) {
                        String fullname = item.getAttributeValue("name");
                        String name = fullname.replaceAll("^MBMM_", "");
                        long id = findObjectIdByName(fullname);
                        if (id == -1)
                            continue;

                        MBMMObject object = new MBMMObject(id, name);
                        String polltime = item.getAttributeValue(MBMMObject.MBMM_PROPERTY_POLLTIME);
                        if (Settings.isNumber(polltime))
                            object.setPolltime(Integer.parseInt(polltime));
                        object.setExchangeModeIdSensor(item.getAttributeValue(MBMMObject.MBMM_PROPERTY_EXCHANGEMODE_ID_SENSOR));

                        for (Device device : getDeviceList(item)) {
                            object.addDevice(device);
                        }

                        for (Gate gate : getGateList(item)) {
                            object.addGate(gate);
                        }

                        modbusObjectList.add(object);
                    } else if (Pattern.matches("MBS_.*", type)) {
                        String fullname = item.getAttributeValue("name");
                        String name = fullname.replaceAll("^MBS_", "");
                        long id = findObjectIdByName(fullname);
                        if (id == -1)
                            continue;
                        MBSObject object = new MBSObject(id, name);

                        modbusObjectList.add(object);
                    }
                }
            }

        return modbusObjectList;
    }

    private List<Device> getDeviceList(final Element root)
    {
        List<Device> list = new ArrayList<>();
        Element deviceList = root.getChild("DeviceList");
        if(deviceList != null) {
            for (Element deviceElement : deviceList.getChildren()) {
                if (deviceElement.getName().equals("item")) {
                    String addr = deviceElement.getAttributeValue(Device.DEVICE_PROPERTY_ADDR);
                    if (addr != null && !addr.isEmpty()) {
                        Device device = new Device(addr);
                        String force = deviceElement.getAttributeValue(Device.DEVICE_PROPERTY_FORCE);
                        device.setForce(force != null && !force.isEmpty() && force.equals("1"));
                        String invert = deviceElement.getAttributeValue(Device.DEVICE_PROPERTY_INVERT);
                        device.setInvert(invert != null && !invert.isEmpty() && invert.equals("1"));
                        String timeout = deviceElement.getAttributeValue(Device.DEVICE_PROPERTY_TIMEOUT);
                        if (Settings.isNumber(timeout))
                            device.setTimeout(Long.parseLong(timeout));
                        device.setRespondSensor(deviceElement.getAttributeValue(Device.DEVICE_PROPERTY_RESPOND_SENSOR));

                        list.add(device);
                    }
                }
            }
        }

        return list;
    }

    private List<Gate> getGateList(final Element root)
    {
        List<Gate> list = new ArrayList<>();
        Element gateList = root.getChild("GateList");
        if(gateList != null) {
            for (Element gateElement : gateList.getChildren()) {
                if (gateElement.getName().equals("item")) {
                    String ip = gateElement.getAttributeValue(Gate.GATE_PROPERTY_IP);
                    String realPort = gateElement.getAttributeValue(Gate.GATE_PROPERTY_REAL_PORT);

                    if (ip != null && !ip.isEmpty() &&
                            realPort != null && !realPort.isEmpty()) {
                        Gate gate = new Gate(ip, realPort);
                        gate.setVstandPort(gateElement.getAttributeValue(Gate.GATE_PROPERTY_VSTAND_PORT));
                        String respondInitTimeout = gateElement.getAttributeValue(Gate.GATE_PROPERTY_RESPOND_INIT_TIMEOUT);
                        if (Settings.isNumber(respondInitTimeout))
                            gate.setRespondInitTimeout(Long.parseLong(respondInitTimeout));
                        String recvTimeout = gateElement.getAttributeValue(Gate.GATE_PROPERTY_RECV_TIMEOUT);
                        if (Settings.isNumber(recvTimeout))
                            gate.setRecv_timeout(Long.parseLong(recvTimeout));
                        String force = gateElement.getAttributeValue(Gate.GATE_PROPERTY_FORCE);
                        gate.setForce(force != null && !force.isEmpty() && force.equals("1"));
                        String invert = gateElement.getAttributeValue(Gate.GATE_PROPERTY_INVERT);
                        gate.setInvert(invert != null && !invert.isEmpty() && invert.equals("1"));
                        gate.setRespondSensor(gateElement.getAttributeValue(Gate.GATE_PROPERTY_RESPOND_SENSOR));

                        list.add(gate);
                    }
                }
            }
        }

        return list;
    }

    public void createLink(String path) throws IOException {
        if(FileUtils.isExists(path + "/configure.xml"))
            return;
        Path p1 = Paths.get(configure.getPath());
        Path p2 = Paths.get(path);
        Path relativePathToConfigure = p2.relativize(p1);

        Files.createSymbolicLink(Paths.get(path + "/configure.xml"), relativePathToConfigure);
    }

    public void addNewSection(String tagSection, String nameSection) throws Throwable {
        String newSection = "<!--" + nameSection + "-->";
        addLineToSectionLast(tagSection, newSection);
    }

    public void addToNode(final XmlNode node)
    {
        nodes.addSubTag(node.getNodeTag(project), true);
        configure.refresh(false, true);
    }
    public void addToNodeLast(final XmlNode node)
    {
        nodes.addBefore(node.getNodeTag(project), getCloseTag(nodes));
    }

    public void addToSettings(final XmlTag tag)
    {
        settings.addSubTag(tag , true);
        configure.refresh(false, true);
    }
    public void addToSettingsLast(final XmlTag tag)
    {
        settings.addAfter(tag, settings.getLastChild());
        configure.refresh(false, true);
    }

    public void addToObjects(final XmlTag tag)
    {
        objects.addSubTag(tag , true);
        configure.refresh(false, true);
    }
    public void addToObjectsLast(final XmlTag tag)
    {
        objects.addAfter(tag, objects.getLastChild());
        //objects.addSubTag(tag , true);
        configure.refresh(false, true);
    }

    public void addToSettings(final XmlTag tag, String sectionName)
    {
        PsiElement sec = findComment(settings, sectionName);
        if(sec == null)
            settings.addSubTag(tag, true);
        else
            settings.addAfter(tag, sec);

        configure.refresh(false, true);
    }

    public void addToObjects(final XmlTag tag, String sectionName)
    {
        PsiElement sec = findComment(objects, sectionName);
        if(sec == null)
            objects.addSubTag(tag, true);
        else
            objects.addAfter(tag, sec);

        configure.refresh(false, true);
    }

    public void addToSensors(final XmlTagSensor xmlTagSensor)
    {
        sensors.addSubTag(xmlTagSensor.getSensorTag(project), true);
        configure.refresh(false, true);
    }

    public void addToSensorsLast(XmlTagSensor xmlTagSensor)
    {
        sensors.addBefore(xmlTagSensor.getSensorTag(project), getCloseTag(sensors));
    }

    public void addToObjectsLast(final XmlTag tag, String sectionName)
    {
        addLastTo(objects, tag, sectionName);
    }
    public void addToSettingsLast(final XmlTag tag, String sectionName)
    {
        addLastTo(settings, tag, sectionName);
    }

    private PsiElement getCloseTag(XmlTag tag)
    {
        for(PsiElement element : tag.getChildren()) {
            if (element instanceof XmlToken) {
                XmlToken token = (XmlToken) element;
                if (token.getTokenType().equals(XmlTokenType.XML_END_TAG_START)) {
                    return element;
                }
            }
        }

        return null;
    }

    private void addLastTo(XmlTag parent, final XmlTag tag, String sectionName)
    {
        PsiElement targetElement = null;
        for(PsiElement element : parent.getChildren())
        {
            if(element instanceof XmlText)
                continue;

            if(element instanceof XmlToken)
            {
                XmlToken token = (XmlToken)element;
                if(token.getTokenType().equals(XmlTokenType.XML_END_TAG_START))
                {
                    if(targetElement == null)
                        parent.addBefore(tag, element);
                    else
                        parent.addAfter(tag, targetElement);

                    break;
                }
            }
            else if(targetElement == null) {
                if (element instanceof XmlCommentImpl) {
                    String value = element.getText()
                            .replaceAll("<!--", "")
                            .replaceAll("-->", "");
                    if (value.equals(sectionName)) {
                        targetElement = element;
                    }
                }
            }
            else if(element instanceof XmlCommentImpl)
            {
                parent.addAfter(tag, targetElement);
                break;
            }
            else {
                targetElement = element;
            }
        }

        configure.refresh(false, true);
    }

    public void editSettingsObject(UnisetObject object)
    {
        XmlTag objectTag = getSettingsTag(object);
        if(objectTag != null) {
            // Удаляем
            for (XmlAttribute attribute : objectTag.getAttributes()) {
                attribute.delete();
            }

            // Добавляем
            for (XmlAttribute attribute : object.getSettingsTag(project).getAttributes()) {
                objectTag.add(attribute);
            }

            configure.refresh(false, true);
        }
    }

    public void editSettingsObject(ModbusObject oldObject, ModbusObject newObject)
    {
        XmlTag objSectionObject = getObjectsTag(oldObject);
        XmlTag settingSectionObject = getSettingsTag(oldObject);
        if(settingSectionObject != null) {
            // Удаляем
            for (XmlAttribute attribute : settingSectionObject.getAttributes()) {
                attribute.delete();
            }
            for(XmlTag child : settingSectionObject.getSubTags())
            {
                child.delete();
            }

            // Добавляем
            // В objects
            objSectionObject.setAttribute("name", newObject.getFullName());
            // В settings
            settingSectionObject.setName(newObject.getFullName());
            XmlTag tag = newObject.getSettingsTag(project);
            for (XmlAttribute attribute : tag.getAttributes()) {
                settingSectionObject.add(attribute);
            }
            for(XmlTag subtag : tag.getSubTags())
            {
                settingSectionObject.addSubTag(subtag, true);
            }

            configure.refresh(false, true);
        }
    }

    public void editNode(Node oldNode, Node newNode)
    {
        XmlTag nodeTag = getNodeTag(oldNode);
        if(nodeTag != null)
        {
            XmlTag newNodeTag = newNode.getNodeTag(project);
            nodes.addBefore(newNodeTag, nodeTag);
            nodeTag.delete();
            configure.refresh(false, true);
        }
    }

    public void removeProps(Sensor sensor, LinkedHashMap<String, String> props)
    {
        XmlTag sensorTag = getSensorTag(sensor);
        if(sensorTag != null)
        {
            for(Map.Entry<String, String> entry : props.entrySet())
            {
                XmlAttribute attribute = sensorTag.getAttribute(entry.getKey());
                if(attribute != null) {
                    String value = attribute.getValue();
                    if(value != null && value.equals(entry.getValue()))
                        attribute.delete();
                }
            }
        }
    }

    public void addProps(Sensor sensor, LinkedHashMap<String, String> props)
    {
        XmlTag sensorTag = getSensorTag(sensor);
        if(sensorTag != null) {
            for (Map.Entry<String, String> entry : props.entrySet())
                sensorTag.setAttribute(entry.getKey(), entry.getValue());
        }
    }

    public void editModbusSensor(ModbusObject oldObject, ModbusObject newObject)
    {
        // Удаляем
        for(ModbusObject.ModbusSensor oldSensor : oldObject.getSensors()) {
            XmlTag sensorTag = getSensorTag(oldSensor.getSensor());

            if (sensorTag != null) {
                String prefix = oldObject.getPrefix();
                if (prefix.isEmpty())
                    prefix = oldObject.getFilterField() + "_";

                Iterator<XmlAttribute> iterator = Arrays.asList(sensorTag.getAttributes()).iterator();
                while (iterator.hasNext()) {
                    XmlAttribute attribute = iterator.next();
                    if (attribute.getName().startsWith(prefix) ||
                            attribute.getName().startsWith(oldObject.getFilterField())) {
                        attribute.delete();
                    }
                }
            }
        }

        // Добавляем
        for(ModbusObject.ModbusSensor newSensor : newObject.getSensors()) {
            XmlTag sensorTag = getSensorTag(newSensor.getSensor());

            if (sensorTag != null) {
                String prefix = newObject.getPrefix();
                if(prefix.isEmpty())
                    prefix = newObject.getFilterField() + "_";

                String filterField = newObject.getFilterField();
                String filterValue = newObject.getFilterValue();
                sensorTag.setAttribute(filterField, filterValue);
                if(newSensor.getMbaddr() != null && !newSensor.getMbaddr().isEmpty())
                    sensorTag.setAttribute(prefix + "mbaddr", newSensor.getMbaddr());
                if(newSensor.getMbfunc() != null && !newSensor.getMbfunc().isEmpty())
                    sensorTag.setAttribute(prefix + "mbfunc", newSensor.getMbfunc());
                if(newSensor.getMbreg() != null && !newSensor.getMbreg().isEmpty())
                    sensorTag.setAttribute(prefix + "mbreg", newSensor.getMbreg());
                if (newSensor.getNbit() != null && !newSensor.getNbit().isEmpty())
                    sensorTag.setAttribute(prefix + "nbit", newSensor.getNbit());
                sensorTag.setAttribute(prefix + "mbtype", "rtu");
            }
        }
    }

    public XmlTag getSensorTag(Sensor sensor)
    {
        return findFirstChildRecurs(sensors, "item", "id", String.valueOf(sensor.getId()));
    }

    public XmlTag getNodeTag(Node node)
    {
        return findFirstChildRecurs(nodes, "item", "name", node.getName());
    }

    public XmlTag getSettingsTag(UnisetObject object)
    {
        return findFirstChildRecurs(settings, object.getTypename(), object.getName());
    }

    public XmlTag getSettingsTag(ModbusObject object)
    {
        return findFirstChildRecurs(settings, object.getFullName(), object.getFullName());
    }

    public XmlTag getObjectsTag(ModbusObject object)
    {
        return findFirstChildRecurs(objects, "item", object.getFullName());
    }

    public List<String> getSectionList() throws JDOMException {
        List<String> settingSections = reader.stax.getCommentsFromSection("settings");
        List<String> objectSections = reader.stax.getCommentsFromSection("objects");
        List<String> result = new ArrayList<>();

        for(String settingName : settingSections)
        {
            settingName = settingName.replaceAll("^\\s*","").replaceAll("\\s*$","");
            for(String objectName : objectSections)
            {
                objectName = objectName.replaceAll("^\\s*","").replaceAll("\\s*$","");
                if(settingName.equals(objectName)) {
                    result.add(settingName);
                    break;
                }
            }
        }

        return result;
    }

    private void addLineToSectionLast( final String tagSection, final String addedLine) throws Throwable {
        List<String> lines = Files.readAllLines(Paths.get(configure.getPath()));
        String findRegex = "</" + tagSection + "[\\s?>].*";

        for(int i = 0; i < lines.size(); ++i)
        {
            String line = lines.get(i);
            if(Pattern.matches(".*" + findRegex + ".*", line))
            {
                String l = lines.get(--i);
                String replaceName = l.replaceAll("^\\s*", "");
                String newLine = "\n" + l.replaceAll(replaceName, addedLine);

                FileUtils.addLineToFile(configure, i, newLine);

                return;
            }
        }

        throw new MainTagNotFound(tagSection);
    }

    private long getMaxNodeId()
    {
        Element objects = reader.stax.findFirtsChild("nodes");
        long maxId = 0;
        for(Element item : objects.getChildren()) {
            long id = Long.parseLong(item.getAttributeValue("id"));
            if(id > maxId)
                maxId = id;
        }

        return maxId;
    }

    private long getMaxObjectId()
    {
        Element objects = reader.stax.findFirtsChild("objects");
        long maxId = 0;
        for(Element item : objects.getChildren()) {
            long id = Long.parseLong(item.getAttributeValue("id"));
            if(id > maxId)
                maxId = id;
        }

        return maxId;
    }

    private long getMaxSensorId()
    {
        long max = 0;
        for(Sensor sensor : getSensorList())
        {
            if(max < sensor.getId())
                max = sensor.getId();
        }

        return max;
    }

    private XmlTag findFirstChildRecurs(XmlTag root, String type)
    {
        List<XmlTag> queue = new ArrayList<>(Arrays.asList(root.getSubTags()));

        while (!queue.isEmpty())
        {
            XmlTag tag = queue.remove(0);
            if(tag.getName().equals(type))
                return tag;
            else
                queue.addAll(Arrays.asList(tag.getSubTags()));
        }

        return null;
    }

    private XmlTag findFirstChildRecurs(XmlTag root, String type, String name)
    {
        List<XmlTag> queue = new ArrayList<>(Arrays.asList(root.getSubTags()));

        while (!queue.isEmpty())
        {
            XmlTag tag = queue.remove(0);
            if(tag.getName().equals(type))
            {
                XmlAttribute attribute = tag.getAttribute("name");
                if(attribute != null)
                {
                    String find_name = attribute.getValue();
                    if(find_name != null && !find_name.isEmpty() && find_name.equals(name))
                        return tag;
                }
            }
            else
                queue.addAll(Arrays.asList(tag.getSubTags()));
        }

        return null;
    }

    private XmlTag findFirstChildRecurs(XmlTag root, String type, String attr, String value)
    {
        List<XmlTag> queue = new ArrayList<>(Arrays.asList(root.getSubTags()));

        while (!queue.isEmpty())
        {
            XmlTag tag = queue.remove(0);
            if(tag.getName().equals(type))
            {
                XmlAttribute attribute = tag.getAttribute(attr);
                if(attribute != null)
                {
                    String find_name = attribute.getValue();
                    if(find_name != null && !find_name.isEmpty() && find_name.equals(value))
                        return tag;
                }
            }
            else
                queue.addAll(Arrays.asList(tag.getSubTags()));
        }

        return null;
    }

    private PsiElement findComment(XmlTag root, String name)
    {
        for(PsiElement element : root.getChildren())
        {
            if(element instanceof XmlCommentImpl) {
                String value = element.getText()
                        .replaceAll("<!--","")
                        .replaceAll("-->", "");
                if(value.equals(name))
                    return element;
            }
        }

        return null;
    }

    /**
     * Exceptions
     */
    public static class MainTagNotFound extends Exception
    {
        private String message;
        MainTagNotFound(String tag)
        {
            super("Section " + tag + " is not found");
            this.message = "Section " + tag + " is not found";
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

    public static class RootTagNotFound extends Exception
    {
        private String message;
        RootTagNotFound()
        {
            super("Root section is not found in xml file");
            this.message = "Root section is not found in xml file";
        }

        @Override
        public String getMessage() {
            return message;
        }
    }

}
