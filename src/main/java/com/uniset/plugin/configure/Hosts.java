package com.uniset.plugin.configure;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.uniset.plugin.FileUtils;
import com.uniset.plugin.Settings;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hosts {
    private static Hosts instance;

    private Project project;
    private VirtualFile file;

    private Hosts(final Project project) throws FileNotFoundException {
        this.project = project;
        file = LocalFileSystem.getInstance().findFileByIoFile(new File(project.getBasePath() + "/conf/misc/hosts"));
        if(file == null)
            throw new FileNotFoundException("File hosts is not found");
    }

    public List<Host> getHosts() throws IOException {
        if(file == null)
            return null;

        List<Host> hosts = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(file.getPath()));
        for(String line = reader.readLine(); line != null; line = reader.readLine())
        {
            if(line.matches(Settings.REGEX_CORRECT_IP + "\\s*\\w*.*"))
            {
                Matcher ipMatch = Pattern.compile(Settings.REGEX_CORRECT_IP).matcher(line);
                String ip = null;
                if(ipMatch.find())
                    ip = ipMatch.group();

                Matcher hostnameMatch = Pattern.compile("^\\w*").matcher(line.replaceAll(ip + "\\s*", ""));
                String hostname = null;
                if(hostnameMatch.find())
                    hostname = hostnameMatch.group(0);

                hosts.add(new Host(hostname, ip, line));
            }
        }

        return hosts;
    }

    public void add(String hostname, String ip) throws IOException {
        Host newHost = new Host(hostname, ip, ip + "\t" + hostname);
        List<Host> friendlyHosts = new ArrayList<>();
        for(Host host : getHosts()) {
            if(host.getIp().matches(ip.replaceAll("[^\\.]*$", "") + "[0-9]*"))
                friendlyHosts.add(host);
        }

        if(friendlyHosts.size() > 0)
        {
            int target = Integer.parseInt(ip.replaceAll("[0-9]*\\.[0-9]*\\.[0-9]*\\.", ""));
            String line = friendlyHosts.get(0).getLine();
            int max = 1;
            for(int i = 0; i < friendlyHosts.size(); ++i)
            {
                int n = Integer.parseInt(friendlyHosts.get(i).getIp().replaceAll("[0-9]*\\.[0-9]*\\.[0-9]*\\.", ""));
                if(n < target && n > max) {
                    line = friendlyHosts.get(i).getLine();
                    max = n;
                }
            }
            FileUtils.replaceLineInFile(file, line, line + System.lineSeparator() + newHost.toString());
        }
        else
        {
            FileUtils.addContentLast(file, newHost.toString());
        }
    }

    public static Hosts getInstance(final Project project) throws FileNotFoundException {
        if(instance == null)
            instance = new Hosts(project);

        return instance;
    }

    class Host
    {
        private String hostname;
        private String ip;
        private String line; // Реальная строка в hosts

        public Host(String hostname, String ip, String line) {
            this.hostname = hostname;
            this.ip = ip;
            this.line = line;
        }

        public String getHostname() {
            return hostname;
        }

        public String getIp() {
            return ip;
        }

        public String getLine() {
            return line;
        }

        @Override
        public String toString() {
            return ip + "\t" + hostname;
        }
    }
}
